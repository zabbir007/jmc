<?php
	include('server.php');
	if (!empty($_SESSION['usermarchantaccountlogin'])) {
	 header('location: index.php');
	}
?>
<!DOCTYPE html>
<html lang="en-US">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<?php
	include_once "head.php";
?>
<body class="home page-template-default page page-id-143 full blog-78  yith-wcan-free wpb-js-composer js-comp-ver-5.4.5 vc_responsive" ">

<div class="page-wrapper">

    <div class="header-wrapper clearfix"><!-- header wrapper -->
                                
                    <header id="header" class="header-separate header-1  sticky-menu-header">
<?php
	include_once "header_top_menu.php";
?>
    
    <div class="header-main" >
        <div class="container">
            <div class="header-left">
				<?php
					$sql_logo = "select * from logo";
					$result_logo = $db->query($sql_logo);
					$data_logo = mysqli_fetch_array($result_logo);
                   // if (isset()) {
                        # code...
                    //}
				?>

                <h1 ><a href="index.php" class="text-white">Marchant Corner</h1> 
                </div>
            
            
               </div>

                
            </div>
        </div>
            </div>
            <div style="width:100%; border-bottom: 2px solid #000;"></div>

            <div class="main-menu-wrap">
         
</div>
</header>
</div><!-- end header wrapper -->
        
        
                <section class="page-top page-header-6">
        <div class="container hide-title">
    <div class="row">
    </div>
</div>    </section>
    
        <div id="main" class="column1 boxed"><!-- main -->

            
                        <div class="container">
                            
            
            <div class="row main-content-wrap">

            <!-- main content -->
            <div class="main-content col-lg-12">

                            
    <div id="content" role="main">
                
            <article class="post-210 page type-page status-publish hentry">
                
                <span class="entry-title" style="display: none;">My Account</span><span class="vcard" style="display: none;"><span class="fn"><a href="author/porto_admin/index.php" title="Posts by Joe Doe" rel="author"></a></span></span><span class="updated" style="display:none"></span>
                <div class="page-content">
                    <div class="woocommerce">

<div class="featured-box align-left porto-user-box">
    <div class="box-content">
				<?php
					include('errors.php');
				?>
				<h2>Marchant Shop Register</h2>
				<a href="register.php"><button class="btn btn-success float-right" style="margin-bottom:10px;" type="button">Login <i class="fa fa-sign-in"></i></button></a>

				<form action="register.php" method="post">

				
					<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label>Enter Name <span class="required">*</span></label>
						<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="marchant_name" value="<?php echo $marchant_name; ?>" required/>
					</p>
					<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label>Enter Email Address <span class="required">*</span></label>
						<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="marchant_email" value="<?php echo $marchant_email; ?>" required/>
					</p>
					<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label>Enter Contact Number <span class="required">*</span></label>
						<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="marchant_contact" value="<?php echo $marchant_contact; ?>" required/>
					</p>
					<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="password">Username <span class="required">*</span></label>
						<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="marchant_username" value="<?php echo $marchant_username; ?>" required />
					</p>
					<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="password">Shop Name <span class="required">*</span></label>
						<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="marchant_shop" value="<?php echo $marchant_shop; ?>" required />
					</p><p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="password">Enter Shop Address <span class="required">*</span></label>
						 <textarea class="form-control" rows="3" name="shop_address" required><?php echo $shop_address; ?></textarea>
					</p><p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="password">Password <span class="required">*</span></label>
						<input class="woocommerce-Input woocommerce-Input--text input-text" name="password1" type="password" required />
					</p><p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="password">Confirm Password <span class="required">*</span></label>
						<input class="woocommerce-Input woocommerce-Input--text input-text" name="password2" type="password" required />
					</p>

					<label>
					<input type="checkbox" value="agree with term and condition" name="agree" required checked> Agree with Term and Condition</label>
					<p class="form-row">
					<button type="submit" class="woocommerce-Button button" name="register" value="Login">Register</button>
						
					</p>

					
				</form>

		
			</div>
</div></div>
                </div>
            </article>

         

        
    </div>

        

</div><!-- end main content -->


    </div>
    </div>


        
            
            </div><!-- end main -->

          <?php include_once"footer.php";?><!-- WP Super Cache is installed but broken. The constant WPCACHEHOME must be set in the file wp-config.php and point at the WP Super Cache plugin directory. -->