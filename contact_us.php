<?php
	include_once "header.php";
	require_once "config.php";
	require_once "config_pos.php";

?>


        
                <section class="page-top page-header-6">
        <div class="container hide-title">
    <div class="row">
        <div class="col-lg-12 clearfix">
            <div class="pt-right d-none">
                <h1 class="page-title">Contact Us</h1>
                            </div>
                            <div class="breadcrumbs-wrap pt-left">
                    <ul class="breadcrumb"><li class="home"><a itemprop="url" href="#" title="Go to Home Page"><span itemprop="title">Home</span></a><i class="delimiter delimiter-2"></i></li><li>Contact Us</li></ul>                </div>
                                </div>
    </div>
</div>    </section>
    
        <div id="main" class="column1 wide clearfix"><!-- main -->

            
                        <div class="container-fluid">
            
            
            <div class="row main-content-wrap">

            <!-- main content -->
            <div class="main-content col-lg-12">

                            
    <div id="content" role="main">
                
            <article class="post-73 page type-page status-publish hentry">
                
                <span class="entry-title" style="display: none;">Contact Us</span><span class="vcard" style="display: none;"><span class="fn"><a href="author/porto_admin/index.html" title="Posts by Joe Doe" rel="author">Joe Doe</a></span></span><span class="updated" style="display:none">2016-06-14T06:15:58+00:00</span>
				<div class="page-content">
					<?php
						$sql = "SELECT * FROM `map`";
						$result = $db->query($sql);
						$data = mysqli_fetch_array($result);
						echo "<p>".$data[1]."</p>";
					?>
				</div>
	  </div></div><div class="vc_row-full-width vc_clearfix"></div></div>
</div></div></div><div class="vc_column_container col-md-6"><div class="wpb_wrapper vc_column-inner">
	<div class="wpb_text_column wpb_content_element " >

	</div>
	
	<?php
	$sql_phone = "select * from web_attribut_info";
	$result_phone = $db->query($sql_phone);
	$data_phone = mysqli_fetch_array($result_phone);
?>
<div class="porto-separator  "><hr class="separator-line  align_center"></div><h4  class="vc_custom_heading align-left heading-primary">The <strong>Office</strong></h4><div class="porto-sicon-wrapper style_1"><div class="porto-sicon-box default-icon" style=""  ><div class="porto-sicon-default"><div id="porto-icon-14228362535a7fcb9ded85c" class="porto-just-icon-wrapper  "><div class="align-icon" style="text-align:center;">
<div class="porto-icon circle "  style="color:#ffffff;background:#0088cc;font-size:14px;display:inline-block;">
	<i class="fa fa-map-marker"></i>
</div></div></div></div><div class="porto-sicon-header" ><h3 class="porto-sicon-title" style="font-size:14px;color:#777777;"><strong>Address:</strong><?=$data_phone[4]?></h3></div> <!-- header --></div> <!-- porto-sicon-box --></div><div class="porto-sicon-wrapper style_1"><div class="porto-sicon-box default-icon" style=""  ><div class="porto-sicon-default"><div id="porto-icon-12739218805a7fcb9dedbf5" class="porto-just-icon-wrapper  "><div class="align-icon" style="text-align:center;">
<div class="porto-icon circle "  style="color:#ffffff;background:#0088cc;font-size:14px;display:inline-block;">
	<i class="fa fa-phone"></i>
</div></div></div></div><div class="porto-sicon-header" ><h3 class="porto-sicon-title" style="font-size:14px;color:#777777;"><strong>Phone:</strong> <?=$data_phone[2]?></h3></div> <!-- header --></div> <!-- porto-sicon-box --></div><div class="porto-sicon-wrapper style_1"><div class="porto-sicon-box default-icon" style=""  ><div class="porto-sicon-default"><div id="porto-icon-1144595415a7fcb9dedf68" class="porto-just-icon-wrapper  "><div class="align-icon" style="text-align:center;">
<div class="porto-icon circle "  style="color:#ffffff;background:#0088cc;font-size:14px;display:inline-block;">
	<i class="fa fa-envelope"></i>
</div></div></div></div><div class="porto-sicon-header" ><h3 class="porto-sicon-title" style="font-size:14px;color:#777777;"><strong>Email:</strong><?=$data_phone[3]?></h3></div> <!-- header --></div> <!-- porto-sicon-box --></div><div class="porto-separator  "><hr class="separator-line  align_center"></div><h4  class="vc_custom_heading align-left heading-primary">Business <strong>Hours</strong></h4><div class="porto-sicon-wrapper style_1"><div class="porto-sicon-box default-icon" style=""  ><div class="porto-sicon-default"><div id="porto-icon-1321733045a7fcb9dee59b" class="porto-just-icon-wrapper  "><div class="align-icon" style="text-align:center;">
<div class="porto-icon none "  style="color:#333;font-size:14px;display:inline-block;">
	<i class="fa fa-clock-o"></i>
	<?php
	$sql_phone = "select * from working_hour";
	$result_phone = $db->query($sql_phone);
	$data_work = mysqli_fetch_array($result_phone);
?>
</div></div></div></div><div class="porto-sicon-header" ><h3 class="porto-sicon-title" style="font-size:14px;color:#777777;"><?=$data_work[1]?><br><br><?=$data_work[2]?></h3></div> <!-- header --></div> <!-- porto-sicon-box --></div><div class="porto-sicon-wrapper style_1"><div class="porto-sicon-box default-icon" style=""  ><div class="porto-sicon-default"><div id="porto-icon-976156215a7fcb9dee89b" class="porto-just-icon-wrapper  "><div class="align-icon" style="text-align:center;">
</div></div></div></div></div>
                </div>
            </article>

            <div class="">
            
                        </div>

        
    </div>

        

</div><!-- end main content -->


    </div>
    </div>


        
            
            </div><!-- end main -->

            
            <?php include_once "footer.php";?>
			
			<!-- WP Super Cache is installed but broken. The constant WPCACHEHOME must be set in the file wp-config.php and point at the WP Super Cache plugin directory. -->