
					
					<?php include_once("header.php");?>
                                 <!-- end header wrapper -->
        
        
                <section class="page-top page-header-6">
        <div class="container hide-title">
    <div class="row">
        <div class="col-lg-12 clearfix">
            <div class="pt-right d-none">
                <h1 class="page-title">Services</h1>
                            </div>
                            <div class="breadcrumbs-wrap pt-left">
                    <ul class="breadcrumb"><li class="home"><a itemprop="url" href="#" title="Go to Home Page"><span itemprop="title">Home</span></a><i class="delimiter delimiter-2"></i></li><li>Services</li></ul>                </div>
                                </div>
    </div>
</div>    </section>
    
        <div id="main" class="column1 boxed"><!-- main -->

            
                        <div class="container">
                            
            
            <div class="row main-content-wrap">

            <!-- main content -->
            <div class="main-content col-lg-12">

                            
    <div id="content" role="main">
                
            <article class="post-67 page type-page status-publish hentry">
                
                <span class="entry-title" style="display: none;">Services</span><span class="vcard" style="display: none;"><span class="fn"><a href="author/porto_admin/index.php" title="Posts by Joe Doe" rel="author">Joe Doe</a></span></span><span class="updated" style="display:none">2016-06-14T05:20:40+00:00</span>
                <div class="page-content">
                    <div class="vc_row wpb_row row"><div class="vc_column_container col-md-12"><div class="wpb_wrapper vc_column-inner"><h2  class="vc_custom_heading align-left">Our <strong>Services</strong></h2></div></div></div><div class="vc_row wpb_row row"><div class="vc_column_container col-md-12 col-lg-10 col-12"><div class="wpb_wrapper vc_column-inner">
	<div class="wpb_text_column wpb_content_element  featured lead" >
		<div class="wpb_wrapper">
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh metus. </p>

		</div>
	</div>
</div></div><div class="push-top vc_column_container col-md-12 col-lg-2 col-12"><div class="wpb_wrapper vc_column-inner"><div class="vc_btn3-container vc_btn3-inline" >	<button class="vc_btn3 vc_btn3-shape-rounded btn btn-lg btn-primary">Contact Us!</button></div><div class="vc_empty_space"   style="height: 32px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div><div class="vc_row wpb_row row"><div class="vc_column_container col-md-12"><div class="wpb_wrapper vc_column-inner"><div class="porto-separator tall "><hr class="separator-line  align_center"></div></div></div></div><div class="vc_row wpb_row row"><div class="vc_column_container col-md-3"><div class="wpb_wrapper vc_column-inner"><div class="porto-content-box featured-boxes wpb_content_element "><div class="featured-box featured-box-primary featured-box-effect-1"><div class="box-content" style=""><i class="icon-featured fa fa-user"></i>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<h4 class="text-uppercase"><span class="text-color-primary">Loved by Customers</span></h4>
<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus.</p>
<p><a class="learn-more text-color-primary" href="#">Learn More <i class="fa fa-angle-right"></i></a></p>

		</div>
	</div>
</div></div></div></div></div><div class="vc_column_container col-md-3"><div class="wpb_wrapper vc_column-inner"><div class="porto-content-box featured-boxes wpb_content_element "><div class="featured-box featured-box-secondary featured-box-effect-1"><div class="box-content" style=""><i class="icon-featured fa fa-book"></i>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<h4 class="text-uppercase"><span class="text-color-secondary">Well Documented</span></h4>
<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus.</p>
<p><a class="learn-more text-color-secondary" href="#">Learn More <i class="fa fa-angle-right"></i></a></p>

		</div>
	</div>
</div></div></div></div></div><div class="vc_column_container col-md-3"><div class="wpb_wrapper vc_column-inner"><div class="porto-content-box featured-boxes wpb_content_element "><div class="featured-box featured-box-tertiary featured-box-effect-1"><div class="box-content" style=""><i class="icon-featured fa fa-trophy"></i>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<h4 class="text-uppercase"><span class="text-color-tertiary">Winner</span></h4>
<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus.</p>
<p><a class="learn-more text-color-tertiary" href="#">Learn More <i class="fa fa-angle-right"></i></a></p>

		</div>
	</div>
</div></div></div></div></div><div class="vc_column_container col-md-3"><div class="wpb_wrapper vc_column-inner"><div class="porto-content-box featured-boxes wpb_content_element "><div class="featured-box featured-box-quaternary featured-box-effect-1"><div class="box-content" style=""><i class="icon-featured fa fa-cogs"></i>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<h4 class="text-uppercase"><span class="text-color-quaternary">Customizable</span></h4>
<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus.</p>
<p><a class="learn-more text-color-quaternary" href="#">Learn More <i class="fa fa-angle-right"></i></a></p>

		</div>
	</div>
</div></div></div></div></div></div><div class="vc_row wpb_row row"><div class="vc_column_container col-md-12"><div class="wpb_wrapper vc_column-inner"><div class="porto-separator tall "><hr class="separator-line  align_center"></div></div></div></div><div class="vc_row wpb_row row m-b-md"><div class="info-box-small-wrap vc_column_container col-md-8"><div class="wpb_wrapper vc_column-inner"><h2  class="vc_custom_heading align-left">Our <strong>Features</strong></h2><div class="vc_row wpb_row vc_inner row"><div class="vc_column_container col-md-6"><div class="wpb_wrapper vc_column-inner"><div class="porto-sicon-wrapper style_1"><div class="porto-sicon-box left-icon" style=""  ><div class="porto-sicon-left"><div id="porto-icon-11760707775a7fcb6d78bda" class="porto-just-icon-wrapper  "><div class="align-icon" style="text-align:center;">
<div class="porto-icon advanced "  style="color:#ffffff;background:#0088cc;border-style:;border-color:#333333;border-width:1px;width:35px;height:35px;line-height:35px;border-radius:500px;font-size:14px;display:inline-block;">
	<i class="fa fa-group"></i>
</div></div></div></div><div class="porto-sicon-body"><div class="porto-sicon-header" ><h3 class="porto-sicon-title" style="color:#0088cc;">Customer Support</h3></div> <!-- header --><div class="porto-sicon-description" style="">Lorem ipsum dolor sit amet, consectetur.</div> <!-- description --></div></div> <!-- porto-sicon-box --></div></div></div><div class="vc_column_container col-md-6"><div class="wpb_wrapper vc_column-inner"><div class="porto-sicon-wrapper style_1"><div class="porto-sicon-box left-icon" style=""  ><div class="porto-sicon-left"><div id="porto-icon-11617945345a7fcb6d7903c" class="porto-just-icon-wrapper  "><div class="align-icon" style="text-align:center;">
<div class="porto-icon advanced "  style="color:#ffffff;background:#0088cc;border-style:;border-color:#333333;border-width:1px;width:35px;height:35px;line-height:35px;border-radius:500px;font-size:14px;display:inline-block;">
	<i class="fa fa-film"></i>
</div></div></div></div><div class="porto-sicon-body"><div class="porto-sicon-header" ><h3 class="porto-sicon-title" style="color:#0088cc;">Sliders</h3></div> <!-- header --><div class="porto-sicon-description" style="">Lorem ipsum dolor sit amet, consectetur.</div> <!-- description --></div></div> <!-- porto-sicon-box --></div></div></div></div><div class="vc_row wpb_row vc_inner row"><div class="vc_column_container col-md-6"><div class="wpb_wrapper vc_column-inner"><div class="porto-sicon-wrapper style_1"><div class="porto-sicon-box left-icon" style=""  ><div class="porto-sicon-left"><div id="porto-icon-16718162235a7fcb6d79510" class="porto-just-icon-wrapper  "><div class="align-icon" style="text-align:center;">
<div class="porto-icon advanced "  style="color:#ffffff;background:#0088cc;border-style:;border-color:#333333;border-width:1px;width:35px;height:35px;line-height:35px;border-radius:500px;font-size:14px;display:inline-block;">
	<i class="fa fa-file"></i>
</div></div></div></div><div class="porto-sicon-body"><div class="porto-sicon-header" ><h3 class="porto-sicon-title" style="color:#0088cc;">HTML5 / CSS3 / JS</h3></div> <!-- header --><div class="porto-sicon-description" style="">Lorem ipsum dolor sit amet, adip.</div> <!-- description --></div></div> <!-- porto-sicon-box --></div></div></div><div class="vc_column_container col-md-6"><div class="wpb_wrapper vc_column-inner"><div class="porto-sicon-wrapper style_1"><div class="porto-sicon-box left-icon" style=""  ><div class="porto-sicon-left"><div id="porto-icon-17475907145a7fcb6d798eb" class="porto-just-icon-wrapper  "><div class="align-icon" style="text-align:center;">
<div class="porto-icon advanced "  style="color:#ffffff;background:#0088cc;border-style:;border-color:#333333;border-width:1px;width:35px;height:35px;line-height:35px;border-radius:500px;font-size:14px;display:inline-block;">
	<i class="fa fa-user"></i>
</div></div></div></div><div class="porto-sicon-body"><div class="porto-sicon-header" ><h3 class="porto-sicon-title" style="color:#0088cc;">Icons</h3></div> <!-- header --><div class="porto-sicon-description" style="">Lorem ipsum dolor sit amet, adip.</div> <!-- description --></div></div> <!-- porto-sicon-box --></div></div></div></div><div class="vc_row wpb_row vc_inner row"><div class="vc_column_container col-md-6"><div class="wpb_wrapper vc_column-inner"><div class="porto-sicon-wrapper style_1"><div class="porto-sicon-box left-icon" style=""  ><div class="porto-sicon-left"><div id="porto-icon-19384391865a7fcb6d79dda" class="porto-just-icon-wrapper  "><div class="align-icon" style="text-align:center;">
<div class="porto-icon advanced "  style="color:#ffffff;background:#0088cc;border-style:;border-color:#333333;border-width:1px;width:35px;height:35px;line-height:35px;border-radius:500px;font-size:14px;display:inline-block;">
	<i class="fa fa-google-plus"></i>
</div></div></div></div><div class="porto-sicon-body"><div class="porto-sicon-header" ><h3 class="porto-sicon-title" style="color:#0088cc;">500+ Google Fonts</h3></div> <!-- header --><div class="porto-sicon-description" style="">Lorem ipsum dolor sit amet, consectetur.</div> <!-- description --></div></div> <!-- porto-sicon-box --></div></div></div><div class="vc_column_container col-md-6"><div class="wpb_wrapper vc_column-inner"><div class="porto-sicon-wrapper style_1"><div class="porto-sicon-box left-icon" style=""  ><div class="porto-sicon-left"><div id="porto-icon-20303388385a7fcb6d7a1a9" class="porto-just-icon-wrapper  "><div class="align-icon" style="text-align:center;">
<div class="porto-icon advanced "  style="color:#ffffff;background:#0088cc;border-style:;border-color:#333333;border-width:1px;width:35px;height:35px;line-height:35px;border-radius:500px;font-size:14px;display:inline-block;">
	<i class="fa fa-reorder"></i>
</div></div></div></div><div class="porto-sicon-body"><div class="porto-sicon-header" ><h3 class="porto-sicon-title" style="color:#0088cc;">Buttons</h3></div> <!-- header --><div class="porto-sicon-description" style="">Lorem ipsum dolor sit, consectetur adip.</div> <!-- description --></div></div> <!-- porto-sicon-box --></div></div></div></div><div class="vc_row wpb_row vc_inner row"><div class="vc_column_container col-md-6"><div class="wpb_wrapper vc_column-inner"><div class="porto-sicon-wrapper style_1"><div class="porto-sicon-box left-icon" style=""  ><div class="porto-sicon-left"><div id="porto-icon-5300074685a7fcb6d7a693" class="porto-just-icon-wrapper  "><div class="align-icon" style="text-align:center;">
<div class="porto-icon advanced "  style="color:#ffffff;background:#0088cc;border-style:;border-color:#333333;border-width:1px;width:35px;height:35px;line-height:35px;border-radius:500px;font-size:14px;display:inline-block;">
	<i class="fa fa-adjust"></i>
</div></div></div></div><div class="porto-sicon-body"><div class="porto-sicon-header" ><h3 class="porto-sicon-title" style="color:#0088cc;">Colors</h3></div> <!-- header --><div class="porto-sicon-description" style="">Lorem ipsum dolor sit amet, adip.</div> <!-- description --></div></div> <!-- porto-sicon-box --></div></div></div><div class="vc_column_container col-md-6"><div class="wpb_wrapper vc_column-inner"><div class="porto-sicon-wrapper style_1"><div class="porto-sicon-box left-icon" style=""  ><div class="porto-sicon-left"><div id="porto-icon-13727147175a7fcb6d7aa74" class="porto-just-icon-wrapper  "><div class="align-icon" style="text-align:center;">
<div class="porto-icon advanced "  style="color:#ffffff;background:#0088cc;border-style:;border-color:#333333;border-width:1px;width:35px;height:35px;line-height:35px;border-radius:500px;font-size:14px;display:inline-block;">
	<i class="fa fa-desktop"></i>
</div></div></div></div><div class="porto-sicon-body"><div class="porto-sicon-header" ><h3 class="porto-sicon-title" style="color:#0088cc;">Lightbox</h3></div> <!-- header --><div class="porto-sicon-description" style="">Lorem sit amet, consectetur adip.</div> <!-- description --></div></div> <!-- porto-sicon-box --></div></div></div></div></div></div><div class="vc_column_container col-md-4"><div class="wpb_wrapper vc_column-inner"><h2  class="vc_custom_heading align-left">and more...</h2><div class="accordion  panel-default " id="accordion1743539404" data-collapsible="" data-active-tab="1"><div class="card card-default"><div class="card-header"><h4 class="card-title m-0"><a class="accordion-toggle" data-toggle="collapse" href="#collapse1430597695"><i class="fa fa-usd"></i>Pricing Tables</a></h4></div><div id="collapse1430597695" class="collapse"><div class="card-body">
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p>Donec tellus massa, tristique sit amet condim vel, facilisis quis sapien. Praesent id enim sit amet odio vulputate eleifend in in tortor.</p>

		</div>
	</div>
</div></div></div> <div class="card card-default"><div class="card-header"><h4 class="card-title m-0"><a class="accordion-toggle" data-toggle="collapse" href="#collapse1227588532"><i class="fa fa-comment"></i>Contact Forms</a></h4></div><div id="collapse1227588532" class="collapse"><div class="card-body">
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p>Donec tellus massa, tristique sit amet condimentum vel, facilisis quis sapien.</p>

		</div>
	</div>
</div></div></div> <div class="card card-default"><div class="card-header"><h4 class="card-title m-0"><a class="accordion-toggle" data-toggle="collapse" href="#collapse2067156419"><i class="fa fa-desktop"></i>Portfolio Pages</a></h4></div><div id="collapse2067156419" class="collapse"><div class="card-body">
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p>Donec tellus massa, tristique sit amet condimentum vel, facilisis quis sapien.</p>

		</div>
	</div>
</div></div></div> </div></div></div></div><div class="vc_row wpb_row row"><div class="vc_column_container col-md-12"><div class="wpb_wrapper vc_column-inner"><div class="porto-separator tall "><hr class="separator-line  align_center"></div></div></div></div><div class="vc_row wpb_row row"><div class="vc_column_container col-md-7"><div class="wpb_wrapper vc_column-inner"><h2  class="vc_custom_heading align-left">Premium <strong>Features</strong></h2>
	<div class="wpb_text_column wpb_content_element  lead" >
		<div class="wpb_wrapper">
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur imperdiet hendrerit volutpat. Sed in nunc nec ligula consectetur mollis in vel justo. Vestibulum ante ipsum primis in faucibus orci.</p>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur imperdiet hendrerit volutpat. Sed in nunc nec ligula consectetur mollis in vel justo. Vestibulum ante ipsum primis in faucibus orci. </p>

		</div>
	</div>
</div></div><div class="vc_column_container col-md-4 offset-md-1"><div class="wpb_wrapper vc_column-inner">
	<div class="wpb_single_image wpb_content_element vc_align_center   push-top">
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img"  src="images/device.png" /></div>
		</div>
	</div>
</div></div></div>
                </div>
            </article>

            <div class="">
            
                        </div>

        
    </div>

        

</div><!-- end main content -->


    </div>
    </div>


        
            
            </div><!-- end main -->

            
        <?php include_once "footer.php";?>
		
		<!-- WP Super Cache is installed but broken. The constant WPCACHEHOME must be set in the file wp-config.php and point at the WP Super Cache plugin directory. -->