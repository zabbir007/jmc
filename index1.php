
<?php
/*
	====================================
	Important Note: 
	>Theme home page body slick slide  style sheet path newslick/css/style.css
	>Theme home page body slick slide  js path newslick/js/index.js
	====================================
*/	?>
<?php
	session_start();
	require_once "config.php";
	require_once "config_pos.php";
?>
<!DOCTYPE html>
<html lang="en-US">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<?php
	include_once "head.php";
?>

<body class="home page-template-default page page-id-143 full blog-78  yith-wcan-free wpb-js-composer js-comp-ver-5.4.5 vc_responsive">

<div class="page-wrapper">

    <div class="header-wrapper clearfix"><!-- header wrapper -->
                                
                    <header id="header" class="header-separate header-1  sticky-menu-header">
<?php
	include_once "header_top_menu.php";
?>
  <div class="header-main">
        <div class="container">
            <div class="header-left">
				<?php
					$sql_logo = "select * from logo";
					$result_logo = $db->query($sql_logo);
					$data_logo = mysqli_fetch_array($result_logo);
				?>
                <h1 class="logo"><a href="index.php" title="<?=$data['title'];?>" rel="home">
                <img class="img-responsive standard-logo" src="<?=$data_logo['logo_path'];?>" alt="<?=$data['title'];?>" /><img class="img-responsive retina-logo" src="<?=$data_logo['logo_path'];?>" alt="<?=$data['title'];?>" style="display:none;" /> </a>
    </h1>            </div>
            <div class="header-center">
                <a class="mobile-toggle"><i class="fa fa-reorder"></i></a>
                    <div class="searchform-popup">
        <a class="search-toggle"><i class="fa fa-search"></i></a>
     <form action="search_category_product.php" method="post"
        class="searchform searchform-cats">
        <fieldset>
            <span class="text"><input name="type_search_category" id="type_search_category" type="text" value="" placeholder="Search&hellip;" autocomplete="off"/></span>
            <input type="hidden" name="post_type" value="product"/>
			<select  name='default_search_category' id='product_cat' class='cat'>
			<option value='0' selected lang="en">All Categories</option>
			<?php
				$sql_category = "select * from 0_stock_category";
				$result_category = $db2->query($sql_category);
				while($data_category = mysqli_fetch_array($result_category)){
			?>
				<option class="level-0" value="<?=$data_category[0];?>"><?=$data_category[1];?></option>
			<?php
				}
			?>
			</select>
            <span class="button-wrap"><button class="btn btn-special" title="Search" type="submit"><i class="fa fa-search"></i></button></span>
        </fieldset>
    </form>
        </div>
                </div>
            <div class="header-right">
                                <div class="header-minicart-arrow">
                           <div style="border: solid 1px #ffdc52; background: #ffdc52; border-radius: 5px;" id="mini-cart" class="mini-cart minicart-arrow effect-fadein-up minicart-style2" >
            <div id="show_add_cart" >
				<div class="cart-head cart-head4" >
					<a href="shop_cart_box.php"><img style="width: 48px;height: 30px; " src="icon/cart.png"/></a>
					<?php
						if(isset($_SESSION['shoping_cart'])){
							$count_shoping_cart = count($_SESSION['shoping_cart']);
						}
						else{
							$count_shoping_cart = 0;
						}
					?>
				 <span class="cart-items" lang="en" style="background: #4e7df1;"><?=$count_shoping_cart;?></span>
				</div>
				<div class="cart-popup widget_shopping_cart">
					<div class="widget_shopping_cart_content">
						<div class="cart">
							<table border="0" cellpadding="5px" cellspacing="0px" style="margin:0px auto;text-align:center">
							<?php
							$total = 0;
							if(!empty($_SESSION['shoping_cart'])){
							?>
							<thead>
							<tr>
							<th></th>
							<th><span lang="en">Name</span></th>
							<th><span lang="en">Qty</span></th>
							<th><span lang="en">Price</span></th>
							<th><i class="fa fa-trash"></i></th>
							</tr>
							</thead>
							<?php
							
							foreach($_SESSION['shoping_cart'] as $keys => $values)
							{
							?>
							<tr>
							<td><img style="width: 500px;height: 50px;" src="<?php echo $product_images_url.$values['item_id'].".jpg";?>" /></td>
							<td><?php echo $values['item_name'];?></td>
							<td><?php echo $values['item_qty'];?></td>
							<td>$<?php echo $values['item_price'];?></td>
							<td><button style="background:none;border:none;cursor:pointer" onclick="remove_product(this.value);" value="<?php echo $values['item_id']?>" ><i class="fa fa-remove" style="font-size:16px;color:red"></i></button></td>
							</tr>
							<?php
							}
							?>
								<tr>
									<td colspan="5" style="text-align:right;"><a style="background: #c3c111;color: azure;padding: 10px;" href="shop_cart_box.php"><span lang="en">Check Out</span></a></td>
								</tr>
							<?php
							}
							else{
							?>
							<tr>
							<td><span lang="en">Your Cart is empty!</span></td>
							</tr>
							<?php
							}
							?>
							</table>
						</div>
					</div>
				</div>
			</div>
        </div>
      </div>

                
            </div>
        </div>
            </div>
          

           <div id="navbarr">
            <div id="main-menu" class="row hiddenbar" >
                         

	<?php
		include_once ("ecomtopmenu.php");
	?>
	
</div>
</div>

</header>
</div><!-- end header wrapper -->
<div class="container">
<section class="dropdown-content" id="myDropdown">
<div class="c">

<div class="wpb_wrapper vc_column-inner ">

<div class="sidebar-content">

	<?php
	include("sidebarmenusticky.php");
	?>
</div>
</div>
</div>
</section>
</div>
<?php
	
	$sql="SELECT * FROM slider_ecom ORDER BY status";
	$result = $db->query($sql);

?>

<div id="main" class="column1 wide clearfix no-breadcrumbs">

	
				<div class="container-fluid">
	
	
	<div class="row main-content-wrap">
	<div class="main-content col-lg-12">

					
<div id="content" role="main">
		
	<article class="post-143 page type-page status-publish hentry">
		
		
		<div class="page-content">
			<div class="vc_row wpb_row"><div class="porto-wrap-container container">
				<div class="row">
				
				
<div class="order-2 home-banner-slider vc_column_container col-md-12 col-lg-9 col-12 vc_custom_1512638641677">
			<script src="wp-content/themes/porto/js/jssor.slider-27.1.0.min.js" type="text/javascript"></script>
			<script type="text/javascript">
			jssor_1_slider_init = function() {
			var jssor_1_SlideoTransitions = [
			[{b:0,d:600,y:-290,e:{y:27}}],
			[{b:0,d:1000,y:185},{b:1000,d:500,o:-1},{b:1500,d:500,o:1},{b:2000,d:1500,r:360},{b:3500,d:1000,rX:30},{b:4500,d:500,rX:-30},{b:5000,d:1000,rY:30},{b:6000,d:500,rY:-30},{b:6500,d:500,sX:1},{b:7000,d:500,sX:-1},{b:7500,d:500,sY:1},{b:8000,d:500,sY:-1},{b:8500,d:500,kX:30},{b:9000,d:500,kX:-30},{b:9500,d:500,kY:30},{b:10000,d:500,kY:-30},{b:10500,d:500,c:{x:125.00,t:-125.00}},{b:11000,d:500,c:{x:-125.00,t:125.00}}],
			[{b:0,d:600,x:535,e:{x:27}}],
			[{b:-1,d:1,o:-1},{b:0,d:600,o:1,e:{o:5}}],
			[{b:-1,d:1,c:{x:250.0,t:-250.0}},{b:0,d:800,c:{x:-250.0,t:250.0},e:{c:{x:7,t:7}}}],
			[{b:-1,d:1,o:-1},{b:0,d:600,x:-570,o:1,e:{x:6}}],
			[{b:-1,d:1,o:-1,r:-180},{b:0,d:800,o:1,r:180,e:{r:7}}],
			[{b:0,d:1000,y:80,e:{y:24}},{b:1000,d:1100,x:570,y:170,o:-1,r:30,sX:9,sY:9,e:{x:2,y:6,r:1,sX:5,sY:5}}],
			[{b:2000,d:600,rY:30}],
			[{b:0,d:500,x:-105},{b:500,d:500,x:230},{b:1000,d:500,y:-120},{b:1500,d:500,x:-70,y:120},{b:2600,d:500,y:-80},{b:3100,d:900,y:160,e:{y:24}}],
			[{b:0,d:1000,o:-0.4,rX:2,rY:1},{b:1000,d:1000,rY:1},{b:2000,d:1000,rX:-1},{b:3000,d:1000,rY:-1},{b:4000,d:1000,o:0.4,rX:-1,rY:-1}]
			];

			var jssor_1_options = {
			$AutoPlay: 1,
			$Idle: 2000,
			$CaptionSliderOptions: {
			$Class: $JssorCaptionSlideo$,
			$Transitions: jssor_1_SlideoTransitions,
			$Breaks: [
			  [{d:2000,b:1000}]
			]
			},
			$ArrowNavigatorOptions: {
			$Class: $JssorArrowNavigator$
			},
			$BulletNavigatorOptions: {
			$Class: $JssorBulletNavigator$
			}
			};

			var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

			/*#region responsive code begin*/

			var MAX_WIDTH = 980;

			function ScaleSlider() {
			var containerElement = jssor_1_slider.$Elmt.parentNode;
			var containerWidth = containerElement.clientWidth;

			if (containerWidth) {

				var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

				jssor_1_slider.$ScaleWidth(expectedWidth);
				jssor_1_slider.$SetSliderHeight(1000);
			}
			else {
				window.setTimeout(ScaleSlider, 30);
			}
			}

			ScaleSlider();

			$Jssor$.$AddEvent(window, "load", ScaleSlider);
			$Jssor$.$AddEvent(window, "resize", ScaleSlider);
			$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
			/*#endregion responsive code end*/
			};
			</script>
			<style>
			/*jssor slider loading skin spin css*/
			.jssorl-009-spin img {
			animation-name: jssorl-009-spin;
			animation-duration: 1.6s;
			animation-iteration-count: infinite;
			animation-timing-function: linear;
			}

			@keyframes jssorl-009-spin {
			from { transform: rotate(0deg); }
			to { transform: rotate(360deg); }
			}

			/*jssor slider bullet skin 052 css*/
			.jssorb052 .i {position:absolute;cursor:pointer;}
			.jssorb052 .i .b {fill:#000;fill-opacity:0.3;}
			.jssorb052 .i:hover .b {fill-opacity:.7;}
			.jssorb052 .iav .b {fill-opacity: 1;}
			.jssorb052 .i.idn {opacity:.3;}

			/*jssor slider arrow skin 053 css*/
			.jssora053 {display:block;position:absolute;cursor:pointer;}
			.jssora053 .a {fill:none;stroke:#fff;stroke-width:640;stroke-miterlimit:10;}
			.jssora053:hover {opacity:.8;}
			.jssora053.jssora053dn {opacity:.5;}
			.jssora053.jssora053ds {opacity:.3;pointer-events:none;}
			.new_slider_images_text{position:absolute;top:320px;left:30px;width:500px;height:40px;background-color:rgba(255,188,5,0.8);font-family:Oswald,sans-serif;font-size:32px;font-weight:200;line-height:1.2;text-align:center;}
			</style>
			<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:400px;overflow:hidden;visibility:hidden;">
			<div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:400px;overflow:hidden;">
				
				<?php while($data=mysqli_fetch_array($result)){ ?>
					<div data-p="170.00">
					<img data-u="image" src="<?=$data[2]?>" />
					<!-- <div class="new_slider_images_text" data-u="caption" data-t="0"><span style="color:red; font-weight:bold;"><?=$data[1]?></span></div> -->
					</div>
				<?php } ?>
			</div>
			<div data-u="navigator" class="jssorb052" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
			<div data-u="prototype" class="i" style="width:16px;height:16px;">
			<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
				<circle class="b" cx="8000" cy="8000" r="5800"></circle>
			</svg>
			</div>
			</div>
			<div data-u="arrowleft" class="jssora053" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
			<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
			<polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
			</svg>
			</div>
			<div data-u="arrowright" class="jssora053" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
			<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
			<polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
			</svg>
			</div>
			</div>
			<script type="text/javascript">jssor_1_slider_init();</script>
            <div class="row">
            	<div class="col-lg-12"><br></div>
            	
            	<div class="col-lg-6 col-md-6 col-sm-12"><div><img src="icon/banner1.png" style="width: 100%;"></div></div>
            	<div class="col-lg-6 col-md-6 col-sm-12"><div><img src="icon/banner2.png" style="width: 100%;"></div></div>

            </div>
</div>



<div class="vc_column_container col-md-12 col-lg-3 col-12 vc_custom_1512638650533">

<div class="wpb_wrapper vc_column-inner a">
<div class="sidebar-content">
	<?php
		include ("sidebarmenu.php");
	?>
</div>
</div>


</div></div></div>

</div>
<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_custom_1489558295141 section section-parallax m-t-none m-b-none vc_row-has-fill">
<div class="container">
<!---
	Php Script Start
--->
<!---
	====================================
	Flash Sale Portion Start.
	====================================
--->

<?php $flashsale = "SELECT * FROM `flashsalestats`"; 
$queryexecute = mysqli_query($db, $flashsale);
$dataflashsale = mysqli_fetch_array($queryexecute);
if ($dataflashsale['stats'] == 0) {
	
} else{ ?> 
<div class="row">
	<div class="col-lg-2 col-md-3 col-sm-3 col-5">
<span style="background: #ffdc52; color: black; font-weight: bold; font-size: 16px; padding: 8px 16px;">Flash Sale</span>
</div> <div class="col-lg-6 col-md-4 col-sm-6 col-7 timer" id="a">
	<div class="display">
		<div class="symbol1">End in</div>
		<div class="symbol"><div class="cur"></div><div class="old"></div></div>
		<div class="symbol"><div class="cur"></div><div class="old"></div></div>
		<div class="delimeter">:</div>
		<div class="symbol"><div class="cur"></div><div class="old"></div></div>
		<div class="symbol"><div class="cur"></div><div class="old"></div></div>
		<div class="delimeter">:</div>
		<div class="symbol"><div class="cur"></div><div class="old"></div></div>
		<div class="symbol"><div class="cur"></div><div class="old"></div></div>
	</div>
</div>
<br>
<br>
</div>
<div class="Container">

  <div id="output" style="display: none;">
  	<p>here data show</p>
  </div>

  <h3 class="Head"><span class="allright"><a href="#">View All</a>&nbsp;&nbsp;<span class="Arrows"></span></span></h3>
  <!-- Carousel Container -->
  <div class="SlickCarousel">
  	<?php
                $sql_for_product = "SELECT * FROM 0_stock_master ORDER BY RAND() LIMIT 20";
				$result_for_product = $db2->query($sql_for_product);
					while($data_for_product = mysqli_fetch_array($result_for_product)){
					$img_name = "";
					$img_name =	$data_for_product[0];
					$img_url = $product_images_url."$img_name.jpg";
					if(file_exists($img_url) == true){
				?>
    <!-- Item -->
   
    <div class="ProductBlock">
      <div class="Content">
      	<center>
      		 <a  href="product_details.php?category_id=<?=$data_category[0];?>&&stock_id=<?=$data_for_product[0];?>">
        <div class="img-fill">
        	
          <img src="<?=$img_url?>">
        
        </div>
         </a>
          </center>
          <div class="productname">
        <p><a style="text-decoration: none; color: #222;" href="product_details.php?category_id=<?=$data_category[0];?>&&stock_id=<?=$data_for_product[0];?>"><?=$data_for_product[3];?></a></p>
        </div>
        <?php
								$sql_for_price = "SELECT * FROM 0_prices WHERE stock_id='$data_for_product[0]'";
								$result_for_price = $db2->query($sql_for_price);
								$data_for_price = mysqli_fetch_array($result_for_price);
							?>
							<center>
							<span style="color: #4e7df1; font-size: 16px; font-weight: bolder;"><span >৳</span><?=$data_for_price[4]?></span>
							<div class="add-links  clearfix">
									<button class="button product_type_simple" onclick="add_to_cart(this.value);" value="<?=$data_for_product[0];?>" lang="en">Add to cart</button>
							   </div>
							   </center>
      </div>
    </div>
<?php } 
}?>
  
  </div>
  <!-- Carousel Container -->
</div>

<?php } ?>
<!---
	====================================
	Flash Sale Portion End.
	====================================
--->
<br>
<br>
<br>
<!---
	====================================
	SHOP BY CATEGORIES Portion Start.
	====================================
--->
<div class="row">
</div>
<div class="Container">
  <h3 class="Head"><span class="shopcategory">SHOP BY CATEGORIES</span><span class="allright"><a href="#">View All</a>&nbsp;&nbsp;<span class="Arrows1"></span></span></h3>
  <!-- Carousel Container -->
  <div class="SlickCarousel1">
  	<?php
  	$sql_for_subdata = "SELECT * FROM 0_stock_image INNER JOIN 0_stock_category ON 0_stock_image.description = 0_stock_category.description WHERE image_path IS NOT NULL AND inactive ='0' ORDER BY RAND() LIMIT 20";
			$data_for_sub = mysqli_query($db2, $sql_for_subdata);
			if (mysqli_num_rows($data_for_sub) == 0) { ?>
				<span>Category Not Added !</span>
			<?php } else {
while ($dispay = mysqli_fetch_array($data_for_sub)) {

	 $des = $dispay['description'];

 ?>

    <!-- Item -->
    <div class="ProductBlock">
      <div class="Content">
      	<a style="color: #222; font-size: 16px; font-weight: bolder; display: block; padding-top: 10px;" href="<?php echo "shop.php?cat=";echo $dispay['category_id']; ?>">
        <div class="img-fill">
        	
          <img src="<?php echo $dispay['image_path']; ?>">
     
        </div>
    </a>
        <div class="productname1">
        <p><a style="color: #222; font-size: 16px; font-weight: bolder; display: block; padding-top: 10px;" href="<?php echo "shop.php?cat=";echo $dispay['category_id']; ?>"><?php echo $dispay['description']; ?></a></p>
   </div>
      </div>
    </div>
<?php } 

//} 

}?>
  
  </div>
  <!-- Carousel Container -->
</div>

<!---
	====================================
	SHOP BY CATEGORIES Portion End.
	====================================
--->


<!---
	====================================
	Dyanamic Main Cats Portion Start.
	====================================
--->
<?php 
       $sqltitle = "SELECT * FROM new_tbl2 WHERE desc2 = '0'";
       $data_for_home = mysqli_query($db2, $sqltitle);
       //main cats execute while start
       
       $counter1 = 2;
       $counter2 = 2;
       
       while ($row = mysqli_fetch_array($data_for_home)) { 

     $sqltitle = "SELECT * FROM 0_stock_category WHERE product_group = '$row[0]'"; 
       //check product group available or not
       $sqlsubcat = mysqli_query($db2, $sqltitle);
       //if poroduct group sub category is  empty, the main cat not showed condition start
       if (mysqli_num_rows($sqlsubcat) == 0) {
      /* Product Category is not appear in homepage without any sub category.
      If sub category is available then product main category showed in homepage product content slider. */
      } else {
      //else poroduct group sub category is not empty, the main cat showed start
      			$values=array();
                 while ($datasubcat = mysqli_fetch_array($sqlsubcat)) {
                 	$id=$datasubcat['category_id'];
                 	$values[] = "'$id'";               	
                 	  
                    
                 }
                  $category_id = implode(',', $values);
                 
                 //IN ($val)

       ?>
<br>
<br>
<br>
<div class="row">
</div>
<div class="Container">
  <h3 class="Head"><span class="shopcategory"><?php echo $row['group_brands']; ?></span><span class="allright"><a href="allcat.php?groupid=<?php echo $row[0]; ?>">View All</a>&nbsp;&nbsp;<span class="Arrows<?php  echo $counter2++; ?>"></span></span></h3>
  <!-- Carousel Container -->
  <div class="SlickCarousel<?php echo $counter1++; ?>">
 	<?php       
 	
				$sql_for_product = "SELECT * FROM 0_stock_master WHERE category_id IN ($category_id) ORDER BY counter DESC LIMIT 20";
				$result_for_product = $db2->query($sql_for_product);
					while($data_for_product = mysqli_fetch_array($result_for_product)){
					$img_name = "";
					$img_name =	$data_for_product[0];
					$img_url = $product_images_url."$img_name.jpg";
					if(file_exists($img_url) == true){
					
				?>
    <!-- Item -->
   
    <div class="ProductBlock">
      <div class="Content">
      	<center>
      		 <a  href="product_details.php?category_id=<?=$data_category[0];?>&&stock_id=<?=$data_for_product[0];?>">
        <div class="img-fill">
        	
          <img src="<?=$img_url?>">
        
        </div>
         </a>
          </center>
          <div class="productname">
        <p><a style="text-decoration: none; color: #222;" href="product_details.php?category_id=<?=$data_category[0];?>&&stock_id=<?=$data_for_product[0];?>"><?=$data_for_product[3];?></a></p>
        </div>
        <?php
								$sql_for_price = "SELECT * FROM 0_prices WHERE stock_id='$data_for_product[0]'";
								$result_for_price = $db2->query($sql_for_price);
								$data_for_price = mysqli_fetch_array($result_for_price);
							?>
							<center>
							<span style="color: #4e7df1; font-size: 16px; font-weight: bolder;"><span >৳</span><?=$data_for_price[4]?></span>
							<div class="add-links  clearfix">
									<button class="button product_type_simple" onclick="add_to_cart(this.value);" value="<?=$data_for_product[0];?>" lang="en">Add to cart</button>
							   </div>
							   </center>
      </div>
    </div>
<?php
}
 } 
 ?>
  
  </div>
  <!-- Carousel Container -->
</div>

<?php }
 //main cats execute while end
}
//if poroduct group sub category is  empty, the main cat not showed condition end
 ?>

<!---
	====================================
	Dyanamic Main Cats Portion End.
	====================================
--->

<!---
	Php Script End
--->

</div>
</div>
</div>
<div class="vc_row-full-width vc_clearfix"></div>
</div>
</article>

<div class="">

</div>


</div>



</div><!-- end main content -->


</div>
</div>



	
	</div>

<?php

	include_once "footer.php";
?>
  
<script src='https://kenwheeler.github.io/slick/slick/slick.js'></script>

  

    <script  src="newslick/js/index.js"></script>
<!--
<script src='js/main.js'></script>
  <script src="slick/slick.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    $(document).on('ready', function() {
      $(".regulart").slick({
        dots: false,
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
          autoplay: true,
  autoplaySpeed: 3000,
      });
    });
</script>
-->
<script type="text/javascript">
	function countdown(dateStr, displayElem, onTimerEnd = null) {
  let targetDate = new Date(dateStr).getTime();
  let displaySymbols = [...displayElem.querySelectorAll('.symbol .cur')]
  let prevValue = '      '
  let timer = setInterval(function() {
    let now = new Date().getTime();
    let remainingTime = targetDate - now;
    if (remainingTime <= 0) {
      clearInterval(timer) // stop timer
      if (onTimerEnd) {
        onTimerEnd() // run callback
      }
    }

    let formattedTimestr = format(remainingTime);
    updateDisplay(formattedTimestr);
  }, 1000);

  function format(time) {
    return [time % 86400000 / 3600000, // hours
            time % 3600000 / 60000, // minutes
            time % 60000 / 1000] // seconds  
      .map(x => Math.floor(x).toString().padStart(2, '0'))
      .join('')
  }

  function updateDisplay(timeString) {
    displaySymbols.forEach((curSymbol, i) => {
      let newValue = timeString[i]
      let currentValue = prevValue[i]
      if (currentValue !== newValue) { // animated change
        let parent = curSymbol.parentNode
        parent.classList.remove('anim')
        curSymbol.textContent = newValue
        curSymbol.nextElementSibling.textContent = currentValue
        var foo = parent.offsetWidth; // reflow hack
        parent.classList.add('anim')
      }
    })
    prevValue = timeString
  }
}

countdown('2123-02-28 16:14:00', document.getElementById('a'))


</script>

<!-- ajax search -->
<script type="text/javascript">
    $(document).ready(function(){
       $("#type_search_category").keyup(function(){
          var query = $(this).val();
          console.log(query);
          if (query != "") {
          	$('#output').css('display', 'block');
          	$.ajax({
              url: 'searchDataShow.php',
              method: 'POST',
              data: {query:query},
              success: function(result){
                // $('#output').css('display', 'block');
 				var result = JSON.parse(result);
 				console.log(result);
              }
            });
          } else {
          $('#output').css('display', 'none');
        }
      });
    });
</script>



</body>
</html>

