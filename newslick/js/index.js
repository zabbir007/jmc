$(document).ready(function(){
  $(".SlickCarousel").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:true, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev"></span>',
    nextArrow:'<span class="Slick-Next"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel1").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:4, // Number Of Carousel
    slidesToScroll:2, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows1"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev1"></span>',
    nextArrow:'<span class="Slick-Next1"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel2").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows2"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev2"></span>',
    nextArrow:'<span class="Slick-Next2"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})



$(document).ready(function(){
  $(".SlickCarousel3").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows3"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev3"></span>',
    nextArrow:'<span class="Slick-Next3"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})


$(document).ready(function(){
  $(".SlickCarousel4").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows4"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev4"></span>',
    nextArrow:'<span class="Slick-Next4"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})




$(document).ready(function(){
  $(".SlickCarousel5").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows5"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev5"></span>',
    nextArrow:'<span class="Slick-Next5"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel6").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows6"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev6"></span>',
    nextArrow:'<span class="Slick-Next6"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel7").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows7"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev7"></span>',
    nextArrow:'<span class="Slick-Next7"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel8").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows8"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev8"></span>',
    nextArrow:'<span class="Slick-Next8"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel9").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows9"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev9"></span>',
    nextArrow:'<span class="Slick-Next9"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel10").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows10"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev10"></span>',
    nextArrow:'<span class="Slick-Next10"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel11").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows11"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev11"></span>',
    nextArrow:'<span class="Slick-Next11"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel12").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows12"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev12"></span>',
    nextArrow:'<span class="Slick-Next12"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel13").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows13"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev13"></span>',
    nextArrow:'<span class="Slick-Next13"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})
$(document).ready(function(){
  $(".SlickCarousel14").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows14"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev14"></span>',
    nextArrow:'<span class="Slick-Next14"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel15").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows15"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev15"></span>',
    nextArrow:'<span class="Slick-Next15"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel16").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows16"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev16"></span>',
    nextArrow:'<span class="Slick-Next16"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel17").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows17"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev17"></span>',
    nextArrow:'<span class="Slick-Next17"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel18").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows18"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev18"></span>',
    nextArrow:'<span class="Slick-Next18"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel19").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows19"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev19"></span>',
    nextArrow:'<span class="Slick-Next19"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel20").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows20"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev20"></span>',
    nextArrow:'<span class="Slick-Next20"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel21").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows21"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev21"></span>',
    nextArrow:'<span class="Slick-Next21"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel22").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows22"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev22"></span>',
    nextArrow:'<span class="Slick-Next22"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel23").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows23"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev23"></span>',
    nextArrow:'<span class="Slick-Next23"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel24").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows24"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev24"></span>',
    nextArrow:'<span class="Slick-Next24"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel25").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows25"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev25"></span>',
    nextArrow:'<span class="Slick-Next25"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel26").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows26"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev26"></span>',
    nextArrow:'<span class="Slick-Next26"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel27").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows27"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev27"></span>',
    nextArrow:'<span class="Slick-Next27"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel28").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows28"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev28"></span>',
    nextArrow:'<span class="Slick-Next28"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel29").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows29"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev29"></span>',
    nextArrow:'<span class="Slick-Next29"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})

$(document).ready(function(){
  $(".SlickCarousel30").slick({
    rtl:false, // If RTL Make it true & .slick-slide{float:right;}
    autoplay:false, 
    autoplaySpeed:5000, //  Slide Delay
    speed:800, // Transition Speed
    slidesToShow:5, // Number Of Carousel
    slidesToScroll:1, // Slide To Move 
    pauseOnHover:false,
    appendArrows:$(".Container .Arrows30"), // Class For Arrows Buttons
    prevArrow:'<span class="Slick-Prev30"></span>',
    nextArrow:'<span class="Slick-Next30"></span>',
    easing:"linear",
    responsive:[
      {breakpoint:801,settings:{
        slidesToShow:3,
      }},
      {breakpoint:641,settings:{
        slidesToShow:3,
      }},
      {breakpoint:481,settings:{
        slidesToShow:2,
      }},
    ],
  })
})