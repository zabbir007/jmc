
                    <!-- header wrapper -->
					<?php include_once("header.php");?>
                                 <!-- end header wrapper -->
        
        
                <section class="page-top page-header-6">
        <div class="container hide-title">
    <div class="row">
        <div class="col-lg-12 clearfix">
            <div class="pt-right d-none">
                <h1 class="page-title">Cart</h1>
                            </div>
                            <div class="breadcrumbs-wrap pt-left">
                    <ul class="breadcrumb"><li class="home"><a itemprop="url" href="#" title="Go to Home Page"><span itemprop="title">Home</span></a><i class="delimiter delimiter-2"></i></li><li><a itemprop="url" href="shop.php"><span itemprop="title">Shop</span></a><i class="delimiter delimiter-2"></i></li><li>Cart</li></ul>                </div>
                                </div>
    </div>
</div>    </section>
    
        <div id="main" class="column1 boxed"><!-- main -->

            
                        <div class="container">
                            
            
            <div class="row main-content-wrap">

            <!-- main content -->
            <div class="main-content col-lg-12">

                            
    <div id="content" role="main">
                
            <article class="post-208 page type-page status-publish hentry">
                
                <span class="entry-title" style="display: none;">Cart</span><span class="vcard" style="display: none;"><span class="fn"><a href="# title="Posts by Joe Doe" rel="author">Joe Doe</a></span></span><span class="updated" style="display:none">2016-06-20T09:22:56+00:00</span>
                <div class="page-content">
                    <div class="woocommerce"><p class="cart-empty">Your cart is currently empty.</p>	<p class="return-to-shop">
		<a class="button wc-backward" href="shop/index.php">
			Return to shop		</a>
	</p>
</div>
                </div>
            </article>

            <div class="">
            
                        </div>

        
    </div>

        

</div><!-- end main content -->


    </div>
    </div>


        
            
            </div><!-- end main -->

              <?php include_once "footer.php";?>
			  
			  <!-- WP Super Cache is installed but broken. The constant WPCACHEHOME must be set in the file wp-config.php and point at the WP Super Cache plugin directory. -->