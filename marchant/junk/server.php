<?php
session_start();
require_once('../config.php');
require_once('../config_pos.php');
$errors= array();
$marchant_name = "";
  $marchant_email = "";
  $marchant_contact= "";
  $marchant_username = "";
  $marchant_shop = "";
  $shop_address = "";
  $verify_email = "";
  $product_name = "";
  $product_quantity = "";
  $quanty_price = "";
  $product_color = "";
  $product_details = "";
  $product_size = "";

//login
if (isset($_POST['login'])) {
  $username = mysqli_real_escape_string($db,$_POST['username']);
  $password1 = mysqli_real_escape_string($db,$_POST['password']);
 $password = md5($password1);
  $sqget = "SELECT * FROM marchant_user WHERE username = '$username' AND password = '$password' AND status = '1' AND admin_approval = '1'";
 $sqllogresult = mysqli_query($db, $sqget);
 if (mysqli_num_rows($sqllogresult) == 1) {
     $_SESSION['usermarchantaccountlogin'] = $username;
  header('location: index.php');
 } else {
   $sqgetanother = "SELECT * FROM marchant_user WHERE username = '$username' AND password = '$password' AND status = '1' AND admin_approval = '0'";
    $sqlresult = mysqli_query($db, $sqgetanother);
    if (mysqli_num_rows($sqlresult) == 1) {
     array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Your Marchant Request Now Pending, Please wait for approval</p></center>");
    } else{
  array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Incorrect Username or Password</p></center>");
}
 }
 
}

//login
if (isset($_POST['register'])) {
  $marchant_name = mysqli_real_escape_string($db,$_POST['marchant_name']);
  $marchant_email = mysqli_real_escape_string($db,$_POST['marchant_email']);
  $marchant_contact= mysqli_real_escape_string($db,$_POST['marchant_contact']);
  $marchant_username = mysqli_real_escape_string($db,$_POST['marchant_username']);
  $agree = mysqli_real_escape_string($db,$_POST['agree']);
  $marchant_shop = mysqli_real_escape_string($db,$_POST['marchant_shop']);
  $shop_address = mysqli_real_escape_string($db,$_POST['shop_address']);
  $password1 = mysqli_real_escape_string($db,$_POST['password1']);
  $password2 = mysqli_real_escape_string($db,$_POST['password2']);

if (!filter_var($marchant_email, FILTER_VALIDATE_EMAIL)) {
      array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Invalid email address</p></center>");
    }
    $sql = "SELECT * FROM marchant_user WHERE marchant_email = '$marchant_email' AND status = '1'";
$result = mysqli_query($db, $sql);
if (mysqli_num_rows($result) == 0) {
  $sql = "SELECT * FROM marchant_user WHERE username = '$marchant_username' AND status = '1'";
$result = mysqli_query($db, $sql);
if (mysqli_num_rows($result) == 0) {
   if (strlen($marchant_contact) !==  11) {
      array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Please select 11 digit valid number</p></center>");
    }
    else{
  if(!preg_match("/^[0]{1}[1]{1}[3-9]{1}\d+\.?\d*$/",$marchant_contact)){
    array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> You selected invalid operator prefix</p></center>");
  }
  
}
if ($password1 !== $password2) {
array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Password did not matched</p></center>");
}
}else{
  array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Username Allready Taken, Choose another one</p></center>");
}
} else{
  array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Email address already registered</p></center>");
}


if(count($errors)==0){
  $password = md5($password1);
  $code = rand(1234, 5678);
  $to = $marchant_email;
        $subject = "Verify Email Address";
        $message = "
<html>
<body>
<p>Your Email Verification Code is  <strong>".$code."</strong></p>
</body>
</html>
";
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= 'From: treeslife09@gmail.com';

mail($to,$subject,$message,$headers);
  $sqget = "INSERT INTO `marchant_user`(`marchant_name`, `marchant_email`, `marchant_contact`, `marchant_shop`, `marchant_shop_address`, `password`, `username`, `status`, `agree_status`, `verify_code`) VALUES ('$marchant_name','$marchant_email','$marchant_contact','$marchant_shop','$shop_address','$password','$marchant_username','0','$agree', '$code')";
  mysqli_query($db, $sqget);
  $_SESSION['confirmemail'] = $code;
  header('location: verify-account.php');
 }
 
}
if (isset($_POST['account_verify'])) {
  $verify_email = mysqli_real_escape_string($db,$_POST['verify_email']);
  $sql = "SELECT * FROM marchant_user WHERE verify_code = '$verify_email' AND status = '0'";
  $resultverify = mysqli_query($db, $sql);
  if (mysqli_num_rows($resultverify) == 0) {

    array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Verification code was incorrect</p></center>");
  }else{
    $sqlupdate = "UPDATE marchant_user SET status = '1' WHERE verify_code = '$verify_email'";
    mysqli_query($db, $sqlupdate);
    $data = mysqli_fetch_array($resultverify);
    $username =  $data['username'];    
 header('location: login.php');

  }
}

if (isset($_POST['addproduct'])) {

  $product_name = mysqli_real_escape_string($db2,$_POST['product_name']);
  $product_quantity = mysqli_real_escape_string($db2,$_POST['product_quantity']);
  $quanty_price= mysqli_real_escape_string($db2,$_POST['quanty_price']);
  $product_color = mysqli_real_escape_string($db2,$_POST['product_color']);
  $product_details = mysqli_real_escape_string($db2,$_POST['product_details']);
  $product_size = mysqli_real_escape_string($db2,$_POST['$product_size']);
  $categories = mysqli_real_escape_string($db2,$_POST['categories']);
  $stock_id = rand(12345, 6789);

  $up = "../photo/".$stock_id.".jpg";
  $path = "photo/".$stock_id.".jpg";
  if(is_uploaded_file($_FILES['img']['tmp_name'])){
    move_uploaded_file($_FILES['img']['tmp_name'],$up);
  }
  $filetmp1 = $_FILES ["img1"]["tmp_name"];
   $filename1 = $_FILES ["img1"]["name"];
   $filetype1 = $_FILES ["img1"]["type"];
   $filepath1 = "photo/" .$filename1;
   $move1 = "../photo/" .$filename1;
   if(is_uploaded_file($_FILES['img1']['tmp_name'])){
    move_uploaded_file($_FILES['img1']['tmp_name'],$move1);
  }
    $filetmp2 = $_FILES ["img2"]["tmp_name"];
   $filename2 = $_FILES ["img2"]["name"];
   $filetype2 = $_FILES ["img2"]["type"];
   $filepath2 = "photo/" .$filename2;
   $move2 = "../photo/" .$filename2;
   if(is_uploaded_file($_FILES['img2']['tmp_name'])){
    move_uploaded_file($_FILES['img2']['tmp_name'],$move2);
  }
   $filetmp3 = $_FILES ["img3"]["tmp_name"];
   $filename3 = $_FILES ["img3"]["name"];
   $filetype3 = $_FILES ["img3"]["type"];
   $filepath3 = "photo/" .$filename3;
   $move3 = "../photo/" .$filename3;
   if(is_uploaded_file($_FILES['img3']['tmp_name'])){
    move_uploaded_file($_FILES['img3']['tmp_name'],$move3);
  }
  $sqget = "INSERT INTO `0_stock_master`(`stock_id`, `category_id`, `description`, `long_description`, `units`, `no_sale`, `no_purchase`, `alt_img1`, `alt_img2`, `alt_img4`) VALUES ('$path', '$categories', '$product_name','$product_details','$product_quantity','0',[value-7],[value-8],[value-9],[value-10],[value-11],[value-12],[value-13],[value-14],[value-15],[value-16],[value-17],[value-18],[value-19],[value-20],[value-21],[value-22],[value-23],[value-24],[value-25],[value-26],[value-27],[value-28],[value-29],[value-30],[value-31])";

}

if(isset($_GET['logout'])){
  session_destroy();
  unset($_SESSION['adminusersession']);
  header('location: index.php');
}


/*===================================
Add New Book, Update, Delete Portion Start
====================================*/
/*
if (isset($_POST['add_book'])) {

  
   $book_name = mysqli_real_escape_string($db,$_POST['book_name']);
   $category = mysqli_real_escape_string($db,$_POST['category']);
   $subject_name = mysqli_real_escape_string($db,$_POST['subject']);
   $author_name = mysqli_real_escape_string($db,$_POST['author_name']);
   $books_quantity = mysqli_real_escape_string($db,$_POST['books_quantity']);
   $book_edition = mysqli_real_escape_string($db,$_POST['book_edition']);
   $department = mysqli_real_escape_string($db,$_POST['department']);
   $filetmp = $_FILES ["bookimg"]["tmp_name"];
   $filename = $_FILES ["bookimg"]["name"];
   $filetype = $_FILES ["bookimg"]["type"];
   $filepath = "photo/" .$filename;
   $move = "../photo/" .$filename;
   $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
	array_push($errors, "<center><p style='color:red; font-weight:bold;'>Invalid Image Format (Please Select jpg/jpeg/png/gif format).</p><center>" );
}
else{

	move_uploaded_file($_FILES["bookimg"]["tmp_name"], $move);
	$sqlinsert = "INSERT INTO books_table (book_name, book_author, available_copy, book_category, subject_name, image_path, edition, department) VALUES ('$book_name','$author_name','$books_quantity','$category','$subject_name','$filepath', '$book_edition', '$department')";
    mysqli_query($db, $sqlinsert); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Book Added Successfuly.</p><center>" );
   
 
}	

 }
//Update Book Details
 if (isset($_POST['update_book_details'])) {
 	$hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);
   $book_name = mysqli_real_escape_string($db,$_POST['book_name']);
   $category = mysqli_real_escape_string($db,$_POST['category']);
   $subject_name = mysqli_real_escape_string($db,$_POST['subject']);
   $author_name = mysqli_real_escape_string($db,$_POST['author_name']);
   $books_quantity = mysqli_real_escape_string($db,$_POST['books_quantity']);
   $book_edition = mysqli_real_escape_string($db,$_POST['book_edition']);
   $department = mysqli_real_escape_string($db,$_POST['department']);
   $filetmp = $_FILES ["bookimg"]["tmp_name"];
   $filename = $_FILES ["bookimg"]["name"];
   $filetype = $_FILES ["bookimg"]["type"];
   
   if (empty($filename)) {
   	$sqlupdate = "UPDATE books_table SET book_name = '$book_name', book_author = '$author_name', available_copy = '$books_quantity', book_category = '$category', subject_name = '$subject_name', edition = '$book_edition', department = '$department'  WHERE id = '$hiddenid'";
    mysqli_query($db, $sqlupdate); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Book Updated Successfuly.</p><center>" );
   } else {
  $filepath = "photo/" .$filename;
   $move = "../photo/" .$filename;
   $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats
	move_uploaded_file($_FILES["bookimg"]["tmp_name"], $move);
	$sqlupdate = "UPDATE books_table SET book_name = '$book_name', book_author = '$author_name', available_copy = '$books_quantity', book_category = '$category', subject_name = '$subject_name', image_path = '$filepath', edition = '$book_edition' WHERE id = '$hiddenid'";
    mysqli_query($db, $sqlupdate); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Book Updated Successfuly.</p><center>" );
   
 }
}
 if (isset($_POST['delete_book'])) {

 	$hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);

	$querydel ="DELETE FROM books_table WHERE id = '$hiddenid';";
    mysqli_query($db, $querydel);

    array_push($errors, "<center><h4 style='color:green;'>Delete Success <a href='book.php?addnewbooks='>Add New Book</a></h4><center>");
 } */
/*===================================
Add New Book, Update, Delete Portion End
====================================*/

/*===================================
Categpory Add Update Delete Portion Start
====================================*/
/*
if (isset($_POST['add_category'])) {
	
	$category_name = mysqli_real_escape_string($db,$_POST['category_name']);

    $query ="INSERT INTO category (category_name) VALUES ('$category_name')";
    mysqli_query($db, $query);

    array_push($errors, "<center><h4 style='color:green;'>New Category Added <a href='book.php?allcategorylist='>View All Category List</a></h4><center>");
   
}

if (isset($_POST['delete_category'])) {
$hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);

	$querydel ="DELETE FROM category WHERE id = '$hiddenid';";
    mysqli_query($db, $querydel);

    array_push($errors, "<center><h4 style='color:green;'>Delete Success <a href='book.php?addnewcategory='>Add New Category</a></h4><center>");
}

if (isset($_POST['update_edited_category'])) {
	$hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);
	$category_name_edit = mysqli_real_escape_string($db,$_POST['category_name_edit']);

	  $queryupdate ="UPDATE category SET category_name ='$category_name_edit' WHERE id = '$hiddenid'";
       mysqli_query($db, $queryupdate);

    array_push($errors, "<center><h4 style='color:green;'>Category Update Successfuly</h4><center>");
} */
/*===================================
Categpory Add Update Delete Portion End
====================================*/

/*===================================
Subject Add Update Delete Portion Start
====================================*/
/*
if (isset($_POST['add_subject'])) {
	$subject_name = mysqli_real_escape_string($db,$_POST['subject_name']);

    $querysubjectadd ="INSERT INTO subject (subject_name) VALUES ('$subject_name')";
    mysqli_query($db, $querysubjectadd);

    array_push($errors, "<center><h4 style='color:green;'>New Subject Added <a href='book.php?viewallsubject='>View All Subject List</a></h4><center>");
   
}
if (isset($_POST['delete_subject'])) {
$hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);

	$querydel ="DELETE FROM subject WHERE id = '$hiddenid';";
    mysqli_query($db, $querydel);

    array_push($errors, "<center><h4 style='color:green;'>Delete Success <a href='book.php?addnewsubject='>Add New Subject</a></h4><center>");
}
if (isset($_POST['update_edited_subject'])) {
	$hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);
	$subject_name_edit = mysqli_real_escape_string($db,$_POST['subject_name_edit']);

	  $queryupdate ="UPDATE subject SET subject_name ='$subject_name_edit' WHERE id = '$hiddenid'";
       mysqli_query($db, $queryupdate);

    array_push($errors, "<center><h4 style='color:green;'>Subject Name Update Successfuly</h4><center>");
}

*/

?>