<?php include ('admincon.php');
if (empty($_SESSION['inthemainathorityaccessadmincontrolifthatsare'])) {

     header('location: login.php');

 } else {
 include('header.php');

 ?>

<body>

    <div id="wrapper">

        <?php //sidebar added here 
         include('sidebar.php');
        ?>

        <div id="page-wrapper">
      <?php include ('errors.php'); 
      if (isset($_GET['editstock'])) { ?>
      
<center><h2 style="text-decoration: underline;">Edit Sub Menu</h2></center>
<?php
$catid = mysqli_real_escape_string($db2, $_GET['0_stock_category_id']);
$imgid = mysqli_real_escape_string($db2, $_GET['0_stock_image_id']); 
?>
	<?php $sqlcats = "SELECT * FROM 0_stock_category WHERE category_id = '$catid'";
	$logoresult = mysqli_query($db2, $sqlcats);
	$rowcategory = mysqli_fetch_array($logoresult); ?>
<div style="width:400px; margin: 0 auto;">
<form action="add-stock-img.php?0_stock_category_id=<?php echo $catid; ?>&0_stock_image_id=<?php echo $imgid; ?>&editstock=" method="POST"  enctype="multipart/form-data">
	 <div class="form-group">
    <label>Category Name</label>
    <input type="text" class="form-control" name="catname" value="<?php echo $rowcategory['description']; ?>" required>
  </div>
  <div class="form-group">
    <label >Select Main Category</label>
    <?php $product_group = $rowcategory['product_group']; 
	$sqlups = "SELECT * FROM new_tbl2 WHERE id = '$product_group'";
    $sqlres = mysqli_query($db2, $sqlups);
    $diss = mysqli_fetch_array($sqlres); ?>
    <select class="form-control" name="maincats">
      <option value="<?php echo $diss['id']; ?>"><?php echo $diss['group_brands']; ?></option>
    <?php $product_group = $rowcategory['product_group']; 
	$sqlups = "SELECT * FROM new_tbl2";
    $sqlres = mysqli_query($db2, $sqlups); 
    while ($dissr = mysqli_fetch_array($sqlres)) { ?>
    	<option value="<?php echo $dissr['id']; ?>"><?php echo $dissr['group_brands']; ?></option>
   <?php }
    ?>
    </select>
  </div>
  <div class="form-group">
    <label>Select image</label>
    <input type="file" name="stockimage" class="form-control">
  </div>
  <div class="form-group">
  	<label>Current Image</label><br>
  	<?php $sql = "SELECT * FROM 0_stock_image WHERE description = '".$rowcategory['description']."'";
                 $sqlres = mysqli_query($db2, $sql);
                 if (mysqli_num_rows($sqlres) == 0) { echo "Image Not Added !"; } else {  $dis = mysqli_fetch_array($sqlres); ?>
  	<img src="../<?php echo $dis['image_path']; ?>" width="60px">
  <?php } ?>
  </div>
<input type="hidden" name="0_stock_category_id" value="<?php echo $catid; ?>">
<input type="hidden" name="0_stock_image_id" value="<?php echo $imgid; ?>">
<input type="submit" name="stockeditup" value="Update Image" class="btn btn-success"> 
</form>
</div>
<br>
<br>
<center>
<a href="add-stock-img.php?viewall=" class="btn btn-primary">View All</a>
</center>


      <?php
      }
      ?>


<?php
if (isset($_GET['viewall'])) { ?>
<div class="row">
            <div style="width:360px; float:right; margin-right: 20px; margin-bottom: 10px; margin-top: 10px;">
                   <form action="add-stock-img.php" method="GET" accept-charset="utf-8">
                    <div class="input-group custom-search-form">
                    	<div style="width: 171px; float: left;">
                    	   <input type="text" class="form-control" placeholder="Enter Category Name" name="searchname">
                    	   </div>
                    	   <div style="width: 150px; float: right;">
                           <div class="form-group">
             <?php
			 $sqlups = "SELECT * FROM new_tbl2";
            $sqlres = mysqli_query($db2, $sqlups);
            ?>
    <select class="form-control" name="manicats">
    	<option value="0">Select Category</option>
    	<?php while ($diss = mysqli_fetch_array($sqlres)) { ?>
    		<option value="<?php echo $diss['id']; ?>"><?php echo $diss['group_brands']; ?></option>
    <?php	} ?>
    </select>
  </div>
</div>
                                <span class="input-group-btn">

    
                                <button class="btn btn-default" type="submit" name="search">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                          </form>
          </div>
        </div>
		<table class="table table-striped">
			<tr>
				<th>ID</th>
				<th>Sub Menu Name</th>
				<th>Sub Menu Image</th>
				<th>Main Menu</th>
				<th>Status</th>
				<th colspan="2" style="text-align: center;">Action</th>
			</tr>
			
				<?php $sqllogo = "SELECT * FROM 0_stock_category";
				$logoresult = mysqli_query($db2, $sqllogo);
				while ($row = mysqli_fetch_array($logoresult)) { ?>
					<tr>
				<td><?php echo $row['category_id']; ?></td>
				<td><strong><?php echo $row['description']; ?></strong></td>
			<?php $product_group = $row['product_group']; 
			 $sqlups = "SELECT * FROM new_tbl2 WHERE id = '$product_group'";
            $sqlres = mysqli_query($db2, $sqlups);
           $diss = mysqli_fetch_array($sqlres); ?>
				<?php $sql = "SELECT * FROM 0_stock_image WHERE description = '".$row['description']."'";
                 $sqlres = mysqli_query($db2, $sql);
                 if (mysqli_num_rows($sqlres) == 0) { ?>
                 <td><form action="add-stock-img.php?viewall=" method="POST"  enctype=multipart/form-data>
	                 <input type="file" class="form-control-file" name="stockimage" required><br>
	                 <input type="hidden" name="stock" value="<?php echo $row['description']; ?>">
                     <input type="submit" name="stockup" class="btn btn-success" value="Add Image">
                 </form>
	             </td>
	             
             <?php    }else{
                 $dis = mysqli_fetch_array($sqlres);
                 	?>
                 <td> 
                 	<img src="../<?php echo $dis['image_path']; ?>" width="60px"></td>
            <?php     
        }   ?>
		<td><strong><?php echo $diss['group_brands']; ?></strong></td>
		<td>
        <center>
			<?php if ($row['inactive'] == 0) { ?>
			<strong style="color: green;">Active</strong>
			<form action="add-stock-img.php?viewall=" method="POST">
			<input type="hidden" name="0_stock_category_id" value="<?php echo $row['category_id']; ?>">
			<button class=" btn btn-danger" name="inactivecat" type="submit">Inactive Now</button>
		</form>
	<?php } else{ ?>
		<strong style="color: red;">Inactive</strong>
			<form action="add-stock-img.php?viewall=" method="POST">
			<input type="hidden" name="0_stock_category_id" value="<?php echo $row['category_id']; ?>">
			<button class=" btn btn-success" name="activecat" type="submit">Active Now</button>
		</form>
<?php } ?> 
</center>
	</td>
		<td><form action="add-stock-img.php" method="GET">
			<input type="hidden" name="0_stock_category_id" value="<?php echo $row['category_id']; ?>">
			<?php if (mysqli_num_rows($sqlres) == 0) { ?>
			<input type="hidden" name="0_stock_image_id" value="0">
	<?php	} else{ ?>
           <input type="hidden" name="0_stock_image_id" value="<?php echo $dis['id']; ?>">
	<?php } ?>
		<button type="submit" name="editstock" class="btn btn-warning"><i class="fa fa-edit"></i> Edit</button></form></td>
		<td><form action="add-stock-img.php?viewall=" method="POST">
			<input type="hidden" name="0_stock_category_id" value="<?php echo $row['category_id']; ?>">
			<input type="hidden" name="0_stock_image_id" value="<?php echo $dis['id']; ?>">
			<button type="submit" name="deletestock" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button></form></td>

			
				</tr>
			<?php } ?>
		</table>
<?php

}

if (isset($_GET['search'])) {
	$searchname = mysqli_real_escape_string($db2, $_GET['searchname']); 
    $manicats = mysqli_real_escape_string($db2, $_GET['manicats']);

	?>

	<div class="row">
            <div style="width:360px; float:right; margin-right: 20px; margin-bottom: 10px; margin-top: 10px;">
                   <form action="add-stock-img.php" method="GET" accept-charset="utf-8">
                    <div class="input-group custom-search-form">
                    	<div style="width: 171px; float: left;">
                    	   <input type="text" class="form-control" placeholder="Enter Category Name" name="searchname">
                    	   </div>
                    	   <div style="width: 150px; float: right;">
                           <div class="form-group">
             <?php
			 $sqlups = "SELECT * FROM new_tbl2";
            $sqlres = mysqli_query($db2, $sqlups);
            ?>
    <select class="form-control" name="manicats">
    	<option value="0">Select Category</option>
    	<?php while ($diss = mysqli_fetch_array($sqlres)) { ?>
    		<option value="<?php echo $diss['id']; ?>"><?php echo $diss['group_brands']; ?></option>
    <?php	} ?>
    </select>
  </div>
</div>
                                <span class="input-group-btn">

    
                                <button class="btn btn-default" type="submit" name="search">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                          </form>
          </div>
        </div>
		<table class="table table-striped">
			<tr>
				<th>ID</th>
				<th>Sub Menu Name</th>
				<th>Sub Menu Image</th>
				<th>Main Menu</th>
				<th colspan="2" style="text-align: center;">Action</th>
			</tr>
			       
				<?php
                     if (empty($searchname)) {

                     	$sqllogo = "SELECT * FROM 0_stock_category WHERE product_group = '$manicats'";
                     }
                      elseif ($manicats == 0) {
				$sqllogo = "SELECT * FROM 0_stock_category WHERE description LIKE '%$searchname%'";

				} elseif(!empty($searchname) AND $manicats !== 0){

				$sqllogo = "SELECT * FROM 0_stock_category WHERE  description LIKE '%$searchname%' AND product_group = '$manicats'";
				}
				$logoresult = mysqli_query($db2, $sqllogo);
				if (mysqli_num_rows($logoresult) == 0) { ?>
					 <tr>
					 	<td colspan="5"><center>Data Not Found !</center></td>
					 </tr>
			<?php	} else{
				while ($row = mysqli_fetch_array($logoresult)) { ?>
					<tr>
				<td><?php echo $row['category_id']; ?></td>
				<td><strong><?php echo $row['description']; ?></strong></td>
			<?php $product_group = $row['product_group']; 
			 $sqlups = "SELECT * FROM new_tbl2 WHERE id = '$product_group'";
            $sqlres = mysqli_query($db2, $sqlups);
           $diss = mysqli_fetch_array($sqlres); ?>
				<?php $sql = "SELECT * FROM 0_stock_image WHERE description = '".$row['description']."'";
                 $sqlres = mysqli_query($db2, $sql);
                 if (mysqli_num_rows($sqlres) == 0) { ?>
                 <td><form action="add-stock-img.php?viewall=" method="POST"  enctype=multipart/form-data>
	                 <input type="file" class="form-control-file" name="stockimage" required><br>
	                 <input type="hidden" name="stock" value="<?php echo $row['description']; ?>">
                     <input type="submit" name="stockup" class="btn btn-success" value="Add Image">
                 </form>
	             </td>
	             
             <?php    }else{
                 $dis = mysqli_fetch_array($sqlres);
                 	?>
                 <td> 
                 	<img src="../<?php echo $dis['image_path']; ?>" width="60px"></td>
            <?php     
        }   ?>
		<td><strong><?php echo $diss['group_brands']; ?></strong></td>
		<td><center>
			<?php if ($row['inactive'] == 0) { ?>
			<strong style="color: green;">Active</strong>
			<form action="add-stock-img.php?viewall=" method="POST">
			<input type="hidden" name="0_stock_category_id" value="<?php echo $row['category_id']; ?>">
			<button class=" btn btn-danger" name="inactivecat" type="submit">Inactive Now</button>
		</form>
	<?php } else{ ?>
		<strong style="color: red;">Inactive</strong>
			<form action="add-stock-img.php?viewall=" method="POST">
			<input type="hidden" name="0_stock_category_id" value="<?php echo $row['category_id']; ?>">
			<button class=" btn btn-success" name="activecat" type="submit">Active Now</button>
		</form>
<?php } ?> 
</center></td>
		<td><form action="add-stock-img.php" method="GET">
			<input type="hidden" name="0_stock_category_id" value="<?php echo $row['category_id']; ?>">
			<?php if (mysqli_num_rows($sqlres) == 0) { ?>
			<input type="hidden" name="0_stock_image_id" value="0">
	<?php	} else{ ?>
           <input type="hidden" name="0_stock_image_id" value="<?php echo $dis['id']; ?>">
	<?php } ?>
		<button type="submit" name="editstock" class="btn btn-warning"><i class="fa fa-edit"></i> Edit</button></form></td>
		<td><form action="add-stock-img.php?viewall=" method="POST">
			<input type="hidden" name="0_stock_category_id" value="<?php echo $row['category_id']; ?>">
			<input type="hidden" name="0_stock_image_id" value="<?php echo $dis['id']; ?>">
			<button type="submit" name="deletestock" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button></form></td>

			
				</tr>
			<?php }
			} ?>
		</table>
<?php		}
?>

  </div>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="vendor/raphael/raphael.min.js"></script>
    <script src="vendor/morrisjs/morris.min.js"></script>
    <script src="data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>

<?php } ?>
