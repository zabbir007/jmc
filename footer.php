<style type="text/css">
	.give_alert_for_add_to_cart{
		display:none;
		position:fixed;
		width:100%;
		height:100%;
		z-index:7777;
		top:0;
		left:0;
		overflow:auto;
		background:rgba(0,0,0,0);
		padding-top:150px;
		animation-name:alertbox;
		animation-duration:4s;
		animation-delay:-2s;
	}
	.alert_text_area{width:50%;margin:0px auto}
	.show_cart_highlight{
		position:fixed;
		z-index:999999;
		top:0;
		left:0;
		overflow:hidden;
		background:rgb(11, 6, 83);
		float:right;
		border-radius:100%;
		text-align:center;
		font-weight:bold;
		color:#fff;
		width:80px;
		height:80px;
		vertical-align:bottom;
		animation-name: example;
		animation-duration: 4s;
		animation-delay: -2s;
	}
	.cart-text{
		width: 50px;
		height: 30px;
		line-height: 0px;
		margin-top: 30px;
		margin-left: 13px;
	}
	.cart_value{margin-left:7px}
	@keyframes example {
    0%   {background-color:red; left:0px; top:0px;}
    25%  {background-color:yellow; left:0px; top:0px;}
    50%  {background-color:blue; left:0px; top:200px;}
    75%  {background-color:green; left:0px; top:0px;}
    100% {background-color:red; left:0px; top:0px;}
	}
	@keyframes alertbox {
    0%   {left:0px; top:0px;}
    25%  {left:0px; top:-100px;}
    50%  {left:0px; top:-100px;}
    75%  {left:0px; top:100px;}
    100% {left:0px; top:0px;}
}
</style>
<div class="give_alert_for_add_to_cart" id="add_to_cart_alert">
	<div class="container">
	<div class="alert_text_area">
	<div class="alert alert-danger" id="alert_area">
		<span id="close_alert" class="close" aria-label="close">&times;</span>
		<strong><span id="alert_product_name"></span></strong>
	</div>
	</div>
	</div>
</div>
<?php
	if($count_shoping_cart==0){
		$display = "display:none";
	}
	else{
		$display = "display:block";
	}
?>
<div class="show_cart_highlight" onmousedown="window.location.assign('shop_cart_box.php')" style="<?=$display?>" id="show_cart_highlight">
	<div class="cart-text">
		<span class="cart_value" id="reload_phpscript" lang="en"><?=$count_shoping_cart?></span>
		<span><img src="icon/cart.png"/></span>
	</div>
</div>
<div class="footer-wrapper ">
<div class="row" style="background: #111;">
	<div class="container">
		<div style="width: 100%; height: 30px;"></div>
	</div>
<div class="container">
	<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-12">
		<div class="row">
	<div class="col-lg-2 col-md-2 col-sm-4 col-2">
	<img src="icon/support.png">
</div>
<div class="col-lg-10 col-md-10 col-sm-8 col-10">
	<span style="color:white; font-weight: bolder; font-size: 22px;">Got question? Call us 24/7 !</span><br>
	<?php
	$sql_phone = "select * from web_attribut_info";
	$result_phone = $db->query($sql_phone);
	$data_phone = mysqli_fetch_array($result_phone);
?>	
<span style="color:#4e7df1; font-weight: bold; font-size: 18px;"><i class="fa fa-phone"></i> <?=$data_phone[2]?></span><br>
   <span style="color:white;"><?=$data_phone[4]?></span>
   <br>
   <br>
   <img class="img-responsive" src="wp-content/uploads/sites/78/2017/03/payment-icon.png" alt="Payment Gateways" />
</div>
	</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-12">
		<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-6">
		<span style="color:white; font-weight: bolder; font-size: 22px;">Our Company</span><br>
		<ul style="list-style: none; padding: 0; margin:0; color: white;">
			<li><a href="#" style="color: white; font-size: 16px;">Terms and conditions of use</a></li>
			<li><a href="#" style="color: white; font-size: 16px;">About us</a></li>
			<li><a href="#" style="color: white; font-size: 16px;">Secure payment</a></li>
			<li><a href="#" style="color: white; font-size: 16px;">Contact us</a></li>

		</ul>
		



	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-6">
		<span style="color:white; font-weight: bolder; font-size: 22px;">Your Account</span><br>
		<ul style="list-style: none; padding: 0; margin:0; color: white;">
			<li><a href="#" style="color: white; font-size: 16px;">Personal Info</a></li>
			<li><a href="#" style="color: white; font-size: 16px;">My Orders</a></li>

		</ul>
	</div>
	</div>
	</div>

	</div>
	<div style="width: 100%; height: 30px;"></div>
</div>
 </div>

 <div class="row" style="background: #2C3E50;">
 	<div class="container">
 		<div class="row">
 	<div class="col-lg-8 col-md-6 col-sm-6 col-12">
 		<?php 
	$sql="SELECT * FROM `footer`";
	$result=$db->query($sql);
	$data=mysqli_fetch_array($result);
?>
<span style="color: white;"><?=$data[1]?></span>

 	</div>
 	<div class="col-lg-4 col-md-6 col-sm-6 col-12">
 		<span  lang="en" style="color: white;">Software solution by </span><a style="color: white;" href="http://www.ngicon.com">Ngicon.com</a>
 	</div>
 	</div>
 </div>
 </div>
</div>
<!-- responsive menu start here-->
<div class="panel-overlay"></div>
<div id="side-nav-panel" class="">
	<a href="#" class="side-nav-panel-close"><i class="fa fa-close"></i></a>
	<div class="menu-wrap">
		<ul id="menu-main-menu-1" class="mobile-menu accordion-menu">
			<li id="accordion-menu-item-744" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-143 current_page_item active">
				<a href="index.php" rel="nofollow" class=" current ">Home</a>
			</li>
			<?php
				$sql_for_menue = "select * from new_tbl2 where desc2='0'";
				$result_for_menue = $db2->query($sql_for_menue);
				while($data_for_menue = mysqli_fetch_array($result_for_menue)){
			?>
					<li id="accordion-menu-item-781" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children  has-sub">
						<a href="#" rel="nofollow" class=""><?=$data_for_menue[1];?></a>
						<span class="arrow"></span>
											<ul class="sub-menu">
												<?php
													$sql_for_submenue = "select * from 0_stock_category where product_group='$data_for_menue[0]'";
													$result_for_submenue = $db2->query($sql_for_submenue);
													while($data_for_submenue = mysqli_fetch_array($result_for_submenue)){
												?>
														<li id="accordion-menu-item-785" class="menu-item menu-item-type-post_type menu-item-object-page ">
															<a href="shop.php?cat=<?=$data_for_submenue[0];?>" rel="nofollow" class=""><?=$data_for_submenue[1];?></a>
														</li>
												<?php
													}
												?>
										
						
						<li id="accordion-menu-item-730" class="menu-item menu-item-type-custom menu-item-object-custom "></li>
						</ul>
					</li>
					<?php
								}
							
							?>
			<?php
				
			?>
			<li id="nav-menu-item-779" class="menu-item menu-item-type-post_type menu-item-object-page " data-cols="1">
				<a href="contact_us.php" class="">Contact Us</a>
			</li>
		</ul>
	</div>
	<div class="menu-wrap">
		<ul id="menu-top-navigation-1" class="top-links accordion-menu show-arrow">
			<?php
				if(isset($_SESSION['00user_email00'])){
			?>
					<li class="menu-item">
						<a title="<?=$_SESSION['00user_title00']." ".$_SESSION['00firstname00'];?>" href="myaccount.php"><button class="btn btn-success"><?=$_SESSION['00user_title00']." ".$_SESSION['00firstname00'];?> <i class="fa fa-user"></i></button></a>
					</li>
					<li class="menu-item">
						<a href="myorder.php"><button class="btn btn-success" lang="en">My Order <i class="fa fa-first-order"></i></button></a>
					</li>
					<li class="menu-item">
						<a href="logout.php"><button class="btn btn-success" lang="en">Logout <i class="fa fa-sign-out"></i></button></a>
					</li>
			<?php
				}
				else{
			?>
					<li class="menu-item">
						<a href="account.php" lang="en">Login</a>
					</li>
			<?php
				}
			?>
		</ul>
	</div>
</div>
  





<!-- responsive menu End-->
<script type="text/javascript">
	jQuery(document).ready(function($) {
function resizeBannerHeight() {
if (Modernizr.mq('only all and (min-width: 992px)')) {
$(".home-banner-slider .porto-ibanner").height($(".porto-links-block").height());
} else {
$(".home-banner-slider .porto-ibanner").height('auto');
}
}
$(window).resize(function() {
resizeBannerHeight();
});
resizeBannerHeight();
});        </script>



<link rel='stylesheet' property='stylesheet' id='vc_google_fonts_abril_fatfaceregular-css'  href='http://fonts.googleapis.com/css?family=Abril+Fatface%3Aregular' type='text/css' media='all' />

<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js'></script>
<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts.js'></script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js'></script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js'></script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js'></script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js'></script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min.js'></script>
<script type='text/javascript' src='wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min.js'></script>
<script type='text/javascript' src='wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl.js'></script>
<script type='text/javascript' src='wp-includes/js/comment-reply.min.js'></script>
<script type='text/javascript' src='wp-includes/js/underscore.min.js'></script>
<script type='text/javascript' src='wp-includes/js/wp-util.min.js'></script>

<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min.js'></script>
<script type='text/javascript' src='wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js'></script>
<script type='text/javascript'>
var js_porto_vars = {"rtl":"","ajax_url":"http:\/\/www.portotheme.com\/wordpress\/porto\/shop2\/wp-admin\/admin-ajax.php","change_logo":"1","container_width":"1170","grid_gutter_width":"20","show_sticky_header":"1","show_sticky_header_tablet":"1","show_sticky_header_mobile":"1","ajax_loader_url":"\/\/www.portotheme.com\/wordpress\/porto\/shop2\/wp-content\/themes\/porto\/images\/ajax-loader@2x.gif","category_ajax":"1","prdctfltr_ajax":"","show_minicart":"1","slider_loop":"1","slider_autoplay":"1","slider_autoheight":"1","slider_speed":"5000","slider_nav":"","slider_nav_hover":"1","slider_margin":"","slider_dots":"1","slider_animatein":"","slider_animateout":"","product_thumbs_count":"4","product_zoom":"1","product_zoom_mobile":"1","product_image_popup":"1","zoom_type":"inner","zoom_scroll":"1","zoom_lens_size":"200","zoom_lens_shape":"square","zoom_contain_lens":"1","zoom_lens_border":"1","zoom_border_color":"#888888","zoom_border":"0","screen_lg":"1190","mfp_counter":"%curr% of %total%","mfp_img_error":"<a href=\"%url%\">The image<\/a> could not be loaded.","mfp_ajax_error":"<a href=\"%url%\">The content<\/a> could not be loaded.","popup_close":"Close","popup_prev":"Previous","popup_next":"Next","request_error":"The requested content cannot be loaded.<br\/>Please try again later."};
</script>
<script type='text/javascript' src='wp-content/themes/porto/js/theme.min.js'></script>
<script type='text/javascript' src='wp-includes/js/wp-embed.min.js'></script>
<script type='text/javascript' src='wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-en.js'></script>
<script type='text/javascript' src='wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js'></script>

<script type='text/javascript' src='wp-content/plugins/wysija-newsletters/js/front-subscribers.js'></script>

<script type="text/javascript">

			jQuery(document).ready(function($){
$("#footer .widget_wysija_cont .wysija-submit").removeClass("btn-default").addClass("btn-primary");
});
			</script>

 <script>
   $(window).scroll(function() {
    
    if ($(this).scrollTop()>760)
     {
        $('.a').fadeOut();
     }
    else
     {
      $('.a').fadeIn();
     }
     if ($(this).scrollTop()>759)
     {
        $('.b').fadeOut(0);
     }
    else
     {
      $('.b').fadeIn();
     }
     if ($(this).scrollTop()>760)
     {
        $('.c').fadeIn(0);
     }
    else
     {
      $('.c').fadeOut();
     }

     if ($(this).scrollTop()>160)
     {
        $('.d').show();
       
     }
    else

     {
      $('.d').hide();
     }

     if ($(this).scrollTop() < 140)
     {
        $('.e').show();

     }
    else
     {
      $('.e').hide();
     }

  
 });

  
</script>

  
<script type="text/javascript">
window.onscroll = function() {myFunctionsticky()};

var navbarr = document.getElementById("navbarr");
var sticky = navbarr.offsetTop;

function myFunctionsticky() {
  if (window.pageYOffset >= sticky) {
    navbarr.classList.add("sticky")
  } else {
    navbarr.classList.remove("sticky");
  }
}
</script>
<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show2");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show2')) {
        openDropdown.classList.remove('show2');
      }
    }
  }
}
</script>

<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction1() {
  document.getElementById("myDropdown1").classList.toggle("show1");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn1')) {
    var dropdowns = document.getElementsByClassName("dropdown-content1");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show1')) {
        openDropdown.classList.remove('show1');
      }
    }
  }
}
</script>
</body>


</html>
