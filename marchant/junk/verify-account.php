<?php include ('server.php'); 
if (empty($_SESSION['confirmemail'])) {
header('location:register.php');
}else{
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Verify Email Address</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Verify Email Address</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <?php include ('errors.php'); ?>
            
            <div class="row">
          <div class="container">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           We sent a 4 digit code in your email address, please check your email inbox or spam folder
                        </div>
                        <div class="panel-body">
                            <div class="row">
                            	<form action="verify-account.php" method="POST">
                                    
                                <div class="col-lg-10">
                                    
                                        <div class="form-group">
                                            <label>Enter 4 digit code <span style="color:red;">*</span></label>
                                            <input class="form-control" type="text" name="verify_email" placeholder="1234" value="<?php echo $verify_email; ?>" required>
                                        </div>                              
                        <button type="submit" name="account_verify" class="btn btn-primary">Verify Email</button>
                   
                      </form>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                       
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->

              </div>
            </div>
            <!-- /.row -->
        </div>



    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
<?php } ?>