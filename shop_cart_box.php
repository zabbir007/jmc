<?php
	include_once "header.php";

?>
<style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: -10px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
<div  id="process_order_list" class="container" style="background:#F5F5F5">
			<div class="cart_header" style="overflow:hidden;margin:30px auto">
				<div class="header_text"  style="float:left">
					<h1><span lang="en">CART</span></h1>
				</div>
				<div class="continue_shoping" style="float:right">
					<button onclick="window.location.assign('index.php');" class="continue_shop" style="cursor: pointer;" lang="en">CONTINUE SHOPPING</button>
				</div>
			</div>
			<!--change for shop cart responsive -->
			<div class="wrapper" >
				<?php
					if(!empty($_SESSION['shoping_cart'])){
				?>
				
				<div style="overflow-x:auto;">
				<table >
					<tr style="font-weight:bold;font-size:10px">
						<td></td>
						<td class="header"><span lang="en">ITEM</span></td>
						<td class="header"><span lang="en">QUANTITY</span></td>
						<td class="header"><span lang="en">UNIT PRICE</span></td>
						<td class="header"><span lang="en">SUB TOTAL</span></td>
						<td></td>
					</tr>
					<?php
					$total = 0;
					$subtotal = 0;
						if(!empty($_SESSION['shoping_cart'])){
						$total = 0;
						foreach($_SESSION['shoping_cart'] as $keys => $values)
						{
						$subtotal = $values['item_qty']*$values['item_price'];
						$total += $subtotal;
					?>
					
					<?php
					
					?>
						<tr>
							<td style="height:10px"></td>
						</tr>
						<tr>
							
							<td><button class="remove_btn" style="background:none;border:none;cursor:pointer" onclick="remove_product(this.value);" value="<?php echo $values['item_id']?>" ><i class="fa fa-remove" style="font-size:16px;color:red"></i></button></td>
							<td class="style_td"><img class="img_style"  src="<?php echo $product_images_url.$values['item_id'].".jpg";?>" />&nbsp;<span class="text_img"><?php echo $values['item_name'];?></span></td>
							<td class="style_td"><input style="width: 50px;text-align: center;" type="text" id="<?=$values['item_id'];?>" name="qty" onblur="update_qty(this.id,this.value);" value="<?php echo $values['item_qty'];?>"/></td>
							<td class="style_td"><?php echo "৳".$values['item_price'];?></td>
							<td class="style_td"><?php echo "৳".$subtotal;?></td>
							<td></td>
						</tr>
					<?php
							
						}
						}
					?>
					<tr>
						<td style="height:20px"></td>
				   </tr>
					
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td class="style_td_tot"><span lang="en">TOTAL: </span></td>
						<td class="style_td_tot"><?php echo "৳ ".$total;?></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td class="style_td_tot"><span lang="en">Delivery Charge:</span></td>
						<td>
                        <select name="" onchange="myFunctiondelivery(event)">
                        <option value="60" lang="en">Inside Dhaka</option>
                        <option value="120" lang="en">Outside Dhaka</option>
                        </select>

					</td>
					
						<td class="style_td_tot"><form action="order-confirm.php" method="POST">
							৳<input readonly id ="myText" type="text" value="60" name="deliverycharge" style=" border:none; background: white; width:50px;"></td>
					</tr>
					<tr>
						<td style="height:10px"></td>
					</tr>
					<tr>
						<td></td>
						<td colspan="5" style="border-top:2px solid #aaa;"></td>
						<td></td>
					</tr>
					<tr>
						<td style="height:10px"></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td> </td>
						<td style="text-align:center;">
							<button type="submit" name="submitdelivery" class="btn_style" lang="en" style="cursor: pointer;">PROCESS TO ORDER</button>
						</form> 
							<!--<button onclick="window.location.assign('order-confirm.php');" class="btn_style">PROCESS TO ORDER</button> --></td>
					</tr>
				</table>
				</div>
					
            <script type="text/javascript">

			function myFunctiondelivery(e) {

    document.getElementById("myText").value = e.target.value
}

			</script>

				<?php
					}else{
						if(isset($_REQUEST['massage'])){
							echo "<center><font size='+2' lang='en'>Your Order send successfully!</font></center>";
						}
						else{
							echo "<center><font size='+2' lang='en'>Your Cart is empty!</font></center>";
						}
					}
				?>
			</div><!--change end-->
		</div>
		<style type="text/css">
			.continue_shop{
				width: 210px;
				border: 2px solid #aaa;
				height: 35px;
				background:none;
			}
			.continue_shop:hover{
				background:#aaa;
				color:#fff;
			}
		</style>
      	
<?php

	include_once "footer.php";
?>