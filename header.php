<?php
	session_start();
	require_once "config.php";
	require_once "config_pos.php";
?>
<!DOCTYPE html>
<html lang="en-US">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<?php
	include_once "head.php";
?>

<body class="home page-template-default page page-id-143 full blog-78  yith-wcan-free wpb-js-composer js-comp-ver-5.4.5 vc_responsive">

<div class="page-wrapper">

    <div class="header-wrapper clearfix"><!-- header wrapper -->
                                
                    <header id="header" class="header-separate header-1  sticky-menu-header">
<?php
	include_once "header_top_menu.php";
?>
   


 <div class="header-main">
        <div class="container">
            <div class="header-left">
				<?php
					$sql_logo = "select * from logo";
					$result_logo = $db->query($sql_logo);
					$data_logo = mysqli_fetch_array($result_logo);
				?>
                <h1 class="logo"><a href="index.php" title="<?=$data['title'];?>" rel="home">
                <img class="img-responsive standard-logo" src="<?=$data_logo['logo_path'];?>" alt="<?=$data['title'];?>" /><img class="img-responsive retina-logo" src="<?=$data_logo['logo_path'];?>" alt="<?=$data['title'];?>" style="display:none;" /> </a>
    </h1>            </div>
            <div class="header-center">
                <a class="mobile-toggle"><i class="fa fa-reorder"></i></a>
                    <div class="searchform-popup">
        <a class="search-toggle"><i class="fa fa-search"></i></a>
     <form action="search_category_product.php" method="post"
        class="searchform searchform-cats">
        <fieldset>
            <span class="text"><input name="type_search_category" id="type_search_category" type="text" value="" placeholder="Search&hellip;" autocomplete="off"/></span>
            <input type="hidden" name="post_type" value="product"/>
			<select  name='default_search_category' id='product_cat' class='cat'>
			<option value='0' selected lang="en">All Categories</option>
			<?php
				$sql_category = "select * from 0_stock_category";
				$result_category = $db2->query($sql_category);
				while($data_category = mysqli_fetch_array($result_category)){
			?>
				<option class="level-0" value="<?=$data_category[0];?>"><?=$data_category[1];?></option>
			<?php
				}
			?>
			</select>
            <span class="button-wrap"><button class="btn btn-special" title="Search" type="submit"><i class="fa fa-search"></i></button></span>
        </fieldset>
    </form>
        </div>
                </div>
            <div class="header-right">
                                <div class="header-minicart-arrow">
                           <div style="border: solid 1px #ffdc52; background: #ffdc52; border-radius: 5px; " id="mini-cart" class="mini-cart minicart-arrow effect-fadein-up minicart-style2" >
            <div id="show_add_cart" >
				<div class="cart-head cart-head4" >
					<a href="shop_cart_box.php"><img style="width: 48px;height: 30px; " src="icon/cart.png"/></a>
					<?php
						if(isset($_SESSION['shoping_cart'])){
							$count_shoping_cart = count($_SESSION['shoping_cart']);
						}
						else{
							$count_shoping_cart = 0;
						}
					?>
				 <span class="cart-items" lang="en" style="background: #4e7df1;"><?=$count_shoping_cart;?></span>
				</div>
				<div class="cart-popup widget_shopping_cart">
					<div class="widget_shopping_cart_content">
						<div class="cart">
							<table border="0" cellpadding="5px" cellspacing="0px" style="margin:0px auto;text-align:center">
							<?php
							$total = 0;
							if(!empty($_SESSION['shoping_cart'])){
							?>
							<thead>
							<tr>
							<th></th>
							<th><span lang="en">Name</span></th>
							<th><span lang="en">Qty</span></th>
							<th><span lang="en">Price</span></th>
							<th><i class="fa fa-trash"></i></th>
							</tr>
							</thead>
							<?php
							
							foreach($_SESSION['shoping_cart'] as $keys => $values)
							{
							?>
							<tr>
							<td><img style="width: 500px;height: 50px;" src="<?php echo $product_images_url.$values['item_id'].".jpg";?>" /></td>
							<td><?php echo $values['item_name'];?></td>
							<td><?php echo $values['item_qty'];?></td>
							<td>$<?php echo $values['item_price'];?></td>
							<td><button style="background:none;border:none;cursor:pointer" onclick="remove_product(this.value);" value="<?php echo $values['item_id']?>" ><i class="fa fa-remove" style="font-size:16px;color:red"></i></button></td>
							</tr>
							<?php
							}
							?>
								<tr>
									<td colspan="5" style="text-align:right;"><a style="background: #c3c111;color: azure;padding: 10px;" href="shop_cart_box.php"><span lang="en">Check Out</span></a></td>
								</tr>
							<?php
							}
							else{
							?>
							<tr>
							<td><span lang="en">Your Cart is empty!</span></td>
							</tr>
							<?php
							}
							?>
							</table>
						</div>
					</div>
				</div>
			</div>
        </div>
                    </div>

                
            </div>
        </div>
            </div>
          

           <div id="navbarr">
            <div id="main-menu" class="row hiddenbar" >
                         

	<?php
		include_once ("allmenu.php");
	?>
	
</div>
</div>

</header>
</div><!-- end header wrapper -->
<div class="container">
<section class="dropdown-content1 " id="myDropdown1">


<div class="wpb_wrapper vc_column-inner">

<div class="sidebar-content">
<div class="fixed d">
	<?php
	include("sidebarmenusticky.php");
	?>
</div>
<div class="fixed1 e">
	<?php
	include("sidebarmenusticky.php");
	?>
</div>
</div>
</div>


</section>

</div>
