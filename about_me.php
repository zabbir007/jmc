
					
					<?php include_once("header.php");?>
                                 <!-- end header wrapper -->
        
        
                <section class="page-top page-header-6">
        <div class="container hide-title">
    <div class="row">
        <div class="col-lg-12 clearfix">
            <div class="pt-right d-none">
                <h1 class="page-title">About Me</h1>
                            </div>
                            <div class="breadcrumbs-wrap pt-left">
                    <ul class="breadcrumb"><li class="home"><a itemprop="url" href="#" title="Go to Home Page"><span itemprop="title">Home</span></a><i class="delimiter delimiter-2"></i></li><li>About Me</li></ul>                </div>
                                </div>
    </div>
</div>    </section>
    
        <div id="main" class="column1 boxed"><!-- main -->

            
                        <div class="container">
                            
            
            <div class="row main-content-wrap">

            <!-- main content -->
            <div class="main-content col-lg-12">

                            
    <div id="content" role="main">
                
            <article class="post-60 page type-page status-publish hentry">
                
                <span class="entry-title" style="display: none;">About Me</span><span class="vcard" style="display: none;"><span class="fn"><a href="author/porto_admin/index.php" title="Posts by Joe Doe" rel="author">Joe Doe</a></span></span><span class="updated" style="display:none">2016-06-14T01:43:48+00:00</span>
                <div class="page-content">
                    <div class="vc_row wpb_row row"><div class="vc_column_container col-md-4"><div class="wpb_wrapper vc_column-inner"><div class="porto-carousel owl-carousel " data-plugin-options="{&quot;stagePadding&quot;:0,&quot;margin&quot;:10,&quot;autoplay&quot;:&quot;yes&quot;,&quot;autoplayTimeout&quot;:3000,&quot;autoplayHoverPause&quot;:false,&quot;items&quot;:1,&quot;lg&quot;:1,&quot;md&quot;:1,&quot;sm&quot;:1,&quot;xs&quot;:1,&quot;nav&quot;:false,&quot;dots&quot;:&quot;yes&quot;,&quot;animateIn&quot;:&quot;&quot;,&quot;animateOut&quot;:&quot;fadeOut&quot;,&quot;loop&quot;:&quot;yes&quot;,&quot;center&quot;:false,&quot;video&quot;:false,&quot;lazyLoad&quot;:false,&quot;fullscreen&quot;:false}">
	<div class="wpb_single_image wpb_content_element vc_align_center">
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper vc_box_outline  vc_box_border_grey"><img width="585" height="585" src="wp-content/uploads/sites/78/2016/06/team-3.jpg" class="vc_single_image-img attachment-full" alt="" srcset="" sizes="(max-width: 585px) 100vw, 585px" /></div>
		</div>
	</div>

	<div class="wpb_single_image wpb_content_element vc_align_center">
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper vc_box_outline  vc_box_border_grey"><img width="585" height="585" src="wp-content/uploads/sites/78/2016/06/team-9.jpg" class="vc_single_image-img attachment-full" alt="" srcset="" sizes="(max-width: 585px) 100vw, 585px" /></div>
		</div>
	</div>
</div></div></div><div class="vc_column_container col-md-8"><div class="wpb_wrapper vc_column-inner"><h2  class="vc_custom_heading m-b-none align-left">Joe <strong>Doe</strong></h2><h4  class="vc_custom_heading align-left heading-primary">Web Designer</h4><div class="porto-separator  "><hr class="separator-line  align_center solid" style="background-color:#dbdbdb;"></div>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc <a href="#">vehicula</a> lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet. In eu justo a felis faucibus ornare vel id metus. Vestibulum ante ipsum primis in faucibus.</p>
<ul class="list list-icons">
<li><i class="fa fa-check"></i> Fusce sit amet orci quis arcu vestibulum vestibulum sed ut felis.</li>
<li><i class="fa fa-check"></i> Phasellus in risus quis lectus iaculis vulputate id quis nisl.</li>
<li><i class="fa fa-check"></i> Iaculis vulputate id quis nisl.</li>
</ul>

		</div>
	</div>
</div></div></div><div class="vc_row wpb_row row p-b-lg"><div class="vc_column_container col-md-12"><div class="wpb_wrapper vc_column-inner"><div class="porto-separator tall "><hr class="separator-line  align_center" style="background-image: -webkit-linear-gradient(left, transparent, #dbdbdb, transparent); background-image: linear-gradient(to right, transparent, #dbdbdb, transparent);"></div><div class="vc_row wpb_row vc_inner row"><div class="vc_column_container col-md-6 col-lg-3"><div class="wpb_wrapper vc_column-inner"><div class= "circular-bar center "><div class="circular-bar-chart" data-percent="75" data-plugin-options="{&quot;trackColor&quot;:&quot;#eeeeee&quot;,&quot;barColor&quot;:&quot;#e36159&quot;,&quot;scaleColor&quot;:&quot;&quot;,&quot;lineCap&quot;:&quot;round&quot;,&quot;lineWidth&quot;:&quot;14&quot;,&quot;size&quot;:&quot;175&quot;,&quot;animate&quot;:{&quot;duration&quot;:&quot;2500&quot;},&quot;labelValue&quot;:&quot;&quot;}" style="height:175px"><strong>HTML/CSS</strong><label><span class="percent">0</span>%</label></div></div></div></div><div class="vc_column_container col-md-6 col-lg-3"><div class="wpb_wrapper vc_column-inner"><div class= "circular-bar center "><div class="circular-bar-chart" data-percent="85" data-plugin-options="{&quot;trackColor&quot;:&quot;#eeeeee&quot;,&quot;barColor&quot;:&quot;#0088cc&quot;,&quot;scaleColor&quot;:&quot;&quot;,&quot;lineCap&quot;:&quot;round&quot;,&quot;lineWidth&quot;:&quot;14&quot;,&quot;size&quot;:&quot;175&quot;,&quot;animate&quot;:{&quot;duration&quot;:&quot;2500&quot;},&quot;labelValue&quot;:&quot;&quot;}" style="height:175px"><strong>Design</strong><label><span class="percent">0</span>%</label></div></div></div></div><div class="vc_column_container col-md-6 col-lg-3"><div class="wpb_wrapper vc_column-inner"><div class= "circular-bar center "><div class="circular-bar-chart" data-percent="60" data-plugin-options="{&quot;trackColor&quot;:&quot;#eeeeee&quot;,&quot;barColor&quot;:&quot;#2baab1&quot;,&quot;scaleColor&quot;:&quot;&quot;,&quot;lineCap&quot;:&quot;round&quot;,&quot;lineWidth&quot;:&quot;14&quot;,&quot;size&quot;:&quot;175&quot;,&quot;animate&quot;:{&quot;duration&quot;:&quot;2500&quot;},&quot;labelValue&quot;:&quot;&quot;}" style="height:175px"><strong>Wordpress</strong><label><span class="percent">0</span>%</label></div></div></div></div><div class="vc_column_container col-md-6 col-lg-3"><div class="wpb_wrapper vc_column-inner"><div class= "circular-bar center "><div class="circular-bar-chart" data-percent="95" data-plugin-options="{&quot;trackColor&quot;:&quot;#eeeeee&quot;,&quot;barColor&quot;:&quot;#734ba9&quot;,&quot;scaleColor&quot;:&quot;&quot;,&quot;lineCap&quot;:&quot;round&quot;,&quot;lineWidth&quot;:&quot;14&quot;,&quot;size&quot;:&quot;175&quot;,&quot;animate&quot;:{&quot;duration&quot;:&quot;2500&quot;},&quot;labelValue&quot;:&quot;&quot;}" style="height:175px"><strong>Photoshop</strong><label><span class="percent">0</span>%</label></div></div></div></div></div></div></div></div><div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-parallax="3" data-vc-parallax-image="#/wp-content/uploads/sites/78/2016/06/parallax-transparent.jpg" class="vc_row wpb_row row p-t-xxl p-b-lg vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving"><div class="vc_column_container col-md-12"><div class="wpb_wrapper vc_column-inner"><div class="porto-carousel owl-carousel " data-plugin-options="{&quot;stagePadding&quot;:0,&quot;margin&quot;:10,&quot;autoplay&quot;:&quot;yes&quot;,&quot;autoplayTimeout&quot;:3000,&quot;autoplayHoverPause&quot;:false,&quot;items&quot;:1,&quot;lg&quot;:1,&quot;md&quot;:1,&quot;sm&quot;:1,&quot;xs&quot;:1,&quot;nav&quot;:false,&quot;dots&quot;:&quot;yes&quot;,&quot;animateIn&quot;:&quot;&quot;,&quot;animateOut&quot;:&quot;&quot;,&quot;loop&quot;:&quot;yes&quot;,&quot;center&quot;:false,&quot;video&quot;:false,&quot;lazyLoad&quot;:false,&quot;fullscreen&quot;:false}"><div class="porto-testimonial wpb_content_element "><div class="testimonial testimonial-style-6 testimonial-with-quotes"><blockquote><p>Joe Doe is the smartest guy I ever met, he provides great tech service for each template and allows me to become more knowledgeable as a designer.</p></blockquote><div class="testimonial-author"><p><strong>Joseph Doe</strong><span>CEO &amp; Founder - Okler</span></p></div></div></div><div class="porto-testimonial wpb_content_element "><div class="testimonial testimonial-style-6 testimonial-with-quotes"><blockquote><p>He provides great tech service for each template and allows me to become more knowledgeable as a designer.</p></blockquote><div class="testimonial-author"><p><strong>Mark Doe</strong><span>CEO &amp; Founder - Okler</span></p></div></div></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div class="vc_row wpb_row row p-t-xxl p-b-lg"><div class="vc_column_container col-md-12"><div class="wpb_wrapper vc_column-inner"><h4  class="vc_custom_heading m-b-none align-left">MY <strong>WORK</strong></h4>
	<div class="wpb_text_column wpb_content_element  m-b" >
		<div class="wpb_wrapper">
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

		</div>
	</div>
<div class="vc_row wpb_row vc_inner row"><div class="vc_column_container col-md-6 col-lg-3"><div class="wpb_wrapper vc_column-inner"><div class="porto-image-frame "><span class="thumb-info thumb-info-lighten"><a href="#" title="" target="_self"><span class="thumb-info-wrapper"><img alt="" src="wp-content/uploads/sites/78/2016/06/project.jpg" class="img-responsive"><span class="thumb-info-title"><span class="thumb-info-inner">Presentation<em></em></span><span class="thumb-info-type">Brand</span></span><span class="thumb-info-action"><span class="thumb-info-action-icon"><i class="fa fa-link"></i></span></span></span></a></span></div></div></div><div class="vc_column_container col-md-6 col-lg-3"><div class="wpb_wrapper vc_column-inner"><div class="porto-image-frame "><span class="thumb-info thumb-info-lighten"><a href="#" title="" target="_self"><span class="thumb-info-wrapper"><img alt="" src="wp-content/uploads/sites/78/2016/06/project-2.jpg" class="img-responsive"><span class="thumb-info-title"><span class="thumb-info-inner">Identity<em></em></span><span class="thumb-info-type">Logo</span></span><span class="thumb-info-action"><span class="thumb-info-action-icon"><i class="fa fa-link"></i></span></span></span></a></span></div></div></div><div class="vc_column_container col-md-6 col-lg-3"><div class="wpb_wrapper vc_column-inner"><div class="porto-image-frame "><span class="thumb-info thumb-info-lighten"><a href="#" title="" target="_self"><span class="thumb-info-wrapper"><img alt="" src="wp-content/uploads/sites/78/2016/06/project-3.jpg" class="img-responsive"><span class="thumb-info-title"><span class="thumb-info-inner">Watch Mockup<em></em></span><span class="thumb-info-type">Brand</span></span><span class="thumb-info-action"><span class="thumb-info-action-icon"><i class="fa fa-link"></i></span></span></span></a></span></div></div></div><div class="vc_column_container col-md-6 col-lg-3"><div class="wpb_wrapper vc_column-inner"><div class="porto-image-frame "><span class="thumb-info thumb-info-lighten"><a href="#" title="" target="_self"><span class="thumb-info-wrapper"><img alt="" src="wp-content/uploads/sites/78/2016/06/project-4.jpg" class="img-responsive"><span class="thumb-info-title"><span class="thumb-info-inner">Three Bottles<em></em></span><span class="thumb-info-type">Logo</span></span><span class="thumb-info-action"><span class="thumb-info-action-icon"><i class="fa fa-link"></i></span></span></span></a></span></div></div></div></div></div></div></div>
                </div>
            </article>

            <div class="">
            
                        </div>

        
    </div>

        

</div><!-- end main content -->


    </div>
    </div>


        
            
            </div><!-- end main -->

            
    	   <?php include_once("footer.php");?>
		   
		   ><!-- WP Super Cache is installed but broken. The constant WPCACHEHOME must be set in the file wp-config.php and point at the WP Super Cache plugin directory. -->