<?php

include('admincon.php');
if (empty($_SESSION['inthemainathorityaccessadmincontrolifthatsare'])) { ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div style="max-width: 70%; min-width: 200px; min-height: 300px; background: white; border-radius: 8px; margin: 0 auto;">
    <p style="text-align: center; color: red;"><i class="fa fa-warning"></i> Unauthorized access request.</p>
</div>
<?php }
else {

include('header.php'); ?>

<body>

    <div id="wrapper">

        <?php //sidebar added here 
         include('sidebar.php');
        ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Order Panel</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
          
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php $sqlpending = "SELECT * FROM order_list WHERE order_status = '0'";
             $sqlexecute = mysqli_query($db, $sqlpending);
            echo  mysqli_num_rows($sqlexecute); ?></div>
                                    <div>Total Pending Order</div>
                                </div>
                            </div>
                        </div>
                        <a href="show_order.php?pendingordelist=">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
               
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-basket fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php $sqlpending = "SELECT * FROM order_list WHERE order_status = '1'";
             $sqlexecute = mysqli_query($db, $sqlpending);
            echo  mysqli_num_rows($sqlexecute);
                                     ?></div>
                                    <div>Total Process Order</div>
                                </div>
                            </div>
                        </div>
                        <a href="show_order.php?processorder=">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
           
             <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-check fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php $sqlpending = "SELECT * FROM order_list WHERE order_status = '2'";
             $sqlexecute = mysqli_query($db, $sqlpending);
            echo  mysqli_num_rows($sqlexecute); ?></div>
                                    <div>Total Completed Order</div>
                                </div>
                            </div>
                        </div>
                        <a href="show_order.php?completeordelist=">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-close fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php $sqlpending = "SELECT * FROM order_list WHERE order_status = '3'";
             $sqlexecute = mysqli_query($db, $sqlpending);
            echo  mysqli_num_rows($sqlexecute); ?></div>
                                    <div>Total Canceled Order</div>
                                </div>
                            </div>
                        </div>
                        <a href="show_order.php?cancelordelist=">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
         </div>
            <!-- /.row -->
            <div class="row">
            	<?php
	if (isset($_GET['pendingordelist'])) { ?>
	
<?php
	$sqlpending = "SELECT * FROM order_list WHERE order_status = '0' ORDER BY id DESC";
	$sqlexecute = mysqli_query($db, $sqlpending); 
   if (mysqli_num_rows($sqlexecute) == 0) {
   	echo "<center><h3>There are no pending order</h3></center>";
   } else{
	?>
		<center><h2 style="text-decoration: underline;">Show Pending Order</h2></center>
	 <center>
	<div style="padding-bottom: 20px; padding-top: 20px;">
	<form action="search.php" method="GET">
		<input type="text" name="searchtext" placeholder="Enter Customer Name" style="height: 35px; width: 250px; border-radius: 8px 0px 0px 8px; border:1px solid #2980B9; padding: 5px; font-size:16px;"><input type="submit" name="searchpendingorder" value="Search" style="height: 35px; width:70px; border-radius: 0px 8px 8px 0px; border:1px solid #2980B9; padding: 5px; font-size:16px;">
	</form>
	</div>
</center>
	<center>
		<table class="table table-striped">
			<thead>
				<tr>
					<th style="color: red;">
						Order Number
					</th>
					<th style="color: red;">
						Customer Email
					</th>
					<th style="color: red;">
						Customer name
					</th>
					<th style="color: red;">
						Order details
					</th>
					<th style="color: red;">
						Confirmation
					</th>
					<th style="color: red;">
						Cancelation
					</th>
				</tr>
				</thead>
	<?php while ($disc = mysqli_fetch_array($sqlexecute)) { 
           
		?>


	  <tbody>
				<tr>
					
					<td>
						<?php echo $disc['order_no'];
						?>
					</td>
					<td>
						<?php echo $disc['email'];
						?>
					</td>
					<td>
						
						<?php echo $disc['user_name'];
						?>
					
					</td>
					<td>
						<form action="show_order.php" method="GET">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
							<button type="submit" name="orderdetails"><i class="fa fa-id-card-o"></i> Order Deatils</button>
						</form>
					</td>
					<td>
						<form action="show_order.php?pendingordelist=" method="POST">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
				       <button type="submit" name="confirmorder" style="color:#186A3B;"><i class="fa fa-check-circle"></i> Confirm Order</button>
						</form>
					</td>
					<td>
						<form action="show_order.php?pendingordelist=" method="POST">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
				       <button type="submit" name="cancelorder" style="color:#E74C3C;"><i class="fa fa-close"></i> Cancel Order</button>
						</form>
					</td>
					
				</tr>
			</tbody>
		

<?php		
	} ?>
	</table>
</center>
<?php 
}
}
if (isset($_GET['orderdetails'])) {
	$deate = mysqli_real_escape_string($db, $_GET['detailsorder']);

    $sqlpender = "SELECT * FROM order_list WHERE order_no = '$deate'";
      $sqlex = mysqli_query($db, $sqlpender); 

       while ($rows = mysqli_fetch_array($sqlex)){
      ?>

        <div class="invoice" id="invoiceprint">
        	<div class="invoicebutton">
        		<?php if($rows['order_status'] == 0){?>
        	<form action="show_order.php?pendingordelist=" method="POST">
							<input type="hidden" name="detailsorder" value="<?php echo $rows['order_no']; ?>">
				       <button type="submit" name="confirmorder" style="color:#186A3B;"><i class="fa fa-check-circle"></i> Confirm Order</button>
						</form>
				<br>
						<form action="show_order.php?pendingordelist=" method="POST">
							<input type="hidden" name="detailsorder" value="<?php echo $rows['order_no']; ?>">
				       <button type="submit" name="cancelorder" style="color:#E74C3C;"><i class="fa fa-close"></i> Cancel Order</button>
						</form>
				<?php	} ?>
			</div>
					
     <h1>babyandmom.com</h1>
     <h4>Invoice No.#<?php echo $rows['id']; ?> </h4>
     <h4>Order No.#<?php echo $rows['order_no']; ?> </h4>
     <h4>Custmer Name:<span> <?php echo $rows['user_name']; ?></span></h4>
     <?php $sqluser = "SELECT * FROM user_id WHERE email = '".$rows['email']."'";
     $sqlid = mysqli_query($db, $sqluser);
     while($display = mysqli_fetch_array($sqlid)){ ?>
     <h4>Delivery Address:<span> <?php echo $display['last_name']; ?></span></h4>
     <h4>Delivery Contact No.#<span> <?php echo $display['desc2']; ?></span></h4>
    <?php } ?>
    <h4>Order Time:<span> <?php echo $rows['order_time']; ?> </span></h4>
    <h4>Preferred Delivery Time:<span> <?php echo $rows['delivery_time']; ?> </span></h4>
    <?php $sqluser = "SELECT * FROM user_id WHERE email = '".$rows['email']."'";
     $sqlid = mysqli_query($db, $sqluser);
     while($display1 = mysqli_fetch_array($sqlid)){ ?>
     <h4>Payment Method:<span> <?php echo $display1['desc4']; ?> </span></h4>

 <?php } ?>
     <h4>Your Order List&nbsp;&nbsp;<i class="fa fa-chevron-down"></i></h4>
      <div>
         <table class="table table-striped">
         	<tr>
         		<th>Product Name</th>
         		<th>Qty</th>
         		<th>Sub Total</th>
         	</tr>
         	<?php $sqlcol = "SELECT * FROM product_order WHERE order_number = '".$rows['order_no']."'";
         	$resulting = mysqli_query($db, $sqlcol); 
             $total = 0;  
         	?>
         
         <?php	while ($dispaly2 = mysqli_fetch_array($resulting)) { 
          $subtotal = $dispaly2['qty']*$dispaly2['price'];
         	?>
         	        	<tr>
         			<td><strong style="color: black;"><?php echo $dispaly2['product_name']; ?></strong></td>
         			<td><strong style="color: black;"><?php echo $dispaly2['qty']; ?></td></strong>
         			<td><strong style="color: black;"><?php echo $subtotal; ?></strong></td>
         		</tr>
         
         
      <?php

         
      	$total += $subtotal;

      } ?>

         </table>
         </div>
         <h4 style="text-align: right; padding-right: 15px;">Total:&nbsp;&nbsp;Tk.&nbsp;<?php echo $total; ?>&nbsp;/-</h4>
         <h4 style="text-align: right; padding-right: 15px;">Delivery Charge:&nbsp;&nbsp;Tk.&nbsp;<?php $deliverychrg = $rows['delivery_charge'];  echo $rows['delivery_charge']; ?>&nbsp;/-</h4>
      	<h4 style="text-align: right; padding-right: 15px;">Grand Total:&nbsp;&nbsp;Tk.&nbsp;<?php $total+=$deliverychrg; echo $total; ?>&nbsp;/-</h4>

						
         <!--
         <h4 style="text-align: right; padding-right: 15px;">Delivery Charge:&nbsp;&nbsp;Tk.&nbsp;<?php // echo $rows['deliverycharge']; ?>&nbsp;/-</h4>
         <h4 style="text-align: right; padding-right: 15px;">Grand Total:&nbsp;&nbsp;Tk.&nbsp;<?php // $totalall =  $rows['deliverycharge'] + $rows['totaltk']; echo $totalall; ?>&nbsp;/-</h4> -->
      
         

          

       
     <!--  <button onclick="printContent('invoiceprint')" style="float: right; margin-right: 15px;"><i class="fa fa-print"></i>Print Invoice</button>
       
    </div>
-->
     </div>
     <?php
   } 
} ?>
<?php
//Process Order Portion
	if (isset($_GET['processorder'])) { ?>
	
		<?php
	$sqlpending = "SELECT * FROM order_list WHERE order_status = '1' OR payment_status = '1' ORDER BY id DESC";
	$sqlexecute = mysqli_query($db, $sqlpending); 
   if (mysqli_num_rows($sqlexecute) == 0) {
   	echo "<center><h3>There are no process order</h3></center>";
   } else{
	?>
		<center><h2 style="text-decoration: underline;">Show Process Order</h2></center>
	<center>
	<div style="padding-bottom: 20px; padding-top: 20px;">
	<form action="search.php" method="GET">
	<input type="text" name="searchtext" placeholder="Enter Customer Name" style="height: 35px; width: 250px; border-radius: 8px 0px 0px 8px; border:1px solid #2980B9; padding: 5px; font-size:16px;"><input type="submit" name="searchprocessorder" value="Search" style="height: 35px; width:70px; border-radius: 0px 8px 8px 0px; border:1px solid #2980B9; padding: 5px; font-size:16px;">
	</form>
	</div>
</center>
	<center>
		<table class="table table-striped">
			<thead>
				<tr>
					<th style="color: red;">
						Order Number
					</th>
					<th style="color: red;">
						Customer Email
					</th>
					<th style="color: red;">
						Customer name
					</th>
					<th style="color: red;">
						Order details
					</th>
					<th style="color: red;">
						Shiping Status
					</th>
					<th style="color: red;">
						Payment Status
					</th>
				</tr>
				</thead>
	<?php while ($disc = mysqli_fetch_array($sqlexecute)) { 
           
		?>


	  <tbody>
				<tr>
					
					<td>
						<?php echo $disc['order_no'];
						?>
					</td>
					<td>
						<?php echo $disc['email'];
						?>
					</td>
					<td>
						
						<?php echo $disc['user_name'];
						?>
					
					</td>
					<td>
						<form action="show_order.php" method="GET">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
							<button type="submit"  name="orderdetails"><i class="fa fa-id-card-o"></i> Order Deatils</button>
						</form>
					</td>
					<td><?php if($disc['shipping_status'] == 0) {
                              echo "Shipment not ready"; ?>
						<form action="show_order.php?processorder=" method="POST">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
				       <button type="submit" name="readytoship" style="color:#186A3B;"><i class="fa fa-check-circle"></i> Ready to shipment</button>
						</form>

						<?php	} elseif($disc['shipping_status'] == 1){
                            echo "Ready to Shipment"; ?>
                            <form action="show_order.php?processorder=" method="POST">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
				       <button type="submit" name="readytodeliver" style="color:#186A3B;"><i class="fa fa-check-circle"></i> Mark Delivery Complete</button>
						</form>
<?php
						}elseif($disc['shipping_status'] == 2){
                         echo "Delivery Complete";
						}

						?>
					</td>
					<td>
						<?php if($disc['payment_status'] == 1) {
                              echo "Payment Pending"; ?>
						<form action="show_order.php?processorder=" method="POST">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
				       <button type="submit" name="paymentsuccess" style="color:#186A3B;"><i class="fa fa-check-circle"></i> Payment Success</button>
						</form>

						<?php } elseif($disc['payment_status'] == 2){
                            echo "Payment Success";
						} ?>
					</td>
					
				</tr>
			</tbody>
		

<?php		
	} ?>
	</table>
</center>
<?php 
} 

}//ending process order
	if (isset($_GET['cancelordelist'])) { ?>
	
<?php
	$sqlpending = "SELECT * FROM order_list WHERE order_status = '3' ORDER BY id DESC";
	$sqlexecute = mysqli_query($db, $sqlpending); 
   if (mysqli_num_rows($sqlexecute) == 0) {
   	echo "<center><h3>There are no canceled order</h3></center>";
   } else{
	?>
	<center><h2 style="text-decoration: underline;">Show Canceled Order</h2></center>
	 <center>
	<div style="padding-bottom: 20px; padding-top: 20px;">
	<form action="search.php" method="GET">
		<input type="text" name="searchtext" placeholder="Enter Customer Name" style="height: 35px; width: 250px; border-radius: 8px 0px 0px 8px; border:1px solid #2980B9; padding: 5px; font-size:16px;"><input type="submit" name="searchcancelorder" value="Search" style="height: 35px; width:70px; border-radius: 0px 8px 8px 0px; border:1px solid #2980B9; padding: 5px; font-size:16px;">
	</form>
	</div>
</center>
	<center>
		<table class="table table-striped">
			<thead>
				<tr>
					<th style="color: red;">
						Order Number
					</th>
					<th style="color: red;">
						Customer Email
					</th>
					<th style="color: red;">
						Customer name
					</th>
					<th style="color: red;">
						Order details
					</th>
					<th style="color: red;">
						Delete
					</th>
				</tr>
				</thead>
	<?php while ($disc = mysqli_fetch_array($sqlexecute)) { 
           
		?>


	  <tbody>
				<tr>
					
					<td>
						<?php echo $disc['order_no'];
						?>
					</td>
					<td>
						<?php echo $disc['email'];
						?>
					</td>
					<td>
						
						<?php echo $disc['user_name'];
						?>
					
					</td>
					<td>
						<form action="show_order.php" method="GET">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
							<button type="submit" name="orderdetails"><i class="fa fa-id-card-o"></i> Order Deatils</button>
						</form>
					</td>
					<td>
						<form action="show_order.php?cancelordelist=" method="POST">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
				       <button type="submit" name="delete_order" style="color:#E74C3C;"><i class="fa fa-close"></i> Delete Order</button>
						</form>
					</td>					
				</tr>
			</tbody>
		

<?php		
	} ?>
	</table>
</center>
<?php 
} 

}//Complete order
	if (isset($_GET['completeordelist'])) { ?>
	
<?php
	$sqlpending = "SELECT * FROM order_list WHERE shipping_status = '2' AND payment_status = '2' ORDER BY id DESC";
	$sqlexecute = mysqli_query($db, $sqlpending);
    if (mysqli_num_rows($sqlexecute) == 0) {
   	echo "<center><h3>There are no completed order</h3></center>";
   } else{
	?>
	<center><h2 style="text-decoration: underline;">Show Completed Order</h2></center>
	 <center>
	<div style="padding-bottom: 20px; padding-top: 20px;">
	<form action="search.php" method="GET">
		<input type="text" name="searchtext" placeholder="Enter Customer Name" style="height: 35px; width: 250px; border-radius: 8px 0px 0px 8px; border:1px solid #2980B9; padding: 5px; font-size:16px;"><input type="submit" name="searchcompleteorder" value="Search" style="height: 35px; width:70px; border-radius: 0px 8px 8px 0px; border:1px solid #2980B9; padding: 5px; font-size:16px;">
	</form>
	</div>
</center>
	<center>
		<table class="table table-striped">
			<thead>
				<tr>
					<th style="color: red;">
						Order Number
					</th>
					<th style="color: red;">
						Customer Email
					</th>
					<th style="color: red;">
						Customer name
					</th>
					<th style="color: red;">
						Order details
					</th>
				</tr>
				</thead>
	<?php while ($disc = mysqli_fetch_array($sqlexecute)) { 
           
		?>


	  <tbody>
				<tr>
					
					<td>
						<?php echo $disc['order_no'];
						?>
					</td>
					<td>
						<?php echo $disc['email'];
						?>
					</td>
					<td>
						
						<?php echo $disc['user_name'];
						?>
					
					</td>
					<td>
						<form action="show_order.php" method="GET">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
							<button type="submit" name="orderdetails"><i class="fa fa-id-card-o"></i> Order Deatils</button>
						</form>
					</td>				
				</tr>
			</tbody>
		

<?php		
	} ?>
	</table>
</center>
<?php 
} 

}?>
                          </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="vendor/raphael/raphael.min.js"></script>
    <script src="vendor/morrisjs/morris.min.js"></script>
    <script src="data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
<?php } ?>

