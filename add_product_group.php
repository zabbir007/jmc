<?php
include('config.php');
include('config_pos.php');

/**********************************************************************
    Copyright (C) NGICON (Next Generation icon) ERP.
    All Rights Reserved By www.ngicon.com
***********************************************************************/
//---------------------------------------------------------------------------
//
//	Entry/Modify Sales Invoice against single delivery
//	Entry/Modify Batch Sales Invoice against batch of deliveries
//
/*
$path_to_root = "..";
$page_security = 'SA_SALESORDER';
include_once($path_to_root . "/sales/includes/cart_class.inc");
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/sales/includes/sales_ui.inc");
include_once($path_to_root . "/sales/includes/ui/sales_order_ui.inc");
include_once($path_to_root . "/sales/includes/sales_db.inc");
include_once($path_to_root . "/sales/includes/db/sales_types_db.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");
include_once($path_to_root . "/gl/includes/db/gl_db_bank_trans.inc");
include_once "head.php";

set_page_security( @$_SESSION['Items']->trans_type,
	array(	ST_SALESORDER=>'SA_SALESORDER',
			ST_SALESQUOTE => 'SA_SALESQUOTE',
			ST_CUSTDELIVERY => 'SA_SALESDELIVERY',
			ST_SALESINVOICE => 'SA_SALESINVOICE'),
	array(	'NewOrder' => 'SA_SALESORDER',
			'ModifyOrderNumber' => 'SA_SALESORDER',
			'AddedID' => 'SA_SALESORDER',
			'UpdatedID' => 'SA_SALESORDER',
			'NewQuotation' => 'SA_SALESQUOTE',
			'ModifyQuotationNumber' => 'SA_SALESQUOTE',
			'NewQuoteToSalesOrder' => 'SA_SALESQUOTE',
			'AddedQU' => 'SA_SALESQUOTE',
			'UpdatedQU' => 'SA_SALESQUOTE',
			'NewDelivery' => 'SA_SALESDELIVERY',
			'AddedDN' => 'SA_SALESDELIVERY', 
			'NewInvoice' => 'SA_SALESINVOICE',
			'AddedDI' => 'SA_SALESINVOICE'
			)
);

*/

$js = '';
$use_popup_windows = "";
$use_date_picker = "";
if ($use_popup_windows) {
	$js .= get_js_open_window(900, 500);
}

if ($use_date_picker) {
	$js .= get_js_date_picker();
}

?>

<title>Products | Service Entry</title>
    <script>
        function print(id){
    w=window.open('print.php?id='+id);
        w.focus();    
      //  w.window.print();
          //  w.close();
        }
    </script>
    <?php

//page($_SESSION['Sale Summary'], false, false, "Sales Summary", $js);?>
<style>
    #addform td,#addform input{
        font-size: 20px;
        line-height: 30px;
    }
    
</style>
<center><br><br><br><br><br>
    <h1>Products Group and Brands Entry</h1>
	<form method="post" enctype="multipart/form-data">
	<table border="0px" cellpadding="10px" cellspacing="0px" style="margin:20px auto">
		<tr>
			<td>Product Group or Brands Name</td>
			<td><input style="width: 100%;height: 28px;" type="text" name="product_group_brands" id="product_group_brands" required></td>
		</tr>
		<tr>
			<td>Select Brands:</td>
			<td>
			<select id="menue_select" name="menue_select" onchange="chang_mettar();" style="width:100%">
				<option value="0" selected>Main Menu</option>
				<option value="2">Sub Menu</option>
			</select>
			</td>
		</tr>
		<tr id="dorpdown_menu" style="display:none">
		<td>Main Brands</td>
		<td><select id="dropdown_select" name="dropdown_select" style="width:100%;">
			<option value="no_select">Select Main Brands</option>
		<?php
			$sql_for_main_brands = "select * from new_tbl2 where desc2='0'";
			$result_for_main_brands = $db2->query($sql_for_main_brands);
			while($data_for_main_brands = mysqli_fetch_array($result_for_main_brands)){
			echo "<option value='$data_for_main_brands[0]'>$data_for_main_brands[1]</option>";
			}
		?>
		</select></td>
		</tr>
			<script type="text/javascript">
			function chang_mettar(){
			var menue_select = document.getElementById('menue_select').value;
			if(menue_select==2){
				document.getElementById('dorpdown_menu').style.display = "table-row";
			}
			else{
				document.getElementById('dorpdown_menu').style.display = "none";
			}
			}
			</script>
		<tr>
			<td>Select Group or Brands</td>
			<td><select style="width: 100%;height: 28px;" name="select_group_brands">
				<option value="0" selected>Product Group</option>
				<option value="1">Product Brands</option>
			</select></td>
		</tr>
		<tr>
			<td>
				Select Group Photo
			</td>
			<td>
				<input type="file" name="group_or_brand_photo" id="group_or_brand_photo"/>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center"><input style="width: 25%;height: 40px;font-size: 17px;" type="submit" value="Submit" name="submit"/></td>
		</tr>
	</table>
	</form>
</center>
<?php
	if(isset($_POST['submit'])){
		extract($_POST);
		$path = strtotime(date("Y-m-d H:i:s"));
		$up_file = "erp/company/0/images/".$path.".jpg";
		$insert_path = "images/".$path.".jpg";
		if(is_uploaded_file($_FILES['group_or_brand_photo']['tmp_name'])){
			move_uploaded_file($_FILES['group_or_brand_photo']['tmp_name'],$up_file );
		}
		if($dropdown_select=="no_select"){
			$db2->query("INSERT INTO `new_tbl2`(`id`, `group_brands`, `atribute`, `desc1`, `desc2`) VALUES ('','$product_group_brands','$select_group_brands','$insert_path','0')");
		}
		else{
			$db2->query("INSERT INTO `new_tbl2`(`id`, `group_brands`, `atribute`, `desc1`, `desc2`) VALUES ('','$product_group_brands','$select_group_brands','$insert_path','$dropdown_select')");
		}
	}
?>