-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 08, 2020 at 09:10 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jmcshop_ecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminlogin`
--

CREATE TABLE `adminlogin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `employeeid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminlogin`
--

INSERT INTO `adminlogin` (`id`, `username`, `password`, `employeeid`) VALUES
(1, 'admin', 'falcon@19', '9');

-- --------------------------------------------------------

--
-- Table structure for table `banner_ecom`
--

CREATE TABLE `banner_ecom` (
  `id` int(11) NOT NULL,
  `banner_text` text NOT NULL,
  `banner_image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner_ecom`
--

INSERT INTO `banner_ecom` (`id`, `banner_text`, `banner_image`) VALUES
(8, '<p>\r\n	<span style=\"color:#800000;\"><span style=\"font-size: 36px;\"><strong>&nbsp;&nbsp;&nbsp; $270.0</strong></span></span></p>\r\n<p>\r\n	<span style=\"color:#800000;\"><span style=\"font-size: 18px;\">&nbsp;', 'banner/1519477389.jpg'),
(9, '<p>\r\n	<span style=\"color:#ffffff;\"><span style=\"font-size: 48px;\"><strong>HUGE SALE</strong></span></span></p>\r\n<p>\r\n	<span style=\"color:#ffffff;\"><span style=\"font-size: 20px;\">NOW STARTING AT $99</s', 'banner/1519452129.jpg'),
(10, '<p>\r\n	<span style=\"color:#ffffff;\"><span style=\"font-family: arial, helvetica, sans-serif;\"><span style=\"font-size: 36px;\"><strong>BIG SALE</strong></span></span></span></p>\r\n<p>\r\n	<span style=\"color:#ffffff;\"><span style=\"font-size: 18px;\">NOW&nbsp; STARTING AT $99</span></span></p>\r\n', 'banner/1519452136.jpg'),
(11, '<p>\r\n	<span style=\"font-size:22px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; AMAZING </span></p>\r\n<p>\r\n	<span style=\"font-size:24px;\"><strong><span style=\"font-family: verdana, geneva, sans-serif;\">COLLECTION</span></strong></span></p>\r\n<p>\r\n	<span style=\"font-size:22px;\"><span style=\"font-family: courier new, courier, monospace;\">CHECK OUR DISCOUNT</span></span></p>\r\n', 'banner/1519452145.jpg'),
(12, '<p>\r\n	<span style=\"color:#a9a9a9;\"><span style=\"font-size: 22px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NEW ARRIVALS</span></span></p>\r\n<p>\r\n	<span style=\"color:#ffffff;\"><span style=\"font-size: 26px;\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; FASHION </strong></span></span></p>\r\n<p>\r\n	<span style=\"color:#808080;\"><span style=\"font-size: 18px;\">CHECK OUR DISCOUNT</span></span></p>\r\n', 'banner/1519464998.jpg'),
(13, '<p>\r\n	<span style=\"color:#a9a9a9;\"><span style=\"font-size: 26px;\"><strong><span style=\"font-family: lucida sans unicode, lucida grande, sans-serif;\">EXECUTIVE SHOES</span></strong></span></span></p>\r\n<p>\r\n	<span style=\"font-size:18px;\"><span style=\"color: rgb(128, 128, 128);\">STARTING AT</span> <strong>$99</strong></span></p>\r\n', 'banner/1519465007.jpg'),
(14, '<p>\r\n	<span style=\"font-size:18px;\">CHECK OUT NOW </span></p>\r\n<p>\r\n	<span style=\"color:#808080;\"><span style=\"font-size: 24px;\">MORE THAN</span></span></p>\r\n<p>\r\n	<span style=\"color:#808080;\"><span style=\"font-size: 24px;\">40 BRANDS</span></span></p>\r\n', 'banner/1519465018.jpg'),
(15, '<p>\r\n	<span style=\"font-size:18px;\">DSLR CAMERAS </span></p>\r\n<p>\r\n	<span style=\"font-size:18px;\">FROM </span></p>\r\n<p>\r\n	<span style=\"font-size:22px;\"><strong>$350.00</strong></span></p>\r\n', 'banner/1519465028.jpg'),
(16, '<p>\r\n	<span style=\"color:#ffffff;\"><span style=\"font-size: 20px;\">DEAL</span></span></p>\r\n<p>\r\n	<span style=\"font-size:20px;\"><strong>PROMOS</strong></span></p>\r\n<p>\r\n	<span style=\"font-size:16px;\">LIMITED SALS</span></p>\r\n', 'banner/1519465038.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `bkash`
--

CREATE TABLE `bkash` (
  `id` int(11) UNSIGNED NOT NULL,
  `bkashnumber` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bkash`
--

INSERT INTO `bkash` (`id`, `bkashnumber`) VALUES
(1, '+8801312340406');

-- --------------------------------------------------------

--
-- Table structure for table `create_order`
--

CREATE TABLE `create_order` (
  `id` int(11) NOT NULL,
  `order_no` int(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `create_order`
--

INSERT INTO `create_order` (`id`, `order_no`) VALUES
(1, 48);

-- --------------------------------------------------------

--
-- Table structure for table `flashsalestats`
--

CREATE TABLE `flashsalestats` (
  `id` int(1) NOT NULL,
  `stats` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flashsalestats`
--

INSERT INTO `flashsalestats` (`id`, `stats`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `footer`
--

CREATE TABLE `footer` (
  `id` int(11) NOT NULL,
  `footer_text` varchar(5000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer`
--

INSERT INTO `footer` (`id`, `footer_text`) VALUES
(2, 'All rights reserved by www.ngicon.com');

-- --------------------------------------------------------

--
-- Table structure for table `logo`
--

CREATE TABLE `logo` (
  `id` int(11) NOT NULL,
  `logo_path` varchar(255) NOT NULL,
  `header_contact_text` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logo`
--

INSERT INTO `logo` (`id`, `logo_path`, `header_contact_text`) VALUES
(1, 'logo/jmc.shopping logo4.png', '01312340405');

-- --------------------------------------------------------

--
-- Table structure for table `map`
--

CREATE TABLE `map` (
  `id` int(11) NOT NULL,
  `map` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `map`
--

INSERT INTO `map` (`id`, `map`) VALUES
(1, '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3651.7045529479165!2d90.43904511409956!3d23.757912884585668!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b80563e5ee67%3A0xeb502d994b9c4928!2sEastern%20Bonobithi%20Shopping%20Complex!5e0!3m2!1sen!2sbd!4v1583176545655!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>');

-- --------------------------------------------------------

--
-- Table structure for table `marchant_item`
--

CREATE TABLE `marchant_item` (
  `id` int(11) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_id` int(11) NOT NULL,
  `stock_type` varchar(255) NOT NULL,
  `offer` varchar(255) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `category` varchar(255) NOT NULL,
  `product_brand` varchar(255) NOT NULL,
  `product_type` varchar(255) NOT NULL,
  `shop_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `shop_address` varchar(255) NOT NULL,
  `main_image` varchar(255) NOT NULL,
  `alt_img` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `marchant_user`
--

CREATE TABLE `marchant_user` (
  `id` int(11) NOT NULL,
  `marchant_name` varchar(255) NOT NULL,
  `marchant_email` varchar(255) NOT NULL,
  `marchant_contact` int(11) NOT NULL,
  `marchant_shop` varchar(255) NOT NULL,
  `marchant_shop_address` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `agree_status` varchar(255) NOT NULL,
  `verify_code` int(11) NOT NULL,
  `admin_approval` int(11) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu_ecom`
--

CREATE TABLE `menu_ecom` (
  `id` int(11) NOT NULL,
  `menu_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_list`
--

CREATE TABLE `order_list` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mobile_number` varchar(100) DEFAULT NULL,
  `order_time` datetime DEFAULT NULL,
  `order_status` int(11) DEFAULT NULL,
  `shipping_status` int(11) DEFAULT NULL,
  `order_no` int(100) DEFAULT NULL,
  `delivery_time` varchar(255) NOT NULL,
  `payment_status` int(11) NOT NULL,
  `delivery_charge` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_order`
--

CREATE TABLE `product_order` (
  `id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `qty` double NOT NULL,
  `price` double NOT NULL,
  `email` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `order_status` int(11) NOT NULL,
  `stock_id` varchar(100) NOT NULL,
  `order_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shop_cart`
--

CREATE TABLE `shop_cart` (
  `id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_price` double NOT NULL,
  `discount` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `qty` int(11) NOT NULL,
  `stock_id` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `slider_ecom`
--

CREATE TABLE `slider_ecom` (
  `id` int(11) NOT NULL,
  `slider_text` text NOT NULL,
  `slider_image` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider_ecom`
--

INSERT INTO `slider_ecom` (`id`, `slider_text`, `slider_image`, `status`) VALUES
(42, 'GRAND OPENING', 'slider/9828371F-2630-478F-9EC8-4003CEBD14B3.jpeg', 1),
(43, '21', 'slider/FCB61BC8-ABA0-4F57-9A06-0747BE5EA3AA.jpeg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post`
--

CREATE TABLE `tbl_post` (
  `post_id` int(11) NOT NULL,
  `post_title` text NOT NULL,
  `post_text` text NOT NULL,
  `post_url` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_id`
--

CREATE TABLE `user_id` (
  `id` int(11) NOT NULL,
  `title` varchar(5) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile_no` varchar(15) NOT NULL,
  `delivery_time` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `desc1` varchar(50) NOT NULL,
  `desc2` varchar(50) NOT NULL,
  `desc3` varchar(50) NOT NULL,
  `desc4` varchar(50) NOT NULL,
  `id_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_id`
--

INSERT INTO `user_id` (`id`, `title`, `first_name`, `last_name`, `email`, `mobile_no`, `delivery_time`, `password`, `desc1`, `desc2`, `desc3`, `desc4`, `id_status`) VALUES
(63, 'Mr.', 'ayon', '', 'ayon7288@gmail.com', '01837227288', '', '6788a8c53dee44a8fe4afbfed8c42251', '1956', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `web_attribut_info`
--

CREATE TABLE `web_attribut_info` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` mediumtext NOT NULL,
  `icon` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_attribut_info`
--

INSERT INTO `web_attribut_info` (`id`, `title`, `mobile`, `email`, `address`, `icon`) VALUES
(1, 'jmc.shopping', '+8801312340405', 'info@jmc.shopping', '<p>\r\n	House:Dakhin Banasree, Eastern Bonobithi Shopping Complex, Dhaka.</p>\r\n<p>\r\n	&nbsp;</p>', 'icon/jmc.shopping logo4.png');

-- --------------------------------------------------------

--
-- Table structure for table `working_hour`
--

CREATE TABLE `working_hour` (
  `id` int(11) NOT NULL,
  `mon_fri` varchar(200) NOT NULL,
  `saturday` varchar(100) NOT NULL,
  `sunday` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `working_hour`
--

INSERT INTO `working_hour` (`id`, `mon_fri`, `saturday`, `sunday`) VALUES
(1, 'Tuesday To Sunday', '09.00 am To 09.00 pm', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminlogin`
--
ALTER TABLE `adminlogin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner_ecom`
--
ALTER TABLE `banner_ecom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bkash`
--
ALTER TABLE `bkash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `create_order`
--
ALTER TABLE `create_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer`
--
ALTER TABLE `footer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map`
--
ALTER TABLE `map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marchant_item`
--
ALTER TABLE `marchant_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marchant_user`
--
ALTER TABLE `marchant_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_ecom`
--
ALTER TABLE `menu_ecom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_order`
--
ALTER TABLE `product_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_cart`
--
ALTER TABLE `shop_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_ecom`
--
ALTER TABLE `slider_ecom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_post`
--
ALTER TABLE `tbl_post`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `user_id`
--
ALTER TABLE `user_id`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_attribut_info`
--
ALTER TABLE `web_attribut_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `working_hour`
--
ALTER TABLE `working_hour`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminlogin`
--
ALTER TABLE `adminlogin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banner_ecom`
--
ALTER TABLE `banner_ecom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `bkash`
--
ALTER TABLE `bkash`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `create_order`
--
ALTER TABLE `create_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `footer`
--
ALTER TABLE `footer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `logo`
--
ALTER TABLE `logo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `map`
--
ALTER TABLE `map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `marchant_item`
--
ALTER TABLE `marchant_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `marchant_user`
--
ALTER TABLE `marchant_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `menu_ecom`
--
ALTER TABLE `menu_ecom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `order_list`
--
ALTER TABLE `order_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `product_order`
--
ALTER TABLE `product_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT for table `shop_cart`
--
ALTER TABLE `shop_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `slider_ecom`
--
ALTER TABLE `slider_ecom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `tbl_post`
--
ALTER TABLE `tbl_post`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_id`
--
ALTER TABLE `user_id`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `web_attribut_info`
--
ALTER TABLE `web_attribut_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `working_hour`
--
ALTER TABLE `working_hour`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
