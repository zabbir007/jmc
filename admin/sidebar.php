 <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Admin Panel</a>
            </div>
            <!-- /.navbar-header -->
            
            <ul class="nav navbar-top-links navbar-right">

                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['inthemainathorityaccessadmincontrolifthatsare']; ?> <i class="fa fa-caret-down"></i> 
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                    
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Change Password</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="index.php?logout=1"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                      
                         <li>
                            <a href="#"><i class="fa fa-bolt"></i> Flash Sale<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                             <a href="change.php?flashsale="> Flash Sale Status</a>
                            <!-- /.nav-second-level -->
                           </li>
                                 <li>
                                    <a href="change.php?flashsaletime="> Set Time</a>
                                </li>
                             
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="add_product_group.php?add_menu="><strong style="color:orange;"><i class="fa fa-bars"></i> Create Menu  <span class="fa arrow"></span></strong></a>
                            <!-- /.nav-second-level -->
                        </li>
                          <li>
                            <a href="add-stock-img.php?viewall="><strong style="color:orange;"><i class="fa fa-list-alt"></i> Add Sub Menu Image <span class="fa arrow"></span></strong></a>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="product.php?viewallproduct="><strong style="color:orange;"><i class="fa fa-building"></i>Add Product Alt Image</strong></a>
 
                        </li>
                         <li>
                              <a href="user_info.php"><strong style="color:black;"><i class="fa fa-user"></i>  User Info <span class="fa arrow"></span></strong></a>
 
                        </li>
                         <li>
                             <a href="show_order.php"><strong style="color:black;"><i class="fa fa-list-alt"></i> Show Order <span class="fa arrow"></span></a></strong>
 
                        </li>
                         <li>
                              <a href="customer_feedback.php"><strong style="color:black;"><i class="fa fa-comments-o"></i>  Customer Feedback<span class="fa arrow"></span></strong></a>
 
                        </li>
                        <li><a href="change.php?bkashpayment="><i class="fa fa-money"></i> Change Bkash Payment Number <span class="fa arrow"></span></a></li>
 
                          <li>
                            <a href="#"><i class="fa fa-edit" aria-hidden="true"></i> Change Theme Style<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                            <a href="change.php?changelogo="><i class="fa fa-image"></i> Change Logo and Header Call Text</a>
                            <!-- /.nav-second-level -->
                        </li>
                                 <li>
                                    <a href="change.php?changetitle="><i class="fa fa-info-circle"></i> Change Title</a>
                                </li>
                                  <li>
                            <a href="add_slider.php"><i class="fa fa-file-image-o" aria-hidden="true"></i> Change Home Slider</a>
                            <!-- /.nav-second-level -->
                        </li>
                                <li>
                             <a href="change.php?changefootercontactnumber="><i class="fa fa-mobile-phone"></i> Change Footer No.</a>
                            <!-- /.nav-second-level -->
                               </li>
                                  <li>
                             <a href="change.php?changeemail="><i class="fa fa-envelope"></i> Change Email Address</a>
                            <!-- /.nav-second-level -->
                               </li>
                                 <li>
                             <a href="change.php?changefootertext="><i class="fa fa-copyright"></i> Change Footer Text</a>
  
                            <!-- /.nav-second-level -->
                               </li>
                                <li>
                            <a href="change.php?changeworkinghour="><i class="fa fa-clock-o"></i> Change Working Hour</a>
  
                            <!-- /.nav-second-level -->
                               </li>
                                <li>
                            <a href="change.php?changemap="><i class="fa fa-map-marker"></i> Change Map</a>
                            <!-- /.nav-second-level -->
                               </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                          
                         <li>
                              <a href="#"> <i class="fa fa-exchange"></i>  Marchant Corner <span class="fa arrow"></span></a>
                              
                               <ul class="nav nav-second-level">
                                <li>
                            <a href="marchant/index.php"> Marchant Request</a>
                            <!-- /.nav-second-level -->
                        </li>
                         <li>
                            <a href="marchant/marchantshop.php?shoplist="> Go to Marchant Shop</a>
                            <!-- /.nav-second-level -->
                        </li>
                      </ul>
 
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>