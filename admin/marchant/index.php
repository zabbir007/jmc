<?php

include('admincon.php');
if (empty($_SESSION['inthemainathorityaccessadmincontrolifthatsare'])) { ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div style="max-width: 70%; min-width: 200px; min-height: 300px; background: white; border-radius: 8px; margin: 0 auto;">
    <p style="text-align: center; color: red;"><i class="fa fa-warning"></i> Unauthorized access request.</p>
</div>
<?php }
else {

include('header.php'); ?>

<body>

    <div id="wrapper">

        <?php //sidebar added here 
         include('sidebar.php');
        ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Marchant Corner</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <?php include ('errors.php');?>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-user fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php $sqlpending = "SELECT * FROM marchant_user WHERE admin_approval = '0'";
             $sqlexecute = mysqli_query($db, $sqlpending);
            echo  mysqli_num_rows($sqlexecute); ?></div>
                                    <div>Total Pending Marchant Request</div>
                                </div>
                            </div>
                        </div>
                        <a href="index.php?marchantrequest=">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
               
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                   <i class="fa fa-check-circle fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php $sqlpending = "SELECT * FROM marchant_user WHERE admin_approval = '1'";
             $sqlexecute = mysqli_query($db, $sqlpending);
            echo  mysqli_num_rows($sqlexecute);
                                     ?></div>
                                    <div>Total Verified Marchant</div>
                                </div>
                            </div>
                        </div>
                        <a href="index.php?verifymarchant=">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
           
             <div class="col-lg-4 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-close fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php $sqlpending = "SELECT * FROM marchant_user WHERE status = '0'";
             $sqlexecute = mysqli_query($db, $sqlpending);
            echo  mysqli_num_rows($sqlexecute); ?></div>
                                    <div>Email Not Verified</div>
                                </div>
                            </div>
                        </div>
                        <a href="index.php?emailnotverified=">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
              
         </div>
            <!-- /.row -->
            <div class="row">
            <?php if (isset($_GET['marchantrequest'])) { 
                  $sqlpending = "SELECT * FROM marchant_user WHERE admin_approval = '0'";
             $sqlexecute = mysqli_query($db, $sqlpending);
            
                ?>
                <div class="row">
                    <center><h3 class="text-primary"><u>Marchant Request</u></h3></center>
                </div>
<?php if (mysqli_num_rows($sqlexecute) == 0) {
               echo "<center><p>Marchant Request Empty !</p></center>";
            } else{ ?>
                <table class="table table-bordered">
                    <tr>
                    <th scope="col" class="text-center">Marchant Name</th>
                    <th scope="col" class="text-center">Contact Number</th>
                    <th scope="col" class="text-center">Email Address</th>
                    <th scope="col" class="text-center">Shop Name</th>
                    <th scope="col" class="text-center">Shop Address</th>
                    <th scope="col" colspan="2" class="text-center">Action</th>
                    </tr>
                    <?php while ($row = mysqli_fetch_array($sqlexecute)) { ?>
                       <tr>
                        <td class="text-center"><?php echo $row['marchant_name']; ?></td>
                        <td class="text-center"><?php echo $row['marchant_contact']; ?></td>
                         <td class="text-center"><?php echo $row['marchant_email']; ?></td>
                        <td class="text-center"><?php echo $row['marchant_shop']; ?></td>
                         <td class="text-center"><?php echo $row['marchant_shop_address']; ?></td>

                        <td><form action="index.php?marchantrequest=" method="POST">
                            <input type="hidden" name="hiddenid" value="<?php echo $row['id']; ?>">
                            <button type="submit" name="deletemarchantusers" class="btn btn-link"><i class="fa fa-close"></i> Delete</button>
                        </form></td>
                        <td><form action="index.php?marchantrequest=" method="POST">
                            <input type="hidden" name="hiddenid" value="<?php echo $row['id']; ?>">
                            <button type="submit" name="approvemarchantusers" class="btn btn-link"><i class="fa fa-check-circle"></i> Approve</button>
                        </form></td>

                    </tr>
                <?php    } ?>
                   
                    
                </table>
           <?php }
           } 

          if (isset($_GET['verifymarchant'])) { 
                  $sqlpending = "SELECT * FROM marchant_user WHERE admin_approval = '1'";
             $sqlexecute = mysqli_query($db, $sqlpending);
            
                ?>
                <div class="row">
                    <center><h3 class="text-primary"><u>Verified Marchant</u></h3></center>
                </div>
<?php if (mysqli_num_rows($sqlexecute) == 0) {
               echo "<center><p>Verified Marchant List is Empty !</p></center>";
            } else{ ?>
                <table class="table table-bordered">
                    <tr>
                    <th scope="col" class="text-center">Marchant Name</th>
                    <th scope="col" class="text-center">Contact Number</th>
                    <th scope="col" class="text-center">Email Address</th>
                    <th scope="col" class="text-center">Shop Name</th>
                    <th scope="col" class="text-center">Shop Address</th>
                    <th scope="col" class="text-center">Username</th>
                    <th scope="col" class="text-center">Action</th>
                    </tr>
                    <?php while ($row = mysqli_fetch_array($sqlexecute)) { ?>
                       <tr>
                        <td class="text-center"><?php echo $row['marchant_name']; ?></td>
                        <td class="text-center"><?php echo $row['marchant_contact']; ?></td>
                         <td class="text-center"><?php echo $row['marchant_email']; ?></td>
                        <td class="text-center"><?php echo $row['marchant_shop']; ?></td>
                         <td class="text-center"><?php echo $row['marchant_shop_address']; ?></td>
                         <td class="text-center"><?php echo $row['username']; ?></td>
                        <td><form action="index.php?verifymarchant=" method="POST">
                            <input type="hidden" name="hiddenid" value="<?php echo $row['id']; ?>">
                            <button type="submit" name="movepending" class="btn btn-link"><i class="fa fa-close"></i> Move Pending</button>
                        </form></td>

                    </tr>
                <?php    } ?>
                   
                    
                </table>
           <?php }
          }
           
          if (isset($_GET['emailnotverified'])) { 
                  $sqlpending = "SELECT * FROM marchant_user WHERE status = '0'";
             $sqlexecute = mysqli_query($db, $sqlpending);
            
                ?>
                <div class="row">
                    <center><h3 class="text-primary"><u>Email Not Verified Users</u></h3></center>
                </div>
<?php if (mysqli_num_rows($sqlexecute) == 0) {
               echo "<center><p>Not Verified Users List is Empty !</p></center>";
            } else{ ?>
                <table class="table table-bordered">
                    <tr>
                    <th scope="col" class="text-center">Marchant Name</th>
                    <th scope="col" class="text-center">Contact Number</th>
                    <th scope="col" class="text-center">Email Address</th>
                    <th scope="col" class="text-center">Shop Name</th>
                    <th scope="col" class="text-center">Shop Address</th>
                    <th scope="col" class="text-center">Action</th>
                    </tr>
                    <?php while ($row = mysqli_fetch_array($sqlexecute)) { ?>
                       <tr>
                        <td class="text-center"><?php echo $row['marchant_name']; ?></td>
                        <td class="text-center"><?php echo $row['marchant_contact']; ?></td>
                         <td class="text-center"><?php echo $row['marchant_email']; ?></td>
                        <td class="text-center"><?php echo $row['marchant_shop']; ?></td>
                         <td class="text-center"><?php echo $row['marchant_shop_address']; ?></td>

                        <td><form action="index.php?emailnotverified=" method="POST">
                            <input type="hidden" name="hiddenid" value="<?php echo $row['id']; ?>">
                            <button type="submit" name="deletemarchantusers" class="btn btn-link"><i class="fa fa-close"></i> Delete</button>
                        </form></td>
                    </tr>
                <?php    } ?>
                   
                    
                </table>
           <?php }
          }
           ?>

                          </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../vendor/raphael/raphael.min.js"></script>
    <script src="../vendor/morrisjs/morris.min.js"></script>
    <script src="../data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
<?php } ?>