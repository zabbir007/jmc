       <?php
					$sql_logo = "select * from logo";
					$result_logo = $db->query($sql_logo);
					$data_logo = mysqli_fetch_array($result_logo);
				?>
        <div class="header-top">
        <div class="container">
            <div class="header-left">
                <div class="switcher-wrap"><ul id="menu-view-switcher" class="view-switcher mega-menu">
                    <li class="menu-item has-sub narrow">
                        <a href="#" onclick="window.lang.change('bn'); return false;" style="color:black;"><img src="wp-content/themes/porto/images/flags/ban.png" alt=""> বাংলা</a>
                        <!--
						<div class="popup">
							<div class="inner">
								
								<ul class="sub-menu">
									<li class="menu-item"><a href="#"><img src="wp-content/themes/porto/images/flags/ban.png" alt="">Bangladesh</a></li>
									<li class="menu-item"><a href="#"><img src="wp-content/themes/porto/images/flags/en.png" alt="">English</a></li>
								</ul>
							
							</div>
						</div>
						-->
                    </li>
                </ul><span class="gap switcher-gap">|</span><ul id="menu-currency-switcher" class="currency-switcher mega-menu">
                    <li class="menu-item has-sub narrow">
                      <a href="#" onclick="window.lang.change('en'); return false;" style="color:black;"><img src="wp-content/themes/porto/images/flags/en.png" alt="">English </a>
                        <!--
						<div class="popup">
							<div class="inner">
								<ul class="sub-menu wcml-switcher">
									<li class="menu-item"><a href="#">BDT</a></li>
									<li class="menu-item"><a href="#">USD</a></li>
								</ul>
							</div>
						</div>
					-->
                    </li>
                </ul>
              <span class="gap switcher-gap">|</span><ul id="menu-currency-switcher" class="currency-switcher mega-menu">
                    <li class="menu-item has-sub narrow">
                      <a href="#" onclick="window.lang.change('en'); return false;" style="color:black;"><i class="Simple-Line-Icons-phone"></i> <span lang="en">CALL US FOR ORDER  </span><?=$data_logo[2]?></a>
                        <!--
						<div class="popup">
							<div class="inner">
								<ul class="sub-menu wcml-switcher">
									<li class="menu-item"><a href="#">BDT</a></li>
									<li class="menu-item"><a href="#">USD</a></li>
								</ul>
							</div>
						</div>
					-->
                    </li>
                </ul>
            </div>            </div>
            <div class="header-right">
                <span class="welcome-msg"></span><span class="gap">|</span><ul id="menu-top-navigation" class="top-links mega-menu show-arrow effect-down subeffect-fadein-left">
				<?php
					if(isset($_SESSION['00user_email00'])){
				?>
						<li class="menu-item" style="margin-right:2px"><a href="myaccount.php" title="<?=$_SESSION['00user_title00']." ".$_SESSION['00firstname00'];?>"><button class="btn btn-success"><?=$_SESSION['00user_title00']." ".$_SESSION['00firstname00'];?> <i class="fa fa-user"></i></button></a></li>
						<li class="menu-item"><a href="myorder.php"><button class="btn btn-success" lang="en">My Order <i class="fa fa-first-order"></i></button></a></li>
						<li class="menu-item"><a href="logout.php"><button class="btn btn-success" lang="en">Logout <i class="fa fa-sign-out"></i></button></a></li>
				<?php
					}
					else{ ?>

						<?php	if(empty($_SESSION['marchantuser10'])) { ?>
							<li class="menu-item"><a href="marchant/index.php"><button type="button" class="btn" style="background: #ffdc52; padding: 6px 12px; border-radius: 4px !important; color: black;"><span lang="en">Marchant Corner </span><i class="fa fa-exchange"></i></button></a></li>
					<?php	
				}
				?>      
					
						<li class="menu-item"><a href="account.php"><button type="button" class="btn" style="background: #ffdc52; padding: 6px 12px; border-radius: 4px !important; color: black;"><span lang="en">Login </span><i class="fa fa-sign-in"></i></button></a></li>
				<?php
					}
				?>
				</ul>
            </div>
        </div>
    </div>
    
