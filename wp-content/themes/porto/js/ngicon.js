function add_to_cart(click_value){
	$.ajaxOper(click_value);
}
function remove_product(click_value){
	$.ajaxOper2(click_value);
}
function update_qty(click_id,click_value){
	$.ajaxOper3(click_id,click_value);
}
$(document).ready(function(){
	$.ajaxOper = function(stock_id){
		$.ajax({
			url:"add_to_cart.php",
			method:"post",
			data:{stock_id:stock_id},
			dataType:"JSON",
			success:function(data){
				$("#show_add_cart").load("header.php #show_add_cart");
				$("#add_to_cart_second").load("header.php #add_to_cart_second");
				alert(data.for_alert+" Add to cart successfully!");
			}
		});
	}
	$.ajaxOper2 = function(stock_id){
		$.ajax({
			url:"delete_to_cart.php",
			method:"post",
			data:{stock_id:stock_id},
			dataType:"JSON",
			success:function(data){
				$("#show_add_cart").load("header.php #show_add_cart");
				$("#add_to_cart_second").load("header.php #add_to_cart_second");
				$("#process_order_list").load("shop_cart_box.php #process_order_list");
				alert(data.for_alert_2+" product delete successfully!");
			}
		});
	}
	$.ajaxOper3 = function(stock_id,qty){
		$.ajax({
			url:"update_qty.php",
			method:"post",
			data:{stock_id:stock_id,qty:qty},
			dataType:"text",
			success:function(data){
				$("#process_order_list").load("shop_cart_box.php #process_order_list");
			}
		});
	}
});