<?php 
session_start();
require_once "config.php";
		//require_once "config_sms.php";
		require_once "config_pos.php";
		
		?>

		<!DOCTYPE html>
<html lang="en-US">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<?php
	include_once "head.php";
?>
<body class="home page-template-default page page-id-143 full blog-78  yith-wcan-free wpb-js-composer js-comp-ver-5.4.5 vc_responsive" ">

<div class="page-wrapper">

    <div class="header-wrapper clearfix"><!-- header wrapper -->
                                
                    <header id="header" class="header-separate header-1  sticky-menu-header">
<?php
	include_once "header_top_menu.php";
?>
    
    <div class="header-main" >
        <div class="container">
            <div class="header-left">
				<?php
					$sql_logo = "select * from logo";
					$result_logo = $db->query($sql_logo);
					$data_logo = mysqli_fetch_array($result_logo);
				?>
                <h1 class="logo">    <a href="index.php" title="<?=$data['title'];?>" rel="home">
                <img class="img-responsive standard-logo" src="<?=$data_logo['logo_path'];?>" alt="<?=$data['title'];?>" /><img class="img-responsive retina-logo" src="<?=$data_logo['logo_path'];?>" alt="<?=$data['title'];?>" style="display:none;" />            </a>
    </h1>            </div>
            <div class="header-center">
                <a class="mobile-toggle"><i class="fa fa-reorder"></i></a>
                    <div class="searchform-popup">
        <a class="search-toggle"><i class="fa fa-search"></i></a>
     <form action="search_category_product.php" method="post"
        class="searchform searchform-cats">
        <fieldset>
            <span class="text"><input name="type_search_category" id="type_search_category" type="text" value="" placeholder="Search&hellip;" autocomplete="off"/></span>
            <input type="hidden" name="post_type" value="product"/>
			<select  name='default_search_category' id='product_cat' class='cat'>
			<option value='0' selected lang="en">All Categories</option>
			<?php
				$sql_category = "select * from 0_stock_category";
				$result_category = $db2->query($sql_category);
				while($data_category = mysqli_fetch_array($result_category)){
			?>
				<option class="level-0" value="<?=$data_category[0];?>"><?=$data_category[1];?></option>
			<?php
				}
			?>
			</select>
            <span class="button-wrap"><button class="btn btn-special" title="Search" type="submit"><i class="fa fa-search"></i></button></span>
        </fieldset>
    </form>
        </div>
                </div>
            <div class="header-right">
                                <div class="header-minicart-arrow">
                    <div class="header-contact" ><i class="Simple-Line-Icons-phone"></i><span lang="en">CALL US FOR ORDER</span><br><span><b><?=$data_logo[2]?></b></span></div>        <div style="    border: solid 1px #0088cc;
    background-color: #0088cc;
    box-shadow: 0 5px 11px -2px #0088cc;" id="mini-cart" class="mini-cart minicart-arrow effect-fadein-up minicart-style2" >
            <div id="show_add_cart" >
				<div class="cart-head cart-head4" >
					<a href="shop_cart_box.php"><img style="width: 48px;height: 30px;margin-left: 10px;" src="icon/cart.png"/></a>
					<?php
						if(isset($_SESSION['shoping_cart'])){
							$count_shoping_cart = count($_SESSION['shoping_cart']);
						}
						else{
							$count_shoping_cart = 0;
						}
					?>
				 <span class="cart-items" lang="en"><?=$count_shoping_cart;?></span>
				</div>
				<div class="cart-popup widget_shopping_cart">
					<div class="widget_shopping_cart_content">
						<div class="cart">
							<table border="0" cellpadding="5px" cellspacing="0px" style="margin:0px auto;text-align:center">
							<?php
							$total = 0;
							if(!empty($_SESSION['shoping_cart'])){
							?>
							<thead>
							<tr>
							<th></th>
							<th><span lang="en">Name</span></th>
							<th><span lang="en">Qty</span></th>
							<th><span lang="en">Price</span></th>
							<th><i class="fa fa-trash"></i></th>
							</tr>
							</thead>
							<?php
							
							foreach($_SESSION['shoping_cart'] as $keys => $values)
							{
							?>
							<tr>
							<td><img style="width: 500px;height: 50px;" src="<?php echo $product_images_url.$values['item_id'].".jpg";?>" /></td>
							<td><?php echo $values['item_name'];?></td>
							<td><?php echo $values['item_qty'];?></td>
							<td>$<?php echo $values['item_price'];?></td>
							<td><button style="background:none;border:none;cursor:pointer" onclick="remove_product(this.value);" value="<?php echo $values['item_id']?>" ><i class="fa fa-remove" style="font-size:16px;color:red"></i></button></td>
							</tr>
							<?php
							}
							?>
								<tr>
									<td colspan="5" style="text-align:right;"><a style="background: #c3c111;color: azure;padding: 10px;" href="shop_cart_box.php"><span lang="en">Check Out</span></a></td>
								</tr>
							<?php
							}
							else{
							?>
							<tr>
							<td><span lang="en">Your Cart is empty!</span></td>
							</tr>
							<?php
							}
							?>
							</table>
						</div>
					</div>
				</div>
			</div>
        </div>
                    </div>

                
            </div>
        </div>
            </div>
            <div style="width:100%; border-bottom: 2px solid #000;"></div>

            <div class="main-menu-wrap">
            <div id="main-menu" class="container ">
                                    <div class="menu-left">
                        <div class="logo">    <a href="index.php" title="<?=$data['title'];?>">
						
                <img class="img-responsive standard-logo" src="<?=$data_logo['logo_path'];?>" alt="<?=$data['title'];?>" /><img class="img-responsive retina-logo" src="<?=$data_logo['logo_path'];?>" alt="<?=$data['title'];?>" style="display:none;" />            </a>
    </div>                    </div>


	<?php
		include_once ("ecomtopmenu.php");
	?>
	
	<div class="menu-right">
<div class="searchform-popup">
<a class="search-toggle"><i class="fa fa-search"></i></a>
<form action="#" method="get"
class="searchform searchform-cats">
<fieldset>
<span class="text"><input name="s" id="s" type="text" value="" placeholder="Search&hellip;" autocomplete="off" /></span>
<input type="hidden" name="post_type" value="product"/>
<select  name='product_cat' id='product_cat' class='cat' >
<option value='0' lang="en">All Categories</option>
<option class="level-0" value="accessories">Accessories</option>
<option class="level-0" value="albums">Albums</option>
<option class="level-0" value="electronics">Electronics</option>
<option class="level-0" value="music">Music</option>
<option class="level-0" value="posters">Posters</option>
</select>
<span class="button-wrap"><button class="btn btn-special" title="Search" type="submit"><i class="fa fa-search"></i></button></span>
</fieldset>
</form>
</div>
<div id="mini-cart" class="mini-cart minicart-arrow effect-fadein-up minicart-style2">
<div id="add_to_cart_second">
<div class="cart-head cart-head4">
<a href="shop_cart_box.php"><img style="width: 48px;height: 30px;margin-left: 6px;" src="icon/cart.png"/></a>
	<span class="cart-items" lang="en"><?=$count_shoping_cart;?></span>
</div>
<div class="cart-popup widget_shopping_cart">
<div class="widget_shopping_cart_content">
<div class="cart">
	<table border="0" cellpadding="5px" cellspacing="0px" style="margin:0px auto;text-align:center">
	<?php
	if(!empty($_SESSION['shoping_cart'])){
	?>
	<thead>
	<tr>
	<th></th>
	<th><span lang="en">Name</span></th>
	<th><span lang="en">Qty</span></th>
	<th><span lang="en">Price</span></th>
	<th><i class="fa fa-trash"></i></th>
	</tr>
	</thead>
	<?php
	$total = 0;
	foreach($_SESSION['shoping_cart'] as $keys => $values)
	{
	?>
	<tr>
	<td><img style="width: 500px;height: 50px;" src="<?php echo $product_images_url.$values['item_id'].".jpg";?>" /></td>
	<td><?php echo $values['item_name'];?></td>
	<td><?php echo $values['item_qty'];?></td>
	<td>$<?php echo $values['item_price'];?></td>
	<td><button style="background:none;border:none;cursor:pointer" onclick="remove_product(this.value);" value="<?php echo $values['item_id']?>" ><i class="fa fa-remove" style="font-size:16px;color:red"></i></button></td>
	</tr>
	<?php
	}
	?>
		<tr>
			<td colspan="5" style="text-align:right;"><a style="background: #c3c111;color: azure;padding: 10px;" href="shop_cart_box.php"><span lang="en">Check Out</span></a></td>
		</tr>
	<?php
	}
	else{
	?>
		<tr>
			<td><span lang="en">Your Cart is empty!</span></td>
		</tr>
	<?php
	}
	?>
	</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</header>
</div><!-- end header wrapper -->
<div class="sign_in_form">
	<?php
			if(isset($_REQUEST['massage'])){
				if (empty($_SESSION['recovermail'])) { ?>
					<center><font size="+2" color="red" lang="en">This email is not registered</font></center>
			<?php	} else{ ?>
				<center><font size="+2" color="red" lang="en">You entered wrong 4 digit code</font></center>
		<?php	}
		?>
			
		<?php
			}
		?>
		<div class="container">
		<h1 class="page_heading" lang="en">Recover Your Account</h1>
		<div class="header_border"></div>
		<div class="sign_full_field">
			<div class="sign_field_second">
			<p style="text-align:center;" ><span lang="en">We will send a email verification code in your email address. </span><b style="color:red;" lang="en">(Sometimes verification code goes to spam or junk folder)</b></p>
			<div class="header_border"></div>
			
			<form action="password-recovery.php" method="POST" onsubmit="return passwrod_validation();">
				<?php if (empty($_SESSION['recovermail'])) {
					# code...
				 ?>
				
			<table border="0px" cellpadding="10px" cellspacing="10px" style="margin:20px auto">
			<tr>
				<center>
				<td colspan="2" style="text-align: center;"><label lang="en">Enter Your Email</label><br>
				<input class="input_type_class" type="text" name="email"  required></td></center>
			</tr> 
			<tr>
                   
				<td colspan="2" style="text-align: center;"><button class="input_type_class2 btn btn-primary" type="submit" name="recover_password" lang="en">Password Recover</button></td>
			</tr>
			</table>
<?php } else { ?>

	<table border="0px" cellpadding="10px" cellspacing="10px" style="margin:20px auto">
			<tr>
				<center>
				<td colspan="2" style="text-align: center;"><label lang="en">Enter Code: (we sent a 4 digit code in your email)</label><br>
				<input class="input_type_class" type="text" name="confirmcode" placeholder="5486" required><br><br>
				<label lang="en">Enter New Password</label><br>
				<input class="input_type_class" type="password" name="password" id="password" placeholder="Enter New Password" required><br><br>
				<input class="input_type_class" type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password" required>
				<input type="hidden" name="hideemail" value="<?php echo $_SESSION['recovermail']; ?>">
			</td></center>
			</tr> 
			<tr>
                   
				<td colspan="2" style="text-align: center;"><button class="input_type_class2 btn btn-primary" type="submit" name="updatepass" lang="en">Update Password</button></td>
			</tr>
			</table>
		<?php } ?>
		</form>
			</div>
		</div>
		</div>
	</div>

			<style type="text/css">
		.sign_in_form{margin-top: 40px;
		 margin-bottom: 40px;}
		.page_heading{text-transform:uppercase;font-size:22px}
		.page_heading_2{text-transform:uppercase;font-size:17px}
		.header_border{margin-top:10px;border-bottom:1px solid #aaa}
		.sign_full_field{border:1px solid #aaa;background:#fff;margin-top:20px}
		.sign_field_second{padding:20px}
		<!--change input type box width from lessframwork -->
		.input_type_class2:hover{background:#42DAB8;color:#fff}
		.input_type_class3{border: 1px solid #aaa;
		width: 100px;height:44px}
		.input_type_class3:hover{background:#42DAB8;color:#fff}
	</style>
	<script type="text/javascript">
		function passwrod_validation(){
			var pasw = document.getElementById("password").value;
			var confpasw = document.getElementById("confirm_password").value;
			if(pasw!=confpasw){
				alert("Password Miss Match");
				return false;
			}
			else{
				return true;
			}
		}
	</script>
<?php

	include_once "footer.php";

?>