<?php
	include_once "header.php";
?>
	<div class="sign_in_form">
		<div class="container">
		<?php
			if(isset($_REQUEST['massage'])){
		?>
			<center><font size="+2" color="red">This Number Already Registered. Use Another Number</font></center>
		<?php
			}
		?>
		<h1 class="page_heading" lang="en">Create an account</h1>
		<div class="header_border"></div>
		<div class="sign_full_field">
			<div class="sign_field_second">
			<h1 class="page_heading_2" lang="en">Your Personal Information Required</h1>
			<div class="header_border"></div>
			<?php include('errors.php') ?>
			<form action="register_request.php" method="post" onsubmit="return passwrod_validation();">
			<table border="0px" cellpadding="10px" cellspacing="10px" style="margin:20px auto">
			<tr>
				<td><span lang="en">Title:</span></td>
				<td>
					<select class="input_type_class" name="user_title"  required>
						<option selected></option>
						<option value="Mr.">Mr.</option>
						<option value="Ms.">Ms.</option>
					</select>
				</td>
			</tr>
			<tr>
				<td style="height:15px"><td/>
			</tr>
			<tr>
				<td><span lang="en">Full Name:&nbsp;&nbsp;</span></td>
				<td colspan="2"><input class="input_type_class" type="text" name="first_name" id="first_name" required></td>
			</tr>
			<tr>
				<td style="height:15px"><td/>
			</tr>
	<!--		<tr>
				<td>Address:&nbsp;&nbsp;</td>
				<td colspan="2"><input class="input_type_class" type="text" name="last_name" id="last_name" required></td>
			</tr>
			<tr>
				<td style="height:15px"><td/>
			</tr> -->
			<tr>
				<td lang="en">Email:&nbsp;&nbsp;</td>
				<td><input class="input_type_class" title="Please fill email out this fill." type="email" name="email" id="email" onblur="check_email(this)"><span id="showAlert" style="color:red;position:absolute"></span></td>
			</tr>
			<tr>
				<td style="height:15px"><td/>
			</tr>
			<tr>
				<td lang="en">Mobile No.:&nbsp;&nbsp;</td>
				<td colspan="2"><input class="input_type_class"  type="text" name="mobile_number" id="mobile_number" required></td>
			</tr>
			<tr>
				<td style="height:15px"><td/>
			</tr>
			<tr>
				<td lang="en">Password:&nbsp;&nbsp;</td>
				<td colspan="2"><input class="input_type_class" type="password" name="password" id="password" required></td>
			</tr>
			<tr>
				<td style="height:15px"><td/>
			</tr>
			<tr>
				<td lang="en">Confirm Password:&nbsp;&nbsp;</td>
				<td colspan="2"><input class="input_type_class" type="password" name="confirm_password" id="confirm_password" required></td>
			</tr>
			<tr>
				<td style="height:15px"><td/>
			</tr>
			<tr>
				<td colspan="2" style="text-align:center"><button class="input_type_class2 btn btn-primary" type="submit" name="create_account" id="create_account" lang="en">Register</button></td>
			</tr>
			</table>
		</form>
			</div>
		</div>
		</div>
	</div>
	<style type="text/css">
		.sign_in_form{margin-top: 40px;
		 margin-bottom: 40px;}
		.page_heading{text-transform:uppercase;font-size:22px}
		.page_heading_2{text-transform:uppercase;font-size:17px}
		.header_border{margin-top:10px;border-bottom:1px solid #aaa}
		.sign_full_field{border:1px solid #aaa;background:#fff;margin-top:20px}
		.sign_field_second{padding:20px}
		<!--change input type box width from lessframwork -->
		.input_type_class2:hover{background:#42DAB8;color:#fff}
		.input_type_class3{border: 1px solid #aaa;
		width: 100px;height:44px}
		.input_type_class3:hover{background:#42DAB8;color:#fff}
	</style>
	<script type="text/javascript">
		function passwrod_validation(){
			var pasw = document.getElementById("password").value;
			var confpasw = document.getElementById("confirm_password").value;
			if(pasw!=confpasw){
				alert("Password Miss Match");
				return false;
			}
			else{
				return true;
			}
		}
	</script>
<?php
	include_once "footer.php";
?>