
	
	<?php include_once("header.php");?>
	
 <!-- end header wrapper -->
        
        
                <section class="page-top page-header-6">
        <div class="container hide-title">
    <div class="row">
        <div class="col-lg-12 clearfix">
            <div class="pt-right d-none">
                <h1 class="page-title">Accessories</h1>
                            </div>
                            <div class="breadcrumbs-wrap pt-left">
                    <ul class="breadcrumb"><li class="home"><a itemprop="url" href="#" title="Go to Home Page"><span itemprop="title">Home</span></a><i class="delimiter delimiter-2"></i></li><li ><a itemprop="url" href="shop/index.php"><span itemprop="title">Shop</span></a><i class="delimiter delimiter-2"></i></li><li>Accessories</li></ul>                </div>
                                </div>
    </div>
</div>    </section>
    
        <div id="main" class="column2 column2-left-sidebar boxed"><!-- main -->

            
                        <div class="container">
                            
            
            <div class="row main-content-wrap">

            <!-- main content -->
            <div class="main-content col-lg-9">

                                                <div id="content-inner-top"><!-- begin content inner top -->
                        <style type="text/css">div.wpb_single_image .vc_single_image-wrapper { display: block; }</style><div class="porto-block "><div class="vc_row wpb_row row"><div class="vc_column_container col-md-12"><div class="wpb_wrapper vc_column-inner"><div class="porto-carousel owl-carousel  porto-standable-carousel" data-plugin-options="{&quot;stagePadding&quot;:0,&quot;margin&quot;:0,&quot;autoplay&quot;:&quot;yes&quot;,&quot;autoplayTimeout&quot;:3000,&quot;autoplayHoverPause&quot;:false,&quot;items&quot;:1,&quot;lg&quot;:1,&quot;md&quot;:1,&quot;sm&quot;:1,&quot;xs&quot;:1,&quot;nav&quot;:false,&quot;dots&quot;:false,&quot;animateIn&quot;:&quot;&quot;,&quot;animateOut&quot;:&quot;fadeOut&quot;,&quot;loop&quot;:&quot;yes&quot;,&quot;center&quot;:false,&quot;video&quot;:false,&quot;lazyLoad&quot;:false,&quot;fullscreen&quot;:false}">
	<div class="wpb_single_image wpb_content_element vc_align_center">
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper vc_box_rounded  vc_box_border_grey"><img class="vc_single_image-img"  src="shop1/wp-content/uploads/banners/fashion-banner.jpg" /></div>
		</div>
	</div>

	<div class="wpb_single_image wpb_content_element vc_align_center">
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper vc_box_rounded  vc_box_border_grey"><img class="vc_single_image-img"  src="shop1/wp-content/uploads/banners/fashion-banner2.jpg" /></div>
		</div>
	</div>
</div></div></div></div></div>                    </div><!-- end content inner top -->
                
<div id="primary" class="content-area"><main id="main" class="site-main" role="main">



    <div class="shop-loop-before clearfix" style="display:none;">
	<form class="woocommerce-ordering" method="get">
    <label>Sort By: </label>
    <select name="orderby" class="orderby">
					<option value="menu_order"  selected='selected'>Default sorting</option>
					<option value="popularity" >Sort by popularity</option>
					<option value="rating" >Sort by average rating</option>
					<option value="date" >Sort by newness</option>
					<option value="price" >Sort by price: low to high</option>
					<option value="price-desc" >Sort by price: high to low</option>
			</select>
	<input type="hidden" name="paged" value="1" />

	</form>    <div class="gridlist-toggle">
        <a href="#" id="grid" title="Grid View"></a><a href="#" id="list" title="List View"></a>
    </div>

<nav class="woocommerce-pagination">

    <form class="woocommerce-viewing" method="get">

        <label>Show: </label>

        <select name="count" class="count">
                            <option value="8"  selected='selected'>8</option>
                            <option value="16" >16</option>
                            <option value="32" >32</option>
                            <option value="48" >48</option>
                    </select>

        <input type="hidden" name="paged" value=""/>

            </form>

	</nav>    </div>

    
    <div class="archive-products">

        <ul class="products pcols-lg-3 pcols-md-3 pcols-xs-2 pcols-ls-2 pwidth-lg-3 pwidth-md-3 pwidth-xs-2 pwidth-ls-1">        
<li class="show-links-outimage product-first post-254 product type-product status-publish has-post-thumbnail product_cat-accessories product_cat-posters product_tag-fashion product_tag-hub product_tag-sports first instock shipping-taxable purchasable product-type-simple">
<div class="product-inner">
	
    <div class="product-image">
						<div class="loader-container"><div class="loader"><i class="porto-ajax-loader"></i></div></div>
				<div class="after-loading-success-message">
					<div class="background-overlay"></div>
					<div class="loader success-message-container">
						<div class="msg-box">
							<div class="msg">You've just added this product to the cart:<p class="product-name text-color-primary">25 Acoustic Noise</p></div>
							<img width="600" height="600" src="wp-content/uploads/sites/78/2013/06/6_2_1.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="" sizes="(max-width: 600px) 100vw, 600px" />						</div>
						<button class="button btn-primay viewcart" data-link="../../cart/index.php">Go to cart page</button>
						<button class="button btn-primay continue_shopping">Continue</button>
					</div>
				</div>
				
        <a  href="product/25-acoustic-noise/index.php">
            <div class="inner img-effect"><img width="300" height="300" src="wp-content/uploads/sites/78/2013/06/6_2_1-300x300.jpg" class=" wp-post-image" alt="" srcset=""  sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="300" src="wp-content/uploads/sites/78/2013/06/8_17-300x300.jpg" class="hover-image" alt="" srcset= "" sizes="(max-width: 300px) 100vw, 300px" /></div>        </a>

        <div class="links-on-image">
            <div class="add-links-wrap">
    <div class="add-links  clearfix">
        <a rel="nofollow" href="index6f3b.html?add-to-cart=254" data-quantity="1" data-product_id="254" data-product_sku="854613298" class="viewcart-style-2 button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="254" data-product_sku="854613298" aria-label="Add &ldquo;25 Acoustic Noise&rdquo; to your cart" rel="nofollow">Add to cart</a>
<div class="yith-wcwl-add-to-wishlist add-to-wishlist-254">
		    <div class="yith-wcwl-add-button show" style="display:block">

	        <a href="indexa81e.html?add_to_wishlist=254" rel="nofollow" data-product-id="254" data-product-type="simple" class="add_to_wishlist" >        Add to Wishlist</a><span class="ajax-loading"></span>
	    </div>

	    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
	        <span class="feedback">Product added!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
	        <span class="feedback">The product is already in the wishlist!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div style="clear:both"></div>
	    <div class="yith-wcwl-wishlistaddresponse"></div>
	
</div>

<div class="clear"></div><div class="quickview" data-id="254" title="Quick View">Quick View</div>    </div>
</div>        </div>
    </div>

	

<div class="rating-wrap">
    <span class="rating-before"><span class="rating-line"></span></span>
    <div class="rating-content"><div class="star-rating" title="4.00"><span style="width:80%"><strong class="rating">4.00</strong> out of 5</span></div></div>
    <span class="rating-after"><span class="rating-line"></span></span>
</div>


    
        <a class="product-loop-title"  href="product/25-acoustic-noise/index.php"><h3 class="woocommerce-loop-product__title">25 Acoustic Noise</h3></a>    
        <div class="description">
        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
    </div>
    
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>299.00</span></span>

    <div class="add-links-wrap">
    <div class="add-links  clearfix">
        <a rel="nofollow" href="index6f3b.html?add-to-cart=254" data-quantity="1" data-product_id="254" data-product_sku="854613298" class="viewcart-style-2 button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="254" data-product_sku="854613298" aria-label="Add &ldquo;25 Acoustic Noise&rdquo; to your cart" rel="nofollow">Add to cart</a>
<div class="yith-wcwl-add-to-wishlist add-to-wishlist-254">
		    <div class="yith-wcwl-add-button show" style="display:block">

	        <a href="indexa81e.html?add_to_wishlist=254" rel="nofollow" data-product-id="254" data-product-type="simple" class="add_to_wishlist" >        Add to Wishlist</a><span class="ajax-loading"></span>
	    </div>

	    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
	        <span class="feedback">Product added!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
	        <span class="feedback">The product is already in the wishlist!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div style="clear:both"></div>
	    <div class="yith-wcwl-wishlistaddresponse"></div>
	
</div>

<div class="clear"></div><div class="quickview" data-id="254" title="Quick View">Quick View</div>    </div>
</div>
</div>
</li>
<li class="show-links-outimage post-262 product type-product status-publish has-post-thumbnail product_cat-accessories product_cat-electronics product_cat-music product_tag-clothes product_tag-fashion  instock sale downloadable shipping-taxable purchasable product-type-simple">
<div class="product-inner">
	
    <div class="product-image">
						<div class="loader-container"><div class="loader"><i class="porto-ajax-loader"></i></div></div>
				<div class="after-loading-success-message">
					<div class="background-overlay"></div>
					<div class="loader success-message-container">
						<div class="msg-box">
							<div class="msg">You've just added this product to the cart:<p class="product-name text-color-primary">Headphone NK</p></div>
							<img width="600" height="600" src="wp-content/uploads/sites/78/2013/06/4_1_12_2.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset= "" sizes="(max-width: 600px) 100vw, 600px" />						</div>
						<button class="button btn-primay viewcart" data-link="../../cart/index.php">Go to cart page</button>
						<button class="button btn-primay continue_shopping">Continue</button>
					</div>
				</div>
				
        <a  href="product_details.php">
            <div class="labels"><div class="onsale">-3%</div></div><div class="inner img-effect"><img width="300" height="300" src="wp-content/uploads/sites/78/2013/06/4_1_12_2-300x300.jpg" class=" wp-post-image" alt="" srcset="" sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="300" src="wp-content/uploads/sites/78/2013/06/6_2_1-300x300.jpg" class="hover-image" alt="" srcset="" sizes="(max-width: 300px) 100vw, 300px" /></div>        </a>

        <div class="links-on-image">
            <div class="add-links-wrap">
    <div class="add-links  clearfix">
        <a rel="nofollow" href="indexc9d4.html?add-to-cart=262" data-quantity="1" data-product_id="262" data-product_sku="654111995" class="viewcart-style-2 button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="262" data-product_sku="654111995" aria-label="Add &ldquo;Headphone NK&rdquo; to your cart" rel="nofollow">Add to cart</a>
<div class="yith-wcwl-add-to-wishlist add-to-wishlist-262">
		    <div class="yith-wcwl-add-button show" style="display:block">

	        <a href="indexcb55.html?add_to_wishlist=262" rel="nofollow" data-product-id="262" data-product-type="simple" class="add_to_wishlist" >        Add to Wishlist</a><span class="ajax-loading"></span>
	    </div>

	    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
	        <span class="feedback">Product added!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
	        <span class="feedback">The product is already in the wishlist!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div style="clear:both"></div>
	    <div class="yith-wcwl-wishlistaddresponse"></div>
	
</div>

<div class="clear"></div><div class="quickview" data-id="262" title="Quick View">Quick View</div>    </div>
</div>        </div>
    </div>

	

<div class="rating-wrap">
    <span class="rating-before"><span class="rating-line"></span></span>
    <div class="rating-content"><div class="star-rating" title="5.00"><span style="width:100%"><strong class="rating">5.00</strong> out of 5</span></div></div>
    <span class="rating-after"><span class="rating-line"></span></span>
</div>


    
        <a class="product-loop-title"  href="product_details.php"><h3 class="woocommerce-loop-product__title">Headphone NK</h3></a>    
        <div class="description">
        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
    </div>
    
	<span class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>299.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>289.00</span></ins></span>

    <div class="add-links-wrap">
    <div class="add-links  clearfix">
        <a rel="nofollow" href="indexc9d4.html?add-to-cart=262" data-quantity="1" data-product_id="262" data-product_sku="654111995" class="viewcart-style-2 button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="262" data-product_sku="654111995" aria-label="Add &ldquo;Headphone NK&rdquo; to your cart" rel="nofollow">Add to cart</a>
<div class="yith-wcwl-add-to-wishlist add-to-wishlist-262">
		    <div class="yith-wcwl-add-button show" style="display:block">

	        <a href="indexcb55.html?add_to_wishlist=262" rel="nofollow" data-product-id="262" data-product-type="simple" class="add_to_wishlist" >        Add to Wishlist</a><span class="ajax-loading"></span>
	    </div>

	    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
	        <span class="feedback">Product added!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
	        <span class="feedback">The product is already in the wishlist!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div style="clear:both"></div>
	    <div class="yith-wcwl-wishlistaddresponse"></div>
	
</div>

<div class="clear"></div><div class="quickview" data-id="262" title="Quick View">Quick View</div>    </div>
</div>
</div>
</li>
<li class="show-links-outimage post-258 product type-product status-publish has-post-thumbnail product_cat-accessories product_cat-posters product_tag-dress product_tag-fashion product_tag-women last instock shipping-taxable purchasable product-type-simple">
<div class="product-inner">
	
    <div class="product-image">
						<div class="loader-container"><div class="loader"><i class="porto-ajax-loader"></i></div></div>
				<div class="after-loading-success-message">
					<div class="background-overlay"></div>
					<div class="loader success-message-container">
						<div class="msg-box">
							<div class="msg">You've just added this product to the cart:<p class="product-name text-color-primary">Headphone SJ</p></div>
							<img width="600" height="600" src="wp-content/uploads/sites/78/2013/06/1_1_3_2.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset= "" sizes="(max-width: 600px) 100vw, 600px" />						</div>
						<button class="button btn-primay viewcart" data-link="../../cart/index.php">Go to cart page</button>
						<button class="button btn-primay continue_shopping">Continue</button>
					</div>
				</div>
				
        <a  href="product/headphone-sj/index.php">
            <div class="inner img-effect"><img width="300" height="300" src="wp-content/uploads/sites/78/2013/06/1_1_3_2-300x300.jpg" class=" wp-post-image" alt="" srcsetsizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="300" src="wp-content/uploads/sites/78/2013/06/2_13_2-300x300.jpg" class="hover-image" alt="" srcset=""  sizes="(max-width: 300px) 100vw, 300px" /></div>        </a>

        <div class="links-on-image">
            <div class="add-links-wrap">
    <div class="add-links  clearfix">
        <a rel="nofollow" href="index69ea.html?add-to-cart=258" data-quantity="1" data-product_id="258" data-product_sku="" class="viewcart-style-2 button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="258" data-product_sku="" aria-label="Add &ldquo;Headphone SJ&rdquo; to your cart" rel="nofollow">Add to cart</a>
<div class="yith-wcwl-add-to-wishlist add-to-wishlist-258">
		    <div class="yith-wcwl-add-button show" style="display:block">

	        <a href="indexe470.html?add_to_wishlist=258" rel="nofollow" data-product-id="258" data-product-type="simple" class="add_to_wishlist" >        Add to Wishlist</a><span class="ajax-loading"></span>
	    </div>

	    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
	        <span class="feedback">Product added!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
	        <span class="feedback">The product is already in the wishlist!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div style="clear:both"></div>
	    <div class="yith-wcwl-wishlistaddresponse"></div>
	
</div>

<div class="clear"></div><div class="quickview" data-id="258" title="Quick View">Quick View</div>    </div>
</div>        </div>
    </div>

	

<div class="rating-wrap">
    <span class="rating-before"><span class="rating-line"></span></span>
    <div class="rating-content"><div class="star-rating" title="0"><span style="width:0%"><strong class="rating">0</strong> out of 5</span></div></div>
    <span class="rating-after"><span class="rating-line"></span></span>
</div>


    
        <a class="product-loop-title"  href="product/headphone-sj/index.php"><h3 class="woocommerce-loop-product__title">Headphone SJ</h3></a>    
        <div class="description">
        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
    </div>
    
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>299.00</span></span>

    <div class="add-links-wrap">
    <div class="add-links  clearfix">
        <a rel="nofollow" href="index69ea.html?add-to-cart=258" data-quantity="1" data-product_id="258" data-product_sku="" class="viewcart-style-2 button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="258" data-product_sku="" aria-label="Add &ldquo;Headphone SJ&rdquo; to your cart" rel="nofollow">Add to cart</a>
<div class="yith-wcwl-add-to-wishlist add-to-wishlist-258">
		    <div class="yith-wcwl-add-button show" style="display:block">

	        <a href="indexe470.html?add_to_wishlist=258" rel="nofollow" data-product-id="258" data-product-type="simple" class="add_to_wishlist" >        Add to Wishlist</a><span class="ajax-loading"></span>
	    </div>

	    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
	        <span class="feedback">Product added!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
	        <span class="feedback">The product is already in the wishlist!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div style="clear:both"></div>
	    <div class="yith-wcwl-wishlistaddresponse"></div>
	
</div>

<div class="clear"></div><div class="quickview" data-id="258" title="Quick View">Quick View</div>    </div>
</div>
</div>
</li>
<li class="show-links-outimage post-260 product type-product status-publish has-post-thumbnail product_cat-accessories product_cat-albums product_cat-music product_tag-dress product_tag-fashion product_tag-sports first instock sale featured downloadable shipping-taxable purchasable product-type-simple">
<div class="product-inner">
	
    <div class="product-image">
						<div class="loader-container"><div class="loader"><i class="porto-ajax-loader"></i></div></div>
				<div class="after-loading-success-message">
					<div class="background-overlay"></div>
					<div class="loader success-message-container">
						<div class="msg-box">
							<div class="msg">You've just added this product to the cart:<p class="product-name text-color-primary">NTX01 - Headphone</p></div>
							<img width="300" height="300" src="wp-content/uploads/sites/78/2013/06/7_11.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset= "" sizes="(max-width: 300px) 100vw, 300px" />						</div>
						<button class="button btn-primay viewcart" data-link="../../cart/index.php">Go to cart page</button>
						<button class="button btn-primay continue_shopping">Continue</button>
					</div>
				</div>
				
        <a  href="product_details.php">
            <div class="labels"><div class="onhot">Hot</div><div class="onsale">-85%</div></div><div class="inner img-effect"><img width="300" height="300" src="wp-content/uploads/sites/78/2013/06/7_11-300x300.jpg" class=" wp-post-image" alt="" /><img width="300" height="300" src="wp-content/uploads/sites/78/2013/06/2_13_2-300x300.jpg" class="hover-image" alt="" srcset="" sizes="(max-width: 300px) 100vw, 300px" /></div>        </a>

        <div class="links-on-image">
            <div class="add-links-wrap">
    <div class="add-links  clearfix">
        <a rel="nofollow" href="index84f3.html?add-to-cart=260" data-quantity="1" data-product_id="260" data-product_sku="154985326" class="viewcart-style-2 button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="260" data-product_sku="154985326" aria-label="Add &ldquo;NTX01 - Headphone&rdquo; to your cart" rel="nofollow">Add to cart</a>
<div class="yith-wcwl-add-to-wishlist add-to-wishlist-260">
		    <div class="yith-wcwl-add-button show" style="display:block">

	        <a href="index0f4b.html?add_to_wishlist=260" rel="nofollow" data-product-id="260" data-product-type="simple" class="add_to_wishlist" >        Add to Wishlist</a><span class="ajax-loading"></span>
	    </div>

	    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
	        <span class="feedback">Product added!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
	        <span class="feedback">The product is already in the wishlist!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div style="clear:both"></div>
	    <div class="yith-wcwl-wishlistaddresponse"></div>
	
</div>

<div class="clear"></div><div class="quickview" data-id="260" title="Quick View">Quick View</div>    </div>
</div>        </div>
    </div>

	

<div class="rating-wrap">
    <span class="rating-before"><span class="rating-line"></span></span>
    <div class="rating-content"><div class="star-rating" title="3.00"><span style="width:60%"><strong class="rating">3.00</strong> out of 5</span></div></div>
    <span class="rating-after"><span class="rating-line"></span></span>
</div>


    
        <a class="product-loop-title"  href="product_details.php"><h3 class="woocommerce-loop-product__title">NTX01 &#8211; Headphone</h3></a>    
        <div class="description">
        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
    </div>
    
	<span class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>1,999.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>299.00</span></ins></span>

    <div class="add-links-wrap">
    <div class="add-links  clearfix">
        <a rel="nofollow" href="index84f3.html?add-to-cart=260" data-quantity="1" data-product_id="260" data-product_sku="154985326" class="viewcart-style-2 button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="260" data-product_sku="154985326" aria-label="Add &ldquo;NTX01 - Headphone&rdquo; to your cart" rel="nofollow">Add to cart</a>
<div class="yith-wcwl-add-to-wishlist add-to-wishlist-260">
		    <div class="yith-wcwl-add-button show" style="display:block">

	        <a href="index0f4b.html?add_to_wishlist=260" rel="nofollow" data-product-id="260" data-product-type="simple" class="add_to_wishlist" >        Add to Wishlist</a><span class="ajax-loading"></span>
	    </div>

	    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
	        <span class="feedback">Product added!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
	        <span class="feedback">The product is already in the wishlist!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div style="clear:both"></div>
	    <div class="yith-wcwl-wishlistaddresponse"></div>
	
</div>

<div class="clear"></div><div class="quickview" data-id="260" title="Quick View">Quick View</div>    </div>
</div>
</div>
</li>
<li class="show-links-outimage post-251 product type-product status-publish has-post-thumbnail product_cat-accessories product_cat-posters product_tag-jacket product_tag-onepiece  instock shipping-taxable purchasable product-type-simple">
<div class="product-inner">
	
    <div class="product-image">
						<div class="loader-container"><div class="loader"><i class="porto-ajax-loader"></i></div></div>
				<div class="after-loading-success-message">
					<div class="background-overlay"></div>
					<div class="loader success-message-container">
						<div class="msg-box">
							<div class="msg">You've just added this product to the cart:<p class="product-name text-color-primary">Vostro</p></div>
							<img width="600" height="600" src="wp-content/uploads/sites/78/2013/06/2_14_4_1_1.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="" sizes="(max-width: 600px) 100vw, 600px" />						</div>
						<button class="button btn-primay viewcart" data-link="../../cart/index.php">Go to cart page</button>
						<button class="button btn-primay continue_shopping">Continue</button>
					</div>
				</div>
				
        <a  href="product/vostro/index.php">
            <div class="inner img-effect"><img width="300" height="300" src="wp-content/uploads/sites/78/2013/06/2_14_4_1_1-300x300.jpg" class=" wp-post-image" alt="" srcset="" sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="300" src="wp-content/uploads/sites/78/2013/06/1_1_4_4_1_1-300x300.jpg" class="hover-image" alt="" srcset="" sizes="(max-width: 300px) 100vw, 300px" /></div>        </a>

        <div class="links-on-image">
            <div class="add-links-wrap">
    <div class="add-links  clearfix">
        <a rel="nofollow" href="indexa528.html?add-to-cart=251" data-quantity="1" data-product_id="251" data-product_sku="9789798" class="viewcart-style-2 button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="251" data-product_sku="9789798" aria-label="Add &ldquo;Vostro&rdquo; to your cart" rel="nofollow">Add to cart</a>
<div class="yith-wcwl-add-to-wishlist add-to-wishlist-251">
		    <div class="yith-wcwl-add-button show" style="display:block">

	        <a href="indexc848.html?add_to_wishlist=251" rel="nofollow" data-product-id="251" data-product-type="simple" class="add_to_wishlist" >        Add to Wishlist</a><span class="ajax-loading"></span>
	    </div>

	    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
	        <span class="feedback">Product added!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
	        <span class="feedback">The product is already in the wishlist!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div style="clear:both"></div>
	    <div class="yith-wcwl-wishlistaddresponse"></div>
	
</div>

<div class="clear"></div><div class="quickview" data-id="251" title="Quick View">Quick View</div>    </div>
</div>        </div>
    </div>

	

<div class="rating-wrap">
    <span class="rating-before"><span class="rating-line"></span></span>
    <div class="rating-content"><div class="star-rating" title="4.00"><span style="width:80%"><strong class="rating">4.00</strong> out of 5</span></div></div>
    <span class="rating-after"><span class="rating-line"></span></span>
</div>


    
        <a class="product-loop-title"  href="product/vostro/index.php"><h3 class="woocommerce-loop-product__title">Vostro</h3></a>    
        <div class="description">
        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
    </div>
    
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>999.00</span></span>

    <div class="add-links-wrap">
    <div class="add-links  clearfix">
        <a rel="nofollow" href="indexa528.html?add-to-cart=251" data-quantity="1" data-product_id="251" data-product_sku="9789798" class="viewcart-style-2 button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="251" data-product_sku="9789798" aria-label="Add &ldquo;Vostro&rdquo; to your cart" rel="nofollow">Add to cart</a>
<div class="yith-wcwl-add-to-wishlist add-to-wishlist-251">
		    <div class="yith-wcwl-add-button show" style="display:block">

	        <a href="indexc848.html?add_to_wishlist=251" rel="nofollow" data-product-id="251" data-product-type="simple" class="add_to_wishlist" >        Add to Wishlist</a><span class="ajax-loading"></span>
	    </div>

	    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
	        <span class="feedback">Product added!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
	        <span class="feedback">The product is already in the wishlist!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div style="clear:both"></div>
	    <div class="yith-wcwl-wishlistaddresponse"></div>
	
</div>

<div class="clear"></div><div class="quickview" data-id="251" title="Quick View">Quick View</div>    </div>
</div>
</div>
</li>
<li class="show-links-outimage post-252 product type-product status-publish has-post-thumbnail product_cat-accessories product_cat-posters product_tag-clothes product_tag-polo last instock shipping-taxable purchasable product-type-variable has-children">
<div class="product-inner">
	
    <div class="product-image">
						<div class="loader-container"><div class="loader"><i class="porto-ajax-loader"></i></div></div>
				<div class="after-loading-success-message">
					<div class="background-overlay"></div>
					<div class="loader success-message-container">
						<div class="msg-box">
							<div class="msg">You've just added this product to the cart:<p class="product-name text-color-primary">ZN501 - Headphone</p></div>
							<img width="600" height="600" src="wp-content/uploads/sites/78/2013/06/5_17_2.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset= "" sizes="(max-width: 600px) 100vw, 600px" />						</div>
						<button class="button btn-primay viewcart" data-link="../../cart/index.php">Go to cart page</button>
						<button class="button btn-primay continue_shopping">Continue</button>
					</div>
				</div>
				
        <a  href="product/zn501-headphone/index.php">
            <div class="inner img-effect"><img width="300" height="300" src="wp-content/uploads/sites/78/2013/06/5_17_2-300x300.jpg" class=" wp-post-image" alt="" srcset="" sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="300" src="wp-content/uploads/sites/78/2013/06/4_1_12_2-300x300.jpg" class="hover-image" alt="" srcset= "" sizes="(max-width: 300px) 100vw, 300px" /></div>        </a>

        <div class="links-on-image">
            <div class="add-links-wrap">
    <div class="add-links  clearfix">
        <a rel="nofollow" href="product/zn501-headphone/index.php" data-quantity="1" data-product_id="252" data-product_sku="13244525" class="viewcart-style-2 button product_type_variable add_to_cart_button" data-product_id="252" data-product_sku="13244525" aria-label="Select options for &ldquo;ZN501 - Headphone&rdquo;" rel="nofollow">Select options</a>
<div class="yith-wcwl-add-to-wishlist add-to-wishlist-252">
		    <div class="yith-wcwl-add-button show" style="display:block">

	        <a href="index34b0.html?add_to_wishlist=252" rel="nofollow" data-product-id="252" data-product-type="variable" class="add_to_wishlist" >        Add to Wishlist</a><span class="ajax-loading"></span>
	    </div>

	    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
	        <span class="feedback">Product added!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
	        <span class="feedback">The product is already in the wishlist!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div style="clear:both"></div>
	    <div class="yith-wcwl-wishlistaddresponse"></div>
	
</div>

<div class="clear"></div><div class="quickview" data-id="252" title="Quick View">Quick View</div>    </div>
</div>        </div>
    </div>

	

<div class="rating-wrap">
    <span class="rating-before"><span class="rating-line"></span></span>
    <div class="rating-content"><div class="star-rating" title="4.00"><span style="width:80%"><strong class="rating">4.00</strong> out of 5</span></div></div>
    <span class="rating-after"><span class="rating-line"></span></span>
</div>


    
        <a class="product-loop-title"  href="product/zn501-headphone/index.php"><h3 class="woocommerce-loop-product__title">ZN501 &#8211; Headphone</h3></a>    
        <div class="description">
        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
    </div>
    
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>249.00</span></span>

    <div class="add-links-wrap">
    <div class="add-links  clearfix">
        <a rel="nofollow" href="product/zn501-headphone/index.php" data-quantity="1" data-product_id="252" data-product_sku="13244525" class="viewcart-style-2 button product_type_variable add_to_cart_button" data-product_id="252" data-product_sku="13244525" aria-label="Select options for &ldquo;ZN501 - Headphone&rdquo;" rel="nofollow">Select options</a>
<div class="yith-wcwl-add-to-wishlist add-to-wishlist-252">
		    <div class="yith-wcwl-add-button show" style="display:block">

	        <a href="index34b0.html?add_to_wishlist=252" rel="nofollow" data-product-id="252" data-product-type="variable" class="add_to_wishlist" >        Add to Wishlist</a><span class="ajax-loading"></span>
	    </div>

	    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
	        <span class="feedback">Product added!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
	        <span class="feedback">The product is already in the wishlist!</span>
	        <a href="wishlist/index.php" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div style="clear:both"></div>
	    <div class="yith-wcwl-wishlistaddresponse"></div>
	
</div>

<div class="clear"></div><div class="quickview" data-id="252" title="Quick View">Quick View</div>    </div>
</div>
</div>
</li>        </ul>
    </div>

    <div class="shop-loop-after clearfix">
	
<nav class="woocommerce-pagination">

    <form class="woocommerce-viewing" method="get">

        <label>Show: </label>

        <select name="count" class="count">
                            <option value="8"  selected='selected'>8</option>
                            <option value="16" >16</option>
                            <option value="32" >32</option>
                            <option value="48" >48</option>
                    </select>

        <input type="hidden" name="paged" value=""/>

            </form>

	</nav>    </div>

</div></main>


</div><!-- end main content -->

    <div class="col-lg-3 sidebar left-sidebar mobile-hide-sidebar"><!-- main sidebar -->
                <div class="sidebar-content">
                        <aside id="woocommerce_product_categories-2" class="widget woocommerce widget_product_categories"><h3 class="widget-title">Categories</h3><ul class="product-categories"><li class="cat-item cat-item-74 current-cat"><a href="index.php">Accessories</a> <span class="count">(6)</span></li>
<li class="cat-item cat-item-75"><a href="p_category_2.php">Albums</a> <span class="count">(2)</span></li>
<li class="cat-item cat-item-76 cat-parent"><a href="clothing/index.php">Clothing</a> <span class="count">(0)</span><ul class='children'>
<li class="cat-item cat-item-82"><a href="clothing/hoodies/index.php">Hoodies</a> <span class="count">(0)</span></li>
<li class="cat-item cat-item-85"><a href="clothing/singles/index.php">Singles</a> <span class="count">(0)</span></li>
</ul>
</li>
<li class="cat-item cat-item-77"><a href="dress/index.php">Dress</a> <span class="count">(0)</span></li>
<li class="cat-item cat-item-78"><a href="electronics/index.php">Electronics</a> <span class="count">(4)</span></li>
<li class="cat-item cat-item-79"><a href="fashion/index.php">Fashion</a> <span class="count">(0)</span></li>
<li class="cat-item cat-item-80"><a href="gifts/index.php">Gifts</a> <span class="count">(0)</span></li>
<li class="cat-item cat-item-81"><a href="home-garden/index.php">Home &amp; Garden</a> <span class="count">(0)</span></li>
<li class="cat-item cat-item-83"><a href="music/index.php">Music</a> <span class="count">(6)</span></li>
<li class="cat-item cat-item-84"><a href="posters/index.php">Posters</a> <span class="count">(4)</span></li>
<li class="cat-item cat-item-86"><a href="sports/index.php">Sports</a> <span class="count">(0)</span></li>
<li class="cat-item cat-item-87"><a href="t-shirts/index.php">T-shirts</a> <span class="count">(0)</span></li>
<li class="cat-item cat-item-88"><a href="test1/index.php">test1</a> <span class="count">(0)</span></li>
<li class="cat-item cat-item-99"><a href="uncategorized/index.php">Uncategorized</a> <span class="count">(0)</span></li>
<li class="cat-item cat-item-89"><a href="women/index.php">Women</a> <span class="count">(0)</span></li>
</ul></aside><aside id="yith-woo-ajax-reset-navigation-2" class="widget yith-woocommerce-ajax-product-filter yith-woo-ajax-reset-navigation yith-woo-ajax-navigation woocommerce widget_layered_nav" style="display:none"></aside><aside id="woocommerce_price_filter-3" class="widget woocommerce widget_price_filter"><h3 class="widget-title">Price</h3><form method="get" action="#/product-category/accessories/">
			<div class="price_slider_wrapper">
				<div class="price_slider" style="display:none;"></div>
				<div class="price_slider_amount">
					<input type="text" id="min_price" name="min_price" value="249" data-min="249" placeholder="Min price" />
					<input type="text" id="max_price" name="max_price" value="999" data-max="999" placeholder="Max price" />
					<button type="submit" class="button">Filter</button>
					<div class="price_label" style="display:none;">
						Price: <span class="from"></span> &mdash; <span class="to"></span>
					</div>
					
					<div class="clear"></div>
				</div>
			</div>
		</form></aside><aside id="woocommerce_layered_nav-2" class="widget woocommerce widget_layered_nav woocommerce-widget-layered-nav"><h3 class="widget-title">Color</h3><ul class="woocommerce-widget-layered-nav-list filter-item-list"><li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term "><a href="indexf9b2.html?filter_color=black" class="filter-color" style="background-color: #000000">Black</a></li><li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term "><a href="indexf641.html?filter_color=blue" class="filter-color" style="background-color: #11426b">Blue</a></li><li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term "><a href="indexe36b.html?filter_color=green" class="filter-color" style="background-color: #83c2bd">Green</a></li><li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term "><a href="indexdc2a.html?filter_color=indigo" class="filter-color" style="background-color: #6085a5">Indigo</a></li><li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term "><a href="index6e6e.html?filter_color=red" class="filter-color" style="background-color: #ab6e6e">Red</a></li></ul></aside><aside id="woocommerce_layered_nav-3" class="widget woocommerce widget_layered_nav woocommerce-widget-layered-nav"><h3 class="widget-title">Sizes</h3><ul class="woocommerce-widget-layered-nav-list"><li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term "><a href="indexd4bc.html?filter_size=large">L</a></li><li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term "><a href="index197e.html?filter_size=medium">M</a></li><li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term "><a href="indexad6d.html?filter_size=small">S</a></li><li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term "><a href="index7dfe.html?filter_size=extra-large">XL</a></li><li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term "><a href="index1318.html?filter_size=extra-small">XS</a></li></ul></aside><aside id="block-widget-5" class="widget widget-block"><h3 class="widget-title">Featured</h3>            <div class="block">
                <div class="porto-block "><div class="vc_row wpb_row row info-box-small-wrap m-t"><div class="vc_column_container col-md-12"><div class="wpb_wrapper vc_column-inner"><div class="porto-carousel owl-carousel  m-b-xl show-nav-title" data-plugin-options="{&quot;stagePadding&quot;:0,&quot;margin&quot;:0,&quot;autoplay&quot;:false,&quot;autoplayTimeout&quot;:5000,&quot;autoplayHoverPause&quot;:false,&quot;items&quot;:1,&quot;lg&quot;:1,&quot;md&quot;:1,&quot;sm&quot;:1,&quot;xs&quot;:1,&quot;nav&quot;:&quot;yes&quot;,&quot;dots&quot;:false,&quot;animateIn&quot;:&quot;slideInRight&quot;,&quot;animateOut&quot;:&quot;slideOutLeft&quot;,&quot;loop&quot;:&quot;yes&quot;,&quot;center&quot;:false,&quot;video&quot;:false,&quot;lazyLoad&quot;:false,&quot;fullscreen&quot;:false}"><div class="vc_widget_woo_products wpb_content_element"><div class="widget woocommerce widget_products"><ul class="product_list_widget">
<li>
    
	<a class="product-image" href="product_details.php" title="IdeaPad">
		<div class="inner img-effect"><img width="85" height="85" src="wp-content/uploads/sites/78/2013/06/1_1_4_4_1_1-85x85.jpg" class=" wp-post-image" alt="" srcset="" sizes="(max-width: 85px) 100vw, 85px" /><img width="85" height="85" src="wp-content/uploads/sites/78/2013/06/4_1_13_4_1_1-85x85.jpg" class="hover-image " alt="" srcset="" sizes="(max-width: 85px) 100vw, 85px" /></div>	</a>

    <div class="product-details">
        <a href="product_details.php" title="IdeaPad">
            IdeaPad        </a>

                    <div class="star-rating" title="4.50"><span style="width:90%"><strong class="rating">4.50</strong> out of 5</span></div>                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>1,999.00</span>    </div>

    </li>
<li>
    
	<a class="product-image" href="product_details.php" title="Headphone NK">
		<div class="inner img-effect"><img width="85" height="85" src="wp-content/uploads/sites/78/2013/06/4_1_12_2-85x85.jpg" class=" wp-post-image" alt="" srcset="" sizes="(max-width: 85px) 100vw, 85px" /><img width="85" height="85" src="" sizes="(max-width: 85px) 100vw, 85px" /></div>	</a>

    <div class="product-details">
        <a href="product_details.php" title="Headphone NK">
            Headphone NK        </a>

                    <div class="star-rating" title="5.00"><span style="width:100%"><strong class="rating">5.00</strong> out of 5</span></div>                <del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>299.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>289.00</span></ins>    </div>

    </li>
<li>
    
	<a class="product-image" href="product_details.php" title="NTX01 - Headphone">
		<div class="inner img-effect"><img width="85" height="85" src="wp-content/uploads/sites/78/2013/06/7_11-85x85.jpg" class=" wp-post-image" alt="" srcset="" sizes="(max-width: 85px) 100vw, 85px" /><img width="85" height="85" src="wp-content/uploads/sites/78/2013/06/2_13_2-85x85.jpg" class="hover-image " alt="" srcset="" sizes="(max-width: 85px) 100vw, 85px" /></div>	</a>

    <div class="product-details">
        <a href="product_details.php" title="NTX01 - Headphone">
            NTX01 - Headphone        </a>

                    <div class="star-rating" title="3.00"><span style="width:60%"><strong class="rating">3.00</strong> out of 5</span></div>                <del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>1,999.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>299.00</span></ins>    </div>

    </li></ul></div></div><div class="vc_widget_woo_products wpb_content_element"><div class="widget woocommerce widget_products"><ul class="product_list_widget">
<li>
    
	<a class="product-image" href="product_details.php" title="IdeaPad">
		<div class="inner img-effect"><img width="85" height="85" src="wp-content/uploads/sites/78/2013/06/1_1_4_4_1_1-85x85.jpg" class=" wp-post-image" alt="" srcset= "" sizes="(max-width: 85px) 100vw, 85px" /><img width="85" height="85" src="" sizes="(max-width: 85px) 100vw, 85px" /></div>	</a>

    <div class="product-details">
        <a href="product_details.php" title="IdeaPad">
            IdeaPad        </a>

                    <div class="star-rating" title="4.50"><span style="width:90%"><strong class="rating">4.50</strong> out of 5</span></div>                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>1,999.00</span>    </div>

    </li>
<li>
    
	<a class="product-image" href="product_details.php" title="Headphone NK">
		<div class="inner img-effect"><img width="85" height="85" src="" sizes="(max-width: 85px) 100vw, 85px" /><img width="85" height="85" src="wp-content/uploads/sites/78/2013/06/6_2_1-85x85.jpg" class="hover-image " alt="" srcset="" sizes="(max-width: 85px) 100vw, 85px" /></div>	</a>

    <div class="product-details">
        <a href="product_details.php" title="Headphone NK">
            Headphone NK        </a>

                    <div class="star-rating" title="5.00"><span style="width:100%"><strong class="rating">5.00</strong> out of 5</span></div>                <del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>299.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>289.00</span></ins>    </div>

    </li>
<li>
    
	<a class="product-image" href="product_details.php" title="NTX01 - Headphone">
		<div class="inner img-effect"><img width="85" height="85" src="" sizes="(max-width: 85px) 100vw, 85px" /><img width="85" height="85" src="wp-content/uploads/sites/78/2013/06/2_13_2-85x85.jpg" class="hover-image " alt="" srcset="" sizes="(max-width: 85px) 100vw, 85px" /></div>	</a>

    <div class="product-details">
        <a href="product_details.php" title="NTX01 - Headphone">
            NTX01 - Headphone        </a>

                    <div class="star-rating" title="3.00"><span style="width:60%"><strong class="rating">3.00</strong> out of 5</span></div>                <del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>1,999.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>299.00</span></ins>    </div>

    </li></ul></div></div></div><div class="porto-separator  "><hr class="separator-line  align_center"></div></div></div></div></div>            </div>
        </aside><aside id="text-2" class="widget widget_text"><h3 class="widget-title">CUSTOM HTML BLOCK</h3>			<div class="textwidget"><h5>This is a custom sub-title.</h5>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non placerat mi. Etiam non tellus </p>

<style>
.sidebar-content .widget .widget-title{margin-bottom: 0px!important;font-weight: 600!important;}
.sidebar-content .textwidget p{font-weight:400;font-size:15px;color:#21293c;line-height:1.42;}
.sidebar-content .textwidget h5{font-family:Open Sans;font-weight:600;font-size:14px;color:#7a7d82;line-height:1;margin-bottom:13px;}
</style></div>
		</aside>        </div>
            </div><!-- end main sidebar -->

    </div>
    </div>


        
        
            
            </div><!-- end main -->

            
         <?php include_once "footer.php";?>
		 
		 <!-- WP Super Cache is installed but broken. The constant WPCACHEHOME must be set in the file wp-config.php and point at the WP Super Cache plugin directory. -->