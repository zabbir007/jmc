<?php

include('server.php');
if (empty($_SESSION['usermarchantaccountlogin'])) { 
    header('location: login.php');
    ?>

<?php }
else {

include('header.php'); ?>

<body>

    <div id="wrapper">

        <?php //sidebar added here 
         include('sidebar.php');
        ?>

        <div id="page-wrapper">
            <?php if (isset($_GET['addproduct'])) { ?>

                       <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Product</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
         <?php include ('errors.php'); ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Product Information Required
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <form action="product-corner.php?addproduct=" method="POST">
                                <div class="col-lg-6">
                                    
                                        <div class="form-group">
                                            <label>Product Name<span style="color:red;">*</span></label>
                                            <input class="form-control" type="text" name="product_name" value="<?php echo $product_name; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Product Quantity<span style="color:red;">*</span></label>
                                            <input class="form-control" type="number" value="<?php echo $product_quantity; ?>" name="product_quantity" required>
                                        </div>
                                       <div class="form-group">
                                            <label>Per Quantity Price (Tk)<span style="color:red;">*</span></label>
                                            <input class="form-control" type="number" name="quanty_price" value="<?php echo $quanty_price; ?>" required>
                                        </div>    
                                        <div class="form-group">
                                            <label>Select Product Categories<span style="color:red;">*</span></label>
                                            <select name="categories" class="form-control">
                                                <?php $sql = "SELECT * FROM 0_Stock_category";
                                                   $result =  mysqli_query($db2, $sql);
                                                   while ($rowdata = mysqli_fetch_array($result)) { ?>
                                                       <option value="<?php echo $rowdata['category_id']; ?>"><?php echo $rowdata['description']; ?></option>
                                            <?php     }
                                                ?>
                                                
                                            </select>
                                        </div>     
                                        <div class="form-group">
                                            <label>Choose Main Image<span style="color:red;">*</span></label>
                                            <input class="form-control" type="file" name="img" required>
                                        </div>     
                                         <div class="form-group">
                                            <label>Choose alt Image 1</label>
                                            <input class="form-control" type="file" name="img1">
                                        </div>     
                                                                            
                                      
                                </div>
                               <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">
                                        <fieldset>
                                            <div class="form-group">
                                            <label>Enter Product Color (if)</label>
                                            <input class="form-control" type="text" name="color" value="<?php echo $product_color; ?>" placeholder="White">
                                        </div>
                                        <div class="form-group">
                                            <label>Enter Product Details<span style="color:red;">*</span></label>
                                            <textarea class="form-control" rows="3" name="product_details" required><?php echo $product_details; ?></textarea>
                                        </div>
                                          
                                         <div class="form-group">
                                            <label>Enter Producte Size (if)</label>
                                            <input class="form-control" name="product_size" placeholder="XL/M/S" type="text" value="<?php echo $product_size; ?>">
                                        </div> 
                                        <div class="form-group">
                                            <label>Choose alt Image 2</label>
                                            <input class="form-control" type="file" name="img2">
                                        </div>         
                                        <div class="form-group">
                                            <label>Choose alt Image 3</label>
                                            <input class="form-control" type="file" name="img3">
                                        </div>                                             
                                        </fieldset>                            
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                           
                            </div>
                                  
                        <button type="submit" name="addproduct" class="btn btn-primary">Add Product</button>
                    </form>
                            <!-- /.row (nested) -->
                        </div>
            
            <?php
            } ?>

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="vendor/raphael/raphael.min.js"></script>
    <script src="vendor/morrisjs/morris.min.js"></script>
    <script src="data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
<?php } ?>