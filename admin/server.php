<?php
session_start();
require_once('config.php');
$errors= array();
$book_img = "";

//login
if (isset($_POST['login'])) {
  $username = mysqli_real_escape_string($db,$_POST['username']);
  $password = mysqli_real_escape_string($db,$_POST['password']);

  $sqget = "SELECT * FROM adninlogin WHERE username = '$username' AND password = '$password' AND status = 1";
 $sqllogresult = mysqli_query($db, $sqget);
 if (mysqli_num_rows($sqllogresult) == 1) {
   $_SESSION['adminusersession'] = $username;
  header('location: index.php');
 } else{
  array_push($errors, "<center><p style='color:red; font-weight:bold;'>Incorrect Username or Password</p><center>" );

 }
 
}

if(isset($_GET['logout'])){
  session_destroy();
  unset($_SESSION['adminusersession']);
  header('location: index.php');
}


/*===================================
Add New Book, Update, Delete Portion Start
====================================*/
if (isset($_POST['add_book'])) {

  
   $book_name = mysqli_real_escape_string($db,$_POST['book_name']);
   $category = mysqli_real_escape_string($db,$_POST['category']);
   $subject_name = mysqli_real_escape_string($db,$_POST['subject']);
   $author_name = mysqli_real_escape_string($db,$_POST['author_name']);
   $books_quantity = mysqli_real_escape_string($db,$_POST['books_quantity']);
   $book_edition = mysqli_real_escape_string($db,$_POST['book_edition']);
   $department = mysqli_real_escape_string($db,$_POST['department']);
   $filetmp = $_FILES ["bookimg"]["tmp_name"];
   $filename = $_FILES ["bookimg"]["name"];
   $filetype = $_FILES ["bookimg"]["type"];
   $filepath = "photo/" .$filename;
   $move = "../photo/" .$filename;
   $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
	array_push($errors, "<center><p style='color:red; font-weight:bold;'>Invalid Image Format (Please Select jpg/jpeg/png/gif format).</p><center>" );
}
else{

	move_uploaded_file($_FILES["bookimg"]["tmp_name"], $move);
	$sqlinsert = "INSERT INTO books_table (book_name, book_author, available_copy, book_category, subject_name, image_path, edition, department) VALUES ('$book_name','$author_name','$books_quantity','$category','$subject_name','$filepath', '$book_edition', '$department')";
    mysqli_query($db, $sqlinsert); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Book Added Successfuly.</p><center>" );
   
 
}	

 }
//Update Book Details
 if (isset($_POST['update_book_details'])) {
 	$hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);
   $book_name = mysqli_real_escape_string($db,$_POST['book_name']);
   $category = mysqli_real_escape_string($db,$_POST['category']);
   $subject_name = mysqli_real_escape_string($db,$_POST['subject']);
   $author_name = mysqli_real_escape_string($db,$_POST['author_name']);
   $books_quantity = mysqli_real_escape_string($db,$_POST['books_quantity']);
   $book_edition = mysqli_real_escape_string($db,$_POST['book_edition']);
   $department = mysqli_real_escape_string($db,$_POST['department']);
   $filetmp = $_FILES ["bookimg"]["tmp_name"];
   $filename = $_FILES ["bookimg"]["name"];
   $filetype = $_FILES ["bookimg"]["type"];
   
   if (empty($filename)) {
   	$sqlupdate = "UPDATE books_table SET book_name = '$book_name', book_author = '$author_name', available_copy = '$books_quantity', book_category = '$category', subject_name = '$subject_name', edition = '$book_edition', department = '$department'  WHERE id = '$hiddenid'";
    mysqli_query($db, $sqlupdate); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Book Updated Successfuly.</p><center>" );
   } else {
  $filepath = "photo/" .$filename;
   $move = "../photo/" .$filename;
   $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats
	move_uploaded_file($_FILES["bookimg"]["tmp_name"], $move);
	$sqlupdate = "UPDATE books_table SET book_name = '$book_name', book_author = '$author_name', available_copy = '$books_quantity', book_category = '$category', subject_name = '$subject_name', image_path = '$filepath', edition = '$book_edition' WHERE id = '$hiddenid'";
    mysqli_query($db, $sqlupdate); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Book Updated Successfuly.</p><center>" );
   
 }
}
 if (isset($_POST['delete_book'])) {

 	$hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);

	$querydel ="DELETE FROM books_table WHERE id = '$hiddenid';";
    mysqli_query($db, $querydel);

    array_push($errors, "<center><h4 style='color:green;'>Delete Success <a href='book.php?addnewbooks='>Add New Book</a></h4><center>");
 }
/*===================================
Add New Book, Update, Delete Portion End
====================================*/

/*===================================
Categpory Add Update Delete Portion Start
====================================*/
if (isset($_POST['add_category'])) {
	
	$category_name = mysqli_real_escape_string($db,$_POST['category_name']);

    $query ="INSERT INTO category (category_name) VALUES ('$category_name')";
    mysqli_query($db, $query);

    array_push($errors, "<center><h4 style='color:green;'>New Category Added <a href='book.php?allcategorylist='>View All Category List</a></h4><center>");
   
}

if (isset($_POST['delete_category'])) {
$hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);

	$querydel ="DELETE FROM category WHERE id = '$hiddenid';";
    mysqli_query($db, $querydel);

    array_push($errors, "<center><h4 style='color:green;'>Delete Success <a href='book.php?addnewcategory='>Add New Category</a></h4><center>");
}

if (isset($_POST['update_edited_category'])) {
	$hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);
	$category_name_edit = mysqli_real_escape_string($db,$_POST['category_name_edit']);

	  $queryupdate ="UPDATE category SET category_name ='$category_name_edit' WHERE id = '$hiddenid'";
       mysqli_query($db, $queryupdate);

    array_push($errors, "<center><h4 style='color:green;'>Category Update Successfuly</h4><center>");
}
/*===================================
Categpory Add Update Delete Portion End
====================================*/

/*===================================
Subject Add Update Delete Portion Start
====================================*/
if (isset($_POST['add_subject'])) {
	$subject_name = mysqli_real_escape_string($db,$_POST['subject_name']);

    $querysubjectadd ="INSERT INTO subject (subject_name) VALUES ('$subject_name')";
    mysqli_query($db, $querysubjectadd);

    array_push($errors, "<center><h4 style='color:green;'>New Subject Added <a href='book.php?viewallsubject='>View All Subject List</a></h4><center>");
   
}
if (isset($_POST['delete_subject'])) {
$hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);

	$querydel ="DELETE FROM subject WHERE id = '$hiddenid';";
    mysqli_query($db, $querydel);

    array_push($errors, "<center><h4 style='color:green;'>Delete Success <a href='book.php?addnewsubject='>Add New Subject</a></h4><center>");
}
if (isset($_POST['update_edited_subject'])) {
	$hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);
	$subject_name_edit = mysqli_real_escape_string($db,$_POST['subject_name_edit']);

	  $queryupdate ="UPDATE subject SET subject_name ='$subject_name_edit' WHERE id = '$hiddenid'";
       mysqli_query($db, $queryupdate);

    array_push($errors, "<center><h4 style='color:green;'>Subject Name Update Successfuly</h4><center>");
}


/*===================================
Department Add Update Delete Portion Start
====================================*/
if (isset($_POST['add_department'])) {
  $department_name = mysqli_real_escape_string($db,$_POST['department_name']);

  $querydepartmentadd ="INSERT INTO department (department_name) VALUES ('$department_name')";
  mysqli_query($db, $querydepartmentadd);

  array_push($errors, "<center><h4 style='color:green;'>New Department Name Added <a href='department.php?viewalldepartment='>View All Department List</a></h4><center>");
   
}
if (isset($_POST['delete_department'])) {
$hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);

  $querydel ="DELETE FROM department WHERE id = '$hiddenid';";
    mysqli_query($db, $querydel);

    array_push($errors, "<center><h4 style='color:green;'>Delete Success <a href='department.php?addnewdepartment='>Add New Department</a></h4><center>");
}
if (isset($_POST['update_edited_department'])) {
  $hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);
  $department_name_edit = mysqli_real_escape_string($db,$_POST['department_name_edit']);

    $queryupdate ="UPDATE department SET department_name ='$department_name_edit' WHERE id = '$hiddenid'";
       mysqli_query($db, $queryupdate);

    array_push($errors, "<center><h4 style='color:green;'>Department Name Updated Successfuly</h4><center>");
}
/*===================================
Department Add Update Delete Portion End
====================================*/

/*===================================
Borrowed book request
====================================*/

if (isset($_POST['book_given'])) {
  $hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);
  $sqlresult = "UPDATE borrowing_list SET taking_status = '1' WHERE id = '$hiddenid'";
  mysqli_query($db, $sqlresult);

  $sql = "SELECT * FROM borrowing_list WHERE id = '$hiddenid'";
 $getresult = mysqli_query($db, $sql);
 $data = mysqli_fetch_array($getresult);
 $book_id = $data['book_id'];
 
  $sqlbooks_table = "SELECT * FROM books_table where id = '$book_id'";
    $result = mysqli_query($db, $sqlbooks_table);
    $data = mysqli_fetch_array($result);
    
    $datatotal = $data['available_copy'] - 1;

    $updatevalue = "UPDATE books_table SET available_copy = '$datatotal' WHERE id = '$book_id'";
    mysqli_query($db, $updatevalue);
  array_push($errors, "<center><h4 style='color:green;'>Books Given Successfuly</h4><center>");
}

if (isset($_POST['book_taking'])) {
 $hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);
 /*$student_name = mysqli_real_escape_string($db,$_POST['student_name']);
 $student_id = mysqli_real_escape_string($db,$_POST['student_id']);
 $student_email = mysqli_real_escape_string($db,$_POST['student_email']);
 $department = mysqli_real_escape_string($db,$_POST['department']);
  $fine_price = mysqli_real_escape_string($db,$_POST['fine_price']); */


 $sql = "SELECT * FROM borrowing_list WHERE id = '$hiddenid'";

 $getresult = mysqli_query($db, $sql);
 $data = mysqli_fetch_array($getresult);
 $book_id = $data['book_id'];

 $bookcopyget = "SELECT * FROM books_table WHERE id = '$book_id'";
 $book_result = mysqli_query($db, $bookcopyget);
 $book_data = mysqli_fetch_array($book_result);
 $update_copy = $book_data['available_copy']+1;

 $bookcopytupdate = "UPDATE books_table SET available_copy = '$update_copy' WHERE id = '$book_id'";
 mysqli_query($db, $bookcopytupdate);

 $sqldelete = "DELETE FROM borrowing_list WHERE id = '$hiddenid'";
 mysqli_query($db,  $sqldelete);
  array_push($errors, "<center><h4 style='color:green;'>Books Recieved Successfuly</h4><center>");
}

/*===================================
Set Fines Ammount
====================================*/
if (isset($_POST['set_ammount'])) {
  $ammount = mysqli_real_escape_string($db,$_POST['ammount']);
  $sql = "SELECT * FROM fines";
  $resultnew = mysqli_query($db, $sql);
  if (mysqli_num_rows($resultnew) == 0) {
    $sqlfineamount = "INSERT INTO fines (amount) VALUES('$ammount')";
    mysqli_query($db, $sqlfineamount);

    array_push($errors, "<center><h4 style='color:green;'>Fines Ammount Set Successfuly</h4><center>");
  }else{
     $sqlfineamount = "UPDATE fines SET amount = '$ammount'";
    mysqli_query($db, $sqlfineamount);

    array_push($errors, "<center><h4 style='color:green;'>Fines Ammount Changed Successfuly</h4><center>");
  }
}
if (isset($_POST['set_days'])) {
  $dayss = mysqli_real_escape_string($db,$_POST['dayss']);
  $sql = "SELECT * FROM fines";
  $resultnew = mysqli_query($db, $sql);
  if (mysqli_num_rows($resultnew) == 0) {
    $sqlfineamount = "INSERT INTO fines (days) VALUES('$dayss')";
    mysqli_query($db, $sqlfineamount);

    array_push($errors, "<center><h4 style='color:green;'>Days Set Successfuly</h4><center>");
  }else{
     $sqlfineamount = "UPDATE fines SET days = '$dayss'";
    mysqli_query($db, $sqlfineamount);

    array_push($errors, "<center><h4 style='color:green;'>Days Changed Successfuly</h4><center>");
  }
}
/*===================================
Set Header Text
====================================*/
if (isset($_POST['set_header'])){
   $header = mysqli_real_escape_string($db,$_POST['header']);
  $sql = "SELECT * FROM header";
  $resultnew = mysqli_query($db, $sql);
  if (mysqli_num_rows($resultnew) == 0) {
    $sqlfineamount = "INSERT INTO header (header_text) VALUES('$header')";
    mysqli_query($db, $sqlfineamount);

    array_push($errors, "<center><h4 style='color:green;'>Header Text Set Successfuly</h4><center>");
  }else{
     $sqlfineamount = "UPDATE header SET header_text = '$header'";
    mysqli_query($db, $sqlfineamount);

    array_push($errors, "<center><h4 style='color:green;'>Header Text Changed Successfuly</h4><center>");
  }
}
/*===================================
Set Footer Text
====================================*/
if (isset($_POST['set_footer'])){
   $footer = mysqli_real_escape_string($db,$_POST['footer']);
  $sql = "SELECT * FROM footer";
  $resultnew = mysqli_query($db, $sql);
  if (mysqli_num_rows($resultnew) == 0) {
    $sqlfineamount = "INSERT INTO footer (footer_text) VALUES('$footer')";
    mysqli_query($db, $sqlfineamount);

    array_push($errors, "<center><h4 style='color:green;'>Footer Text Set Successfuly</h4><center>");
  }else{
     $sqlfineamount = "UPDATE footer SET footer_text = '$footer'";
    mysqli_query($db, $sqlfineamount);

    array_push($errors, "<center><h4 style='color:green;'>Footer Text Changed Successfuly</h4><center>");
  }
}

/*===================================
Set Library Hours
====================================*/

if (isset($_POST['set_library_hours'])){

   $libraryhours1 = mysqli_real_escape_string($db,$_POST['libraryhours1']);
   $libraryhours2 = mysqli_real_escape_string($db,$_POST['libraryhours2']);
   $offday = mysqli_real_escape_string($db,$_POST['offday']);

  $sql = "SELECT * FROM library_hours";
  $resultnew = mysqli_query($db, $sql);
  if (mysqli_num_rows($resultnew) == 0) {
    $sqlfineamount = "INSERT INTO library_hours (working_hours, offday) VALUES('$libraryhours1 To $libraryhours2', '$offday')";
    mysqli_query($db, $sqlfineamount);

    array_push($errors, "<center><h4 style='color:green;'>Library Hours Updated</h4><center>");
  }else{
     $sqlfineamount = "UPDATE library_hours SET working_hours = '$libraryhours1 To $libraryhours2', offday = '$offday'";
    mysqli_query($db, $sqlfineamount);

    array_push($errors, "<center><h4 style='color:green;'>Library Hours Changed Successfuly</h4><center>");
  }
}



?>