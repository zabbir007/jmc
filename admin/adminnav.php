<div class="sidenav">
  <a href="logo.php"><i class="fa fa-image"></i> Change logo</a>
  <a href="change.php?changetitle="><i class="fa fa-info-circle"></i> Change Title</a>
  <a href="change.php?changefootercontactnumber="><i class="fa fa-mobile-phone"></i> Change Footer No.</a>
  <a href="change.php?changeemail="><i class="fa fa-envelope"></i> Change Email Address</a>
  <a href="change.php?changeofficeaddress="><i class="fa fa-building"></i> Change Office Address</a>
  <a href="add_slider.php"><i class="fa fa-file-image-o" aria-hidden="true"></i> Change Home Slider</a>
  <a href="add_product_group.php"><strong style="color:orange;"><i class="fa fa-building"></i> Create Menu</strong></a>
   <a href="add-stock-img.php"><strong style="color:orange;"><i class="fa fa-building"></i> Add Sub Menu Image</strong></a>
  <a href="user_info.php"><strong style="color:black;"><i class="fa fa-user-circle-o"></i> User Info</a></strong>
  <a href="show_order.php"><strong style="color:black;"><i class="fa fa-list-alt"></i> Show Order</a></strong>
  <a href="change.php?changefootertext="><i class="fa fa-copyright"></i> Change Footer Text</a>
  <a href="change.php?changeworkinghour="><i class="fa fa-clock-o"></i> Change Working Hour</a>
  <a href="change.php?changemap="><i class="fa fa-map-marker"></i> Change Map</a>
</div>
