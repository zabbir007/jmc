
                    <!-- header wrapper -->
					
					<?php include_once("header.php");?>
					
                                 <!-- end header wrapper -->
        
        
                <section class="page-top page-header-6">
        <div class="container hide-title">
    <div class="row">
        <div class="col-lg-12 clearfix">
            <div class="pt-right d-none">
                <h1 class="page-title">Careers</h1>
                            </div>
                            <div class="breadcrumbs-wrap pt-left">
                    <ul class="breadcrumb"><li class="home"><a itemprop="url" href="#" title="Go to Home Page"><span itemprop="title">Home</span></a><i class="delimiter delimiter-2"></i></li><li>Careers</li></ul>                </div>
                                </div>
    </div>
</div>    </section>
    
        <div id="main" class="column1 boxed"><!-- main -->

            
                        <div class="container">
                            
            
            <div class="row main-content-wrap">

            <!-- main content -->
            <div class="main-content col-lg-12">

                            
    <div id="content" role="main">
                
            <article class="post-68 page type-page status-publish hentry">
                
                <span class="entry-title" style="display: none;">Careers</span><span class="vcard" style="display: none;"><span class="fn"><a href="author/porto_admin/index.php" title="Posts by Joe Doe" rel="author">Joe Doe</a></span></span><span class="updated" style="display:none">2017-12-17T05:45:05+00:00</span>
                <div class="page-content">
                    <div class="vc_row wpb_row row"><div class="vc_column_container col-md-12 col-12"><div class="wpb_wrapper vc_column-inner"><h2  class="vc_custom_heading align-left"><strong>Rockstars</strong> wanted!</h2>
	<div class="wpb_text_column wpb_content_element  featured lead m-b-xl" >
		<div class="wpb_wrapper">
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus. pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.</p>

		</div>
	</div>
<div class="porto-separator  "><hr class="separator-line  align_center"></div></div></div></div><div class="vc_row wpb_row row"><div class="m-t-md vc_column_container col-md-12 col-lg-8"><div class="wpb_wrapper vc_column-inner"><section class="toggle  "><label>Technical Support Representative</label><div class="toggle-content"><p><strong>Location:</strong> New York &#8211; <strong>Department:</strong> Engineering</p>
<p>Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra.</p>
<p><a class="btn btn-primary push-bottom" href="#">Apply Now</a></p>
</div></section><section class="toggle  "><label>Copywriter</label><div class="toggle-content"><p><strong>Location:</strong> New York &#8211; <strong>Department:</strong> Engineering</p>
<p>Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra.</p>
<p><a class="btn btn-primary push-bottom" href="#">Apply Now</a></p>
</div></section><section class="toggle  "><label>Customer Care Specialist</label><div class="toggle-content"><p><strong>Location:</strong> New York &#8211; <strong>Department:</strong> Engineering</p>
<p>Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra.</p>
<p><a class="btn btn-primary push-bottom" href="#">Apply Now</a></p>
</div></section><section class="toggle  "><label>Interactive Art Director</label><div class="toggle-content"><p><strong>Location:</strong> New York &#8211; <strong>Department:</strong> Engineering</p>
<p>Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra.</p>
<p><a class="btn btn-primary push-bottom" href="#">Apply Now</a></p>
</div></section><section class="toggle  "><label>Mobile Developer</label><div class="toggle-content"><p><strong>Location:</strong> New York &#8211; <strong>Department:</strong> Engineering</p>
<p>Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra.</p>
<p><a class="btn btn-primary push-bottom" href="#">Apply Now</a></p>
</div></section><section class="toggle  "><label>Technical Support Representative</label><div class="toggle-content"><p><strong>Location:</strong> New York &#8211; <strong>Department:</strong> Engineering</p>
<p>Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra.</p>
<p><a class="btn btn-primary push-bottom" href="#">Apply Now</a></p>
</div></section><section class="toggle  "><label>Copywriter</label><div class="toggle-content"><p><strong>Location:</strong> New York &#8211; <strong>Department:</strong> Engineering</p>
<p>Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra.</p>
<p><a class="btn btn-primary push-bottom" href="#">Apply Now</a></p>
</div></section><section class="toggle  "><label>Customer Care Specialist</label><div class="toggle-content"><p><strong>Location:</strong> New York &#8211; <strong>Department:</strong> Engineering</p>
<p>Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra.</p>
<p><a class="btn btn-primary push-bottom" href="#">Apply Now</a></p>
</div></section><section class="toggle  "><label>Interactive Art Director</label><div class="toggle-content"><p><strong>Location:</strong> New York &#8211; <strong>Department:</strong> Engineering</p>
<p>Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra.</p>
<p><a class="btn btn-primary push-bottom" href="#">Apply Now</a></p>
</div></section><section class="toggle  "><label>Mobile Developer</label><div class="toggle-content"><p><strong>Location:</strong> New York &#8211; <strong>Department:</strong> Engineering</p>
<p>Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis ligula ut ipsum mattis viverra.</p>
<p><a class="btn btn-primary push-bottom" href="#">Apply Now</a></p>
</div></section></div></div><div class="vc_column_container col-md-12 col-lg-4"><div class="wpb_wrapper vc_column-inner"><div class="porto-content-box featured-boxes wpb_content_element  featured-box-primary"><div class="featured-box  align-center" style=""><div class="box-content" style="">
	<div class="wpb_flickr_widget wpb_content_element m-t-n-sm" >
		<div class="wpb_wrapper">
			<h2 class="wpb_heading wpb_flickr_heading">The Benefits</h2>
			<script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=9&amp;display=latest&amp;size=s&amp;layout=x&amp;source=user&amp;user=93691989@N03"></script>
			<p class="flickr_stream_wrap"><a class="wpb_follow_btn wpb_flickr_stream" href="http://www.flickr.com/photos/93691989@N03">View stream on flickr</a></p>
		</div>
	</div>
<div class="porto-separator  "><hr class="separator-line  align_center"></div>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<ul class="list icons list-unstyled">
<li><i class="fa fa-check"></i>Fusce sit orci quis arcu vestibulum.</li>
<li><i class="fa fa-check"></i>Fusce sit orci quis arcu vestibulum.</li>
<li><i class="fa fa-check"></i>Fusce sit orci quis arcu vestibulum.</li>
</ul>

		</div>
	</div>
</div></div></div></div></div></div>
                </div>
            </article>

            <div class="">
            
                        </div>

        
    </div>

        

</div><!-- end main content -->


    </div>
    </div>


        
            
            </div><!-- end main -->

            
             <?php include_once "footer.php";?>
			 
			 <!-- WP Super Cache is installed but broken. The constant WPCACHEHOME must be set in the file wp-config.php and point at the WP Super Cache plugin directory. -->