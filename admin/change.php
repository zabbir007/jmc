<?php include ('admincon.php');
if (empty($_SESSION['inthemainathorityaccessadmincontrolifthatsare'])) {

     header('location: login.php');

 } else {
 include('header.php');

 ?>

<body>

    <div id="wrapper">

        <?php //sidebar added here 
         include('sidebar.php');
        ?>

        <div id="page-wrapper">
      <?php include ('errors.php'); ?>

<?php if (isset($_GET['changelogo'])) { ?>
  <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Change Logo</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
             <?php $sqllogo = "SELECT * FROM logo";
        $logoresult = mysqli_query($db, $sqllogo);
        $datalogo = mysqli_fetch_array($logoresult);?>
             <?php
if (isset($_POST['changeimg'])) { ?>
  
<div class="row">
<form action="change.php?changelogo=" method="POST"  enctype=multipart/form-data>
  <div class="form-group">
     <input type="hidden" name="hiddenlogoid" value="<?php echo $datalogo['id']; ?>">
     <input type="file" name="logo" required>
     <div>
      <br>

  <div class="form-group">
  <input type="submit" name="changeimage" class="btn btn-primary" value="Update Image"> 
  </div>
</form>
</div>
<?php
} 
if (isset($_POST['changeheadertext'])) { ?>
  
<div class="row">
<form action="change.php?changelogo=" method="POST" >
  <div class="form-group">
     <input type="hidden" name="hiddenlogoid" value="<?php echo $datalogo['id']; ?>">
          <textarea style="padding: 10px; font-size: 18px;" placeholder="Enter Your Number" name="changenumber" required></textarea>
     <div>
      <br>

  <div class="form-group">
  <input type="submit" name="changeheadernumber" class="btn btn-primary" value="Update Number"> 
  </div>
</form>
</div>
<?php
} ?>
<center>
               <div class="row col-lg-3">
             
  <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Current Image</th>
      <th scope="col">Current Header Call Text</th>
    </tr>
  </thead>
  <tbody>

       
          <tr>
        <td><img src="../<?php echo $datalogo['logo_path']; ?>" width="110px"></td>
        <td><h3><?php echo $datalogo['header_contact_text']; ?></h3></td>
        </tr>
     <tr>
      <form action="change.php?changelogo=" method="POST">
      <td><button type="submit" name="changeimg" class="btn btn-link">Change image</button></td>
      <td><button type="submit" name="changeheadertext" class="btn btn-link">Change Header Call Text</button></td>
      </form>
     </tr>
   </tbody>
 </table>
</div>
</center>
<?php

}
?>
<?php
if (isset($_GET['changetitle'])) {
  

  ?>
  <center><h2 style="text-decoration: underline;">Change Title and Title Logo</h2></center>

  <center>
    <table>
      <tr>
        <th>Current Title Logo</th>
        <th>Current Title</th>
      </tr>
      
        <?php $sqllogo = "SELECT * FROM web_attribut_info";
        $logoresult = mysqli_query($db, $sqllogo);
        while ($row = mysqli_fetch_array($logoresult)) { ?>
          <tr>
        <td><img src="../<?php echo $row['icon']; ?>" width="110px"></td>
        <td><strong><?php echo $row['title']; ?></strong></td>
        </tr>
      <tr>
        <form action="change.php?changetitle=" method="POST">
        <td style="border:1px solid white;"><button type="submit" class="btn btn-link" name="changetitleicon">Change Title Logo</button></td>
        <td style="border:1px solid white;"><button type="submit" class="btn btn-link" name="changetitletext">Change Title</button></td>
        </form>
      </tr>
    </table>
  </center>

<?php 
if (isset($_POST['changetitleicon'])) { ?>
<center>
   <div style="padding-top: 20px;">
   <h3 style="text-decoration: underline;">Change Title Logo</h3>
     <form action="change.php?changetitle=" method="POST"  enctype=multipart/form-data>
     <input type="hidden" name="hiddenlogoid" value="<?php echo $row['id']; ?>">
  <input type="file" name="icon" required>
  <br>
  <br>
  <input type="submit" name="changeicon" value="Update Image"> 
</form>
</div>
</center>
<?php }


if (isset($_POST['changetitletext'])) { ?>
<center>
    <div style="padding-top: 20px;">
      <h3 style="text-decoration: underline;">Change Title</h3>
       <form action="change.php?changetitle=" method="POST">
             <input type="hidden" name="hiddenlogoid" value="<?php echo $row['id']; ?>">
          <textarea style="padding: 10px; font-size: 18px;" placeholder="Enter Title Name" name="changetitlet" required></textarea>
          <br>
          <br>
          <input type="submit" name="changetitletxt" value="Update Title Text"> 
</form>
</div>
</center>
<?php }

} //While End
} //end change title 
if (isset($_GET['changefootercontactnumber'])) {  ?>
         
  <center><h2 style="text-decoration: underline;">Change Footer Contact Number</h2></center>

  <center>
    <table>
      <tr>
        <th>Current Footer Contact Info</th>
      </tr>
      
        <?php $sqllogo = "SELECT * FROM web_attribut_info";
        $logoresult = mysqli_query($db, $sqllogo);
        while ($row = mysqli_fetch_array($logoresult)) { ?>
        <tr>
        <td><center><strong><?php echo $row['mobile']; ?></strong></center></td>
        </tr>
        <tr>
        <form action="change.php?changefootercontactnumber=" method="POST">
        <td style="border:1px solid white;"><button class="btn btn-link" type="submit" name="changecontact">Change Contact Info</button></td>
        </form>
      </tr>
    </table>
  </center>

<?php
if (isset($_POST['changecontact'])) { ?>
  <center>
    <div style="padding-top: 20px;">
      <h3 style="text-decoration: underline;">Change Footer Contact</h3>
       <form action="change.php?changefootercontactnumber=" method="POST">
             <input type="hidden" name="hiddenlogoid" value="<?php echo $row['id']; ?>">
          <textarea style="padding: 10px; font-size: 18px;" placeholder="01169565654, 5989565965" name="changefcontact" required></textarea>
          <br>
          <br>
          <input type="submit" class="btn btn-link" name="changefcon" value="Update Number"> 
</form>
</div>
</center>

<?php }

 } //while end

}//end change footer contact

if (isset($_GET['flashsale'])) { ?>
  <div class="row">
    <div class="col-lg-12">
    <h2 class="page-header"><i class="fa fa-bolt"></i> Flash Sale Status</h2>
    </div>
    <!-- /.col-lg-12 -->
    </div>
    <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-body">
        <?php $sqlstats = "SELECT * FROM flashsalestats";
        $sqlget = mysqli_query($db, $sqlstats);
        $datastats = mysqli_fetch_array($sqlget);
        if ($datastats['stats'] == 0) { ?>
        <center>
          <h3 class="text-danger">Flash Sale are currently closed</h3>
          <form action="change.php?flashsale=" method="POST">
          <button class="btn btn-success" name="registrationon" type="submit"> Start Sale</button>
         </form>
        </center>
<?php
                  }
         
          if ($datastats['stats'] == 1) { ?>
        <center>
          <h3 class="text-success">Flash Sale are Open Now</h3>
          <form action="change.php?flashsale=" method="POST">
          <button class="btn btn-danger" name="registrationoff" type="submit"> End Sale</button>
          </form>
        </center>
<?php
                  }
         ?>


        </div>
      </div>
    </div>
    <?php
}

if (isset($_GET['flashsaletime'])) { ?>
  <div class="row">
    <div class="col-lg-12">
    <h2 class="page-header"><i class="fa fa-bolt"></i> Flash Sale Start and Ending Time</h2>
    </div>
    <!-- /.col-lg-12 -->
    </div>
    <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-body">
        
           <div class="form-group">
    <label for="exampleFormControlInput1">Start In</label>
    <input type="date" class="form-control" id="exampleFormControlInput1">
  </div>
      <div class="form-group">
    <label for="exampleFormControlInput1">Start In</label>
    <input type="date" class="form-control" id="exampleFormControlInput1">
  </div>
        </div>
      </div>
    </div>
    <?php
}
//start email change
if (isset($_GET['changeemail'])) { ?>
  <center><h2 style="text-decoration: underline;">Change Email Address</h2></center>
  <center>
    <table>
      <tr>
        <th>Current Email Address</th>
      </tr>
      
        <?php $sqllogo = "SELECT * FROM web_attribut_info";
        $logoresult = mysqli_query($db, $sqllogo);
        while ($row = mysqli_fetch_array($logoresult)) { ?>
          <tr>
        <td><strong><?php echo $row['email']; ?></strong></td>
        </tr>
          <tr>
        <form action="change.php?changeemail=" method="POST">
        <td style="border:1px solid white;"><button class="btn btn-link" type="submit" name="changeemail">Change Email Address</button></td>
        </form>
      </tr>
    </table>
  </center>
  <?php

  if (isset($_POST['changeemail'])) { ?>
  
<center>
    <div style="padding-top: 20px;">
      <h3 style="text-decoration: underline;">Change Email Address</h3>
       <form action="change.php?changeemail=" method="POST">
             <input type="hidden" name="hiddenlogoid" value="<?php echo $row['id']; ?>">
          <textarea style="padding: 10px; font-size: 18px;" placeholder="info@example.com, support@example.com" name="changeemailt" required></textarea>
          <br>
          <br>
          <input type="submit" class="btn btn-link" name="changeemailup" value="Update Email"> 
</form>
</div>
</center>

<?php    } 
} //end while
}//end change email

//start change office address
if (isset($_GET['changeofficeaddress'])) { ?>
  <center><h2 style="text-decoration: underline;">Change Office Address</h2></center>

  <center>
    <table>
      <tr>
        <th>Current Office Address</th>
      </tr>
      
        <?php $sqllogo = "SELECT * FROM web_attribut_info";
        $logoresult = mysqli_query($db, $sqllogo);
        while ($row = mysqli_fetch_array($logoresult)) { ?>
          <tr>
        <td><strong><?php echo $row['address']; ?></strong></td>
        </tr>
          <tr>
        <form action="change.php?changeofficeaddress=" method="POST">
        <td style="border:1px solid white;"><button class="btn btn-link" type="submit" name="editofficeaddress">Change Office Address</button></td>
        </form>
      </tr>
    </table>
  </center>
<?php
if (isset($_POST['editofficeaddress'])) { ?>
  <center>
    <div style="padding-top: 20px;">
      <h3 style="text-decoration: underline;">Change Office Address</h3>
       <form action="change.php?changeofficeaddress=" method="POST">
             <input type="hidden" name="hiddenlogoid" value="<?php echo $row['id']; ?>">
          <textarea style="padding: 10px; font-size: 18px;" placeholder="Shop-942, Rad-5, Sec- 7, Milk Vita Road, Mirpur, Dhaka-1216" name="changeoffice" required></textarea>
          <br>
          <br>
          <input type="submit" class="btn btn-link" name="changeofficeup" value="Update Address"> 
</form>
</div>
</center>

  <?php
}

 }//While end

}

if (isset($_GET['changefootertext'])) { ?>
  <center><h2 style="text-decoration: underline;">Change Footer Text</h2></center>

  <center>
    <table>
      <tr>
        <th>Current Footer Text</th>
      </tr>
      
        <?php $sqllogo = "SELECT * FROM footer";
        $logoresult = mysqli_query($db, $sqllogo);
        while ($row = mysqli_fetch_array($logoresult)) { ?>
          <tr>
        <td><strong><?php echo $row['footer_text']; ?></strong></td>
        </tr>
          <tr>
        <form action="change.php?changefootertext=" method="POST">
        <td style="border:1px solid white;"><button class="btn btn-link" type="submit" name="editfootertext">Change Footer Text</button></td>
        </form>
      </tr>
    </table>
  </center>
<?php
if (isset($_POST['editfootertext'])) { ?>
  <center>
    <div style="padding-top: 20px;">
      <h3 style="text-decoration: underline;">Change Footer Text</h3>
       <form action="change.php?changefootertext=" method="POST">
             <input type="hidden" name="hiddenlogoid" value="<?php echo $row['id']; ?>">
          <textarea style="padding: 10px; font-size: 18px;" placeholder="©2018.All right reserved NGICON" name="changefooter" required></textarea>
          <br>
          <br>
          <input type="submit" class="btn btn-link" name="changefootertextup" value="Change Footer Text"> 
</form>
</div>
</center>

  <?php
}

 }//While end

}
//change working hour
if (isset($_GET['changeworkinghour'])) { ?>
  <center><h2 style="text-decoration: underline;">Change Working Hour</h2></center>

  <center>
    <table>
      <tr>
        <th>Current Working Day</th>
        <th>Current Working Hour</th>
      </tr>
      
        <?php $sqllogo = "SELECT * FROM working_hour";
        $logoresult = mysqli_query($db, $sqllogo);
        while ($row = mysqli_fetch_array($logoresult)) { ?>
          <tr>
        <td><strong><?php echo $row['mon_fri']; ?></strong></td>
        <td><strong><?php echo $row['saturday']; ?></strong></td>
        </tr>
          <tr>
        <form action="change.php?changeworkinghour=" method="POST">
        <td style="border:1px solid white;"><button type="submit" class="btn btn-link" name="editworkinghour">Change Working Day And Hour</button></td>
        </form>
      </tr>
    </table>
  </center>
<?php
if (isset($_POST['editworkinghour'])) { ?>
  <center>
    <div style="padding-top: 20px;">
      <h3 style="text-decoration: underline;">Change Working Day</h3>
       <form action="change.php?changeworkinghour=" method="POST">
             <input type="hidden" name="hiddenlogoid" value="<?php echo $row['id']; ?>">
         <select name="day1">
          <option value="Saturday">Saturday</option>
          <option value="Sunday">Sunday</option>
          <option value="Monday">Monday</option>
          <option value="Tuesday">Tuesday</option>
          <option value="Wednesday">Wednesday</option>
          <option value="Thursday">Thursday</option>
          <option value="Friday">Friday</option>
         </select> To <select name="day2">
          <option value="Saturday">Saturday</option>
          <option value="Sunday">Sunday</option>
          <option value="Monday">Monday</option>
          <option value="Tuesday">Tuesday</option>
          <option value="Wednesday">Wednesday</option>
          <option value="Thursday">Thursday</option>
          <option value="Friday">Friday</option>
         </select>
          <br>
          <br>
          <h3 style="text-decoration: underline;">Change Working Hour</h3>
          <select name="time1">
          <option value="04.00 am">04.00 am</option>
          <option value="05.00 am">05.00 am</option>
          <option value="06.00 am">06.00 am</option>
          <option value="07.00 am">07.00 am</option>
          <option value="08.00 am">08.00 am</option>
          <option value="09.00 am">09.00 am</option>
          <option value="10.00 am">10.00 am</option>
          <option value="11.00 am">11.00 am</option>
          <option value="12.00 pm">12.00 pm</option>
          <option value="01.00 pm">01.00 pm</option>
          <option value="02.00 pm">02.00 pm</option>
          <option value="03.00 pm">03.00 pm</option>
          <option value="04.00 pm">04.00 pm</option>
          <option value="05.00 pm">05.00 pm</option>
          <option value="06.00 pm">06.00 pm</option>
          <option value="07.00 pm">07.00 pm</option>
          <option value="08.00 pm">08.00 pm</option>
          <option value="09.00 pm">09.00 pm</option>
          <option value="10.00 pm">10.00 pm</option>
          <option value="11.00 pm">11.00 pm</option>
          <option value="12.00 am">12.00 am</option>
          <option value="01.00 am">01.00 am</option>
          <option value="02.00 am">02.00 am</option>
          <option value="03.00 am">03.00 am</option>
         </select> To <select name="time2">
          <option value="04.00 am">04.00 am</option>
          <option value="05.00 am">05.00 am</option>
          <option value="06.00 am">06.00 am</option>
          <option value="07.00 am">07.00 am</option>
          <option value="08.00 am">08.00 am</option>
          <option value="09.00 am">09.00 am</option>
          <option value="10.00 am">10.00 am</option>
          <option value="11.00 am">11.00 am</option>
          <option value="12.00 pm">12.00 pm</option>
          <option value="01.00 pm">01.00 pm</option>
          <option value="02.00 pm">02.00 pm</option>
          <option value="03.00 pm">03.00 pm</option>
          <option value="04.00 pm">04.00 pm</option>
          <option value="05.00 pm">05.00 pm</option>
          <option value="06.00 pm">06.00 pm</option>
          <option value="07.00 pm">07.00 pm</option>
          <option value="08.00 pm">08.00 pm</option>
          <option value="09.00 pm">09.00 pm</option>
          <option value="10.00 pm">10.00 pm</option>
          <option value="11.00 pm">11.00 pm</option>
          <option value="12.00 am">12.00 am</option>
          <option value="01.00 am">01.00 am</option>
          <option value="02.00 am">02.00 am</option>
          <option value="03.00 am">03.00 am</option>
         </select>
          <br>
          <br>
          <input type="submit" class="btn btn-link" name="changeworkinghour" value="Change Working Day & Hour"> 
</form>
</div>
</center>

  <?php
}

 }//While end

}
//change map 
if (isset($_GET['changemap'])) { ?>
  <center><h2 style="text-decoration: underline;">Change Map</h2></center>

  
      
        <?php $sqllogo = "SELECT * FROM map";
        $logoresult = mysqli_query($db, $sqllogo);
        while ($row = mysqli_fetch_array($logoresult)) { ?>
      
  <center>
    <div style="padding-top: 20px;">
       <form action="change.php?changemap=" method="POST">
             <input type="hidden" name="hiddenlogoid" value="<?php echo $row['id']; ?>">
          <textarea style="padding: 10px; font-size: 18px;" placeholder='<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d116771.43426670252!2d90.3102889645064!3d23.850322523727314!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfb7e8791e2545121!2sNGICON!5e0!3m2!1sbn!2sbd!4v1524898766874" width="1700" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>' name="changempup" required></textarea>
          <br>
          <br>
          <input type="submit" class="btn btn-link" name="changemap" value="Change Map"> 
</form>
</div>
</center>

  <?php

 }//While end

}

if (isset($_GET['bkashpayment'])) { ?>
  <center><h2 style="text-decoration: underline;">Change Bkash Payment Number</h2></center>

  <center>
    <table>
      <tr>
        <th>Current Bkash Number</th>
      </tr>
      
        <?php $sqllogo = "SELECT * FROM bkash ORDER BY id DESC";
        $logoresult = mysqli_query($db, $sqllogo);
        $row = mysqli_fetch_array($logoresult) ?>
          <tr>
        <td><strong><?php echo $row['bkashnumber']; ?></strong></td>
        </tr>
          <tr>
        <form action="change.php?bkashpayment=" method="POST">
        <td style="border:1px solid white;"><button class="btn btn-link" type="submit" name="changebkashnumber">Change Bkash Number</button></td>
        </form>
      </tr>
    </table>
  </center>
  <?php

  if (isset($_POST['changebkashnumber'])) { ?>
  
<center>
    <div style="padding-top: 20px;">
      <h3 style="text-decoration: underline;">Change Bkash Number</h3>
       <form action="change.php?bkashpayment=" method="POST">
             <input type="hidden" name="hiddenlogoid" value="<?php echo $row['id']; ?>">
          <textarea style="padding: 10px; font-size: 18px;" placeholder="01633055588" name="changebkasht" required></textarea>
          <br>
          <br>
          <input type="submit" class="btn btn-link" name="changebkashup" value="Update Number"> 
</form>
</div>
</center>

<?php    
} //end while
}//end change email
?>

  </div>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="vendor/raphael/raphael.min.js"></script>
    <script src="vendor/morrisjs/morris.min.js"></script>
    <script src="data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>

<?php } ?>