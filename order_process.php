<?php
		session_start();
		require_once "config.php";
		//require_once "config_sms.php";
		require_once "config_pos.php";

		$db->query("ALTER TABLE `product_order` ADD `stock_id` VARCHAR(100) NOT NULL AFTER `order_status`;");
		$db->query("IF NOT EXISTS(SELECT NULL FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'product_order' AND table_schema = '".$con['dbname']."' AND column_name = 'stock_id')  THEN

		ALTER TABLE `product_order` ADD `stock_id` VARCHAR(100) NOT NULL AFTER `order_status`;

		END IF;");
		$db->query("ALTER TABLE `product_order` ADD `order_number` INT NOT NULL AFTER `stock_id`;");
		$db->query("IF NOT EXISTS(SELECT NULL FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'product_order' AND table_schema = '".$con['dbname']."' AND column_name = 'order_number')  THEN

		ALTER TABLE `product_order` ADD `order_number` VARCHAR(100) NOT NULL AFTER `stock_id`;

		END IF;");
		
		$db->query("CREATE TABLE IF NOT EXISTS  `create_order` (`id` INT NOT NULL AUTO_INCREMENT ,`order_no` INT(200),PRIMARY KEY ( id ))");
		$db->query("CREATE TABLE IF NOT EXISTS `order_list` (`id` INT NOT NULL AUTO_INCREMENT ,`user_id` INT(11),`user_name` VARCHAR(255),`email` VARCHAR(100),`mobile_number` VARCHAR(100),`order_time` DATETIME,`order_status` INT(11),`shipping_status` INT(11),`order_no` INT(100),PRIMARY KEY ( id ))");
		$company_sql = "SELECT * FROM `web_attribut_info`";
		$company_result = $db->query($company_sql);
		$company_data = mysqli_fetch_array($company_result);
		$company_email = strip_tags($company_data[3]);
		
	if(!empty($_SESSION['00user_email00'])){
		$email = $_SESSION['00user_email00'];
        $mobile_number = $_SESSION['00mobile_number00'];
        $user_name = $_SESSION['00user_title00']." ".$_SESSION['00firstname00'];
		
		
		
		$sql = "SELECT * FROM `web_attribut_info`";
		$result = $db->query($sql);
		$data = mysqli_fetch_array($result);
		
		$company_name = $_SESSION['company_name'] = $data[1];
		$all_price = 0;
		foreach($_SESSION['shoping_cart'] as $keys => $values){
		    $item_qty = $values['item_qty'];
			$item_price = $values['item_price'];
			$all_price+= $item_qty*$item_price;
			
		}
		
		if(!empty($_SESSION['shoping_cart'])){
			$sql_create_order = "SELECT * FROM `create_order`";
			$result_create_order = $db->query($sql_create_order);
			$check_create_order = mysqli_num_rows($result_create_order);
			if($check_create_order>0){
				$data_create_order = mysqli_fetch_array($result_create_order);
				$order_number_0 = $data_create_order[1];
				$order_number_0++;
				$db->query("UPDATE `create_order` SET `order_no`='$order_number_0'");
			}
			else{
				$db->query("INSERT INTO `create_order`(`id`,`order_no`) VALUES ('','1')");
				$sql_create_order7 = "SELECT * FROM `create_order`";
				$result_create_order7 = $db->query($sql_create_order7);
				$data_create_order7 = mysqli_fetch_array($result_create_order7);
				$order_number_0 = $data_create_order7[1];
			}
			$sql_customer_info = "SELECT * FROM `user_id` WHERE email='$email'";
			$result_customer_info = $db->query($sql_customer_info);
			$data_customer_info = mysqli_fetch_array($result_customer_info);
             
			$customer_order_name = $data_customer_info[1]." ".$data_customer_info[2];
                   $deliverycharge  =  $_SESSION['delivery'];
			$db->query("INSERT INTO `order_list`(`id`, `user_id`, `user_name`, `email`, `mobile_number`, `order_time`, `order_status`, `shipping_status`,`order_no`, `delivery_time`, `delivery_charge`) VALUES ('','$data_customer_info[0]','$customer_order_name','$data_customer_info[4]','$data_customer_info[5]',NOW(),'0','0','$order_number_0', '$data_customer_info[6]', '$deliverycharge')");
			print_r($db);
                
		$total = 0;
		foreach($_SESSION['shoping_cart'] as $keys => $values){
			$item_name = $values['item_name'];
			$item_qty = $values['item_qty'];
			$item_price = $values['item_price'];
			$stock_id = $values['item_id'];
			$db->query("INSERT INTO `product_order`(`id`, `product_name`, `qty`, `price`, `email`, `date`,`stock_id`,`order_number`)
			VALUES ('','$item_name','$item_qty','$item_price','$email',NOW(),'$stock_id','$order_number_0')");
			unset($_SESSION['shoping_cart'][$keys]);
		}
		$to = "mdshaifullah81@gmail.com";
        $subject = "Test html template email";
        $htmlcontent = '
            
            <!DOCTYPE HTML>
            <html lang="en-US">
            <head>
            	<meta charset="UTF-8">
            	<title>Product Order to Ecomerch</title>
            </head>
            <body>
            	<div class="main_email_template">
            		<div class="header_email" style="width:50%;height:38px;background:red;margin:0px auto;color:#fff">
            			<center><h1>
            			    '.$company_name.'
            			</h1></center>
            		</div>
            		<div class="order_details" style="margin:0px auto;width:50%;background:green;color:yellow">
            			<center><h1>Product Order Details</h1></center>
            		</div>
            		<div class="congralutation" style="width:50%;margin:0px auto">
            			<center><h3 style="color: #ff00cf;
            font-size: 23px;">Thank You For Order in </h3></center>
            		</div>
            		<div class="customer_info" style="width:50%;margin:0px auto;background:#aaa">
            			<div class="header_text">
            			<h3 style="background:yellow;padding:7px;color:green">Customer Info</h3>
            			</div>
            			<div class="customer_information" style="width:100%;margin:0px auto">
            				<table cellpadding="5px" cellspacing="0px" style="margin:0px auto;width:100%">
            				<thead>
            				<tr class="td_border">
            				<th>Name</th>
            				<th>Email</th>
            				<th>Mobile Number</th>
            				</tr>
            				
            				
            				
            				<tr style="background:#ddd;text-align:center">
            				<td>'.$user_name.'</td>
            				<td>'.$email.'</td>
            				<td>'.$mobile_number.'</td>
            				</tr>
            				</thead>
            				</table>
            			</div>
            		</div>
            		<div class="product_info" style="width:50%;margin:0px auto;background:#aaa">
            			<div class="header_text">
            			<h3 style="background:yellow;padding:7px;color:green">Product Info</h3>
            			</div>
            			<div class="customer_information" style="width:100%;margin:0px auto">
            			<h1>Total Product Price = '.$all_price.'</h1>
            			</div>
            		</div>
            		<div class="footer" style="width:50%;margin:0px auto;background:#454533;color:#fff">
            			<center><p>Software solution by <a style="color:#fff" href="http://www.ngicon.com">ngicon</a></p></center>
            		</div>
            	</div>
            </body>
            <style type="text/css">
            	.td_border{background:#ddd}
            </style>
            </html>
        ';
        
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: <info@safwanas.com>' . "\r\n";
        mail($to,$subject,$htmlcontent,$headers);
        $to = $email;
        $subject = "Order Confirm";
        mail($to,$subject,$htmlcontent,$headers);
		header("Location:shop_cart_box.php?massage=1");
	}
	else{
		header("Location:index.php");
	}
	}
	else{
		header("Location:account.php");
	}
?>