
                    <!-- header wrapper -->
					<?php include_once("header.php");?>
                                 <!-- end header wrapper -->
        
        
                <section class="page-top page-header-6">
        <div class="container hide-title">
    <div class="row">
        <div class="col-lg-12 clearfix">
            <div class="pt-right d-none">
                <h1 class="page-title">Order Tracking</h1>
            </div>
                            <div class="breadcrumbs-wrap pt-left">
                    <ul class="breadcrumb"><li class="home"><a itemprop="url" href="#" title="Go to Home Page"><span itemprop="title">Home</span></a><i class="delimiter delimiter-2"></i></li><li>Order Tracking</li>
					</ul>                </div>
                                </div>
    </div>
</div>    </section>
    
        <div id="main" class="column1 boxed"><!-- main -->

            
                        <div class="container">
                            
            
            <div class="row main-content-wrap">

            <!-- main content -->
            <div class="main-content col-lg-12">

                            
    <div id="content" role="main">
                
            <article class="post-327 page type-page status-publish hentry">
                
                <span class="entry-title" style="display: none;">Order Tracking</span><span class="vcard" style="display: none;"><span class="fn"><a href="#" title="Posts by Joe Doe" rel="author">Joe Doe</a></span></span><span class="updated" style="display:none">2016-06-24T00:38:47+00:00</span>
                <div class="page-content">
                    <div class="woocommerce">
<div class="row">
    <div class="col-lg-6 offset-lg-3">
        <form action="#/order-tracking/" method="post" class="track_order featured-box align-left m-t-none">
            <div class="box-content">
                <p>To track your order please enter your Order ID in the box below and press the &quot;Track&quot; button. This was given to you on your receipt and in the confirmation email you should have received.</p>

                <p class="form-row"><label for="orderid">Order ID</label> <input class="input-text" type="text" name="orderid" id="orderid" value="" placeholder="Found in your order confirmation email." /></p>                <p class="form-row"><label for="order_email">Billing email</label> <input class="input-text" type="text" name="order_email" id="order_email" value="" placeholder="Email you used during checkout." /></p>                <div class="clear"></div>

                <p class="form-row clearfix"><button type="submit" class="button btn-lg pt-right" name="track" value="Track">Track</button></p>
                <input type="hidden" id="_wpnonce" name="_wpnonce" value="7740fbd539" /><input type="hidden" name="_wp_http_referer" value="/wordpress/porto/shop2/order-tracking/" />            </div>
        </form>
    </div>
</div>

</div>
                </div>
            </article>

            <div class="">
            
                        </div>

        
    </div>

        

</div><!-- end main content -->


    </div>
    </div>


        
            
            </div><!-- end main -->

            
               <?php include_once "footer.php";?>
			   
			   <!-- WP Super Cache is installed but broken. The constant WPCACHEHOME must be set in the file wp-config.php and point at the WP Super Cache plugin directory. -->