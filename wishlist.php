
					
<?php include_once("header.php");?>					

                                 <!-- end header wrapper -->
        
        
                <section class="page-top page-header-6">
        <div class="container hide-title">
    <div class="row">
        <div class="col-lg-12 clearfix">
            <div class="pt-right d-none">
                <h1 class="page-title">Wishlist</h1>
                            </div>
                            <div class="breadcrumbs-wrap pt-left">
                    <ul class="breadcrumb"><li class="home"><a itemprop="url" href="#" title="Go to Home Page"><span itemprop="title">Home</span></a><i class="delimiter delimiter-2"></i></li><li>Wishlist</li></ul>                </div>
                                </div>
    </div>
</div>    </section>
    
        <div id="main" class="column1 boxed"><!-- main -->

            
                        <div class="container">
                            
            
            <div class="row main-content-wrap">

            <!-- main content -->
            <div class="main-content col-lg-12">

                            
    <div id="content" role="main">
                
            <article class="post-3 page type-page status-publish hentry">
                
                <span class="entry-title" style="display: none;">Wishlist</span><span class="vcard" style="display: none;"><span class="fn"><a href="#" title="Posts by Joe Doe" rel="author">Joe Doe</a></span></span><span class="updated" style="display:none">2017-09-27T15:53:35+00:00</span>
                <div class="page-content">
                    
<div id="yith-wcwl-messages"></div>


<form id="yith-wcwl-form" action="#/wishlist/view/" method="post">

    <div class="featured-box align-left m-t"><div class="box-content">
        <!-- TITLE -->
        
        <!-- WISHLIST TABLE -->
        <table class="shop_table responsive cart wishlist_table" cellspacing="0" data-pagination="no" data-per-page="5" data-page="1" data-id="0">
            <thead>
            <tr>
                                <th class="product-remove"></th>
                
                <th class="product-thumbnail"></th>

                <th class="product-name">
                    <span class="nobr">Product Name</span>
                </th>

                                    <th class="product-price">
                        <span class="nobr">
                            Unit Price                        </span>
                    </th>
                
                                    <th class="product-stock-stauts">
                        <span class="nobr">
                            Stock Status                        </span>
                    </th>
                
                                    <th class="product-add-to-cart"></th>
                            </tr>
            </thead>

            <tbody>
                            <tr class="pagination-row">
                    <td colspan="6" class="wishlist-empty">No products were added to the wishlist</td>
                </tr>
                        </tbody>

            
        </table>

        <input type="hidden" id="yith_wcwl_edit_wishlist" name="yith_wcwl_edit_wishlist" value="ab122e0cdb" /><input type="hidden" name="_wp_http_referer" value="/wordpress/porto/shop2/wishlist/" />
                    <input type="hidden" value="" name="wishlist_id" id="wishlist_id">
        
            </div>
</form>

                </div>
            </article>

            <div class="">
            
                        </div>

        
    </div>

        

</div><!-- end main content -->


    </div>
    </div>


        
            
            </div><!-- end main -->

            
        <?php include_once "footer.php";?>
		
		<!-- WP Super Cache is installed but broken. The constant WPCACHEHOME must be set in the file wp-config.php and point at the WP Super Cache plugin directory. -->