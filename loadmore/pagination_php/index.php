<?php
include('connection.php');
include('paginator.class.php');
$countSql = "SELECT COUNT(id) FROM 0_prices";  
$result = mysqli_query($conn, $countSql);   
$row = mysqli_fetch_row($result);  
$num_rows = $row[0]; 

$pages = new Paginator($num_rows,9); 
$start=$pages->limit_start;
$end=$pages->limit_end;
$p = "SELECT * FROM 0_prices ORDER BY id ASC LIMIT $start, $end";  
$rs_result = mysqli_query($conn, $p); 

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
<script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.0.3.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css_js/simplePagination.css" />
<script src="css_js/jquery.simplePagination.js"></script>
<title>PHP Pagination Class: </title>

</head>
<body>
<div class="container" style="padding-top:20px;">
<div class="container" style="background:#e6e7e8">

</div>
<br>
<nav><ul class="pagination">
     <?php 
	 echo "<li>";
	 echo $pages->display_pages(); 
	  echo "</li>";
	 ?>
    </ul></nav>
<?php 
//echo $pages->display_pages(); 
echo "<span class=\"\">".$pages->display_jump_menu().$pages->display_items_per_page()."</span>";
?>
<br><br><br>
<div class="col-sm-12">	
<table class="table table-bordered table-striped" >  
<thead style="background:red; color:white;">  
<tr>  
    <th>Serial</th>
    <th>stock_id</th>
    <th>sales_type_id</th>
    <th>price</th>  
</tr>  
</thead>  
<tbody id="target-content">
<?php 
$sl=1; 
while ($row = mysqli_fetch_assoc($rs_result)) {
?>  
            <tr>  
            <td><?=$sl++;?></td>
            <td><?=$row['stock_id'];?></td>
            <td><?=$row['sales_type_id'];?></td>
            <td><?=$row['price'];?></td> 
            </tr>  
<?php  
};  
?>
</tbody> 
</table>
</div>

<nav><ul class="pagination">
     <?php 
	 echo "<li>";
	 echo $pages->display_pages(); 
	  echo "</li>";
	  echo "&nbsp;";echo "&nbsp;";echo "&nbsp;";
	 ?>
    </ul></nav>
<?php
	echo "<p class=\"paginate\">Page: $pages->current_page of $pages->num_pages</p>\n";
	//echo "<p class=\"paginate\">SELECT * FROM table LIMIT $pages->limit_start,$pages->limit_end (retrieve records $pages->limit_start-".($pages->limit_start+$pages->limit_end)." from table - $pages->total_items item total / $pages->items_per_page items per page)";

?>

</div>
</body>
</html>