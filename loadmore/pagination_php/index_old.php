<?php
include('connection.php');
$countSql = "SELECT COUNT(id) FROM 0_prices";  
$result = mysqli_query($conn, $countSql);   
$row = mysqli_fetch_row($result);  
$total_records = $row[0];  
$tot_pages = ceil($total_records / $limit);
if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };  
$start_from = ($page-1) * $limit;  
$p = "SELECT * FROM 0_prices ORDER BY id ASC LIMIT $start_from, $limit";  
$rs_result = mysqli_query($conn, $p); 
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
<script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.0.3.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css_js/simplePagination.css" />
<script src="css_js/jquery.simplePagination.js"></script>
<title>Techjunkgigs</title>
<script type="text/javascript">
$(document).ready(function(){
$('.pagination').pagination({
        items: <?php echo $total_records;?>,
        itemsOnPage: <?php echo $limit;?>,
        
		currentPage : 1,
		onPageClick : function(pageNumber) {
			jQuery("#target-content").html('loading......');
			jQuery("#target-content").load("logic.php?page=" + pageNumber);
		}
    });
});
</script>
</head>
<body>
<div class="container" style="padding-top:20px;">
<div class="container" style="background:#e6e7e8">

</div>
<br>
<div class="col-sm-12">	
<table class="table table-bordered table-striped" >  
<thead style="background:red; color:white;">  
<tr>  
    <th>Serial</th>
    <th>stock_id</th>
    <th>sales_type_id</th>
    <th>price</th>  
</tr>  
</thead>  
<tbody id="target-content">
<?php 
$sl=1; 
while ($row = mysqli_fetch_assoc($rs_result)) {
?>  
            <tr>  
            <td><?=$sl++;?></td>
            <td><?=$row['stock_id'];?></td>
            <td><?=$row['sales_type_id'];?></td>
            <td><?=$row['price'];?></td> 
            </tr>  
<?php  
};  
?>
</tbody> 
</table>
</div>
<nav><ul class="pagination">
<?php if(!empty($tot_pages)):for($i=1; $i<=$tot_pages; $i++):  
            if($i == 1):?>
            <li class='active'  id="<?php echo $i;?>"><a href='logic.php?page=<?php echo $i;?>'><?php echo $i;?></a></li> 
            <?php else:?>
            <li id="<?php echo $i;?>"><a href='logic.php?page=<?php echo $i;?>'><?php echo $i;?></a></li>
        <?php endif;?>          
<?php endfor;endif;?>
</ul></nav>
</div>
</body>
</html>