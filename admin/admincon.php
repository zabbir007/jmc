<?php

session_start();
require_once('../config.php');
require_once('../config_pos.php');
$keyword ="";
$username="";
$email="";
$filename = "";
$productupload = "";
$productname = "";
$productweight = "";
$quantity= "";
$cprice= "";
$pprice= "";
$pcompanyname= "";
$mrp= "";

$errors= array();
//$db = mysqli_connect('localhost', 'root', '', 'babyandm_db6918' );
//$con = mysqli_connect('localhost', 'root', '', 'babyandm_erpdb6910' );


//admin_login
if(isset($_POST['adminlogin'])){

$username= mysqli_real_escape_string($db,$_POST['username']);
$password= mysqli_real_escape_string($db,$_POST['password']);
	

$query =" SELECT * FROM adminlogin WHERE username ='$username' AND password='$password'";
$result = mysqli_query($db, $query);
if(mysqli_num_rows($result) == 1){
	$_SESSION['inthemainathorityaccessadmincontrolifthatsare'] = $username;
	header('location: index.php');
	
}

else{
	array_push($errors, "<center><p style='color:red;'>Username or Password was wrong</p><center>" );
}


}
//confirm order

if (isset($_POST['confirmorder'])) {

	$deate = mysqli_real_escape_string($db, $_POST['detailsorder']);

	$sqlconfirm = "UPDATE order_list SET order_status = '1', payment_status = '1' WHERE order_no = '$deate'";
	mysqli_query($db, $sqlconfirm);

	$sqlconfirm1 = "UPDATE product_order SET order_status = '1' WHERE order_number = '$deate'";
	mysqli_query($db, $sqlconfirm1);

	array_push($errors, "<center><p style='color:#186A3B; font-weight:bold;'>Order Confirm Successfuly.</p><center>" );


}
//cancel order

if (isset($_POST['cancelorder'])) {

	$deate = mysqli_real_escape_string($db, $_POST['detailsorder']);

	$sqlconfirm = "UPDATE order_list SET order_status = '3' WHERE order_no = '$deate'";
	mysqli_query($db, $sqlconfirm);

	$sqlconfirm1 = "UPDATE product_order SET order_status = '3' WHERE order_number = '$deate'";
	mysqli_query($db, $sqlconfirm1);

	array_push($errors, "<center><p style='color:red; font-weight:bold;'>Order Cancel Successfuly.</p><center>" );


}
//ready to ship
if (isset($_POST['readytoship'])) {
	$deate = mysqli_real_escape_string($db, $_POST['detailsorder']);
	$sqlconfirm = "UPDATE order_list SET shipping_status = '1' WHERE order_no = '$deate'";
	mysqli_query($db, $sqlconfirm);
	array_push($errors, "<center><p style='color:green; font-weight:bold;'>Shipment Status Updated.</p><center>" );

}
if (isset($_POST['readytodeliver'])) {
	$deate = mysqli_real_escape_string($db, $_POST['detailsorder']);
	$sqlconfirm = "UPDATE order_list SET shipping_status = '2', order_status='2' WHERE order_no = '$deate'";
	mysqli_query($db, $sqlconfirm);
	array_push($errors, "<center><p style='color:green; font-weight:bold;'>Delivery Status Updated.</p><center>" );

}

if (isset($_POST['paymentsuccess'])) {
	$deate = mysqli_real_escape_string($db, $_POST['detailsorder']);
	$sqlconfirm = "UPDATE order_list SET payment_status = '2' WHERE order_no = '$deate'";
	mysqli_query($db, $sqlconfirm);
	array_push($errors, "<center><p style='color:green; font-weight:bold;'>Payment Status Updated.</p><center>" );

}
//delete order
if (isset($_POST['delete_order'])) {
	$deate = mysqli_real_escape_string($db, $_POST['detailsorder']);
	$sqlconfirm = "DELETE FROM order_list WHERE order_no = '$deate'";
	mysqli_query($db, $sqlconfirm);
	$sqlconfirm1 = "DELETE FROM product_order WHERE order_no = '$deate'";
	mysqli_query($db, $sqlconfirm1);
	array_push($errors, "<center><p style='color:red; font-weight:bold;'>Order Deleted Successfuly.</p><center>" );
}

//change logo image
if (isset($_POST['changeimage'])) {
	
	$image = mysqli_real_escape_string($db, $_POST['hiddenlogoid']);
	$filetmp = $_FILES ["logo"]["tmp_name"];
	$filename = $_FILES ["logo"]["name"];
	$filetype = $_FILES ["logo"]["type"];
	$filepath = "logo/" .$filename;
	$move = "../logo/" .$filename;
    $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
	array_push($errors, "<center><p style='color:red; font-weight:bold;'>Invalid Image Format (Please Select jpg/jpeg/png/gif format).</p><center>" );
}
else{

	move_uploaded_file($_FILES["logo"]["tmp_name"], $move);
	$sqlinsert = "UPDATE logo SET logo_path = '$filepath' WHERE id = '$image'";
    $res = mysqli_query($db, $sqlinsert); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Image Upload Successful.</p><center>" );
   
 
}	
 
}

//Change Header Call text
if(isset($_POST['changeheadernumber'])){
	$hiddenid = mysqli_real_escape_string($db, $_POST['hiddenlogoid']);
    $changenumber = mysqli_real_escape_string($db, $_POST['changenumber']);
	$sqlinsert = "UPDATE logo SET header_contact_text = '$changenumber' WHERE id = '$hiddenid'";
    $res = mysqli_query($db, $sqlinsert); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );
}
//Change Icon
if (isset($_POST['changeicon'])) {
	
	$image = mysqli_real_escape_string($db, $_POST['hiddenlogoid']);
	$filetmp = $_FILES ["icon"]["tmp_name"];
	$filename = $_FILES ["icon"]["name"];
	$filetype = $_FILES ["icon"]["type"];
	$filepath = "icon/" .$filename;
	$move = "../icon/" .$filename;
    $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "ico"){
	array_push($errors, "<center><p style='color:red; font-weight:bold;'>Invalid Image Format (Please Select jpg/ico/png/gif format).</p><center>" );
}
else{

	move_uploaded_file($_FILES["icon"]["tmp_name"], $move);
	$sqlinsert = "UPDATE web_attribut_info SET icon = '$filepath' WHERE id = '$image'";
    $res = mysqli_query($db, $sqlinsert); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Image Upload Successful.</p><center>" );
   
 
}	
}

//Change Header title text
if(isset($_POST['changetitletxt'])){
	$hiddenid = mysqli_real_escape_string($db, $_POST['hiddenlogoid']);
    $changetitle = mysqli_real_escape_string($db, $_POST['changetitlet']);
	$sqlinsert = "UPDATE web_attribut_info SET title = '$changetitle' WHERE id = '$hiddenid'";
    $res = mysqli_query($db, $sqlinsert); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );
}

//Change Footer Contact
if (isset($_POST['changefcon'])) {
	$hiddenid = mysqli_real_escape_string($db, $_POST['hiddenlogoid']);
	$changefcontact = mysqli_real_escape_string($db, $_POST['changefcontact']);
	$sqlinsert = "UPDATE web_attribut_info SET mobile = '$changefcontact' WHERE id = '$hiddenid'";
    $res = mysqli_query($db, $sqlinsert); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );
}
//Change Email Address
if (isset($_POST['changeemailup'])) {
	$hiddenid = mysqli_real_escape_string($db, $_POST['hiddenlogoid']);
	$changeemail = mysqli_real_escape_string($db, $_POST['changeemailt']);
	$sqlinsert = "UPDATE web_attribut_info SET email = '$changeemail' WHERE id = '$hiddenid'";
    $res = mysqli_query($db, $sqlinsert); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );
}
//change Office Address
if (isset($_POST['changeofficeup'])) {
	$hiddenid = mysqli_real_escape_string($db, $_POST['hiddenlogoid']);
	$changeoffice = mysqli_real_escape_string($db, $_POST['changeoffice']);
	$sqlinsert = "UPDATE web_attribut_info SET address = '$changeoffice' WHERE id = '$hiddenid'";
    $res = mysqli_query($db, $sqlinsert); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );
}
//add new slide
if (isset($_POST['changeimages'])) {
    $slidetext = mysqli_real_escape_string($db, $_POST['slidetext']);
    $position = mysqli_real_escape_string($db, $_POST['position']);
	$filetmp = $_FILES ["slideadd"]["tmp_name"];
	$filename = $_FILES ["slideadd"]["name"];
	$filetype = $_FILES ["slideadd"]["type"];
	$filepath = "slider/" .$filename;
	$move = "../slider/" .$filename;
    $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
	array_push($errors, "<center><p style='color:red; font-weight:bold;'>Invalid Image Format (Please Select jpg/ico/png/gif format).</p><center>" );
}
else{

	move_uploaded_file($_FILES["slideadd"]["tmp_name"], $move);
	$sqlinsert = "INSERT INTO slider_ecom (slider_text, slider_image, status) VALUES ('$slidetext', '$filepath', '$position')";
    $res = mysqli_query($db, $sqlinsert); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Slide Image Upload Successful.</p><center>" );
   
 
}	
}

//delete slide
if (isset($_POST['deleteslide'])) {
     $slideid = mysqli_real_escape_string($db, $_POST['status']);
     $sql = "DELETE FROM slider_ecom WHERE status='$slideid'";
     mysqli_query($db, $sql);

                }

 //Change Slide
 if (isset($_POST['changeslideup'])) {
 	$image = mysqli_real_escape_string($db, $_POST['hiddenlogoid']);
	$filetmp = $_FILES ["updateimg"]["tmp_name"];
	$filename = $_FILES ["updateimg"]["name"];
	$filetype = $_FILES ["updateimg"]["type"];
	$filepath = "slider/" .$filename;
	$move = "../slider/" .$filename;
    $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
	array_push($errors, "<center><p style='color:red; font-weight:bold;'>Invalid Image Format (Please Select jpg/jpeg/png/gif format).</p><center>" );
}
else{

	move_uploaded_file($_FILES["updateimg"]["tmp_name"], $move);
	$sqlinsert = "UPDATE slider_ecom SET slider_image = '$filepath' WHERE status = '$image'";
    $res = mysqli_query($db, $sqlinsert); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Image Upload Successful.</p><center>" );
   
 
}	
 }
 //slide image text
 if (isset($_POST['submitnewslidetext'])) {
 	$slidetextid = mysqli_real_escape_string($db, $_POST['statusid']);
 	$changeslidetext = mysqli_real_escape_string($db, $_POST['changeslidetext']);
 	$sqlinsert = "UPDATE slider_ecom SET slider_text = '$changeslidetext' WHERE status = '$slidetextid'";
    $res = mysqli_query($db, $sqlinsert); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );

 }


 //change footer
 if (isset($_POST['changefootertextup'])) {
	$hiddenid = mysqli_real_escape_string($db, $_POST['hiddenlogoid']);
	$changefooter = mysqli_real_escape_string($db, $_POST['changefooter']);
	$sqlinsert = "UPDATE footer SET footer_text = '$changefooter' WHERE id = '$hiddenid'";
    $res = mysqli_query($db, $sqlinsert); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );
}
//change working hour

 if (isset($_POST['changeworkinghour'])) {
	$hiddenid = mysqli_real_escape_string($db, $_POST['hiddenlogoid']);
	$day1 = mysqli_real_escape_string($db, $_POST['day1']);
	$day2 = mysqli_real_escape_string($db, $_POST['day2']);
	$time1 = mysqli_real_escape_string($db, $_POST['time1']);
	$time2 = mysqli_real_escape_string($db, $_POST['time2']);

	$sqlinsert = "UPDATE working_hour SET mon_fri = '$day1 To $day2', saturday = '$time1 To $time2'  WHERE id = '$hiddenid'";
    $res = mysqli_query($db, $sqlinsert); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );
}
 //change map
 if (isset($_POST['changemap'])) {
	$hiddenid = mysqli_real_escape_string($db, $_POST['hiddenlogoid']);
	$changefooter = mysqli_real_escape_string($db, $_POST['changempup']);
	$sqlinsert = "UPDATE map SET map = '$changefooter' WHERE id = '$hiddenid'";
    $res = mysqli_query($db, $sqlinsert); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );
}
//Menu create
if (isset($_POST['createmenu'])) {
	$menuname = mysqli_real_escape_string($db2, $_POST['addnewtext']);
	$sqlinsert = "INSERT INTO new_tbl2 (group_brands) VALUES ('$menuname')";
    $res = mysqli_query($db2, $sqlinsert); 
    array_push($errors, "<center><p style='color:green; font-weight:bold;'>Created Successfuly.</p><center>" );
}
//delete menu
if (isset($_POST['deletemenu'])) {
	 $hiddenmenuid = mysqli_real_escape_string($db2, $_POST['hiddenmenuid']);
     $sql = "DELETE FROM new_tbl2 WHERE id ='$hiddenmenuid'";
     mysqli_query($db2, $sql);
     array_push($errors, "<center><p style='color:red; font-weight:bold;'>Deleted Successfuly.</p><center>" );
}
//update menu
if (isset($_POST['updatemenu'])) {
	 $hiddenmenuid = mysqli_real_escape_string($db2, $_POST['hiddenmenuid']);
	 $changegroupbrand = mysqli_real_escape_string($db2, $_POST['changegroupbrand']);
     $sql = "UPDATE new_tbl2 SET group_brands = '$changegroupbrand' WHERE id ='$hiddenmenuid'";
     mysqli_query($db2, $sql);
     array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );
}

//add product group
if(isset($_POST['addproductgroup'])){
		extract($_POST);
	$filetmp = $_FILES ["group_or_brand_photo"]["tmp_name"];
	$filename = $_FILES ["group_or_brand_photo"]["name"];
	$filetype = $_FILES ["group_or_brand_photo"]["type"];
	$filepath = "group/" .$filename;
	$move = "../group/" .$filename;
	if($dropdown_select=="no_select"){
	$dropdown_select = 0;	
	}
    $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
   // Allow certain file formats
   if (empty($filename) || empty($filetmp)) {
   	$db2->query("INSERT INTO `new_tbl2`(`id`, `group_brands`, `atribute`, `desc2`) VALUES ('','$product_group_brands','$select_group_brands','$dropdown_select')");
			  array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );
   } else{
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
	array_push($errors, "<center><p style='color:red; font-weight:bold;'>Invalid Image Format (Please Select jpg/jpeg/png/gif format).</p><center>" );
}
else{

			move_uploaded_file($_FILES["group_or_brand_photo"]["tmp_name"], $move);
			$db2->query("INSERT INTO `new_tbl2`(`id`, `group_brands`, `atribute`, `desc1`, `desc2`) VALUES ('','$product_group_brands','$select_group_brands','$filepath','$dropdown_select')");
			  array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );
		
	}
}
}
//Update Product Group


if(isset($_POST['updateproductgroup'])){
	

	$id = mysqli_real_escape_string($db2, $_POST['id']);
	$product_group_brands = mysqli_real_escape_string($db2, $_POST['product_group_brands']);
	$menue_select = mysqli_real_escape_string($db2, $_POST['menue_select']);
	$dropdown_select = mysqli_real_escape_string($db2, $_POST['dropdown_select']);
	$select_group_brands = mysqli_real_escape_string($db2, $_POST['select_group_brands']);
    
    if ($menue_select == "NULL") {
    	array_push($errors, "<center><p style='color:red; font-weight:bold;'>Menu Type is Required.</p><center>" );
    }

    if (count($errors) == 0) {

    if($dropdown_select=="no_select"){
    	$dropdown_select = 0; 
    }
    $filetmp = $_FILES ["group_or_brand_photo"]["tmp_name"];
	$filename = $_FILES ["group_or_brand_photo"]["name"];
	$filetype = $_FILES ["group_or_brand_photo"]["type"];
	$filepath = "group/" .$filename;
	$move = "../group/" .$filename;
    $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
   // Allow certain file formats
   if (empty($filename) || empty($filetmp)) {
   	$db2->query("UPDATE `new_tbl2`SET group_brands = '$product_group_brands', atribute = '$select_group_brands', desc2 = '$dropdown_select' WHERE id = '$id'");
     array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );
   } else{
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
	array_push($errors, "<center><p style='color:red; font-weight:bold;'>Invalid Image Format (Please Select jpg/jpeg/png/gif format).</p><center>" );
}
else{
			move_uploaded_file($_FILES["group_or_brand_photo"]["tmp_name"], $move);
			$db2->query("UPDATE `new_tbl2` group_brands = '$product_group_brands', atribute = '$product_group_brands', desc1 = '$filepath', desc2 = '$dropdown_select' WHERE id = '$id'");
			  array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );
		
	}
}
    	
    }
}

if (isset($_POST['deletegroup'])) {
	 $id = mysqli_real_escape_string($db2, $_POST['id']);
	 $sql = "DELETE FROM new_tbl2 WHERE id = '$id'";
	 mysqli_query($db2, $sql);
}

if (isset($_POST['changebkashup'])) {
	$hiddenmenuid = mysqli_real_escape_string($db, $_POST['hiddenlogoid']);
	$changebkasht = mysqli_real_escape_string($db, $_POST['changebkasht']);
	
	$table="";
 $table = "bkash";
 if ($result = "SHOW TABLES LIKE '".$table."'") {
     $resultt = mysqli_query($db, $result);
    if($resultt->num_rows == 1) {
    	$sql = "UPDATE bkash SET bkashnumber = '$changebkasht' WHERE id = '$hiddenmenuid'";
    	mysqli_query($db, $sql);
    	array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );
    }
    else{
    	$sqlcreate = "CREATE TABLE bkash(
id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
bkashnumber VARCHAR(255) NOT NULL)";
     mysqli_query($db, $sqlcreate);
     $sql = "INSERT INTO bkash (bkashnumber) VALUES('$changebkasht')";
    	mysqli_query($db, $sql);
    	array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );
    }
}
}
if (isset($_POST['img1update'])) {
	$hiddenmenuid = mysqli_real_escape_string($db2, $_POST['hiddenid']);
	$result = "SHOW COLUMNS FROM 0_stock_master LIKE 'alt_img1'";
     $resultt = mysqli_query($db2, $result);
    if(mysqli_num_rows($resultt) == 0) {
  $sql = "ALTER TABLE `0_stock_master` ADD `alt_img1` VARCHAR(255) NOT NULL AFTER `fa_class_id`, ADD `alt_img2` VARCHAR(255) NOT NULL AFTER `alt_img1`, ADD `alt_img4` VARCHAR(255) NOT NULL AFTER `alt_img2`";
		mysqli_query($db2, $sql);
    $filetmp = $_FILES ["alt_img1"]["tmp_name"];
	$filename = $_FILES ["alt_img1"]["name"];
	$filetype = $_FILES ["alt_img1"]["type"];
	$filepath = "photo/" .$filename;
	$move = "../photo/" .$filename;
    $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
	echo "<center>File Format is Inavalid ! Only JPG/JPEG/PNG/GIF File format are allowed to upload.</center>";
}else{
    	move_uploaded_file($_FILES["alt_img1"]["tmp_name"], $move);
	$sqlinsert = "UPDATE 0_stock_master SET alt_img1 = '$filepath' WHERE stock_id = '$hiddenmenuid'";
    $res = mysqli_query($db2, $sqlinsert);
    } 
	} else{
        $filetmp = $_FILES ["alt_img1"]["tmp_name"];
	$filename = $_FILES ["alt_img1"]["name"];
	$filetype = $_FILES ["alt_img1"]["type"];
	$filepath = "photo/" .$filename;
	$move = "../photo/" .$filename;
    $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
	echo "<center>File Format is Inavalid ! Only JPG/JPEG/PNG/GIF File format are allowed to upload.</center>";
}else{
    	move_uploaded_file($_FILES["alt_img1"]["tmp_name"], $move);
	$sqlinsert = "UPDATE 0_stock_master SET alt_img1 = '$filepath' WHERE stock_id = '$hiddenmenuid'";
    $res = mysqli_query($db2, $sqlinsert);
    } 
		
	}
}

if (isset($_POST['img2update'])) {
	$hiddenmenuid = mysqli_real_escape_string($db2, $_POST['hiddenid']);
	$result = "SHOW COLUMNS FROM 0_stock_master LIKE 'alt_img2'";
     $resultt = mysqli_query($db2, $result);
    if(mysqli_num_rows($resultt) == 0) {
  $sql = "ALTER TABLE `0_stock_master` ADD `alt_img1` VARCHAR(255) NOT NULL AFTER `fa_class_id`, ADD `alt_img2` VARCHAR(255) NOT NULL AFTER `alt_img1`, ADD `alt_img4` VARCHAR(255) NOT NULL AFTER `alt_img2`";
		mysqli_query($db2, $sql);
    $filetmp = $_FILES ["alt_img2"]["tmp_name"];
	$filename = $_FILES ["alt_img2"]["name"];
	$filetype = $_FILES ["alt_img2"]["type"];
	$filepath = "photo/" .$filename;
	$move = "../photo/" .$filename;
    $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
	echo "<center>File Format is Inavalid ! Only JPG/JPEG/PNG/GIF File format are allowed to upload.</center>";
}else{
    	move_uploaded_file($_FILES["alt_img2"]["tmp_name"], $move);
	$sqlinsert = "UPDATE 0_stock_master SET alt_img2 = '$filepath' WHERE stock_id = '$hiddenmenuid'";
    $res = mysqli_query($db2, $sqlinsert);
    } 
	} else{
        $filetmp = $_FILES ["alt_img2"]["tmp_name"];
	$filename = $_FILES ["alt_img2"]["name"];
	$filetype = $_FILES ["alt_img2"]["type"];
	$filepath = "photo/" .$filename;
	$move = "../photo/" .$filename;
    $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
	echo "<center>File Format is Inavalid ! Only JPG/JPEG/PNG/GIF File format are allowed to upload.</center>";
}else{
    	move_uploaded_file($_FILES["alt_img2"]["tmp_name"], $move);
	$sqlinsert = "UPDATE 0_stock_master SET alt_img2 = '$filepath' WHERE stock_id = '$hiddenmenuid'";
    $res = mysqli_query($db2, $sqlinsert);
    } 
		
	}
}

if (isset($_POST['img3update'])) {
	$hiddenmenuid = mysqli_real_escape_string($db2, $_POST['hiddenid']);
	$result = "SHOW COLUMNS FROM 0_stock_master LIKE 'alt_img4'";
     $resultt = mysqli_query($db2, $result);
    if(mysqli_num_rows($resultt) == 0) {
  $sql = "ALTER TABLE `0_stock_master` ADD `alt_img1` VARCHAR(255) NOT NULL AFTER `fa_class_id`, ADD `alt_img2` VARCHAR(255) NOT NULL AFTER `alt_img1`, ADD `alt_img4` VARCHAR(255) NOT NULL AFTER `alt_img2`";
		mysqli_query($db2, $sql);
    $filetmp = $_FILES ["alt_img3"]["tmp_name"];
	$filename = $_FILES ["alt_img3"]["name"];
	$filetype = $_FILES ["alt_img3"]["type"];
	$filepath = "photo/" .$filename;
	$move = "../photo/" .$filename;
    $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
	echo "<center>File Format is Inavalid ! Only JPG/JPEG/PNG/GIF File format are allowed to upload.</center>";
}else{
    	move_uploaded_file($_FILES["alt_img3"]["tmp_name"], $move);
	$sqlinsert = "UPDATE 0_stock_master SET alt_img4 = '$filepath' WHERE stock_id = '$hiddenmenuid'";
    $res = mysqli_query($db2, $sqlinsert);
    } 
	} else{
        $filetmp = $_FILES ["alt_img3"]["tmp_name"];
	$filename = $_FILES ["alt_img3"]["name"];
	$filetype = $_FILES ["alt_img3"]["type"];
	$filepath = "photo/" .$filename;
	$move = "../photo/" .$filename;
    $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
	echo "<center>File Format is Inavalid ! Only JPG/JPEG/PNG/GIF File format are allowed to upload.</center>";
}else{
    	move_uploaded_file($_FILES["alt_img3"]["tmp_name"], $move);
	$sqlinsert = "UPDATE 0_stock_master SET alt_img4 = '$filepath' WHERE stock_id = '$hiddenmenuid'";
    $res = mysqli_query($db2, $sqlinsert);
    } 
		
	}
}
if (isset($_POST['delete1'])) {
$hiddenmenuid = mysqli_real_escape_string($db2, $_POST['hiddenid']);
$sql = "UPDATE 0_stock_master SET alt_img1 = '' WHERE stock_id = '$hiddenmenuid'";
mysqli_query($db2, $sql);

}
if (isset($_POST['delete2'])) {
$hiddenmenuid = mysqli_real_escape_string($db2, $_POST['hiddenid']);
$sql = "UPDATE 0_stock_master SET alt_img2 = '' WHERE stock_id = '$hiddenmenuid'";
mysqli_query($db2, $sql);

}

if (isset($_POST['delete3'])) {
$hiddenmenuid = mysqli_real_escape_string($db2, $_POST['hiddenid']);
$sql = "UPDATE 0_stock_master SET alt_img4 = '' WHERE stock_id = '$hiddenmenuid'";
mysqli_query($db2, $sql);

}

if (isset($_POST['stockup'])) {

$table="";
 $table = "0_stock_image";
 if ($result = "SHOW TABLES LIKE '".$table."'") {
     $resultt = mysqli_query($db2, $result);
    if($resultt->num_rows == 1) {
        
    $stock = mysqli_real_escape_string($db2, $_POST['stock']);
	$filetmp = $_FILES ["stockimage"]["tmp_name"];
	$filename = $_FILES ["stockimage"]["name"];
	$filetype = $_FILES ["stockimage"]["type"];
	$filepath = "photo/" .$filename;
	$move = "../photo/" .$filename;
    $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
	array_push($errors, "<center><p style='color:red; font-weight:bold;'>Invalid Image Format (Please Select jpg/jpeg/png/gif format).</p><center>" );
}else{
    
    	move_uploaded_file($_FILES["stockimage"]["tmp_name"], $move);
	$sqlinsert = "INSERT INTO 0_stock_image (image_type, image_name, image_path, description) VALUES ('$filetype','$filename', '$filepath', '$stock')";
    $res = mysqli_query($db2, $sqlinsert);
	array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );
	
 
}	
    }

else {
   
$sqlcreate = "CREATE TABLE 0_stock_image(
id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
image_type VARCHAR(255) NOT NULL,
image_name VARCHAR(255) NOT NULL,
image_path VARCHAR(255) NOT NULL,
description VARCHAR(255) NOT NULL)";
     mysqli_query($db2, $sqlcreate);


     
    $stock = mysqli_real_escape_string($db2,$_POST['stock']);
	$filetmp = $_FILES ["stockimage"]["tmp_name"];
	$filename = $_FILES ["stockimage"]["name"];
	$filetype = $_FILES ["stockimage"]["type"];
	$filepath = "photo/" .$filename;
	$move = "../photo/" .$filename;
    $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
	array_push($errors, "<center><p style='color:red; font-weight:bold;'>Invalid Image Format (Please Select jpg/jpeg/png/gif format).</p><center>" );
}else{

	
    	move_uploaded_file($_FILES["stockimage"]["tmp_name"], $move);
	$sqlinsert = "INSERT INTO 0_stock_image (image_type, image_name, image_path, description) VALUES ('$filetype','$filename', '$filepath', '$stock')";
    $res = mysqli_query($db2, $sqlinsert);
	array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );
 
}

}

}

	  
}

if (isset($_POST['deletestock'])) {
	
$catid = mysqli_real_escape_string($db2, $_POST['0_stock_category_id']);
$imgid = mysqli_real_escape_string($db2, $_POST['0_stock_image_id']);
$sql = "DELETE FROM 0_stock_category WHERE category_id = '$catid'";
mysqli_query($db2, $sql);
$sql = "DELETE FROM 0_stock_image WHERE id = '$imgid'";
mysqli_query($db2, $sql);
array_push($errors, "<center><p style='color:red; font-weight:bold;'>Deleted Successfuly.</p><center>" );
}

if (isset($_POST['stockeditup'])) { //Main if start
    $catname = mysqli_real_escape_string($db2, $_POST['catname']);
    $maincats = mysqli_real_escape_string($db2, $_POST['maincats']);
    $catid = mysqli_real_escape_string($db2, $_POST['0_stock_category_id']);
    $imgid = mysqli_real_escape_string($db2, $_POST['0_stock_image_id']);

    $filetmp = $_FILES ["stockimage"]["tmp_name"];
	$filename = $_FILES ["stockimage"]["name"];
	$filetype = $_FILES ["stockimage"]["type"];
	$filepath = "photo/" .$filename;
	$move = "../photo/" .$filename;

if (empty($filename) || empty($filetmp)) { //if nested 1 start
 $table="";
 $table = "0_stock_image";
 $result = "SHOW TABLES LIKE '".$table."'";
 $resultt = mysqli_query($db2, $result);
 if($resultt->num_rows == 1) { //if nested 1:2 start

	$sqlpdatecat = "UPDATE 0_stock_category SET description = '$catname', product_group = '$maincats' WHERE category_id = '$catid'";
    $res = mysqli_query($db2, $sqlpdatecat);
   $sqlpdateimage = "UPDATE 0_stock_image SET description = '$catname' WHERE id = '$imgid'";
    mysqli_query($db2, $sqlpdateimage);
	array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );

} //if nested 1:2 End
    
else { //else nested 1:2 start
   
$sqlcreate = "CREATE TABLE 0_stock_image(
id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
image_type VARCHAR(255) NOT NULL,
image_name VARCHAR(255) NOT NULL,
image_path VARCHAR(255) NOT NULL,
description VARCHAR(255) NOT NULL)";
     mysqli_query($db2, $sqlcreate);

	$sqlpdatecat = "UPDATE 0_stock_category SET description = '$catname', product_group = '$maincats' WHERE category_id = '$catid'";
    $res = mysqli_query($db2, $sqlpdatecat);
     $sqlpdateimage = "UPDATE 0_stock_image SET description = '$catname' WHERE id = '$imgid'";
    mysqli_query($db2, $sqlpdateimage);
	array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>");

} //else nested 1:2 End

	} //if nested 1 end 

	else { // Else Nested 1 start

 $table="";
 $table = "0_stock_image";
 $result = "SHOW TABLES LIKE '".$table."'";
 $resultt = mysqli_query($db2, $result);
 if($resultt->num_rows == 1) { //if nested 2 start

$imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){ //if nested 2:1 start
	array_push($errors, "<center><p style='color:red; font-weight:bold;'>Invalid Image Format (Please Select jpg/jpeg/png/gif format).</p><center>" );
} //if nested 2:1 end

else{ //else nested 2:1 start

  	if ($imgid == 0) {

	move_uploaded_file($_FILES["stockimage"]["tmp_name"], $move);
	$sqlinsert = "INSERT INTO 0_stock_image (image_type, image_name, image_path, description) VALUES ('$filetype','$filename', '$filepath', '$catname')";
    $res = mysqli_query($db2, $sqlinsert);
	
	}
    else {

    move_uploaded_file($_FILES["stockimage"]["tmp_name"], $move);
   $sqlupdateimg = "UPDATE 0_stock_image SET image_type = '$filetype', image_name = '$filename', image_path = '$filepath', description = '$catname' WHERE id = '$imgid'";
    mysqli_query($db2, $sqlupdateimg);
 }

$sqlpdatecat = "UPDATE 0_stock_category SET description = '$catname', product_group = '$maincats' WHERE category_id = '$catid'";
    $res = mysqli_query($db2, $sqlpdatecat);
	array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );

} //else nested 2:1 end

} //if nested 2 end

else{ //Else Nested 2 End

$sqlcreate = "CREATE TABLE 0_stock_image(
id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
image_type VARCHAR(255) NOT NULL,
image_name VARCHAR(255) NOT NULL,
image_path VARCHAR(255) NOT NULL,
description VARCHAR(255) NOT NULL)";
     mysqli_query($db2, $sqlcreate);
$imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){ //if nested 3:1 start
	array_push($errors, "<center><p style='color:red; font-weight:bold;'>Invalid Image Format (Please Select jpg/jpeg/png/gif format).</p><center>" );
} //if nested 3:1 end

else{ //else nested 3:1 start

  	if ($imgid == 0) {

	move_uploaded_file($_FILES["stockimage"]["tmp_name"], $move);
	$sqlinsert = "INSERT INTO 0_stock_image (image_type, image_name, image_path, description) VALUES ('$filetype','$filename', '$filepath', '$catname')";
    $res = mysqli_query($db2, $sqlinsert);
	
	}
    else {

    move_uploaded_file($_FILES["stockimage"]["tmp_name"], $move);
	$sqlupdateimg = "UPDATE 0_stock_image SET image_type = '$filetype', image_name = '$filename', image_path = '$filepath', description = '$catname' WHERE id = '$imgid'";
    mysqli_query($db2, $sqlupdateimg);
 }

$sqlpdatecat = "UPDATE 0_stock_category SET description = '$catname', product_group = '$maincats' WHERE category_id = '$catid'";
    $res = mysqli_query($db2, $sqlpdatecat);
	array_push($errors, "<center><p style='color:green; font-weight:bold;'>Updated Successfuly.</p><center>" );

} //else nested 3:1 end

} ////Else Nested 2 End

	} //Else Nested 1 End

} //Main if End

if (isset($_POST['inactivecat'])) {
	$catid = mysqli_real_escape_string($db2, $_POST['0_stock_category_id']);
$sqlpdatecat = "UPDATE 0_stock_category SET inactive = '1' WHERE category_id = '$catid'";
 mysqli_query($db2, $sqlpdatecat);
}
if (isset($_POST['activecat'])) {
	$catid = mysqli_real_escape_string($db2, $_POST['0_stock_category_id']);
$sqlpdatecat = "UPDATE 0_stock_category SET inactive = '0' WHERE category_id = '$catid'";
 mysqli_query($db2, $sqlpdatecat);
}
if (isset($_POST['registrationon'])) {
	$sql = "UPDATE flashsalestats SET stats = '1'";
	mysqli_query($db, $sql);
}
if (isset($_POST['registrationoff'])) {
	$sql = "UPDATE flashsalestats SET stats = '0'";
	mysqli_query($db, $sql);
}
//logout
if(isset($_GET['logout'])){
	session_destroy();
	unset($_SESSION['inthemainathorityaccessadmincontrolifthatsare']);
	header('location: index.php');
}
