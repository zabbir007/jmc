<?php
	include_once "header.php";
?>		
                                 <!-- end header wrapper -->
        
        
                <section class="page-top page-header-6">
        <div class="container hide-title">
   
</div>    </section>
    
        <div id="main" class="column1 boxed"><!-- main -->

            
                        <div class="container">
                            
            
            <div class="row main-content-wrap">

            <!-- main content -->
            <div class="main-content col-lg-12">

                            
    <div id="content" role="main">
                
            <article class="post-210 page type-page status-publish hentry">
                
              
                <div class="page-content">
                    <div class="woocommerce">

<div class="featured-box align-left porto-user-box">
    <div class="box-content">
		<div class="my_info">
			<table class="info_table"> 
				<h2 lang="en">Your Order</h2>
				<thead style="text-align:center">
					<th lang="en">Order sl.</th>
					<th lang="en">Full Name</th>
					<th lang="en">Email</th>
					<th lang="en">Mobile No.</th>
					<th lang="en">Order Time</th>
					<th lang="en">Show Order</th>
					<th lang="en">Order Status</th>
					<th lang="en">Payment Status</th>
				</thead>
				<tbody>
					<?php
						$customer_order_email = $_SESSION['00user_email00'];
						$sql_order = "SELECT * FROM `order_list` WHERE email='$customer_order_email' ORDER BY id DESC";
						$result_order = $db->query($sql_order);
						while($data_order = mysqli_fetch_array($result_order)){
					?>
							<tr style="text-align:center"> 
								<td><?=$data_order[8]?></td>
								<td><?=$data_order[2]?></td>
								<td><?=$data_order[3]?></td>
								<td><?=$data_order[4]?></td>
								<td><?=$data_order[5]?></td>
								

									
								<td>
									<form action="show_order_product.php" method="GET">
										<input type="hidden" name="order_nmber" value="<?=$data_order[8]?>">
									<button class="myBtn" name="showproduct" style="cursor: pointer;" lang="en">View</button>
                                    </form>
								</td>
								<td><?php if ($data_order[6] == 0) {
									echo "<span lang='en'>Order Pending</span>";
								}elseif ($data_order[6] == 1 && $data_order[7] == 0) {
									echo "<span lang='en'>Order Confirmed</span>";
								}

								elseif($data_order[6] == 1 && $data_order[7] == 1){
                                     echo "<span style='color:orange;' lang='en'>Ready To Shipment</span>";
								}
								elseif($data_order[6] == 2){
                                     echo "<span style='color:green;' lang='en'>Delivery Completed</span>";
								}elseif($data_order[6] == 3){
                                     echo "<span style='color:red;' lang='en'>Order Canceled</span>";
								} ?> </td>
								<td><?php if ($data_order['payment_status'] == 2) {
									echo "<span style='color:green;' lang='en'>Payment Success</span>";
								 } elseif ($data_order[6] == 3) {
								 	echo "<span style='color:grey;'>---</span>";
								 } 

								else{
									echo "<span style='color:red;' lang='en'>Payment Pending</span>";
								} ?></td>
							</tr>
					<?php
						}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>


</div>
                </div>
            </article>


        
    </div>

        

</div><!-- end main content -->


    </div>
    </div>
<style>
	@media 
	only screen and (max-width: 760px),
	(min-device-width: 768px) and (max-device-width: 1024px)  {
		table, thead, tbody, th, td, tr { 
			display: block; 
		}
		thead tr { 
			position: absolute;
			top: -9999px;
			left: -9999px;
		}
		
		tr {
			border-bottom:3px solid #aaa;
		}tr:last-child {
			border-bottom:none;
		}
		
		td { 		
			position: relative;
			padding-left: 50%; 
		}
		
		td:before { 
			position: absolute;
			top: 6px;
			left: 6px;
			width: 45%; 
			padding-right: 10px; 
			white-space: nowrap;
		}
		td:nth-of-type(1):before { content: "Order sl."; }
		td:nth-of-type(2):before { content: "User Id"; }
		td:nth-of-type(3):before { content: "User Name"; }
		td:nth-of-type(4):before { content: "Email"; }
		td:nth-of-type(5):before { content: "Mobile No."; }
		td:nth-of-type(6):before { content: "Order Time"; }
		td:nth-of-type(7):before { content: "Show Order"; }
		td:nth-of-type(8):before { content: "Order Status"; }
	}
	@media only screen
	and (min-device-width : 320px)
	and (max-device-width : 480px) {
		my_info { 
			padding: 0; 
			margin: 0; 
			width: 320px; }
		}
	@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
		my_info { 
			width: 495px; 
		}
	}
	
	</style>

            
            </div><!-- end main -->

          <?php include_once"footer.php";?><!-- WP Super Cache is installed but broken. The constant WPCACHEHOME must be set in the file wp-config.php and point at the WP Super Cache plugin directory. -->