<?php

include('admincon.php');
if (empty($_SESSION['inthemainathorityaccessadmincontrolifthatsare'])) { ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div style="max-width: 70%; min-width: 200px; min-height: 300px; background: white; border-radius: 8px; margin: 0 auto;">
    <p style="text-align: center; color: red;"><i class="fa fa-warning"></i> Unauthorized access request.</p>
</div>
<?php }
else {

include('header.php'); ?>

<body>

    <div id="wrapper">

        <?php //sidebar added here 
         include('sidebar.php');
        ?>

        <div id="page-wrapper">
           
            <div class="row">
            	<?php if(isset($_GET['search'])){
$search = mysqli_real_escape_string($db, $_GET['searchtext']);
 $sqlsearch = "SELECT * FROM user_id WHERE title LIKE '%$search%' OR first_name LIKE '%$search%' OR email LIKE '%$search%'";
 $searchresult = mysqli_query($db, $sqlsearch);
 if (mysqli_num_rows($searchresult) == 0) { ?>
 	 <center>
	<div style="padding-bottom: 20px;">
	<form action="search.php" method="GET">
		<input type="text" name="searchtext" placeholder="Enter Customer Name or Email" style="height: 35px; width: 250px; border-radius: 8px 0px 0px 8px; border:1px solid #2980B9; padding: 5px; font-size:16px;"><input type="submit" name="search" value="Search" style="height: 35px; width:70px; border-radius: 0px 8px 8px 0px; border:1px solid #2980B9; padding: 5px; font-size:16px;">
	</form>
	</div>
</center>
<?php
 echo "<center><p style='color:red;'><i class='fa fa-warning'></i> Your search <b>" .$search. "</b> did not match any accounts !</p> <a href='user_info.php'>Back to user info</a></center>"; 

 }else{ ?>
	<center>
	<div style="padding-bottom: 20px;">
	<form action="search.php" method="GET">
		<input type="text" name="searchtext" placeholder="Enter Customer Name or Email" style="height: 35px; width: 250px; border-radius: 8px 0px 0px 8px; border:1px solid #2980B9; padding: 5px; font-size:16px;"><input type="submit" name="search" value="Search" style="height: 37px; width:70px; border-radius: 0px 8px 8px 0px; border:1px solid #2980B9; padding: 5px; font-size:16px;">
	</form>
	</div>
</center>
	<div class="performbody">
	<center>
		<table  class="table table-striped">
			<thead>
				<tr>
					<th style="color: red;">
						User Id
					</th>
					<th style="color: red;">
						Customer Name
					</th>
					<th style="color: red;">
						Email
					</th>
					<th style="color: red;">
						Mobile Number
					</th>
					<th style="color: red;">
						Account Status
					</th>
				</tr>
				</thead>
<?php while ($disc = mysqli_fetch_array($searchresult)) { ?>
  <tbody>
				<tr>
					
					<td>
						<?php echo $disc['id'];
						?>
					</td>
					<td>
						<?php echo $disc['title']; echo $disc['first_name'];
						?>
					</td>
					<td>
						
						<?php echo $disc['email'];
						?>
					
					</td>
					<td>
						
						<?php echo $disc['mobile_no'];
						?>
					
					</td>
					<td>
						<?php if ($disc['id_status'] == 0) {
							echo "<span style='color:red;'>Not Verified</span>";
						} 
						elseif ($disc['id_status'] == 1) {
							echo "<span style='color:green;'>Verified</span>";
						} 
						?>
					
					</td>					
				</tr>
			</tbody>
		
<?php
} 
  ?>
  	</table>
</center>
<?php } ?>
</div>
<?php } 

//pending order search
?>

<?php if(isset($_GET['searchpendingorder'])){
$search = mysqli_real_escape_string($db, $_GET['searchtext']);
 $sqlsearch = "SELECT * FROM order_list WHERE user_name LIKE '%$search%' AND order_status = '0'";
 $searchresult = mysqli_query($db, $sqlsearch);
 if (mysqli_num_rows($searchresult) == 0) { ?>
 	<center><h2 style="text-decoration: underline;">Show Pending Order</h2></center>
 	<center>
	<div style="padding-bottom: 20px; padding-top: 20px;">
	<form action="search.php" method="GET">
		<input type="text" name="searchtext" placeholder="Enter Customer Name" style="height: 35px; width: 250px; border-radius: 8px 0px 0px 8px; border:1px solid #2980B9; padding: 5px; font-size:16px;"><input type="submit" name="searchpendingorder" value="Search" style="height: 35px; width:70px; border-radius: 0px 8px 8px 0px; border:1px solid #2980B9; padding: 5px; font-size:16px;">
	</form>
	</div>
</center>
<?php
 echo "<center><p style='color:red;'><i class='fa fa-warning'></i> Your search <b>" .$search. "</b> did not match any accounts !</p> <a href='show_order.php?pendingordelist='><i class='fa fa-arrow-left'></i> Back to All Pending Order</a></center>"; 

 }else{ ?>
 	<center><h2 style="text-decoration: underline;">Show Pending Order</h2></center>
 	<a href='show_order.php?pendingordelist='><i class='fa fa-arrow-left'></i> Back to All Pending Order</a>
<center>
	<div style="padding-bottom: 20px; padding-top: 20px;">
	<form action="search.php" method="GET">
		<input type="text" name="searchtext" placeholder="Enter Customer Name" style="height: 35px; width: 250px; border-radius: 8px 0px 0px 8px; border:1px solid #2980B9; padding: 5px; font-size:16px;"><input type="submit" name="searchpendingorder" value="Search" style="height: 35px; width:70px; border-radius: 0px 8px 8px 0px; border:1px solid #2980B9; padding: 5px; font-size:16px;">
	</form>
	</div>
</center>
	<div class="performbody">
	<center>
		<table class="table table-striped">
			<thead>
				<tr>
					<th style="color: red;">
						Order Number
					</th>
					<th style="color: red;">
						Customer Email
					</th>
					<th style="color: red;">
						Customer name
					</th>
					<th style="color: red;">
						Order details
					</th>
					<th style="color: red;">
						Confirmation
					</th>
					<th style="color: red;">
						Cancelation
					</th>
				</tr>
				</thead>
<?php while ($disc = mysqli_fetch_array($searchresult)) { ?>
  
	  <tbody>
				<tr>
					
					<td>
						<?php echo $disc['order_no'];
						?>
					</td>
					<td>
						<?php echo $disc['email'];
						?>
					</td>
					<td>
						
						<?php echo $disc['user_name'];
						?>
					
					</td>
					<td>
						<form action="show_order.php" method="GET">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
							<button type="submit" name="orderdetails"><i class="fa fa-id-card-o"></i> Order Deatils</button>
						</form>
					</td>
					<td>
						<form action="show_order.php?pendingordelist=" method="POST">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
				       <button type="submit" name="confirmorder" style="color:#186A3B;"><i class="fa fa-check-circle"></i> Confirm Order</button>
						</form>
					</td>
					<td>
						<form action="show_order.php?pendingordelist=" method="POST">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
				       <button type="submit" name="cancelorder" style="color:#E74C3C;"><i class="fa fa-close"></i> Cancel Order</button>
						</form>
					</td>
					
				</tr>
			</tbody>
		
<?php
} 
  ?>
  	</table>
</center>
<?php } ?>
</div>
<?php } 
//Search Process order
?>

<?php if(isset($_GET['searchprocessorder'])){
$search = mysqli_real_escape_string($db, $_GET['searchtext']);
 $sqlsearch = "SELECT * FROM order_list WHERE user_name LIKE '%$search%' AND order_status = '1'";
 $searchresult = mysqli_query($db, $sqlsearch);
 if (mysqli_num_rows($searchresult) == 0) { ?>
 	<center><h2 style="text-decoration: underline;">Show Process Order</h2></center>
 	<center>
	<div style="padding-bottom: 20px; padding-top: 20px;">
	<form action="search.php" method="GET">
		<input type="text" name="searchtext" placeholder="Enter Customer Name" style="height: 35px; width: 250px; border-radius: 8px 0px 0px 8px; border:1px solid #2980B9; padding: 5px; font-size:16px;"><input type="submit" name="searchprocessorder" value="Search" style="height: 35px; width:70px; border-radius: 0px 8px 8px 0px; border:1px solid #2980B9; padding: 5px; font-size:16px;">
	</form>
	</div>
</center>
<?php
 echo "<center><p style='color:red;'><i class='fa fa-warning'></i> Your search <b>" .$search. "</b> did not match any accounts !</p> <a style='text-align:left;'' href='show_order.php?processorder='><i class='fa fa-arrow-left'></i> Back to All Process Order</a></center>"; 

 }else{ ?>
 	<center><h2 style="text-decoration: underline;">Show Process Order</h2></center>
 	<a style="text-align:left;" href='show_order.php?processorder='><i class="fa fa-arrow-left"></i> Back to All Process Order</a>
<center>
	<div style="padding-bottom: 20px; padding-top: 20px;">
	<form action="search.php" method="GET">
		<input type="text" name="searchtext" placeholder="Enter Customer Name" style="height: 35px; width: 250px; border-radius: 8px 0px 0px 8px; border:1px solid #2980B9; padding: 5px; font-size:16px;"><input type="submit" name="searchprocessorder" value="Search" style="height: 35px; width:70px; border-radius: 0px 8px 8px 0px; border:1px solid #2980B9; padding: 5px; font-size:16px;">
	</form>
	</div>
</center>
	<div class="performbody">
	<center>
		<table class="table table-striped">
			<thead>
				<tr>
					<th style="color: red;">
						Order Number
					</th>
					<th style="color: red;">
						Customer Email
					</th>
					<th style="color: red;">
						Customer name
					</th>
					<th style="color: red;">
						Order details
					</th>
					<th style="color: red;">
						Shiping Status
					</th>
					<th style="color: red;">
						Payment Status
					</th>
				</tr>
				</thead>
<?php while ($disc = mysqli_fetch_array($searchresult)) { ?>
  
	    <tbody>
				<tr>
					
					<td>
						<?php echo $disc['order_no'];
						?>
					</td>
					<td>
						<?php echo $disc['email'];
						?>
					</td>
					<td>
						
						<?php echo $disc['user_name'];
						?>
					
					</td>
					<td>
						<form action="show_order.php" method="GET">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
							<button type="submit" name="orderdetails"><i class="fa fa-id-card-o"></i> Order Deatils</button>
						</form>
					</td>
					<td><?php if($disc['shipping_status'] == 0) {
                              echo "Shipment not ready"; ?>
						<form action="show_order.php?processorder=" method="POST">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
				       <button type="submit" name="readytoship" style="color:#186A3B;"><i class="fa fa-check-circle"></i> Ready to shipment</button>
						</form>

						<?php	} else{
                            echo "Ready to Shipment";
						}?>
					</td>
					<td>
						<?php //if($disc['shipping_status'] == 1) {
                              echo "Payment Pending"; ?>
						<form action="show_order.php?processorder=" method="POST">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
				       <button type="submit" name="paymentsuccess" style="color:#186A3B;"><i class="fa fa-check-circle"></i> Payment Success</button>
						</form>

						<?php	/*} else{
                            echo "Payment Success";
						} */?>
					</td>
					
				</tr>
			</tbody>
		

<?php		
	} ?>
	</table>
</center>
<?php 
}  ?>
</div>
<?php } 
//cancel order search
?>
<?php if(isset($_GET['searchcancelorder'])){
$search = mysqli_real_escape_string($db, $_GET['searchtext']);
 $sqlsearch = "SELECT * FROM order_list WHERE user_name LIKE '%$search%' AND order_status = '3'";
 $searchresult = mysqli_query($db, $sqlsearch);
 if (mysqli_num_rows($searchresult) == 0) { ?>
 	<center><h2 style="text-decoration: underline;">Show Canceled Order</h2></center>
	 <center>
	<div style="padding-bottom: 20px; padding-top: 20px;">
	<form action="search.php" method="GET">
		<input type="text" name="searchtext" placeholder="Enter Customer Name" style="height: 35px; width: 250px; border-radius: 8px 0px 0px 8px; border:1px solid #2980B9; padding: 5px; font-size:16px;"><input type="submit" name="searchcancelorder" value="Search" style="height: 35px; width:70px; border-radius: 0px 8px 8px 0px; border:1px solid #2980B9; padding: 5px; font-size:16px;">
	</form>
	</div>
</center>
<?php
 echo "<center><p style='color:red;'><i class='fa fa-warning'></i> Your search <b>" .$search. "</b> did not match any accounts !</p> <a style='text-align:left;'' href='show_order.php?cancelordelist='><i class='fa fa-arrow-left'></i> Back to All Caneled Order</a></center>"; 

 }else{ ?>
 	<center><h2 style="text-decoration: underline;">Show Canceled Order</h2></center>
 	<a style="text-align:left;" href='show_order.php?cancelordelist='><i class="fa fa-arrow-left"></i> Back to All Canceled Order</a>
 <center>
	<div style="padding-bottom: 20px; padding-top: 20px;">
	<form action="search.php" method="GET">
		<input type="text" name="searchtext" placeholder="Enter Customer Name" style="height: 35px; width: 250px; border-radius: 8px 0px 0px 8px; border:1px solid #2980B9; padding: 5px; font-size:16px;"><input type="submit" name="searchcancelorder" value="Search" style="height: 35px; width:70px; border-radius: 0px 8px 8px 0px; border:1px solid #2980B9; padding: 5px; font-size:16px;">
	</form>
	</div>
</center>
	<div class="performbody">
	<center>
		<table class="table table-striped">
			<thead>
				<tr>
					<th style="color: red;">
						Order Number
					</th>
					<th style="color: red;">
						Customer Email
					</th>
					<th style="color: red;">
						Customer name
					</th>
					<th style="color: red;">
						Order details
					</th>
					<th style="color: red;">
						Delete
					</th>
				</tr>
				</thead>
<?php while ($disc = mysqli_fetch_array($searchresult)) { ?>
  
	    <tbody>
				<tr>
					
					<td>
						<?php echo $disc['order_no'];
						?>
					</td>
					<td>
						<?php echo $disc['email'];
						?>
					</td>
					<td>
						
						<?php echo $disc['user_name'];
						?>
					
					</td>
					<td>
						<form action="show_order.php" method="GET">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
							<button type="submit" name="orderdetails"><i class="fa fa-id-card-o"></i> Order Deatils</button>
						</form>
					</td>
					<td>
						<form action="show_order.php?cancelordelist=" method="POST">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
				       <button type="submit" name="delete_order" style="color:#E74C3C;"><i class="fa fa-close"></i> Delete Order</button>
						</form>
					</td>					
				</tr>
			</tbody>
		

<?php		
	} ?>
	</table>
</center>
<?php 
}  ?>
</div>
<?php } 
//completed order
 if(isset($_GET['searchcompleteorder'])){
$search = mysqli_real_escape_string($db, $_GET['searchtext']);
 $sqlsearch = "SELECT * FROM order_list WHERE user_name LIKE '%$search%' AND order_status = '2'";
 $searchresult = mysqli_query($db, $sqlsearch);
 if (mysqli_num_rows($searchresult) == 0) { ?>
 	<center><h2 style="text-decoration: underline;">Show Canceled Order</h2></center>
	 <center>
	<div style="padding-bottom: 20px; padding-top: 20px;">
	<form action="search.php" method="GET">
		<input type="text" name="searchtext" placeholder="Enter Customer Name" style="height: 35px; width: 250px; border-radius: 8px 0px 0px 8px; border:1px solid #2980B9; padding: 5px; font-size:16px;"><input type="submit" name="searchcompleteorder" value="Search" style="height: 35px; width:70px; border-radius: 0px 8px 8px 0px; border:1px solid #2980B9; padding: 5px; font-size:16px;">
	</form>
	</div>
</center>
<?php
 echo "<center><p style='color:red;'><i class='fa fa-warning'></i> Your search <b>" .$search. "</b> did not match any accounts !</p> <a style='text-align:left;' href='show_order.php?completeordelist='><i class='fa fa-arrow-left'></i> Back to All completed Order</a></center>"; 

 }else{ ?>
 	<center><h2 style="text-decoration: underline;">Show Canceled Order</h2></center>
 	<a style="text-align:left;" href='show_order.php?completeordelist='><i class="fa fa-arrow-left"></i> Back to All Completed Order</a>
 <center>
	<div style="padding-bottom: 20px; padding-top: 20px;">
	<form action="search.php" method="GET">
		<input type="text" name="searchtext" placeholder="Enter Customer Name" style="height: 35px; width: 250px; border-radius: 8px 0px 0px 8px; border:1px solid #2980B9; padding: 5px; font-size:16px;"><input type="submit" name="searchcompleteorder" value="Search" style="height: 35px; width:70px; border-radius: 0px 8px 8px 0px; border:1px solid #2980B9; padding: 5px; font-size:16px;">
	</form>
	</div>
</center>
	<div class="performbody">
	<center>
		<table class="table table-striped">
			<thead>
				<tr>
					<th style="color: red;">
						Order Number
					</th>
					<th style="color: red;">
						Customer Email
					</th>
					<th style="color: red;">
						Customer name
					</th>
					<th style="color: red;">
						Order details
					</th>
				</tr>
				</thead>
<?php while ($disc = mysqli_fetch_array($searchresult)) { ?>
  
	    <tbody>
				<tr>
					
					<td>
						<?php echo $disc['order_no'];
						?>
					</td>
					<td>
						<?php echo $disc['email'];
						?>
					</td>
					<td>
						
						<?php echo $disc['user_name'];
						?>
					
					</td>
					<td>
						<form action="show_order.php" method="GET">
							<input type="hidden" name="detailsorder" value="<?php echo $disc['order_no']; ?>">
							<button type="submit" name="orderdetails"><i class="fa fa-id-card-o"></i> Order Deatils</button>
						</form>
					</td>
							
				</tr>
			</tbody>
		

<?php		
	} ?>
	</table>
</center>
<?php 
}  ?>
</div>
<?php } ?>
            	
                          </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="vendor/raphael/raphael.min.js"></script>
    <script src="vendor/morrisjs/morris.min.js"></script>
    <script src="data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
<?php } ?>