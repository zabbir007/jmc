<?php

	include('server.php');
   if (empty($_SESSION['usermarchantaccountlogin'])) {
	 header('location: login.php');
	} else{
	include_once "header.php";
	
	$sql="SELECT * FROM slider_ecom ORDER BY status";
	$result = $db->query($sql);

?>

<div id="main" class="column1 wide clearfix no-breadcrumbs">

	
				<div class="container-fluid">
	
	
	<div class="row main-content-wrap">
	<div class="main-content col-lg-12">

					
<div id="content" role="main">
		
	<article class="post-143 page type-page status-publish hentry">
		
		
		<div class="page-content">
			<div class="vc_row wpb_row"><div class="porto-wrap-container container">
				<div class="row">
				
			



</div></div>

</div>
<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_custom_1489558295141 section section-parallax m-t-none m-b-none vc_row-has-fill">
<div class="porto-wrap-container container">
<div class="row">
<div class="vc_column_container col-md-12">
<div class="wpb_wrapper vc_column-inner">

<!---
	Php Script Start
--->


   <?php 
       if (isset($_GET['uploadimg'])) { 
       $sql ="SELECT * FROM marchant_user WHERE username = '".$_SESSION['usermarchantaccountlogin']."' AND status = '1' AND admin_approval = '1'"; 
      $sqlresult = mysqli_query($db, $sql);
      $datamarchant = mysqli_fetch_array($sqlresult);
        ?>
      <div class="container">
    <div class="col-lg-12 bg-dark">
        <div class="row float-left">
        <h3 class="text-dark" style="font-size:20px;" lang="en">Update Profile Photo</h3>
        </div>
    </div>
    </div>
    <?php
                    include('errors.php');
                ?>
    <div class="container">
        <div class="row">
        
        <div class="col-md-4 mx-auto"><center>
            <img src="photo/userimagedemo.png" style="width:180px; height: 160px;">
            <br>
            <br>
            <form action="edit-profile.php?uploadimg=" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="hiddenid" value="<?php echo $datamarchant['id']; ?>">
            <input type="file" name="upimg" required><br><br>
            <button type="submit" name="uploadimageprofile" class="btn btn-link" lang="en"><i class="fa fa-upload"></i> Upload Image</button>
            </form>
            <br>
            <br>
</center></div>
        
    </div>
    </div>
          
 
  <?php         
    }
    ?>
<?php 
       if (isset($_GET['changeimg'])) { 
       $sql ="SELECT * FROM marchant_user WHERE username = '".$_SESSION['usermarchantaccountlogin']."' AND status = '1' AND admin_approval = '1'"; 
      $sqlresult = mysqli_query($db, $sql);
      $datamarchant = mysqli_fetch_array($sqlresult);
        ?>
      <div class="container">
    <div class="col-lg-12 bg-dark">
        <div class="row float-left">
        <h3 class="text-dark" style="font-size:20px;" lang="en">Change Profile Photo</h3>
        </div>
    </div>
    </div>
    <?php
                    include('errors.php');
                ?>
    <div class="container">
        <div class="row">
        
        <div class="col-md-4 mx-auto"><center>
            <img src="<?php echo $datamarchant['image_path']; ?>" style="width:180px; height: 160px;">
            <br>
            <br>
            <form action="edit-profile.php?changeimg=" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="hiddenid" value="<?php echo $datamarchant['id']; ?>">
            <input type="file" name="changeimg" required><br><br>
            <button type="submit" name="changeimageprofile" class="btn btn-link" lang="en"><i class="fa fa-upload"></i> Change Image</button>
            </form>
            <br>
            <br>
</center></div>
        
    </div>
    </div>
          
 
  <?php         
    }
    ?>

    <?php 
       if (isset($_GET['profile-edit'])) { 
       $sql ="SELECT * FROM marchant_user WHERE username = '".$_SESSION['usermarchantaccountlogin']."' AND status = '1' AND admin_approval = '1'"; 
      $sqlresult = mysqli_query($db, $sql);
      $datamarchant = mysqli_fetch_array($sqlresult);
        ?>
      <div class="container">
    <div class="col-lg-12 bg-dark">
        <div class="row float-left">
        <h3 class="text-dark" style="font-size:20px;" lang="en">Update Profile Info</h3>
        </div>
    </div>
    </div>
<?php
                    include('errors.php');
                ?>
    <div class="container" style="font-family: Arial, Helvetica, sans-serif;">
            <div class="row">
        
         

        <div class="col-lg-6">
            <form action="edit-profile.php?profile-edit=" method="post">
                        <h2 lang="en"><i class="fa fa-tag"></i> Personal Info</h2>     
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide text-dark">
                        <label><b><span lang="en">Enter Name</span> <span class="required">*</span></b></label>
                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text text-dark" name="marchant_name" value="<?php echo $datamarchant['marchant_name']; ?>" required/>
                    </p>
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide text-dark">
                        <label><b><span lang="en">Email Address (Can not changed) </span><span class="required">*</span></b></label>
                        <input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="marchant_email" value="<?php echo $datamarchant['marchant_email']; ?>" disabled/>
                    </p>
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide text-dark">
                        <label><b><span lang="en">Enter Contact Number </span><span class="required">*</span></b></label>
                        <input class="woocommerce-Input woocommerce-Input--text input-text text-dark" type="text" name="marchant_contact" value="0<?php echo $datamarchant['marchant_contact']; ?>" required/>
                    </p>
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide text-dark">
                       <label for="password"><b><span lang="en">Enter Gender </span><span class="required">*</span></b></label>
                       <select name="gender" class="text-dark">
                           <option value="<?php echo $datamarchant['gender']; ?>"><?php echo $datamarchant['gender']; ?></option>
                           <option value="Male">Male</option>
                           <option value="Female">Female</option>
                       </select>
                    </p>
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide text-dark">
                        <label><b><span lang="en">Enter Country Name </span><span class="required">*</span></b></label>
                        <input class="woocommerce-Input woocommerce-Input--text input-text text-dark" type="text" name="country" value="<?php echo $datamarchant['country']; ?>" required/>
                    </p>
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide text-dark">
                        <label><b><span lang="en">Enter City Name </span><span class="required">*</span></b></label>
                        <input class="woocommerce-Input woocommerce-Input--text input-text text-dark" type="text" name="city" value="<?php echo $datamarchant['city']; ?>" required/>
                    </p>
                </div>
                <div class="col-lg-6">
                    <h2 lang="en"><i class="fa fa-tag"></i> Shop Info</h2>
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide text-dark">
                        <label for="password"><b><span lang="en">Enter Shop Name </span><span class="required">*</span></b></label>
                        <input class="woocommerce-Input woocommerce-Input--text input-text text-dark" type="text" name="marchant_shop" value="<?php echo $datamarchant['marchant_shop']; ?>" required />
                    </p><p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide text-dark">
                        <label for="password"><b><span lang="en">Enter Shop Address </span><span class="required">*</span></b></label>
                         <textarea class="form-control text-dark" rows="3" name="shop_address" required><?php echo $datamarchant['marchant_shop_address']; ?></textarea>
                    </p>
                    

                    
               

        </div>
        <p class="form-row">
                    <input type="hidden" name="hiddenid" value="<?php echo $datamarchant['id']; ?>">
                    <button type="submit" class="woocommerce-Button button" name="updateinfo" lang="en"> Update </button>
                        
                    </p>
         </form>
    </div>
    </div>
          
 
  <?php         
    }
    ?>
    
<!---
	Php Script End
--->

</div>
</div>
</div>
</div>
</div>
</div>
<div class="vc_row-full-width vc_clearfix"></div>
</div>
</article>

<div class="">

</div>


</div>



</div><!-- end main content -->


</div>
</div>



	
	</div>

<?php

	include_once "footer.php";

}
?>
