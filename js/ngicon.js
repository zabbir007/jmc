
$(document).ready(function(){
	$('#close_alert').click(function(){
		$('#add_to_cart_alert').css("display","none");
	});
});
window.onclick = function(event){
	var add_to_cart_alert = document.getElementById('add_to_cart_alert');
	if(event.target==add_to_cart_alert){
		add_to_cart_alert.style.display = "none";
	}
}
function add_to_cart(click_value){
	$.ajaxOper(click_value);
}
function add_to_cart2(click_value){
	$.ajaxOper4(click_value);
}
function remove_product(click_value){
	$.ajaxOper2(click_value);
}
function update_qty(click_id,click_value){
	$.ajaxOper3(click_id,click_value);
}
function check_email(obj){
	var email = obj.value;
	if(email.indexOf("@")>-1 && email.indexOf(".")>-1){
		document.getElementById("create_account").disabled = false;
		document.getElementById("showAlert").innerHTML = "";
		$.ajaxOperation1(email);
	}
	else{
		document.getElementById("showAlert").innerHTML = "Email is Invalid!";
		document.getElementById("create_account").disabled = true;
	}
}
$(document).ready(function(){
	$.ajaxOper = function(stock_id){
		$.ajax({
			url:"add_to_cart.php",
			method:"post",
			data:{stock_id:stock_id},
			dataType:"JSON",
			success:function(data){
				$("#show_add_cart").load("header.php #show_add_cart");
				$("#add_to_cart_second").load("header.php #add_to_cart_second");
				//$('#alert_product_name').html(data.for_alert+" Add Successfully In Cart!");
				$('#alert_area').removeClass("alert alert-danger");
				$('#alert_area').addClass("alert alert-success");
			//	$('#add_to_cart_alert').css("display","block");
			//	$('#show_cart_highlight').css("display","block");
				$('#reload_phpscript').load("index.php #reload_phpscript");
			}
		});
	}
	$.ajaxOper2 = function(stock_id){
		$.ajax({
			url:"delete_to_cart.php",
			method:"post",
			data:{stock_id:stock_id},
			dataType:"JSON",
			success:function(data){
				$("#show_add_cart").load("header.php #show_add_cart");
				$("#add_to_cart_second").load("header.php #add_to_cart_second");
				$("#process_order_list").load("shop_cart_box.php #process_order_list");
				$('#alert_product_name').html(data.for_alert_2+" Remove Successfully From Cart.");
				$('#add_to_cart_alert').delay(2000).fadeOut('slow');
				$('#alert_area').removeClass("alert alert-success");
				$('#alert_area').addClass("alert alert-danger");
				$('#add_to_cart_alert').css("display","block");
				$('#show_cart_highlight').css("display","block");
				$('#reload_phpscript').load("index.php #reload_phpscript");
			}
		});
	}
	$.ajaxOper3 = function(stock_id,qty){
		$.ajax({
			url:"update_qty.php",
			method:"post",
			data:{stock_id:stock_id,qty:qty},
			dataType:"text",
			success:function(data){
				$("#process_order_list").load("shop_cart_box.php #process_order_list");
			}
		});
	}
	$.ajaxOper4 = function(stock_id){
		// alert(stock_id);
		// alert('jabbir');
		var qty = $('#'+stock_id+'quantity_5a7fcb602cc2f').val();

		$.ajax({
			url:"add_to_cart2.php",
			method:"post",
			data:{stock_id:stock_id,qty:qty},
			dataType:"JSON",
			success:function(data){
				$("#show_add_cart").load("header.php #show_add_cart");
				$("#add_to_cart_second").load("header.php #add_to_cart_second");
				$('#alert_product_name').html(data.for_alert_8+" Add Successfully In Cart!");
				$('#add_to_cart_alert').delay(2000).fadeOut('slow');
				$('#alert_area').addClass("alert alert-success");
				$('#add_to_cart_alert').css("display","block");
				$('#show_cart_highlight').css("display","block");
				$('#reload_phpscript').load("index.php #reload_phpscript");
			}
		});
	}
	$.ajaxOperation1 = function(email){
		$.ajax({
			url:"checkemail.php",
			method:"post",
			data:{email:email},
			dataType:"text",
			success:function(data){
				if(data=="1"){
					document.getElementById("create_account").disabled = true;
					document.getElementById("showAlert").innerHTML = "Email All Ready Is Used!";
				}
				else{
					document.getElementById("create_account").disabled = false;
					document.getElementById("showAlert").innerHTML = "";
				}
			}
		});
	}
});