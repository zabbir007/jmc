<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
<link rel="pingback" href="xmlrpc.php" />
<?php 
	$sql="SELECT * FROM web_attribut_info";
	$result=$db->query($sql);
	$data=mysqli_fetch_array($result);
?>
<link rel="shortcut icon" href="<?=$data[5]?>" type="image/x-icon" />
<link rel="apple-touch-icon" href="wp-content/themes/porto/images/logo/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="120x120" href="wp-content/themes/porto/images/logo/apple-touch-icon_120x120.png">
<link rel="apple-touch-icon" sizes="76x76" href="wp-content/themes/porto/images/logo/apple-touch-icon_76x76.png">
<link rel="apple-touch-icon" sizes="152x152" href="wp-content/themes/porto/images/logo/apple-touch-icon_152x152.png">

		<script type="text/javascript">document.documentElement.className = document.documentElement.className + ' yes-js js_active js'</script>
<?php
	$sql = "select * from web_attribut_info";
	$result = $db->query($sql);
	$data = mysqli_fetch_array($result);
?>
<title><?=$data['title'];?></title>
	<style>
		.wishlist_table .add_to_cart, a.add_to_wishlist.button.alt { border-radius: 16px; -moz-border-radius: 16px; -webkit-border-radius: 16px; }			</style>
<style rel="stylesheet" property="stylesheet" type="text/css">.ms-loading-container .ms-loading, .ms-slide .ms-slide-loading { background-image: none !important; background-color: transparent !important; box-shadow: none !important; } #header .logo { max-width: 170px; } @media (min-width: 1190px) { #header .logo { max-width: 250px; } } @media (max-width: 991px) { #header .logo { max-width: 110px; } } @media (max-width: 767px) { #header .logo { max-width: 110px; } } #header.sticky-header .logo { width: 86.25px; }</style><link rel='dns-prefetch' href='http://fonts.googleapis.com/' />

<link rel="alternate" type="application/rss+xml" title="Porto Shop 2 &raquo; Feed" href="feed/index.php" />
<link rel="alternate" type="application/rss+xml" title="Porto Shop 2 &raquo; Comments Feed" href="comments/feed/index.php" />
<style type="text/css">
img.wp-smiley,
img.emoji {
display: inline !important;
border: none !important;
box-shadow: none !important;
height: 1em !important;
width: 1em !important;
margin: 0 .07em !important;
vertical-align: -0.1em !important;
background: none !important;
padding: 0 !important;
}



</style>

<link rel='stylesheet'  href='mystyle.css' type='text/css'/>
<link rel='stylesheet'  href='responsive.css' type='text/css'/>
<link rel='stylesheet' id='validate-engine-css-css'  href='wp-content/plugins/wysija-newsletters/css/validationEngine.jquery.css' type='text/css' media='all' />
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="icon/css/font-awesome.min.css" type="text/css" />
<link rel='stylesheet' id='contact-form-7-css'  href='wp-content/plugins/contact-form-7/includes/css/styles.css' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce_prettyPhoto_css-css'  href='wp-content/plugins/woocommerce/assets/css/prettyPhoto.css' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-selectBox-css'  href='wp-content/plugins/yith-woocommerce-wishlist/assets/css/jquery.selectBox.css' type='text/css' media='all' />

<link rel='stylesheet' id='yith-wcwl-main-css'  href='wp-content/plugins/yith-woocommerce-wishlist/assets/css/style.css' type='text/css' media='all' />
<link rel='stylesheet' id='instag-slider-css'  href='wp-content/plugins/instagram-slider-widget/assets/css/instag-slider.css' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='wp-content/plugins/js_composer/assets/css/js_composer.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='porto-bootstrap-css'  href='wp-content/themes/porto/css/bootstrap_78.css' type='text/css' media='all' />
<link rel='stylesheet' id='porto-plugins-css'  href='wp-content/themes/porto/css/plugins.css' type='text/css' media='all' />
<link rel='stylesheet' id='porto-google-fonts-css'  href='http://fonts.googleapis.com/css?family=Open+Sans%3A200%2C300%2C400%2C700%2C800%2C600%7CShadows+Into+Light%3A200%2C300%2C400%2C700%2C800%2C600%7COswald%3A200%2C300%2C400%2C700%2C800%2C600%7C&amp;subset=cyrillic%2Ccyrillic-ext%2Cgreek%2Cgreek-ext%2Ckhmer%2Clatin%2Clatin-ext%2Cvietnamese%2Ccyrillic%2Ccyrillic-ext%2Cgreek%2Cgreek-ext%2Ckhmer%2Clatin%2Clatin-ext%2Cvietnamese' type='text/css' media='all' />
<link rel='stylesheet' id='porto-theme-css'  href='wp-content/themes/porto/css/theme.css' type='text/css' media='all' />
<link rel='stylesheet' id='porto-theme-shop-css'  href='wp-content/themes/porto/css/theme_shop.css' type='text/css' media='all' />
<link rel='stylesheet' id='porto-dynamic-style-css'  href='wp-content/themes/porto/css/dynamic_style_78.css' type='text/css' media='all' />
<link rel='stylesheet' id='porto-skin-css'  href='wp-content/themes/porto/css/skin_78.css' type='text/css' media='all' />
<link rel='stylesheet' id='porto-style-css'  href='wp-content/themes/porto/style.css' type='text/css' media='all' />
<link rel='stylesheet' href='css/mystyle.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/lessframwork.css' />

<script type="text/javascript" src="js/jquery.elevatezoom.js"></script>
<script type='text/javascript' src='wp-includes/js/jquery/jquery.js'></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.min.js'></script>
<script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js'></script>
<script type='text/javascript' src='wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart.js'></script>
<script type='text/javascript' src='wp-content/plugins/instagram-slider-widget/assets/js/jquery.flexslider-min.js'></script>
<script type='text/javascript' src='wp-content/themes/porto/js/popper.min.js'></script>
<script type='text/javascript' src='wp-content/themes/porto/js/bootstrap.optimized.min.js'></script>
<script type='text/javascript' src='wp-content/themes/porto/js/plugins.min.js'></script>
<script type='text/javascript' src='js/ngicon.js'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.4" />
<meta name="generator" content="WooCommerce 3.3.0" />
<link rel="canonical" href="index.php" />
<link rel='shortlink' href='index.php' />
<link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embedef63.json?url=http%3A%2F%2Fwww.portotheme.com%2Fwordpress%2Fporto%2Fshop2%2F" />
<link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embed4bfc?url=http%3A%2F%2Fwww.portotheme.com%2Fwordpress%2Fporto%2Fshop2%2F&amp;format=xml" />
<style type="text/css">.porto-standable-carousel {
margin-bottom: 10px;
}
.porto-ibanner {
text-align: left;
cursor: default;
background: none;
}
.home-banner-slider .porto-ibanner {
margin-bottom: 0;
}
.home-banner-slider .porto-ibanner .porto-ibanner-desc {
top: 23%;
left: 63px;
padding: 0;
}
@media (max-width: 575px) {
.home-banner-slider .porto-ibanner .porto-ibanner-desc {
left: 8%;
}
}
.porto-ibanner .porto-ibanner-title {
font-weight: 700;
text-transform: uppercase;
}
.porto-ibanner .porto-ibanner-desc .btn {
background-color:#010204;
color:#fff;
font-family:Oswald;
font-size:14px;
letter-spacing:0.025em;
font-weight:400;
padding:13px 34px;
border-radius:3px;
margin-top:42px;
border:none;
text-transform:uppercase;
}
@media (max-width: 991px) {
.home-banner-slider .owl-carousel .owl-dots {
bottom: 10px;
}
.home-banner-slider .porto-ibanner .porto-ibanner-desc {
top: 15%;
}
.home-banner-slider .porto-ibanner .porto-ibanner-desc .btn {
font-size: 12px;
padding: 7px 15px;
margin-top: 10px;
}
.home-banner-slider .porto-ibanner .porto-ibanner-title {
font-size: 12px !important;
}
.home-banner-slider .porto-ibanner .porto-ibanner-title span {
line-height: 20px !important;
}
.home-banner-slider .porto-ibanner .porto-ibanner-title b {
font-size: 14px !important;
}
.home-banner-slider .porto-ibanner .porto-ibanner-title div {
font-size: 22px !important;
line-height: 20px !important;
margin-bottom: 5px !important;
}
}</style>	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
<meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<link rel="stylesheet" href="css/cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
<link rel="stylesheet" type="text/css" href="./slick/slick.css">
  <link rel="stylesheet" type="text/css" href="./slick/slick-theme.css">
  
        <link href="jquery-bar-rating-master/style.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <link href='jquery-bar-rating-master/dist/themes/fontawesome-stars.css' rel='stylesheet' type='text/css'>
        
        <!-- Script -->
        <script src="jquery-bar-rating-master/dist/jquery.barrating.min.js" type="text/javascript"></script>
	<script src="js/js.cookie.js" charset="utf-8" type="text/javascript"></script>
	<script src="js/jquery-lang.js" charset="utf-8" type="text/javascript"></script>
	<!--<script src="js/langpack/nonDynamic.js" charset="utf-8" type="text/javascript"></script> -->
	<script type="text/javascript">
		var lang = new Lang();
		lang.dynamic('bn', 'js/langpack/th.json');
		lang.init({
			defaultLang: 'en'
		});
	</script>
        <script type="text/javascript">
        $(function() {
            $('.rating').barrating({
                theme: 'fontawesome-stars',
                onSelect: function(value, text, event) {

                    // Get element id by data-id attribute
                    var el = this;
                    var el_id = el.$elem.data('id');

                    // rating was selected by a user
                    if (typeof(event) !== 'undefined') {
                        
                        var split_id = el_id.split("_");

                        var postid = split_id[1];  // postid

                        // AJAX Request
                        $.ajax({
                            url: 'rating_ajax.php',
                            type: 'post',
                            data: {postid:postid,rating:value},
                            dataType: 'json',
                            success: function(data){
                                // Update average
                                var average = data['averageRating'];
                                $('#avgrating_'+postid).text(average);
                            }
                        });
                    }
                }
            });
        });
      
        </script>
        
<style>

#navbarr {
  overflow: hidden;

}

.sticky {
  position: fixed;
  top: 0;
  z-index: 99;
  width: 100%;
}
.hiddenbar{
    background: #4e7df1; 

    height:60px;
}

@media (max-width: 991px) {
    .hiddenbar{
        display: none;
    }
}
</style>
<style>
.dropbtn {
  background-color:none;
 
  display: block;
  width: 100%;
  border: none;
  cursor: pointer;
}

.dropbtn:hover, .dropbtn:focus {
  
}



.dropdown-content {
  display: none;
  position: fixed;
  top: 60px;
  z-index: 999;
}
.dropdown-content .sidebar-menu .popup{
  left: 271px !important;
  width: 430px;
  z-index: 999;
  background: white;
  overflow-y: auto;
  min-height: 200px;
  max-height: 450px;
  overflow-x: hidden;

}

.dropdown-content .sidebar-menu .popup:hover{
  display: block;
}
.show2 {display: block;
width: 271px;
}

.dropbtn1 {
  background-color:none;
 
  display: block;
  width: 100%;
  border: none;
  cursor: pointer;
}

.dropbtn:hover, .dropbtn:focus {
  
}



.dropdown-content1 {
  display: none;

  position: fixed;
  z-index: 999;
}
.fixed{
  width: 271px;
  position: fixed;
  top: 60px;
}
.fixed1{
  width: 271px;
  position: fixed;
  top: 230px;
}
.dropdown-content1 .sidebar-menu .popup{
  left: 271px !important;
  width: 430px;
  background: white;
  overflow-y: scroll;
  min-height: 200px;
  max-height: 450px;
  overflow-x: hidden;
  z-index: 999;

}

.dropdown-content1 .sidebar-menu .popup:hover{
  display: block;
}
.show1 {display: block;
width: 271px;
}

.allcattop{
background: rgb(255,220,82);
background: radial-gradient(circle, rgba(255,220,82,1) 20%, rgba(78,125,241,1) 83%);
height: 80px;
text-align: center;
width: 100%;
}
.allcattop h2{
  font-size: 26px;
  line-height: 80px;
  text-align: center;
  color: black;
}


.timer {
  
  font-size: 20px;
  margin-top: -12px;
  text-align:  center;
  line-height: 40px;
  border-radius: 10px;
  cursor:  default;
}

.display {
  width:300px;
  height: 50px;
  color: #222;
  font-weight: bold;

}
.symbol1 {
  width: 70px;
  line-height: 50px;
  height: 50px;
  float:  left;
  overflow:  hidden;
}
.symbol {
  width: 27px;
  height: 50px;
  float:  left;
  overflow:  hidden;
}
.symbol div {
}
.cur,.old{
  width: 92%;
  height: 40px;
  float: left;
  background: #ffdc52;
  border-radius: 3px;
  margin: 5px 1px;
  transform: translateY(-50px);
  box-shadow: inset 1px 1px 3px 0px rgba(255, 255, 255, 0.5);

}
.symbol.anim div {
  transform: translateY(0px);
  transition: transform .7s ease-in-out;
}
.delimeter {
  width: 9px;
  float:  left;
  line-height: 47px;
  margin-left: 0px;
  text-indent: 0px;
}

@media (max-width: 575px) {
   .allcattop{
background: rgb(255,220,82);
background: radial-gradient(circle, rgba(255,220,82,1) 20%, rgba(78,125,241,1) 83%);
height: 50px;
text-align: center;
width: 100%;
}
.allcattop h2{
  font-size: 24px;
  line-height: 50px;
  text-align: center;
  color: black;
}
.symbol1 {
  display: none;
}
}


</style>
    <!-- slick slider style-->
      <link rel="stylesheet" href="newslick/css/style.css">
      <!-- slick slider style-->
     
</head>