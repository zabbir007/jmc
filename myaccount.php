<?php
	include_once "header.php";
?>		
                                 <!-- end header wrapper -->
        
        
                <section class="page-top page-header-6">
        <div class="container hide-title">
   
</div>    </section>
    
        <div id="main" class="column1 boxed"><!-- main -->

            
                        <div class="container">
                            
            
            <div class="row main-content-wrap">

            <!-- main content -->
            <div class="main-content col-lg-12">

                            
    <div id="content" role="main">
                
            <article class="post-210 page type-page status-publish hentry">
                
              
                <div class="page-content">
                    <div class="woocommerce">

<div class="featured-box align-left porto-user-box">
    <div class="box-content">
		<div class="my_info">
			<table class="info_table"> 
				<h2 lang="en">Your information</h2>
				<thead> 
					<th lang="en">Title</th>
					<th lang="en">Full Name</th>
					<th lang="en">Email</th>
					<th lang="en">Mobile No.</th>
				</thead>
				<tbody>
					<tr> 
						<td><?=$_SESSION['00user_title00']?></td>
						<td><?=$_SESSION['00firstname00']?></td>
						<td><?=$_SESSION['00user_email00']?></td>
						<td><?=$_SESSION['00mobile_number00']?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>


</div>
                </div>
            </article>

            <div class="">
            
                        </div>

        
    </div>

        

</div><!-- end main content -->


    </div>
    </div>
<style>
	@media 
	only screen and (max-width: 760px),
	(min-device-width: 768px) and (max-device-width: 1024px)  {
		table, thead, tbody, th, td, tr { 
			display: block; 
		}
		thead tr { 
			position: absolute;
			top: -9999px;
			left: -9999px;
		}
		
		tr {}
		
		td { 		
			position: relative;
			padding-left: 50%; 
		}
		
		td:before { 
			position: absolute;
			top: 6px;
			left: 6px;
			width: 45%; 
			padding-right: 10px; 
			white-space: nowrap;
		}
		td:nth-of-type(1):before { content: "Title"; }
		td:nth-of-type(2):before { content: "First Name"; }
		td:nth-of-type(3):before { content: "Last Name"; }
		td:nth-of-type(4):before { content: "Email"; }
		td:nth-of-type(5):before { content: "Mobile No"; }
	}
	@media only screen
	and (min-device-width : 320px)
	and (max-device-width : 480px) {
		my_info { 
			padding: 0; 
			margin: 0; 
			width: 320px; }
		}
	@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
		my_info { 
			width: 495px; 
		}
	}
	
	</style>

        
            
            </div><!-- end main -->

          <?php include_once"footer.php";?><!-- WP Super Cache is installed but broken. The constant WPCACHEHOME must be set in the file wp-config.php and point at the WP Super Cache plugin directory. -->