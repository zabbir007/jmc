<?php include ('server.php'); ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Marchant Register</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Marchant Register</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <?php include ('errors.php'); ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Please Register
                        </div>
                        <div class="panel-body">
                            <div class="row">
                            	<form action="register.php" method="POST">
                                <div class="col-lg-6">
                                    
                                        <div class="form-group">
                                            <label>Enter Name <span style="color:red;">*</span></label>
                                            <input class="form-control" type="text" name="marchant_name" placeholder="MD Jakir Hosen" value="<?php echo $marchant_name; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Enter Email Address <span style="color:red;">*</span></label>
                                            <input class="form-control" placeholder="example@gmail.com" type="text" value="<?php echo $marchant_email; ?>" name="marchant_email" required>
                                        </div>
                                       <div class="form-group">
                                            <label>Enter Contact Number <span style="color:red;">*</span></label>
                                            <input class="form-control" type="text" name="marchant_contact" placeholder="016********" value="<?php echo $marchant_contact; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Username <span style="color:red;">*</span></label>
                                            <input class="form-control" placeholder="jakir0010" type="text" name="marchant_username" value="<?php echo $marchant_username; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="agree with term and condition" name="agree" required checked> Agree with Term and Condition
                                                </label>
                                        </div>
                                    </div>
                                         
                                      
                                </div>
                               <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">
                                        <fieldset>
                                            <div class="form-group">
                                            <label>Enter Shop or Company Name<span style="color:red;">*</span></label>
                                            <input class="form-control" type="text" name="marchant_shop" value="<?php echo $marchant_shop; ?>" placeholder="Shop Name" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Enter Shop Address <span style="color:red;">*</span></label>
                                            <textarea class="form-control" rows="3" name="shop_address" required><?php echo $shop_address; ?></textarea>
                                        </div>
                                          
                                         <div class="form-group">
                                            <label>Enter Password <span style="color:red;">*</span></label>
                                            <input class="form-control" name="password1" type="password" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Confirm Password <span style="color:red;">*</span></label>
                                            <input class="form-control" name="password2" type="password" required>
                                        </div>
                                         
                                        </fieldset>                            
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                           
                            </div>
                                  
                        <button type="submit" name="register" class="btn btn-primary">Register</button>
                    
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                       
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->

                </form>
            </div>
            <!-- /.row -->
        </div>



    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
