<?php include ('admincon.php');
if (empty($_SESSION['inthemainathorityaccessadmincontrolifthatsare'])) {

     header('location: login.php');

 } else {
 include('header.php');

 ?>

<body>

    <div id="wrapper">

        <?php //sidebar added here 
         include('sidebar.php');
        ?>

        <div id="page-wrapper">
      <?php include ('errors.php'); ?>

<center><h2 style="text-decoration: underline;">User Accounts</h2></center>
<center>
	<div style="padding-bottom: 20px;">
	<form action="search.php" method="GET">
		<input type="text" name="searchtext" placeholder="Enter Customer Name or Email" style="height: 37px; width: 250px; border-radius: 8px 0px 0px 8px; border:1px solid #2980B9; padding: 5px; font-size:16px;"><input type="submit" name="search" value="Search" style="height: 37px; width:70px; border-radius: 0px 8px 8px 0px; border:1px solid #2980B9; padding: 5px; font-size:16px;">
	</form>
	</div>
</center>

	<?php
$sqlpending = "SELECT * FROM user_id ORDER BY id DESC";
	$sqlexecute = mysqli_query($db, $sqlpending); 
   
	?>
	<center>
		<table class="table table-striped">
			<thead>
				<tr>
					<th style="color: red;">
						User Id
					</th>
					<th style="color: red;">
						Customer Name
					</th>
					<th style="color: red;">
						Email
					</th>
					<th style="color: red;">
						Mobile Number
					</th>
					<th style="color: red;">
						Account Status
					</th>
				</tr>
				</thead>
	<?php while ($disc = mysqli_fetch_array($sqlexecute)) { 
           
		?>


	  <tbody>
				<tr>
					
					<td>
						<?php echo $disc['id'];
						?>
					</td>
					<td>
						<?php echo $disc['title']; echo $disc['first_name'];
						?>
					</td>
					<td>
						
						<?php echo $disc['email'];
						?>
					
					</td>
					<td>
						
						<?php echo $disc['mobile_no'];
						?>
					
					</td>
					<td>
						<?php if ($disc['id_status'] == 0) {
							echo "<span style='color:red;'>Not Verified</span>";
						} 
						elseif ($disc['id_status'] == 1) {
							echo "<span style='color:green;'>Verified</span>";
						} 
						?>
					
					</td>					
				</tr>
			</tbody>
		

<?php		
	} ?>
	</table>
</center>
  </div>

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="vendor/raphael/raphael.min.js"></script>
    <script src="vendor/morrisjs/morris.min.js"></script>
    <script src="data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>

<?php } ?>

