<?php
	include('server.php');
	 if (!empty($_SESSION['usermarchantaccountlogin'])) {
	 header('location: index.php');
	}
?>

<!DOCTYPE html>
<html lang="en-US">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<?php
	include_once "head.php";
?>
<body class="home page-template-default page page-id-143 full blog-78  yith-wcan-free wpb-js-composer js-comp-ver-5.4.5 vc_responsive" ">

<div class="page-wrapper">

    <div class="header-wrapper clearfix"><!-- header wrapper -->
                                
                    <header id="header" class="header-separate header-1  sticky-menu-header">
<?php
	include_once "header_top_menu.php";
?>
    
    <div class="header-main" >
        <div class="container">
            <div class="header-left">
				<?php
					$sql_logo = "select * from logo";
					$result_logo = $db->query($sql_logo);
					$data_logo = mysqli_fetch_array($result_logo);
                   // if (isset()) {
                        # code...
                    //}
				?>

                <h1 ><a href="index.php" class="text-white" lang="en">Marchant Corner</h1> 
                </div>
            
            
               </div>

                
            </div>
        </div>
            </div>
            <div style="width:100%; border-bottom: 2px solid #000;"></div>

            <div class="main-menu-wrap">
         
</div>
</header>
</div><!-- end header wrapper -->



        
        
                <section class="page-top page-header-6">
        <div class="container hide-title">
    <div class="row">
    </div>
</div>    </section>
    
        <div id="main" class="column1 boxed"><!-- main -->

            
                        <div class="container">
                            
            
            <div class="row main-content-wrap">

            <!-- main content -->
            <div class="main-content col-lg-12">

                            
    <div id="content" role="main">
                
            <article class="post-210 page type-page status-publish hentry">
                
                <span class="entry-title" style="display: none;">My Account</span><span class="vcard" style="display: none;"><span class="fn"><a href="author/porto_admin/index.php" title="Posts by Joe Doe" rel="author"></a></span></span><span class="updated" style="display:none"></span>
                <div class="page-content">
                    <div class="woocommerce">

<div class="featured-box align-left porto-user-box">
    <div class="box-content">
				<?php
					include('errors.php');
				?>
				<h2 lang="en">Marchant Login</h2>
				<a href="register.php"><button class="btn btn-secondary float-right" style="margin-bottom:10px;" type="button" lang="en">Register <i class="fa fa-sign-in"></i></button></a>

				<form action="login.php" method="post">

					
					<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="username" lang="en">Username <span class="required">*</span></label>
						<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="" />					</p>
					<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="password" lang="en">Password <span class="required">*</span></label>
						<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
					</p>

					
					<p class="form-row">
						<input type="hidden" id="woocommerce-login-nonce" name="woocommerce-login-nonce" value="8338162ad8" /><input type="hidden" name="_wp_http_referer" value="/wordpress/porto/shop2/my-account/" />						<button type="submit" class="woocommerce-Button button" name="login" value="Login" lang="en">Login</button>
						<label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
							<span lang="en">Forgot Password? </span><span><a href="password-recovery.php" lang="en">Click Here</a></span>
						</label>
					</p>

					
				</form>

		
			</div>
</div></div>
                </div>
            </article>

            <div class="">
            
                        </div>

        
    </div>

        

</div><!-- end main content -->
    </div>
    </div>


        
            
            </div><!-- end main -->

          <?php include_once"footer.php";?><!-- WP Super Cache is installed but broken. The constant WPCACHEHOME must be set in the file wp-config.php and point at the WP Super Cache plugin directory. -->