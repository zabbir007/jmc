<style type="text/css">
	.give_alert_for_add_to_cart{
		display:none;
		position:fixed;
		width:100%;
		height:100%;
		z-index:7777;
		top:0;
		left:0;
		overflow:auto;
		background:rgba(0,0,0,0);
		padding-top:150px;
		animation-name:alertbox;
		animation-duration:4s;
		animation-delay:-2s;
	}
	.alert_text_area{width:50%;margin:0px auto}
	.show_cart_highlight{
		position:fixed;
		z-index:999999;
		top:0;
		left:0;
		overflow:hidden;
		background:rgb(11, 6, 83);
		float:right;
		border-radius:100%;
		text-align:center;
		font-weight:bold;
		color:#fff;
		width:80px;
		height:80px;
		vertical-align:bottom;
		animation-name: example;
		animation-duration: 4s;
		animation-delay: -2s;
	}
	.cart-text{
		width: 50px;
		height: 30px;
		line-height: 0px;
		margin-top: 30px;
		margin-left: 13px;
	}
	.cart_value{margin-left:7px}
	@keyframes example {
    0%   {background-color:red; left:0px; top:0px;}
    25%  {background-color:yellow; left:0px; top:0px;}
    50%  {background-color:blue; left:0px; top:200px;}
    75%  {background-color:green; left:0px; top:0px;}
    100% {background-color:red; left:0px; top:0px;}
	}
	@keyframes alertbox {
    0%   {left:0px; top:0px;}
    25%  {left:0px; top:-100px;}
    50%  {left:0px; top:-100px;}
    75%  {left:0px; top:100px;}
    100% {left:0px; top:0px;}
}
</style>
<div class="give_alert_for_add_to_cart" id="add_to_cart_alert">
	<div class="container">
	<div class="alert_text_area">
	<div class="alert alert-danger" id="alert_area">
		<span id="close_alert" class="close" aria-label="close">&times;</span>
		<strong><span id="alert_product_name"></span></strong>
	</div>
	</div>
	</div>
</div>


<div class="footer-wrapper ">

                
                    
<div id="footer" class="footer-1 show-ribbon">
            <div class="footer-main">
            <div class="container">
                                    <div class="footer-ribbon" lang="en">Get in touch</div>
                
                                    <div class="row">
                                                        <div class="col-lg-3">
                                    <aside id="contact-info-widget-2" class="widget contact-info"><h3 class="widget-title" lang="en">CONTACT INFORMATION</h3> 
<?php
	$sql_phone = "select * from web_attribut_info";
	$result_phone = $db->query($sql_phone);
	$data_phone = mysqli_fetch_array($result_phone);
?>									<div class="contact-info contact-info-block">
                        <ul class="contact-details">
                <li><i class="fa fa-map-marker"></i> <strong lang="en">ADDRESS:</strong> <span><?=$data_phone[4]?></span></li>                <li><i class="fa fa-phone">
				</i> <strong lang="en">PHONE:</strong> <span><?=$data_phone[2]?></span></li>                <li><i class="fa fa-envelope"></i> <strong lang="en">EMAIL:</strong> <span><a href="<?=$data_phone[3]?>"><?=$data_phone[3]?></a></span></li>               
<?php 
	$sql_work="select * from working_hour";
	$result_work=$db->query($sql_work);
	$data_work = mysqli_fetch_array($result_work);
?>
				<li><i class="fa fa-clock-o"></i> <strong lang="en">WORKING DAYS/HOURS:</strong> <span><?=$data_work[1];?><br><?=$data_work[2];?><br><?=$data_work[3];?></span></li>            </ul>
                    </div>

        </aside>                             </div>
                                                            <div class="col-lg-9">
                                    <aside id="block-widget-4" class="widget widget-block">            <div class="block">
                <style type="text/css">.vc_custom_1489841894940{border-top-width: 1px !important;padding-top: 32px !important;border-top-color: #3d3d38 !important;border-top-style: solid !important;}.vc_custom_1506170369566{margin-top: 28px !important;margin-bottom: 0px !important;}.vc_custom_1491024296020{margin-bottom: 16px !important;}.vc_custom_1491024306428{margin-bottom: 10px !important;}.vc_custom_1491024314022{margin-bottom: 10px !important;}</style><div class="porto-block "><div class="vc_row wpb_row row"><div class="vc_column_container col-md-6"><div class="wpb_wrapper vc_column-inner"><h4  class="vc_custom_heading vc_custom_1491024296020 align-left" lang="en">BE THE FIRST TO KNOW</h4>
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<p style="line-height: 20px;">Get all the latest information on Events, Sales and Offers.<br />
Sign up for newsletter today.</p>

		</div>
	</div>
</div></div><div class="vc_column_container col-md-6 vc_custom_1506170369566"><div class="wpb_wrapper vc_column-inner">
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<div class="widget_wysija_cont shortcode_wysija"><div id="msg-form-wysija-shortcode5a7fcaf47b573-1" class="wysija-msg ajax"></div></div>

		</div>
	</div>
</div></div></div><div class="vc_row wpb_row row vc_custom_1489841894940 vc_row-has-fill"><div class="vc_column_container col-md-5"></div><div class="vc_column_container col-md-7"><div class="wpb_wrapper vc_column-inner"><div class="vc_row wpb_row vc_inner row"><div class="vc_column_container col-md-6"><div class="wpb_wrapper vc_column-inner">
<div class="wpb_text_column wpb_content_element " >
<div class="wpb_wrapper">

</div>
</div>
</div></div><div class="vc_column_container col-md-6"><div class="wpb_wrapper vc_column-inner">
<div class="wpb_text_column wpb_content_element " >
<div class="wpb_wrapper">

</div>
</div>
</div></div></div></div></div></div></div>            </div>
</aside>                                </div>
</div>

</div>
</div>

<div class="footer-bottom">
<div class="container">
<div class="footer-left">
<?php 
	$sql="SELECT * FROM `footer`";
	$result=$db->query($sql);
	$data=mysqli_fetch_array($result);
?>
<?=$data[1]?>
</div>

<div class="footer-center">
<img class="img-responsive" src="wp-content/uploads/sites/78/2017/03/payment-icon.png" alt="Payment Gateways" />
</div>

</div>
<center><span lang="en">Software solution by </span><a href="http://www.ngicon.com">Ngicon.com</a></center>
</div>
</div>

</div>
</div>
<!-- responsive menu start here-->
<div class="panel-overlay"></div>
<div id="side-nav-panel" class="">
	<a href="#" class="side-nav-panel-close"><i class="fa fa-close"></i></a>
	<div class="menu-wrap">
		<ul id="menu-main-menu-1" class="mobile-menu accordion-menu">
			<li id="accordion-menu-item-744" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-143 current_page_item active">
				<a href="index.php" rel="nofollow" class=" current ">Home</a>
			</li>
			<?php
				$sql_for_menue = "select * from new_tbl2 where desc2='0'";
				$result_for_menue = $db2->query($sql_for_menue);
				while($data_for_menue = mysqli_fetch_array($result_for_menue)){
			?>
					<li id="accordion-menu-item-781" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children  has-sub">
						<a href="#" rel="nofollow" class=""><?=$data_for_menue[1];?></a>
						<span class="arrow"></span>
											<ul class="sub-menu">
												<?php
													$sql_for_submenue = "select * from 0_stock_category where product_group='$data_for_menue[0]'";
													$result_for_submenue = $db2->query($sql_for_submenue);
													while($data_for_submenue = mysqli_fetch_array($result_for_submenue)){
												?>
														<li id="accordion-menu-item-785" class="menu-item menu-item-type-post_type menu-item-object-page ">
															<a href="shop.php?cat=<?=$data_for_submenue[0];?>" rel="nofollow" class=""><?=$data_for_submenue[1];?></a>
														</li>
												<?php
													}
												?>
										
						
						<li id="accordion-menu-item-730" class="menu-item menu-item-type-custom menu-item-object-custom "></li>
						</ul>
					</li>
					<?php
								}
							
							?>
			<?php
				
			?>
			<li id="nav-menu-item-779" class="menu-item menu-item-type-post_type menu-item-object-page " data-cols="1">
				<a href="contact_us.php" class="">Contact Us</a>
			</li>
		</ul>
	</div>
	<div class="menu-wrap">
		<ul id="menu-top-navigation-1" class="top-links accordion-menu show-arrow">
			<?php
				if(isset($_SESSION['00user_email00'])){
			?>
					<li class="menu-item">
						<a title="<?=$_SESSION['00user_title00']." ".$_SESSION['00firstname00'];?>" href="myaccount.php"><button class="btn btn-success"><?=$_SESSION['00user_title00']." ".$_SESSION['00firstname00'];?> <i class="fa fa-user"></i></button></a>
					</li>
					<li class="menu-item">
						<a href="myorder.php"><button class="btn btn-success">My order <i class="fa fa-first-order"></i></button></a>
					</li>
					<li class="menu-item">
						<a href="logout.php"><button class="btn btn-success">Logout <i class="fa fa-sign-out"></i></button></a>
					</li>
			<?php
				}
				else{
			?>
					<li class="menu-item">
						<a href="account.php">Log In</a>
					</li>
			<?php
				}
			?>
		</ul>
	</div>
</div>








<!-- responsive menu End-->
<script type="text/javascript">
	jQuery(document).ready(function($) {
function resizeBannerHeight() {
if (Modernizr.mq('only all and (min-width: 992px)')) {
$(".home-banner-slider .porto-ibanner").height($(".porto-links-block").height());
} else {
$(".home-banner-slider .porto-ibanner").height('auto');
}
}
$(window).resize(function() {
resizeBannerHeight();
});
resizeBannerHeight();
});        </script>



<link rel='stylesheet' property='stylesheet' id='vc_google_fonts_abril_fatfaceregular-css'  href='http://fonts.googleapis.com/css?family=Abril+Fatface%3Aregular' type='text/css' media='all' />

<script type='text/javascript' src='../wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js'></script>
<script type='text/javascript' src='../wp-content/plugins/contact-form-7/includes/js/scripts.js'></script>
<script type='text/javascript' src='../wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js'></script>
<script type='text/javascript' src='../wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js'></script>
<script type='text/javascript' src='../wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js'></script>
<script type='text/javascript' src='../wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js'></script>
<script type='text/javascript' src='../wp-content/plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min.js'></script>
<script type='text/javascript' src='../wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min.js'></script>
<script type='text/javascript' src='../wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl.js'></script>
<script type='text/javascript' src='../wp-includes/js/comment-reply.min.js'></script>
<script type='text/javascript' src='../wp-includes/js/underscore.min.js'></script>
<script type='text/javascript' src='../wp-includes/js/wp-util.min.js'></script>

<script type='text/javascript' src='../wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min.js'></script>
<script type='text/javascript' src='../wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js'></script>
<script type='text/javascript'>
var js_porto_vars = {"rtl":"","ajax_url":"http:\/\/www.portotheme.com\/wordpress\/porto\/shop2\/wp-admin\/admin-ajax.php","change_logo":"1","container_width":"1170","grid_gutter_width":"20","show_sticky_header":"1","show_sticky_header_tablet":"1","show_sticky_header_mobile":"1","ajax_loader_url":"\/\/www.portotheme.com\/wordpress\/porto\/shop2\/wp-content\/themes\/porto\/images\/ajax-loader@2x.gif","category_ajax":"1","prdctfltr_ajax":"","show_minicart":"1","slider_loop":"1","slider_autoplay":"1","slider_autoheight":"1","slider_speed":"5000","slider_nav":"","slider_nav_hover":"1","slider_margin":"","slider_dots":"1","slider_animatein":"","slider_animateout":"","product_thumbs_count":"4","product_zoom":"1","product_zoom_mobile":"1","product_image_popup":"1","zoom_type":"inner","zoom_scroll":"1","zoom_lens_size":"200","zoom_lens_shape":"square","zoom_contain_lens":"1","zoom_lens_border":"1","zoom_border_color":"#888888","zoom_border":"0","screen_lg":"1190","mfp_counter":"%curr% of %total%","mfp_img_error":"<a href=\"%url%\">The image<\/a> could not be loaded.","mfp_ajax_error":"<a href=\"%url%\">The content<\/a> could not be loaded.","popup_close":"Close","popup_prev":"Previous","popup_next":"Next","request_error":"The requested content cannot be loaded.<br\/>Please try again later."};
</script>
<script type='text/javascript' src='../wp-content/themes/porto/js/theme.min.js'></script>
<script type='text/javascript' src='../wp-includes/js/wp-embed.min.js'></script>
<script type='text/javascript' src='../wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-en.js'></script>
<script type='text/javascript' src='../wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js'></script>

<script type='text/javascript' src='../wp-content/plugins/wysija-newsletters/js/front-subscribers.js'></script>

<script type="text/javascript">

			jQuery(document).ready(function($){
$("#footer .widget_wysija_cont .wysija-submit").removeClass("btn-default").addClass("btn-primary");
});
			</script>

 

</body>


</html>