<?php 
session_start();

require_once "config.php";
 
                 	
                 

		//require_once "config_sms.php";
		require_once "config_pos.php";
if(empty($_SESSION['shoping_cart'])){
	header('location: index.php');
	
}

if (empty($_SESSION['00user_email00'])) {
		header('location: account.php');
		} else{
		
		?>

<!DOCTYPE html>
<html lang="en-US">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<?php
	include_once "head.php";
?>

<body class="home page-template-default page page-id-143 full blog-78  yith-wcan-free wpb-js-composer js-comp-ver-5.4.5 vc_responsive" ">

<div class="page-wrapper">

    <div class="header-wrapper clearfix"><!-- header wrapper -->
                                
                    <header id="header" class="header-separate header-1  sticky-menu-header">
<?php
	include_once "header_top_menu.php";
?>
   


 <div class="header-main">
        <div class="container">
            <div class="header-left">
				<?php
					$sql_logo = "select * from logo";
					$result_logo = $db->query($sql_logo);
					$data_logo = mysqli_fetch_array($result_logo);
				?>
                <h1 class="logo"><a href="index.php" title="<?=$data['title'];?>" rel="home">
                <img class="img-responsive standard-logo" src="<?=$data_logo['logo_path'];?>" alt="<?=$data['title'];?>" /><img class="img-responsive retina-logo" src="<?=$data_logo['logo_path'];?>" alt="<?=$data['title'];?>" style="display:none;" /> </a>
    </h1>            </div>
            <div class="header-center">
                <a class="mobile-toggle"><i class="fa fa-reorder"></i></a>
                    <div class="searchform-popup">
        <a class="search-toggle"><i class="fa fa-search"></i></a>
     <form action="search_category_product.php" method="post"
        class="searchform searchform-cats">
        <fieldset>
            <span class="text"><input name="type_search_category" id="type_search_category" type="text" value="" placeholder="Search&hellip;" autocomplete="off"/></span>
            <input type="hidden" name="post_type" value="product"/>
			<select  name='default_search_category' id='product_cat' class='cat'>
			<option value='0' selected lang="en">All Categories</option>
			<?php
				$sql_category = "select * from 0_stock_category";
				$result_category = $db2->query($sql_category);
				while($data_category = mysqli_fetch_array($result_category)){
			?>
				<option class="level-0" value="<?=$data_category[0];?>"><?=$data_category[1];?></option>
			<?php
				}
			?>
			</select>
            <span class="button-wrap"><button class="btn btn-special" title="Search" type="submit"><i class="fa fa-search"></i></button></span>
        </fieldset>
    </form>
        </div>
                </div>
            <div class="header-right">
                                <div class="header-minicart-arrow">
                           <div style="border: solid 1px #ffdc52; background: #ffdc52; border-radius: 5px; " id="mini-cart" class="mini-cart minicart-arrow effect-fadein-up minicart-style2" >
            <div id="show_add_cart" >
				<div class="cart-head cart-head4" >
					<a href="shop_cart_box.php"><img style="width: 48px;height: 30px; " src="icon/cart.png"/></a>
					<?php
						if(isset($_SESSION['shoping_cart'])){
							$count_shoping_cart = count($_SESSION['shoping_cart']);
						}
						else{
							$count_shoping_cart = 0;
						}
					?>
				 <span class="cart-items" lang="en" style="background: #4e7df1;"><?=$count_shoping_cart;?></span>
				</div>
				<div class="cart-popup widget_shopping_cart">
					<div class="widget_shopping_cart_content">
						<div class="cart">
							<table border="0" cellpadding="5px" cellspacing="0px" style="margin:0px auto;text-align:center">
							<?php
							$total = 0;
							if(!empty($_SESSION['shoping_cart'])){
							?>
							<thead>
							<tr>
							<th></th>
							<th><span lang="en">Name</span></th>
							<th><span lang="en">Qty</span></th>
							<th><span lang="en">Price</span></th>
							<th><i class="fa fa-trash"></i></th>
							</tr>
							</thead>
							<?php
							
							foreach($_SESSION['shoping_cart'] as $keys => $values)
							{
							?>
							<tr>
							<td><img style="width: 500px;height: 50px;" src="<?php echo $product_images_url.$values['item_id'].".jpg";?>" /></td>
							<td><?php echo $values['item_name'];?></td>
							<td><?php echo $values['item_qty'];?></td>
							<td>$<?php echo $values['item_price'];?></td>
							<td><button style="background:none;border:none;cursor:pointer" onclick="remove_product(this.value);" value="<?php echo $values['item_id']?>" ><i class="fa fa-remove" style="font-size:16px;color:red"></i></button></td>
							</tr>
							<?php
							}
							?>
								<tr>
									<td colspan="5" style="text-align:right;"><a style="background: #c3c111;color: azure;padding: 10px;" href="shop_cart_box.php"><span lang="en">Check Out</span></a></td>
								</tr>
							<?php
							}
							else{
							?>
							<tr>
							<td><span lang="en">Your Cart is empty!</span></td>
							</tr>
							<?php
							}
							?>
							</table>
						</div>
					</div>
				</div>
			</div>
        </div>
                    </div>

                
            </div>
        </div>
            </div>
          

           <div id="navbarr">
            <div id="main-menu" class="row hiddenbar" >
                         

	<?php
		include_once ("allmenu.php");
	?>
	
</div>
</div>

</header>
</div><!-- end header wrapper -->
<div class="container">
<section class="dropdown-content1 " id="myDropdown1">


<div class="wpb_wrapper vc_column-inner">

<div class="sidebar-content">
<div class="fixed d">
	<?php
	include("sidebarmenusticky.php");
	?>
</div>
<div class="fixed1 e">
	<?php
	include("sidebarmenusticky.php");
	?>
</div>
</div>
</div>


</section>

</div>
<!--Header End -->
<div class="sign_in_form">
		<div class="container">
		<h1 class="page_heading" lang="en">Confirm Your Order</h1>
		<div class="header_border"></div>
		<div class="sign_full_field">
			<div class="sign_field_second">
			<h1 class="page_heading_2" lang="en">Billing Information Required</h1>
			<div class="header_border"></div>

			<?php

			 $sqla = "SELECT * FROM user_id WHERE mobile_no = ".$_SESSION['00mobile_number00']." LIMIT 1";
              $resultalt = mysqli_query($db, $sqla);
              while ($value = mysqli_fetch_array($resultalt)) {		 ?>
			<form action="order-confirm.php" method="post" onsubmit="return passwrod_validation();">
			<table border="0px" cellpadding="10px" cellspacing="10px" style="margin:20px auto">
			<tr>
				<td lang="en">Enter Delivery Name:&nbsp;&nbsp;</td>
				<td colspan="2"><input class="input_type_class" type="text" name="D_name" value="<?php echo $value['title']; echo $value['first_name'] ;?>"  required></td>
			</tr>
			<tr>
				<td style="height:15px"><td/>
			</tr>
			<tr>
				<td lang="en">Delivery Address:&nbsp;&nbsp;</td>
				<td colspan="2"><input class="input_type_class" type="text" name="address" value="<?php echo $value['last_name']; ?>" required></td>
			</tr>
			<tr>
				<td style="height:15px"><td/>
			</tr>
			<tr>
				<td lang="en">Email:&nbsp;&nbsp;</td>
				<td><input class="input_type_class" title="Please fill email out this fill." type="email" name="email" id="email" value="<?php echo $value['email'] ;?>"><span id="showAlert" style="color:red;position:absolute"></span></td>
			</tr>
			<tr>
				<td style="height:15px"><td/>
			</tr>
			<tr>
				<td lang="en">Delivery Mobile No.:&nbsp;&nbsp;</td>
				<td colspan="2"><input class="input_type_class" value="<?php echo $value['mobile_no'] ;?>"  type="text" name="D_mobile_number" id="mobile_number" required></td>
			</tr>
<?php } ?>

            <tr>
				<td style="height:15px"><td/>
			</tr>
			<tr>
				<td lang="en">Select Payment Option:&nbsp;&nbsp;</td>
				<td colspan="2"><select name="paymentoption">
					<option value="Cash On Delivery">Cash On Delivery</option>
					<option value="Online Payment">Bkash Payment</option>
				</select></td>
			</tr>
             <tr>
				<td style="height:15px"><td/>
			</tr>
			<tr>
				<td lang="en">Preferred Delivery Time:&nbsp;&nbsp;</td>
				<td colspan="2">

                 <?php  
                  date_default_timezone_set("Asia/Dhaka");
               if(date("l") == 'Wednesday'){ ?>
					<select name="daypost">
					<option value="Thursday">Thursday</option>
					<option value="Friday">Friday</option>
					<option value="Saturday">Saturday</option>
					<option value="Sunday">Sunday</option>
					<option value="Monday">Monday</option>
				</select>
                     <select name="timepost">
                     	<option value="08.00 am - 09.00 am">08.00 am -- 09.00 am</option>
                     	<option value="09.00 am - 10.00 am">09.00 am -- 10.00 am</option>
                     	<option value="10.00 am - 11.00 am">10.00 am -- 11.00 am</option>
                     	<option value="11.00 am - 12.00 pm">11.00 am -- 12.00 pm</option>
                     	<option value="12.00 pm - 01.00 pm">12.00 pm -- 01.00 pm</option>
                     	<option value="01.00 pm - 02.00 pm">01.00 pm -- 02.00 pm</option>
                     	<option value="02.00 pm - 03.00 pm">02.00 pm -- 03.00 pm</option>
                     	<option value="03.00 pm - 04.00 pm">03.00 pm -- 04.00 pm</option>
                     	<option value="04.00 pm - 05.00 pm">04.00 pm -- 05.00 pm</option>
                     	<option value="05.00 pm - 06.00 pm">05.00 pm -- 06.00 pm</option>
                     	<option value="06.00 pm - 07.00 pm">06.00 pm -- 07.00 pm</option>
                     	<option value="07.00 pm - 08.00 pm">07.00 pm -- 08.00 pm</option>
                     	<option value="08.00 pm - 09.00 pm">08.00 pm -- 09.00 pm</option>
                     </select>
                <?php }

                date_default_timezone_set("Asia/Dhaka");
               if(date("l") == 'Thursday'){ 
               date_default_timezone_set("Asia/Dhaka"); ?>
					<select name="daypost">
					<option value="Friday">Friday</option>
					<option value="Saturday">Saturday</option>
					<option value="Sunday">Sunday</option>
					<option value="Monday">Monday</option>
					<option value="Tuesday">Tuesday</option>
				</select>
                     <select name="timepost">
                     	<option value="08.00 am -- 09.00 am">08.00 am -- 09.00 am</option>
                     	<option value="09.00 am -- 10.00 am">09.00 am -- 10.00 am</option>
                     	<option value="10.00 am -- 11.00 am">10.00 am -- 11.00 am</option>
                     	<option value="11.00 am -- 12.00 pm">11.00 am -- 12.00 pm</option>
                     	<option value="12.00 pm -- 01.00 pm">12.00 pm -- 01.00 pm</option>
                     	<option value="01.00 pm -- 02.00 pm">01.00 pm -- 02.00 pm</option>
                     	<option value="02.00 pm -- 03.00 pm">02.00 pm -- 03.00 pm</option>
                     	<option value="03.00 pm -- 04.00 pm">03.00 pm -- 04.00 pm</option>
                     	<option value="04.00 pm -- 05.00 pm">04.00 pm -- 05.00 pm</option>
                     	<option value="05.00 pm -- 06.00 pm">05.00 pm -- 06.00 pm</option>
                     	<option value="06.00 pm -- 07.00 pm">06.00 pm -- 07.00 pm</option>
                     	<option value="07.00 pm -- 08.00 pm">07.00 pm -- 08.00 pm</option>
                     	<option value="08.00 pm -- 09.00 pm">08.00 pm -- 09.00 pm</option>
                     </select>

                <?php } 

                   date_default_timezone_set("Asia/Dhaka");
               if(date("l") == 'Friday'){ 
               date_default_timezone_set("Asia/Dhaka"); ?>
					<select name="daypost">
					<option value="Saturday">Saturday</option>
					<option value="Sunday">Sunday</option>
					<option value="Monday">Monday</option>
					<option value="Tuesday">Tuesday</option>
					<option value="Wednesday">Wednesday</option>
				</select>
                     <select name="timepost">
                     	<option value="08.00 am -- 09.00 am">08.00 am -- 09.00 am</option>
                     	<option value="09.00 am -- 10.00 am">09.00 am -- 10.00 am</option>
                     	<option value="10.00 am -- 11.00 am">10.00 am -- 11.00 am</option>
                     	<option value="11.00 am -- 12.00 pm">11.00 am -- 12.00 pm</option>
                     	<option value="12.00 pm -- 01.00 pm">12.00 pm -- 01.00 pm</option>
                     	<option value="01.00 pm -- 02.00 pm">01.00 pm -- 02.00 pm</option>
                     	<option value="02.00 pm -- 03.00 pm">02.00 pm -- 03.00 pm</option>
                     	<option value="03.00 pm -- 04.00 pm">03.00 pm -- 04.00 pm</option>
                     	<option value="04.00 pm -- 05.00 pm">04.00 pm -- 05.00 pm</option>
                     	<option value="05.00 pm -- 06.00 pm">05.00 pm -- 06.00 pm</option>
                     	<option value="06.00 pm -- 07.00 pm">06.00 pm -- 07.00 pm</option>
                     	<option value="07.00 pm -- 08.00 pm">07.00 pm -- 08.00 pm</option>
                     	<option value="08.00 pm -- 09.00 pm">08.00 pm -- 09.00 pm</option>
                     </select>
                 
<?php } 

                   date_default_timezone_set("Asia/Dhaka");
               if(date("l") == 'Saturday'){ 
               date_default_timezone_set("Asia/Dhaka"); ?>
					<select name="daypost">
					<option value="Sunday">Sunday</option>
					<option value="Monday">Monday</option>
					<option value="Tuesday">Tuesday</option>
					<option value="Wednesday">Wednesday</option>
					<option value="Thursday">Thursday</option>
				</select>
                     <select name="timepost">
                     	<option value="08.00 am -- 09.00 am">08.00 am -- 09.00 am</option>
                     	<option value="09.00 am -- 10.00 am">09.00 am -- 10.00 am</option>
                     	<option value="10.00 am -- 11.00 am">10.00 am -- 11.00 am</option>
                     	<option value="11.00 am -- 12.00 pm">11.00 am -- 12.00 pm</option>
                     	<option value="12.00 pm -- 01.00 pm">12.00 pm -- 01.00 pm</option>
                     	<option value="01.00 pm -- 02.00 pm">01.00 pm -- 02.00 pm</option>
                     	<option value="02.00 pm -- 03.00 pm">02.00 pm -- 03.00 pm</option>
                     	<option value="03.00 pm -- 04.00 pm">03.00 pm -- 04.00 pm</option>
                     	<option value="04.00 pm -- 05.00 pm">04.00 pm -- 05.00 pm</option>
                     	<option value="05.00 pm -- 06.00 pm">05.00 pm -- 06.00 pm</option>
                     	<option value="06.00 pm -- 07.00 pm">06.00 pm -- 07.00 pm</option>
                     	<option value="07.00 pm -- 08.00 pm">07.00 pm -- 08.00 pm</option>
                     	<option value="08.00 pm -- 09.00 pm">08.00 pm -- 09.00 pm</option>
                     </select>
                      

                      <?php } 

                   date_default_timezone_set("Asia/Dhaka");
               if(date("l") == 'Sunday'){ 
               date_default_timezone_set("Asia/Dhaka"); ?>
					<select name="daypost">
					<option value="Monday">Monday</option>
					<option value="Tuesday">Tuesday</option>
					<option value="Wednesday">Wednesday</option>
					<option value="Thursday">Thursday</option>
					<option value="Friday">Friday</option>
				</select>
                     <select name="timepost">
                     	<option value="08.00 am -- 09.00 am">08.00 am -- 09.00 am</option>
                     	<option value="09.00 am -- 10.00 am">09.00 am -- 10.00 am</option>
                     	<option value="10.00 am -- 11.00 am">10.00 am -- 11.00 am</option>
                     	<option value="11.00 am -- 12.00 pm">11.00 am -- 12.00 pm</option>
                     	<option value="12.00 pm -- 01.00 pm">12.00 pm -- 01.00 pm</option>
                     	<option value="01.00 pm -- 02.00 pm">01.00 pm -- 02.00 pm</option>
                     	<option value="02.00 pm -- 03.00 pm">02.00 pm -- 03.00 pm</option>
                     	<option value="03.00 pm -- 04.00 pm">03.00 pm -- 04.00 pm</option>
                     	<option value="04.00 pm -- 05.00 pm">04.00 pm -- 05.00 pm</option>
                     	<option value="05.00 pm -- 06.00 pm">05.00 pm -- 06.00 pm</option>
                     	<option value="06.00 pm -- 07.00 pm">06.00 pm -- 07.00 pm</option>
                     	<option value="07.00 pm -- 08.00 pm">07.00 pm -- 08.00 pm</option>
                     	<option value="08.00 pm -- 09.00 pm">08.00 pm -- 09.00 pm</option>
                     </select>

                     <?php } 

                   date_default_timezone_set("Asia/Dhaka");
               if(date("l") == 'Monday'){ 
               date_default_timezone_set("Asia/Dhaka"); ?>
					<select name="daypost">
					<option value="Tuesday">Tuesday</option>
					<option value="Wednesday">Wednesday</option>
					<option value="Thursday">Thursday</option>
					<option value="Friday">Friday</option>
					<option value="Saturday">Saturday</option>
				</select>
                     <select name="timepost">
                     	<option value="08.00 am -- 09.00 am">08.00 am -- 09.00 am</option>
                     	<option value="09.00 am -- 10.00 am">09.00 am -- 10.00 am</option>
                     	<option value="10.00 am -- 11.00 am">10.00 am -- 11.00 am</option>
                     	<option value="11.00 am -- 12.00 pm">11.00 am -- 12.00 pm</option>
                     	<option value="12.00 pm -- 01.00 pm">12.00 pm -- 01.00 pm</option>
                     	<option value="01.00 pm -- 02.00 pm">01.00 pm -- 02.00 pm</option>
                     	<option value="02.00 pm -- 03.00 pm">02.00 pm -- 03.00 pm</option>
                     	<option value="03.00 pm -- 04.00 pm">03.00 pm -- 04.00 pm</option>
                     	<option value="04.00 pm -- 05.00 pm">04.00 pm -- 05.00 pm</option>
                     	<option value="05.00 pm -- 06.00 pm">05.00 pm -- 06.00 pm</option>
                     	<option value="06.00 pm -- 07.00 pm">06.00 pm -- 07.00 pm</option>
                     	<option value="07.00 pm -- 08.00 pm">07.00 pm -- 08.00 pm</option>
                     	<option value="08.00 pm -- 09.00 pm">08.00 pm -- 09.00 pm</option>
                     </select>
                   
                     <?php } 

                   date_default_timezone_set("Asia/Dhaka");
               if(date("l") == 'Tuesday'){ 
               date_default_timezone_set("Asia/Dhaka"); ?>
					<select name="daypost">
					<option value="Wednesday">Wednesday</option>
					<option value="Thursday">Thursday</option>
					<option value="Friday">Friday</option>
					<option value="Saturday">Saturday</option>
					<option value="Tuesday">Sunday</option>
				</select>
                     <select name="timepost">
                     	<option value="08.00 am -- 09.00 am">08.00 am -- 09.00 am</option>
                     	<option value="09.00 am -- 10.00 am">09.00 am -- 10.00 am</option>
                     	<option value="10.00 am -- 11.00 am">10.00 am -- 11.00 am</option>
                     	<option value="11.00 am -- 12.00 pm">11.00 am -- 12.00 pm</option>
                     	<option value="12.00 pm -- 01.00 pm">12.00 pm -- 01.00 pm</option>
                     	<option value="01.00 pm -- 02.00 pm">01.00 pm -- 02.00 pm</option>
                     	<option value="02.00 pm -- 03.00 pm">02.00 pm -- 03.00 pm</option>
                     	<option value="03.00 pm -- 04.00 pm">03.00 pm -- 04.00 pm</option>
                     	<option value="04.00 pm -- 05.00 pm">04.00 pm -- 05.00 pm</option>
                     	<option value="05.00 pm -- 06.00 pm">05.00 pm -- 06.00 pm</option>
                     	<option value="06.00 pm -- 07.00 pm">06.00 pm -- 07.00 pm</option>
                     	<option value="07.00 pm -- 08.00 pm">07.00 pm -- 08.00 pm</option>
                     	<option value="08.00 pm -- 09.00 pm">08.00 pm -- 09.00 pm</option>
                     </select>
<?php
}
                 ?>
			</td>
			</tr>

			<tr>
				<td style="height:15px"><td/> 
			</tr>
			<tr>
				<td colspan="2" style="text-align:center"><button class="input_type_class2 btn btn-primary" type="submit" name="order_confirm" lang="en">Confirm Order</button></td>
			</tr>
			</table>
		</form>
			</div>
		</div>
		</div>
	</div>

			<style type="text/css">
		.sign_in_form{margin-top: 40px;
		 margin-bottom: 40px;}
		.page_heading{text-transform:uppercase;font-size:22px}
		.page_heading_2{text-transform:uppercase;font-size:17px}
		.header_border{margin-top:10px;border-bottom:1px solid #aaa}
		.sign_full_field{border:1px solid #aaa;background:#fff;margin-top:20px}
		.sign_field_second{padding:20px}
		<!--change input type box width from lessframwork -->
		.input_type_class2:hover{background:#42DAB8;color:#fff}
		.input_type_class3{border: 1px solid #aaa;
		width: 100px;height:44px}
		.input_type_class3:hover{background:#42DAB8;color:#fff}
	</style>
	<script type="text/javascript">
		function passwrod_validation(){
			var pasw = document.getElementById("password").value;
			var confpasw = document.getElementById("confirm_password").value;
			if(pasw!=confpasw){
				alert("Password Miss Match");
				return false;
			}
			else{
				return true;
			}
		}
	</script>
<?php

	include_once "footer.php";
}
?>