<?php

include('admincon.php');
if (empty($_SESSION['inthemainathorityaccessadmincontrolifthatsare'])) { ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div style="max-width: 70%; min-width: 200px; min-height: 300px; background: white; border-radius: 8px; margin: 0 auto;">
    <p style="text-align: center; color: red;"><i class="fa fa-warning"></i> Unauthorized access request.</p>
</div>
<?php }
else {

include('header.php'); ?>

<body>

    <div id="wrapper">

        <?php //sidebar added here 
         include('sidebar.php');
        ?>

        <div id="page-wrapper">
            
            <!-- /.row -->
            <div class="row">

                <?php
                if (isset($_GET['shoplist'])) {
                    # code...
               
             $sqlpending = "SELECT * FROM marchant_user WHERE admin_approval = '1'";
             $sqlexecute = mysqli_query($db, $sqlpending);
            
                ?>
                <div class="row">
                    <center><h3 class="text-primary"><u>Marchant Shop List</u></h3></center>
                </div>
<?php if (mysqli_num_rows($sqlexecute) == 0) {
               echo "<center><p>Marchant Request Empty !</p></center>";
            } else{ ?>
                <table class="table table-striped">
                    <tr>
                    <th scope="col" class="text-center">Marchant Name</th>
                    <th scope="col" class="text-center">Contact Number</th>
                    <th scope="col" class="text-center">Shop Name</th>
                    <th scope="col" class="text-center">Shop Address</th>
                    <th scope="col" colspan="2" class="text-center">Go to shop</th>
                    </tr>
                    <?php while ($row = mysqli_fetch_array($sqlexecute)) { ?>
                       <tr>
                        <td class="text-center"><?php echo $row['marchant_name']; ?></td>
                        <td class="text-center"><?php echo $row['marchant_contact']; ?></td>
                        <td class="text-center"><?php echo $row['marchant_shop']; ?></td>
                        <td class="text-center"><?php echo $row['marchant_shop_address']; ?></td>
                        <td><center><form action="marchantshop.php" method="GET">
                            <input type="hidden" name="shopid" value="<?php echo $row['id']; ?>">
                            <button type="submit" name="shopdetails" class="btn btn-link">Shop</button>
                        </form></center></td>

                    </tr>
                <?php    } ?>
                   
                    
                </table>
          <?php } 

             }
             if (isset($_GET['shopdetails'])) { ?>
              
         <?php    } ?>

                          </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../vendor/raphael/raphael.min.js"></script>
    <script src="../vendor/morrisjs/morris.min.js"></script>
    <script src="../data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
<?php } ?>