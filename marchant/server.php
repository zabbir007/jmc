<?php
	session_start();
	$marchant_name = "";
  $marchant_email = "";
  $marchant_contact= "";
  $marchant_username = "";
  $marchant_shop = "";
  $shop_address = "";
  $verify_email = "";
  $price = "";
  $offer = "";
  $product_title = "";
  $product_description = "";
  $category = "";
  $product_brand = "";
  $product_type = "";
  $imgfile = "";
  $alt_imgae = "";

require_once "../config.php";
require_once "../config_pos.php";

if (isset($_POST['login'])) {
  $username = mysqli_real_escape_string($db,$_POST['username']);
  $password1 = mysqli_real_escape_string($db,$_POST['password']);
 $password = md5($password1);
  $sqget = "SELECT * FROM marchant_user WHERE username = '$username' AND password = '$password' AND status = '1' AND admin_approval = '1'";
 $sqllogresult = mysqli_query($db, $sqget);
 if (mysqli_num_rows($sqllogresult) == 1) {
     $_SESSION['usermarchantaccountlogin'] = $username;
  header('location: index.php');
 } else {
   $sqgetanother = "SELECT * FROM marchant_user WHERE username = '$username' AND password = '$password' AND status = '1' AND admin_approval = '0'";
    $sqlresult = mysqli_query($db, $sqgetanother);
    if (mysqli_num_rows($sqlresult) == 1) {
     array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Your Marchant Request is Now Pending, Please wait for approval</p></center>");
    } else{
  array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Incorrect Username or Password</p></center>");
}
 }
 
}


if (isset($_POST['register'])) {
  $marchant_name = mysqli_real_escape_string($db,$_POST['marchant_name']);
  $marchant_email = mysqli_real_escape_string($db,$_POST['marchant_email']);
  $marchant_contact= mysqli_real_escape_string($db,$_POST['marchant_contact']);
  $marchant_username = mysqli_real_escape_string($db,$_POST['marchant_username']);
  $agree = mysqli_real_escape_string($db,$_POST['agree']);
  $marchant_shop = mysqli_real_escape_string($db,$_POST['marchant_shop']);
  $shop_address = mysqli_real_escape_string($db,$_POST['shop_address']);
  $password1 = mysqli_real_escape_string($db,$_POST['password1']);
  $password2 = mysqli_real_escape_string($db,$_POST['password2']);

if (!filter_var($marchant_email, FILTER_VALIDATE_EMAIL)) {
      array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Invalid email address</p></center>");
    }
    $sql = "SELECT * FROM marchant_user WHERE marchant_email = '$marchant_email' AND status = '1'";
$result = mysqli_query($db, $sql);
if (mysqli_num_rows($result) == 0) {
  $sql = "SELECT * FROM marchant_user WHERE username = '$marchant_username' AND status = '1'";
$result = mysqli_query($db, $sql);
if (mysqli_num_rows($result) == 0) {
   if (strlen($marchant_contact) !==  11) {
      array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Please select 11 digit valid number</p></center>");
    }
    else{
  if(!preg_match("/^[0]{1}[1]{1}[3-9]{1}\d+\.?\d*$/",$marchant_contact)){
    array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> You selected invalid operator prefix</p></center>");
  }
  
}
if ($password1 !== $password2) {
array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Password did not matched</p></center>");
}
}else{
  array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Username Allready Taken, Choose another one</p></center>");
}
} else{
  array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Email address already registered</p></center>");
}


if(count($errors)==0){
  $password = md5($password1);
  $code = rand(1234, 5678);
  $to = $marchant_email;
        $subject = "Verify Email Address";
        $message = "
<html>
<body>
<p>Your Email Verification Code is  <strong>".$code."</strong></p>
</body>
</html>
";
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= 'From: treeslife09@gmail.com';

mail($to,$subject,$message,$headers);
  $sqget = "INSERT INTO `marchant_user`(`marchant_name`, `marchant_email`, `marchant_contact`, `marchant_shop`, `marchant_shop_address`, `password`, `username`, `status`, `agree_status`, `verify_code`) VALUES ('$marchant_name','$marchant_email','$marchant_contact','$marchant_shop','$shop_address','$password','$marchant_username','0','$agree', '$code')";
  mysqli_query($db, $sqget);
  $_SESSION['confirmemail'] = $code;
  header('location: verify-account.php');
 }
 
}

if (isset($_POST['account_verify'])) {
  $verify_email = mysqli_real_escape_string($db,$_POST['verify_email']);
  $sql = "SELECT * FROM marchant_user WHERE verify_code = '$verify_email' AND status = '0'";
  $resultverify = mysqli_query($db, $sql);
  if (mysqli_num_rows($resultverify) == 0) {

    array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Verification code was incorrect</p></center>");
  }else{
    $sqlupdate = "UPDATE marchant_user SET status = '1' WHERE verify_code = '$verify_email'";
    mysqli_query($db, $sqlupdate);
    $data = mysqli_fetch_array($resultverify);
    $username =  $data['username'];    
 header('location: login.php');

  }
}

if (isset($_POST['uploadimageprofile'])) {
 $hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);
   $filetmp =  $_FILES ["upimg"]["tmp_name"];
   $filename = $_FILES ["upimg"]["name"];
   $filetype = $_FILES ["upimg"]["type"];
   $filepath = "photo/" .$filename;
   $move = "photo/" .$filename;
   $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
  array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> invalid Image Format, Please Select (JPG/JPEG/PNG/GIF) Format Image</p></center>");
}
else{
  
  move_uploaded_file($_FILES["upimg"]["tmp_name"], $move);
  $sql = "UPDATE marchant_user SET image_path = '$filepath' WHERE id = '$hiddenid'";
  mysqli_query($db, $sql);
  header('location: index.php');

}

}
if (isset($_POST['changeimageprofile'])) {
 $hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);
   $filetmp =  $_FILES ["changeimg"]["tmp_name"];
   $filename = $_FILES ["changeimg"]["name"];
   $filetype = $_FILES ["changeimg"]["type"];
   $filepath = "photo/" .$filename;
   $move = "photo/" .$filename;
   $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
  array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> invalid Image Format, Please Select (JPG/JPEG/PNG/GIF) Format Image</p></center>");
}
else{
  
  move_uploaded_file($_FILES["changeimg"]["tmp_name"], $move);
  $sql = "UPDATE marchant_user SET image_path = '$filepath' WHERE id = '$hiddenid'";
  mysqli_query($db, $sql);
  header('location: index.php');

}

}

if (isset($_POST['updateinfo'])) {
  $marchant_name = mysqli_real_escape_string($db,$_POST['marchant_name']);
  $marchant_contact= mysqli_real_escape_string($db,$_POST['marchant_contact']);
  $city = mysqli_real_escape_string($db,$_POST['city']);
  $country = mysqli_real_escape_string($db,$_POST['country']);
  $gender = mysqli_real_escape_string($db,$_POST['gender']);
  $marchant_shop = mysqli_real_escape_string($db,$_POST['marchant_shop']);
  $shop_address = mysqli_real_escape_string($db,$_POST['shop_address']);
  $hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);

   if (strlen($marchant_contact) !==  11) {
      array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Please select 11 digit valid number</p></center>");
    }
    else{
  if(!preg_match("/^[0]{1}[1]{1}[3-9]{1}\d+\.?\d*$/",$marchant_contact)){
    array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> You selected invalid operator prefix</p></center>");
  }
}

if (count($errors) == 0) {
  $sql ="UPDATE `marchant_user` SET `marchant_name`= '$marchant_name',`marchant_contact`= '$marchant_contact',`marchant_shop`= '$marchant_shop',`marchant_shop_address`= '$shop_address',`gender`= '$gender',`country`= '$country',`city`= '$city' WHERE id = '$hiddenid'";
  mysqli_query($db, $sql);
  header('location: index.php');
}

}
if (isset($_POST['updateproductinfo'])) {

  $hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);
  $itemcode = mysqli_real_escape_string($db,$_POST['itemcode']);
  $stock = mysqli_real_escape_string($db,$_POST['stock']);
  $price = mysqli_real_escape_string($db,$_POST['price']);
  $offer = mysqli_real_escape_string($db,$_POST['offer']);
  $product_title = mysqli_real_escape_string($db,$_POST['product_title']);
  $product_description = mysqli_real_escape_string($db,$_POST['product_description']);
  $category = mysqli_real_escape_string($db,$_POST['category']);
  $product_brand = mysqli_real_escape_string($db,$_POST['product_brand']);
  $product_type = mysqli_real_escape_string($db,$_POST['product_type']);
  $marchant_shop = mysqli_real_escape_string($db,$_POST['marchant_shop']);
  $shop_address = mysqli_real_escape_string($db,$_POST['shop_address']);

   $filetmp =  $_FILES["imgfile"]["tmp_name"];
   $filename = $_FILES["imgfile"]["name"];
   $filetype = $_FILES["imgfile"]["type"];
   $filepath = "photo/".$filename;
   $move = "photo/".$filename;
   $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
  array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Invalid Main Image Format, Please Select (JPG/JPEG/PNG/GIF) Format Image</p></center>");
}

   $filetmpp =  $_FILES["alt_image"]["tmp_name"];
   $filenamee = $_FILES["alt_image"]["name"];
   $filetypee = $_FILES["alt_image"]["type"];
   $filepathh = "photo/" .$filenamee;
   $movee = "photo/".$filenamee;
   if (empty($filetmpp)) {
     $filepathh = "";
   }else{
   $imageFileTypee = strtolower(pathinfo($movee,PATHINFO_EXTENSION));
    // Allow certain file formats
if($imageFileTypee != "jpg" && $imageFileTypee != "png" && $imageFileTypee != "jpeg"
&& $imageFileTypee != "gif"){
  array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Invalid Alternative Image Format, Please Select (JPG/JPEG/PNG/GIF) Format Image</p></center>");
}
}
  if (empty($price)) {
    array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Product Price is Required</p></center>");
  }
  if (empty($product_title)) {
    array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Product Title is Required</p></center>");
  }
  if (empty($product_description)) {
    array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Product Description is Required</p></center>");
  }
  /*if (empty($product_brand)) {
    array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Product Brand is Required</p></center>");
  }*/
  if (empty($product_type)) {
    array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Product Type is Required</p></center>");
  }
  if (empty($price)) {
    array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Product Price is Required</p></center>");
  }

if (count($errors) == 0) {

  move_uploaded_file($_FILES["alt_image"]["tmp_name"], $movee);
  move_uploaded_file($_FILES["imgfile"]["tmp_name"], $move);

  $slug = preg_replace('~[^\pL\d]+~u', '-', $product_title);
  $sqlinsert = "INSERT INTO marchant_item (`item_name`, `item_id`, `stock_type`, `offer`, `description`, `category`, `product_brand`, `product_type`, `shop_name`, `status`, `shop_address`, `main_image`, `alt_img`, `user_id`, `slug`, `price`) VALUES ('$product_title','$itemcode','$stock', '$offer','$product_description','$category','$product_brand', '$product_type','$marchant_shop','0','$shop_address','$filepath','$filepathh','$hiddenid', '$slug', '$price')";
  
   mysqli_query($db, $sqlinsert);
  //echo $lastid = mysqli_insert_id($db);

}

}

if (isset($_POST['editedproductinfo'])) {

  $hiddenid = mysqli_real_escape_string($db,$_POST['hiddenid']);
  $itemcode = mysqli_real_escape_string($db,$_POST['itemcode']);
  $stock = mysqli_real_escape_string($db,$_POST['stock']);
  $price = mysqli_real_escape_string($db,$_POST['price']);
  $offer = mysqli_real_escape_string($db,$_POST['offer']);
  $product_title = mysqli_real_escape_string($db,$_POST['product_title']);
  $product_description = mysqli_real_escape_string($db,$_POST['product_description']);
  $category = mysqli_real_escape_string($db,$_POST['category']);
  $product_brand = mysqli_real_escape_string($db,$_POST['product_brand']);
  $product_type = mysqli_real_escape_string($db,$_POST['product_type']);
  $marchant_shop = mysqli_real_escape_string($db,$_POST['marchant_shop']);
  $shop_address = mysqli_real_escape_string($db,$_POST['shop_address']);

   

   $filetmp =  $_FILES["imgfile"]["tmp_name"];
   $filename = $_FILES["imgfile"]["name"];
   $filetype = $_FILES["imgfile"]["type"];
   $filepath = "photo/".$filename;
   $move = "photo/".$filename;
   $imageFileType = strtolower(pathinfo($move,PATHINFO_EXTENSION));
    // Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif"){
  array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Invalid Main Image Format, Please Select (JPG/JPEG/PNG/GIF) Format Image</p></center>");
}

   $filetmpp =  $_FILES["alt_image"]["tmp_name"];
   $filenamee = $_FILES["alt_image"]["name"];
   $filetypee = $_FILES["alt_image"]["type"];
   $filepathh = "photo/" .$filenamee;
   $movee = "photo/".$filenamee;
   if (empty($filetmpp)) {
     $filepathh = "";
   }else{
   $imageFileTypee = strtolower(pathinfo($movee,PATHINFO_EXTENSION));
    // Allow certain file formats
if($imageFileTypee != "jpg" && $imageFileTypee != "png" && $imageFileTypee != "jpeg"
&& $imageFileTypee != "gif"){
  array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Invalid Alternative Image Format, Please Select (JPG/JPEG/PNG/GIF) Format Image</p></center>");
}
}
  if (empty($price)) {
    array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Product Price is Required</p></center>");
  }
  if (empty($product_title)) {
    array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Product Title is Required</p></center>");
  }
  if (empty($product_description)) {
    array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Product Description is Required</p></center>");
  }
  /*if (empty($product_brand)) {
    array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Product Brand is Required</p></center>");
  }*/
  if (empty($product_type)) {
    array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Product Type is Required</p></center>");
  }
  if (empty($price)) {
    array_push($errors, "<center><p style='padding:10px; background:#f2dede; border-radius:5px; border: 1px solid #a94442; color:#a94442;'><i class='fa fa-warning'></i> Product Price is Required</p></center>");
  }

if (count($errors) == 0) {

  move_uploaded_file($_FILES["alt_image"]["tmp_name"], $movee);
  move_uploaded_file($_FILES["imgfile"]["tmp_name"], $move);

  $slug = preg_replace('~[^\pL\d]+~u', '-', $product_title);
  $sqlinsert = "INSERT INTO marchant_item (`item_name`, `item_id`, `stock_type`, `offer`, `description`, `category`, `product_brand`, `product_type`, `shop_name`, `status`, `shop_address`, `main_image`, `alt_img`, `user_id`, `slug`, `price`) VALUES ('$product_title','$itemcode','$stock', '$offer','$product_description','$category','$product_brand', '$product_type','$marchant_shop','0','$shop_address','$filepath','$filepathh','$hiddenid', '$slug', '$price')";
  
   mysqli_query($db, $sqlinsert);
  //echo $lastid = mysqli_insert_id($db);

}

}


if(isset($_GET['logout'])){
  session_destroy();
  unset($_SESSION['usermarchantaccountlogin']);
  header('location: login.php');
}
?>