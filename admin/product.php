<?php include ('admincon.php');
if (empty($_SESSION['inthemainathorityaccessadmincontrolifthatsare'])) {

     header('location: login.php');

 } else {
 include('header.php');

 ?>

<body>

    <div id="wrapper">

        <?php //sidebar added here 
         include('sidebar.php');
        ?>

        <div id="page-wrapper">
      <?php include ('errors.php'); ?>

<center><h2 style="text-decoration: underline;">Add Product Image</h2></center>
	
<?php if (isset($_GET['searchproduct'])) {
	$productname = mysqli_real_escape_string($db2, $_GET['productname']);
	$sql = "SELECT * FROM 0_stock_master WHERE description LIKE '%$productname%'";
	$result = mysqli_query($db2, $sql);
	if (mysqli_num_rows($result) == 0) {
		echo "<center><p style='color:red;'>Search Keyword <b>" .$productname."</b> Not Found</p></center>";
	} else{ ?>
		<center>
        <form action="product.php" method="GET">
		<input type="text" name="productname" placeholder="Enter Product Name" style="height: 35px; width: 250px; border-radius: 8px 0px 0px 8px; border:1px solid #2980B9; padding: 5px; font-size:16px;"><input type="submit" name="searchproduct" value="Search" style="height: 35px; width:70px; border-radius: 0px 8px 8px 0px; border:1px solid #2980B9; padding: 5px; font-size:16px;">
	</form>
	</center>
	<br>
</center>
	<br>
	<center>
		<table class="table table-bordered">
			<tr>
				<th>Product Name</th>
				<th>Main Image</th>
				<th>Alt Image 1</th>
				<th>Alt Image 2</th>
				<th>Alt Image 3</th>
				
			</tr>
			
			<?php
				while ($row = mysqli_fetch_array($result)) { ?>
				<?php	$img_name = "";
					$img_name =	$row[0];
					$img_url = $product_images_url."$img_name.jpg"; ?>
					<tr>
				<td><?=$row['description']?></td>
				<td><img style="width:60px;height:60px" src="../<?=$img_url?>" class=" wp-post-image" alt="" srcset="" /></td>
				<td><?php if (empty($row['alt_img1'])) { ?>
				      <form action="product.php?viewallproduct=" method="POST" enctype=multipart/form-data>
				      <center><span>Image not uploaded</span></center>
				      <br>
				      <center>
				      	<input type="file" name="alt_img1" required>
				      	<br>
				      	<br>
				      	<input type="hidden" name="hiddenid" value="<?php echo $row['stock_id']; ?>">
				      	<input type="submit" name="img1update" value="Upload image">
				      </center>
				      </form>
				<?php } else{ ?>
					<form action="product.php?viewallproduct=" method="POST">
                      <input type="hidden" name="hiddenid" value="<?php echo $row['stock_id']; ?>"><center>
                      <img style="width:60px;height:60px" src="../<?php echo $row['alt_img1']; ?>" class=" wp-post-image" alt="" srcset="" />
                      <br>
                      <input type="submit" name="delete1" value="Delete">
                      </center>
                      </form>
			<?php 	} ?></td>
				<td><?php if (empty($row['alt_img2'])) { ?>
				      <form action="product.php?viewallproduct=" method="POST" enctype=multipart/form-data>
				      <center><span>Image not uploaded</span></center>
				      <br>
				      <center>
				      	<input type="file" name="alt_img2" required>
				      	<br>
				      	<br>
				      	<input type="hidden" name="hiddenid" value="<?php echo $row['stock_id']; ?>">
				      	<input type="submit" name="img2update" value="Upload image">
				      </center>
				      </form>
				<?php } else{ ?>
                      <form action="product.php?viewallproduct=" method="POST">
                      <input type="hidden" name="hiddenid" value="<?php echo $row['stock_id']; ?>"><center>
                      <img style="width:60px;height:60px" src="../<?php echo $row['alt_img2']; ?>" class=" wp-post-image" alt="" srcset="" />
                      <br>
                      <input type="submit" name="delete2" value="Delete">
                      </center>
                      </form>
			<?php 	} ?></td>
				<td><?php if (empty($row['alt_img4'])) { ?>
				      <form action="product.php?viewallproduct=" method="POST" enctype=multipart/form-data>
				      <center><span>Image not uploaded</span></center>
				      <br>
				      <center>
				      	<input type="file" name="alt_img3" required>
				      	<br>
				      	<br>
				      	<input type="hidden" name="hiddenid" value="<?php echo $row['stock_id']; ?>">
				      	<input type="submit" name="img3update" value="Upload image">
				      </center>
				      </form>
				<?php } else{ ?>
                     <form action="product.php?viewallproduct=" method="POST">
                      <input type="hidden" name="hiddenid" value="<?php echo $row['stock_id']; ?>"><center>
                      <img style="width:60px;height:60px" src="../<?php echo $row['alt_img4']; ?>" class=" wp-post-image" alt="" srcset="" />
                      <br>
                      <input type="submit" name="delete3" value="Delete">
                      </center>
                      </form>
			<?php 	} ?></td>

            
			<!--	<td><form action="parentsmenu.php" method="POST">
					<input type="hidden" name="hiddenmenuid" value="<?php //echo $row['id']; ?>">
					<button type="submit" name="editmenu">Edit Menu <?php //echo $row['id']; ?></button>
					<button style="color: red;" type="submit" name="deletemenu">Delete Menu</button>
				</form>
			</td> -->
				</tr>
			<?php } ?>
		</table>
		
	</center>
<?php	}
	
} else{ ?>

		<center>
        <form action="product.php" method="GET">
		<input type="text" name="productname" placeholder="Enter Product Name" style="height: 35px; width: 250px; border-radius: 8px 0px 0px 8px; border:1px solid #2980B9; padding: 5px; font-size:16px;"><input type="submit" name="searchproduct" value="Search" style="height: 35px; width:70px; border-radius: 0px 8px 8px 0px; border:1px solid #2980B9; padding: 5px; font-size:16px;">
	</form>
	</center>
	<br>
	<?php
	     $_PHP_SELF = "";
         $rec_limit = 10;
         $sql = "SELECT count(stock_id) FROM 0_stock_master ";
         $retval = mysqli_query( $db2 , $sql);
         
         if(! $retval ) {
            die('Could not get data: ' . mysqli_error());
         }
         $row = mysqli_fetch_array($retval );
        $rec_count = $row[0];
         
         if( isset($_GET{'page'} ) ) {
            $page = $_GET{'page'} + 1;
            $offset = $rec_limit * $page ;
         }else {
            $page = 0;
            $offset = 0;
         }
$left_rec = $rec_count - ($page * $rec_limit);
	 ?>
	 <!--
	 <ul class="pagination">
    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item active"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#">Next</a></li>
  </ul> -->
	 <?php
	 /*
	 echo "<br>".$page."<br>";
	 for ($i=0; $i < $page; $i++) { 
	 	echo "<br>".$i."<br>";
	 
	 		
	 	
	 } */

	 		  if( $page > 0 ) {
            $last = $page - 2;
            echo "<a href = \"$_PHP_SELF?page=$last\" class='btn btn-primary'><i class='fa fa-chevron-left'></i> Prev 10 Products</a> | ";
            echo "<a href = \"$_PHP_SELF?page=$page\" class='btn btn-primary'>Next 10 Products <i class='fa fa-chevron-right'></i></a>";
         }else if( $page == 0 ) {
            echo "<a href = \"$_PHP_SELF?page=$page\" class='btn btn-primary'>Next 10 Products <i class='fa fa-chevron-right'></i></a>";
         }else if( $left_rec < $rec_limit ) {
            $last = $page - 2;
            echo "<a href = \"$_PHP_SELF?page=$last\"><i class='fa fa-chevron-left'></i> Prev 10 Products</a>";
         } ?>
         <br>
         <br>
	<center>
		<table class="table table-bordered">
			<tr>
				<th>Product Name</th>
				<th>Main Image</th>
				<th>Alt Image 1</th>
				<th>Alt Image 2</th>
				<th>Alt Image 3</th>
				
			</tr>
			
				<?php $sqllogo = "SELECT * FROM 0_stock_master ORDER BY stock_id DESC LIMIT ".$offset.", ".$rec_limit."";
				$logoresult = mysqli_query($db2, $sqllogo);
				while ($row = mysqli_fetch_array($logoresult)) { ?>
				<?php	$img_name = "";
					$img_name =	$row[0];
					$img_url = $product_images_url."$img_name.jpg"; ?>
					<tr>
				<td><?=$row['description']?></td>
				<td><img style="width:60px;height:60px" src="../<?=$img_url?>" class=" wp-post-image" alt="" srcset="" /></td>
				<td><?php if (empty($row['alt_img1'])) { ?>
				      <form action="product.php?viewallproduct=" method="POST" enctype=multipart/form-data>
				      <center><span>Image not uploaded</span></center>
				      <br>
				      <center>
				      	<input type="file" name="alt_img1" required>
				      	<br>
				      	<br>
				      	<input type="hidden" name="hiddenid" value="<?php echo $row['stock_id']; ?>">
				      	<input type="submit" name="img1update" value="Upload image">
				      </center>
				      </form>
				<?php } else{ ?>
					<form action="product.php?viewallproduct=" method="POST">
                      <input type="hidden" name="hiddenid" value="<?php echo $row['stock_id']; ?>"><center>
                      <img style="width:60px;height:60px" src="../<?php echo $row['alt_img1']; ?>" class=" wp-post-image" alt="" srcset="" />
                      <br>
                      <input type="submit" name="delete1" value="Delete">
                      </center>
                      </form>
			<?php 	} ?></td>
				<td><?php if (empty($row['alt_img2'])) { ?>
				      <form action="product.php?viewallproduct=" method="POST" enctype=multipart/form-data>
				      <center><span>Image not uploaded</span></center>
				      <br>
				      <center>
				      	<input type="file" name="alt_img2" required>
				      	<br>
				      	<br>
				      	<input type="hidden" name="hiddenid" value="<?php echo $row['stock_id']; ?>">
				      	<input type="submit" name="img2update" value="Upload image">
				      </center>
				      </form>
				<?php } else{ ?>
                      <form action="product.php?viewallproduct=" method="POST">
                      <input type="hidden" name="hiddenid" value="<?php echo $row['stock_id']; ?>"><center>
                      <img style="width:60px;height:60px" src="../<?php echo $row['alt_img2']; ?>" class=" wp-post-image" alt="" srcset="" />
                      <br>
                      <input type="submit" name="delete2" value="Delete">
                      </center>
                      </form>
			<?php 	} ?></td>
				<td><?php if (empty($row['alt_img4'])) { ?>
				      <form action="product.php?viewallproduct=" method="POST" enctype=multipart/form-data>
				      <center><span>Image not uploaded</span></center>
				      <br>
				      <center>
				      	<input type="file" name="alt_img3" required>
				      	<br>
				      	<br>
				      	<input type="hidden" name="hiddenid" value="<?php echo $row['stock_id']; ?>">
				      	<input type="submit" name="img3update" value="Upload image">
				      </center>
				      </form>
				<?php } else{ ?>
                     <form action="product.php?viewallproduct=" method="POST">
                      <input type="hidden" name="hiddenid" value="<?php echo $row['stock_id']; ?>"><center>
                      <img style="width:60px;height:60px" src="../<?php echo $row['alt_img4']; ?>" class=" wp-post-image" alt="" srcset="" />
                      <br>
                      <input type="submit" name="delete3" value="Delete">
                      </center>
                      </form>
			<?php 	} ?></td>

            
			<!--	<td><form action="parentsmenu.php" method="POST">
					<input type="hidden" name="hiddenmenuid" value="<?php //echo $row['id']; ?>">
					<button type="submit" name="editmenu">Edit Menu <?php //echo $row['id']; ?></button>
					<button style="color: red;" type="submit" name="deletemenu">Delete Menu</button>
				</form>
			</td> -->
				</tr>
			<?php } ?>
		</table>
		<?php
		  if( $page > 0 ) {
            $last = $page - 2;
            echo "<a href = \"$_PHP_SELF?page=$last\" class='btn btn-primary'><i class='fa fa-chevron-left'></i> Prev 10 Products</a> | ";
            echo "<a href = \"$_PHP_SELF?page=$page\" class='btn btn-primary'>Next 10 Products <i class='fa fa-chevron-right'></i></a>";
         }else if( $page == 0 ) {
            echo "<a href = \"$_PHP_SELF?page=$page\" class='btn btn-primary'>Next 10 Products <i class='fa fa-chevron-right'></i></a>";
         }else if( $left_rec < $rec_limit ) {
            $last = $page - 2;
            echo "<a href = \"$_PHP_SELF?page=$last\"><i class='fa fa-chevron-left'></i> Prev 10 Products</a>";
         } ?>
         <br>
         <br>
	</center>

<?php } ?>
</div>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="vendor/raphael/raphael.min.js"></script>
    <script src="vendor/morrisjs/morris.min.js"></script>
    <script src="data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>

<?php } ?>

