<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Ngicon Phone Verification</title>
  <script src="js/firebase.js"></script>
  <script>
    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyDGQQUw4xRJagflxcmct5k5Bqgc4x1_Yik",
      authDomain: "phoneauth-bbe99.firebaseapp.com",
      databaseURL: "https://phoneauth-bbe99.firebaseio.com",
      projectId: "phoneauth-bbe99",
      storageBucket: "phoneauth-bbe99.appspot.com",
      messagingSenderId: "1071451028890"
    };
    firebase.initializeApp(config);
  </script>
  <script src="js/firebaseui.js"></script>
  <link type="text/css" rel="stylesheet" href="phone/firebaseui.css" />
  <link href="phone/style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="container">
	<h3>Sign in Phone Verification</h3>
	<div id="loading">Loading...</div>
		<div id="loaded" class="hidden">
			<div id="main">
				<div id="user-signed-in" class="hidden">
				
				</div>
				<div id="user-signed-out" class="hidden">
					<div id="firebaseui-spa">
						<div id="firebaseui-container"></div>
					</div>
				</div>
			</div>
		</div>
		</div>
	<script src="js/app.js"></script>
</body>
</html>