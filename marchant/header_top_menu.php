        <div class="header-top">
        <div class="container">
            <div class="header-left">
                <div class="switcher-wrap"><ul id="menu-view-switcher" class="view-switcher mega-menu">
                     <li class="menu-item has-sub narrow">
                        <a href="#" onclick="window.lang.change('bn'); return false;" style="color:white;"><img src="../wp-content/themes/porto/images/flags/ban.png" alt=""> বাংলা</a>
                    </li>
                </ul><span class="gap switcher-gap">|</span><ul id="menu-currency-switcher" class="currency-switcher mega-menu">
                    <li class="menu-item has-sub narrow">
                      <a href="#" onclick="window.lang.change('en'); return false;" style="color:white;"><img src="../wp-content/themes/porto/images/flags/en.png" alt="">English </a>
                        <!--
						<div class="popup">
							<div class="inner">
								<ul class="sub-menu wcml-switcher">
									<li class="menu-item"><a href="#">BDT</a></li>
									<li class="menu-item"><a href="#">USD</a></li>
								</ul>
							</div>
						</div>
					-->
                    </li>
                </ul></div>            </div>
            <div class="header-right">
                <span class="welcome-msg"></span><span class="gap">|</span><ul id="menu-top-navigation" class="top-links mega-menu show-arrow effect-down subeffect-fadein-left">
				<?php
					if(isset($_SESSION['usermarchantaccountlogin'])){
				?>
					
						<li class="menu-item"><a href="index.php"><button type="button" class="btn btn-warning"><?php echo $_SESSION['usermarchantaccountlogin']; ?> <i class="fa fa-user"></i></button></a></li>      
					    <li class="menu-item"><a href="index.php?logout="><button type="button" class="btn btn-danger" lang="en">Logout <i class="fa fa-sign-out"></i></button></a></li>
						<li class="menu-item"><a href="../index.php"><button type="button" class="btn btn-success" lang="en"><i class="fa fa-arrow-circle-left"></i> Go To Shop </button></a></li>
				<?php
					}
					else{ ?>

				
							<li class="menu-item"><a href="login.php"><button type="button" class="btn btn-warning" lang="en">Login <i class="fa fa-sign-in"></i></button></a></li>      
					
						<li class="menu-item"><a href="../index.php"><button type="button" class="btn btn-success" lang="en"><i class="fa fa-arrow-circle-left"></i> Go To Shop </button></a></li>
				<?php
					}
				?>
				</ul>
            </div>
        </div>
    </div>