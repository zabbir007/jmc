<?php include ('admincon.php');
if (empty($_SESSION['inthemainathorityaccessadmincontrolifthatsare'])) {

     header('location: login.php');

 } else {
 include('header.php');

 ?>

<body>

    <div id="wrapper">

        <?php //sidebar added here 
         include('sidebar.php');
        ?>

        <div id="page-wrapper">
      <?php include ('errors.php'); ?>

<?php
if (isset($_GET['add_menu'])) { ?>
	
	<?php


$js = '';
$use_popup_windows = "";
$use_date_picker = "";
if ($use_popup_windows) {
	$js .= get_js_open_window(900, 500);
}

if ($use_date_picker) {
	$js .= get_js_date_picker();
}

?>

<title>Products | Service Entry</title>
    <script>
        function print(id){
    w=window.open('print.php?id='+id);
        w.focus();    
      //  w.window.print();
          //  w.close();
        }
    </script>
    <?php

//page($_SESSION['Sale Summary'], false, false, "Sales Summary", $js);?>
<style>
    #addform td,#addform input{
        font-size: 20px;
        line-height: 30px;
    }
    
</style>
<center><br><br><br><br><br>
    <h1>Products Group and Brands Entry</h1>
	<form method="post" enctype="multipart/form-data">
	<table class="table table-striped">
		<tr>
			<td>Product Group or Brands Name</td>
			<td><input style="width: 100%;height: 28px;" type="text" name="product_group_brands" id="product_group_brands" required></td>
		</tr>
		<tr>
			<td>Select Brands:</td>
			<td>
			<select id="menue_select" name="menue_select" onchange="chang_mettar();" style="width:100%">
				<option value="0" selected>Main Menu</option>
				<option value="2">Sub Menu</option>
			</select>
			</td>
		</tr>
		<tr id="dorpdown_menu" style="display:none">
		<td>Main Brands</td>
		<td><select id="dropdown_select" name="dropdown_select" style="width:100%;">
			<option value="no_select">Select Main Brands</option>
		<?php
			$sql_for_main_brands = "select * from new_tbl2 where desc2='0'";
			$result_for_main_brands = $db2->query($sql_for_main_brands);
			while($data_for_main_brands = mysqli_fetch_array($result_for_main_brands)){
			echo "<option value='$data_for_main_brands[0]'>$data_for_main_brands[1]</option>";
			}
		?>
		</select></td>
		</tr>
			<script type="text/javascript">
			function chang_mettar(){
			var menue_select = document.getElementById('menue_select').value;
			if(menue_select==2){
				document.getElementById('dorpdown_menu').style.display = "table-row";
			}
			else{
				document.getElementById('dorpdown_menu').style.display = "none";
			}
			}
			</script>
		<tr>
			<td>Select Group or Brands</td>
			<td><select style="width: 100%;height: 28px;" name="select_group_brands">
				<option value="0" selected>Product Group</option>
				<option value="1">Product Brands</option>
			</select></td>
		</tr>
		<tr>
			<td>
				Select Group Photo
			</td>
			<td>
				<input type="file" name="group_or_brand_photo" id="group_or_brand_photo"/>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center"><input style="width: 25%;height: 40px;font-size: 17px;" type="submit" value="Submit" name="addproductgroup"/></td>
		</tr>
	</table>
	</form>
</center>
<center>
<a href="add_product_group.php?viewall=" class="btn btn-primary"><i class="fa fa-list"></i>View All</a>
</center>


<?php
	}
if (isset($_GET['viewall'])) { ?>
<center>
<a href="add_product_group.php?add_menu=" class="btn btn-primary"><i class="fa fa-plus"></i>Add Menu</a>
</center>
		<table class="table table-striped">
			<tr>
				<th>Name</th>
				<th>Menu Image</th>
				<th>Type</th>
				<th>Main Menu</th>
				<th colspan="2"><center>Action</center></th>
			</tr>
			
				<?php $sqllogo = "SELECT * FROM new_tbl2";
				$logoresult = mysqli_query($db2, $sqllogo);
				while ($row = mysqli_fetch_array($logoresult)) { ?>
					<tr>
				<td style="padding: 10px;"><?php echo $row['group_brands']; ?></td>
				<td style="padding: 10px;"><?php if (empty($row['desc1'])) {
					echo "Image not added !";
				} else{ ?><img src="../<?php echo $row['desc1']; ?>" width="80px"> <?php } ?></td>
				<td style="padding: 10px;"><strong><?php if($row['desc2'] == 0){
					echo "<strong style='color:green; padding: 10px;'>Main Menu</strong>";
				}else{ echo "Submenu"; } ?></strong></td>
				<td>
				<?php $sql = "SELECT * FROM new_tbl2 WHERE id = '".$row['desc2']."'";
                 $sqlres = mysqli_query($db2, $sql);
                 while($data = mysqli_fetch_array($sqlres)){
                 if (mysqli_num_rows($sqlres) == 0) {
                 	echo "--";
                 }else{
                 	echo "<strong style='color:green; padding: 10px;'>".$data['group_brands']. "</strong>";
                 }
             }
                 ?>
                 <!--
			<td><form action="parentsmenu.php" method="POST">
					<input type="hidden" name="hiddenmenuid" value="<?php //echo $row['id']; ?>">
					<button type="submit" name="editmenu">Edit Menu <?php //echo $row['id']; ?></button>
					<button style="color: red;" type="submit" name="deletemenu">Delete Menu</button>
				</form> -->
			</td> 
			<td><form action="add_product_group.php" method="GET">
				<input type="hidden" name="id" value="<?php echo $row['id']; ?>">
				<button type="submit" name="editgroup" class="btn btn-warning"><i class="fa fa-edit"></i> Edit</button>
			</form>
		</td>
		<td><form action="add_product_group.php?viewall=" method="POST">
				<input type="hidden" name="id" value="<?php echo $row['id']; ?>">
				<button type="submit" name="deletegroup" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
			</form>
		</td>
				</tr>
			<?php } ?>
		</table>
		

<?php
}
?>

<?php if (isset($_GET['editgroup'])) { 
	$id = $_GET['id'];
	$slq = "SELECT * FROM new_tbl2 WHERE id='$id'";
	$sqlquery = mysqli_query($db2, $slq);
	$productgroup = mysqli_fetch_array($sqlquery);

	$js = '';
$use_popup_windows = "";
$use_date_picker = "";
if ($use_popup_windows) {
	$js .= get_js_open_window(900, 500);
}

if ($use_date_picker) {
	$js .= get_js_date_picker();
}

?>

<title>Products | Service Entry</title>
    <script>
        function print(id){
    w=window.open('print.php?id='+id);
        w.focus();    
      //  w.window.print();
          //  w.close();
        }
    </script>
    <?php

//page($_SESSION['Sale Summary'], false, false, "Sales Summary", $js);?>
<style>
    #addform td,#addform input{
        font-size: 20px;
        line-height: 30px;
    }
    
</style>
<center><br><br><br><br><br>
    <h1>Edit Products Group and Brands Entry</h1>
	<form method="post" action="add_product_group.php?id=<?php echo $id; ?>&editgroup=" enctype="multipart/form-data">
	<table class="table table-striped">
		<tr>
			<td>Product Group or Brands Name</td>
			<td><input style="width: 100%;height: 28px;" type="text" name="product_group_brands" id="product_group_brands" required value="<?php echo $productgroup['group_brands']; ?>"></td>
		</tr>
		<tr>
			<td>Select Brands:</td>
			<td>
			<select id="menue_select" name="menue_select" onchange="chang_mettar();" style="width:100%">
				<option value="NULL">Select Menu Type</option>
				<option value="0">Main Menu</option>
				<option value="2">Sub Menu</option>
			</select>
			</td>
		</tr>
		<tr id="dorpdown_menu" style="display:none">
		<td>Main Brands</td>
		<td><select id="dropdown_select" name="dropdown_select" style="width:100%;">
			<option value="no_select">Select Main Brands</option>
		<?php
			$sql_for_main_brands = "select * from new_tbl2 where desc2='0'";
			$result_for_main_brands = $db2->query($sql_for_main_brands);
			while($data_for_main_brands = mysqli_fetch_array($result_for_main_brands)){
			echo "<option value='$data_for_main_brands[0]'>$data_for_main_brands[1]</option>";
			}
		?>
		</select></td>
		</tr>
			<script type="text/javascript">
			function chang_mettar(){
			var menue_select = document.getElementById('menue_select').value;
			if(menue_select==2){
				document.getElementById('dorpdown_menu').style.display = "table-row";
			}
			else{
				document.getElementById('dorpdown_menu').style.display = "none";
			}
			}
			</script>
		<tr>
			<td>Select Group or Brands</td>
			<td><select style="width: 100%;height: 28px;" name="select_group_brands">
				<?php if ($productgroup['atribute'] == 0) { ?>
					<option value="0" selected>Product Group</option>
				    <option value="1">Product Brands</option>
					
			<?php	}else { ?>
                    <option value="0" >Product Group</option>
				    <option value="1" selected>Product Brands</option>

<?php } ?>
				
			</select></td>
		</tr>
		<tr>
			<td>
				Change Group Photo
			</td>
			<td>
				<input type="file" name="group_or_brand_photo" id="group_or_brand_photo"/><br>
				<?php if (empty($productgroup['desc1'])) {
					echo "Image not added !";
				} else{ ?><img src="../<?php echo $productgroup['desc1']; ?>" width="80px"> <?php } ?>
			</td>
		</tr>
		<tr>
			<input type="hidden" name="id" value="<?php echo $id; ?>">
			<td colspan="2" style="text-align:center"><input style="width: 25%;height: 40px;font-size: 17px;" type="submit" value="Update" name="updateproductgroup"/></td>
		</tr>
	</table>
	</form>
</center>
<center>
<a href="add_product_group.php?viewall=" class="btn btn-primary"><i class="fa fa-list"></i>View All</a>
</center>


<?php } ?>
  </div>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="vendor/raphael/raphael.min.js"></script>
    <script src="vendor/morrisjs/morris.min.js"></script>
    <script src="data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>

<?php } ?>